<?php
 if ( ! defined('BASEPATH')) 
 	exit('No direct script access allowed');
class Courses extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();  
		
		$this->data['sel'] = 'courses';
		
	}

	function index()
	{
		$this->data['meta']  = getMetaContent('courses','meta');
		
		$this->data['body']='front/courses';
		
		$this->load->model('courses_model');
		
		$this->data['course_list']=$this->courses_model->getCourseListTitle();
				
		$this->load->view('front/structure',$this->data);
	}
	
	
	
    

}

?>