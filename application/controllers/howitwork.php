<?php
class Howitwork extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'howitwork';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('how_it_work');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/how_it_work';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function membership()
	{ 
		$this->data['meta']  = getMetaContent('membership_tour','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/membership_tour',$this->data);
	}
	
	function comparison()
	{ 
		$this->data['meta']  = getMetaContent('how_it_work');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$meta = getMetaContent('comparison_slide_down','data');
		
		$this->data['comparison_content'] = $meta['data'];
		
		$this->data['body']='front/comparison_slide_down';
		
		$this->load->view('front/structure',$this->data);
	}

}

?>