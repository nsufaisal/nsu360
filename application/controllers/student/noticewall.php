<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Noticewall extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
		
		if ($this->session->userdata('user_type')!='ST')
		{
			redirect(site_url());
		}
		
        $this->data = array();

        $this->data['sel'] = 'noticewall';
		
		$this->load->model(array('courses_model','general_model', 'noticewall_model'));

    }

/*
|-----------------------------------------------------------------------------
| displays list of notices of course take by the student
|-----------------------------------------------------------------------------
*/	

    function index() 
	{	
		$selected_course=0;
			
		$selected_semester=0;
		
		$member_id=$this->session->userdata('member_id');
		
		$semester_id = $this->session->userdata('semester_id');
		
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['semesters']=$this->general_model->get_select_record('semester_id,title, is_current','semesters', 'result',array('is_active'=>1,),'');
		
		$this->data['my_notices']=$this->noticewall_model->get_student_course_notices($member_id, $semester_id);
				
		$this->data['body'] = 'front/student/noticewall';

        $this->load->view('front/structure', $this->data);		
    }
	
	/*
|-----------------------------------------------------------------------------
| View an individual Notice
|-----------------------------------------------------------------------------
*/	

    function view($noticewall_id=0) 
	{
		$member_id=$this->session->userdata('member_id');
		
		if($this->noticewall_model->is_my_course_notice($member_id, $noticewall_id))
		{
			$this->data['notice_post']=$this->noticewall_model->get_notice_post($noticewall_id);
			
			$this->data['notice_courses']=$this->noticewall_model->get_notice_courses($noticewall_id);
			
			$this->data['meta'] = getMetaContent('instructor_mystudents');

	        $this->data['content'] = $this->data['meta']['data'];	
		
			$this->data['body'] = 'front/student/noticewall_view';
	
	        $this->load->view('front/structure', $this->data);				
		}
		else {
			redirect('/student/noticewall');
		}	
    }

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~LINE OF ACTIVE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*
|-----------------------------------------------------------------------------
| New add/Edit a Notice Post
|-----------------------------------------------------------------------------
*/	

    function remove($noticewall_id=0) 
	{
        	$this->data['noticewall_id']=$noticewall_id;
			
            $this->data['meta'] = getMetaContent('model_message_remove_message','data');

            $this->data['content'] = $this->data['meta']['data'];

            $this->load->view('front/instructor/notice_remove', $this->data);
	}
    function remove_confirmation($noticewall_id=0) 
	{
        $member_id = $this->session->userdata('member_id');

        if ($this->noticewall_model->is_my_notice($noticewall_id, $member_id)) /* check if this message ownership matches */
        {
        	$this->noticewall_model->remove($noticewall_id);
			
        	$this->data['confirmation_content']="Notice has been removed from the wall";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/instructor/noticewall_remove_confirmation', $this->data, true));

        	redirect('/instructor/noticewall');
        }
	}
	
/*
|-----------------------------------------------------------------------------
| New add/Edit a Notice Post
|-----------------------------------------------------------------------------
*/	

    function edit($noticewall_id=0) 
	{
		$member_id=$this->session->userdata('member_id');
		
		$this->data['add_edit']='add';
		
		if($noticewall_id>0)
		{
			$this->data['add_edit']='edit';
			
			$this->data['my_notice']=$this->noticewall_model->get_my_notice($noticewall_id, $member_id);
		}
		
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
		$semester_id=$this->session->userdata('semester_id');
		
		$this->data['my_courses']=$this->courses_model->get_instructor_courses($member_id,$semester_id);
	
		$this->data['body'] = 'front/instructor/noticewall_new';

        $this->load->view('front/structure', $this->data);		
    }

/*
|-----------------------------------------------------------------------------
| Save/Post a New Notice on the wall
|-----------------------------------------------------------------------------
*/		
	function post($noticewall_id=0)
	{
		$member_id=$this->session->userdata('member_id');
		
		$post=$this->input->post();
	
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('notice_body', 'Notice Body', 'trim|xss_clean|required');

        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}
		else 
		{
			$this->noticewall_model->save($post, $member_id, $noticewall_id);			
			
			$this->data['confirmation_content']="Notice has been added to the wall for posting";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/instructor/noticewall_post_confirmation', $this->data, true));

        	redirect('/instructor/noticewall');
		}
		
	}
	
	function empty_msg()
	{
		$this->data['heading']="Forgot Something?";
		
		$this->data['content']='<p><span style="color:blue;">Notice Body</span> cannot be left empty.</p>';
		
		$this->load->view('front/warning_msg',$this->data);
	}
	


}
