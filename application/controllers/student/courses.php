<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Courses extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
		
		if ($this->session->userdata('user_type')!='ST')
		{
			redirect(site_url());
		}
		
        $this->data = array();

        $this->data['sel'] = 'my_courses';
		
		$this->load->model(array('courses_model','general_model',));

    }

/*
|-----------------------------------------------------------------------------
| Remove a course from list show pop up
|-----------------------------------------------------------------------------
*/	

    function remove($semester_course_id=0) 
	{
        	$this->data['semester_course_id']=$semester_course_id;
			
            $this->data['meta'] = getMetaContent('model_message_remove_message','data');

            $this->data['content'] = $this->data['meta']['data'];

            $this->load->view('front/student/mycourse_remove', $this->data);
	}
    function remove_confirmation($semester_course_id=0) 
	{
        $member_id = $this->session->userdata('member_id');

        if ($semester_course_id>0) /* check if this message ownership matches */
        {
        	$this->courses_model->remove_enrole_course($member_id, $semester_course_id);
			
        	$this->data['confirmation_content']="Course has been removed successfully";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/student/mycourse_remove_confirmation', $this->data, true));

        	redirect('/student/courses');
        }
	}

/*
|-----------------------------------------------------------------------------
| Remove a course from add course page redirection is different
|-----------------------------------------------------------------------------
*/		
	function remove2($semester_course_id=0) 
	{
        	$this->data['semester_course_id']=$semester_course_id;
			
            $this->data['meta'] = getMetaContent('model_message_remove_message','data');

            $this->data['content'] = $this->data['meta']['data'];

            $this->load->view('front/student/mycourse_remove_from_add_page', $this->data);
	}
    
    function remove_confirmation2($semester_course_id=0) 
	{
        $member_id = $this->session->userdata('member_id');

        if ($semester_course_id>0) /* check if this message ownership matches */
        {
        	$this->courses_model->remove_enrole_course($member_id, $semester_course_id);
			
        	$this->data['confirmation_content']="Course has been removed successfully";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/student/mycourse_remove_confirmation', $this->data, true));

        	redirect('/student/courses/add');
        }
	}
/*
|-----------------------------------------------------------------------------
| Edit form of a student
|-----------------------------------------------------------------------------
*/	

    function index() 
	{
		$member_id = $this->session->userdata('member_id');
		
		$semester_id = $this->session->userdata('semester_id');
				
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['advising_approval']=TRUE;
		
		$this->data['semesters']=$this->general_model->get_select_record('semester_id,title, is_current','semesters', 'result',array('is_active'=>1,),'');
				
		$this->data['my_courses']=$this->courses_model->get_student_courses($member_id, $semester_id);
				
		$this->data['body'] = 'front/student/mycourses';

        $this->load->view('front/structure', $this->data);		
    }

/*
|-----------------------------------------------------------------------------
| New Course add
|-----------------------------------------------------------------------------
*/	

    function add($semester_course_id=0) 
	{
		$advising_approval=TRUE;

		$member_id = $this->session->userdata('member_id');
		
		$semester_id = $this->session->userdata('semester_id');
		
		if($advising_approval)
		{				
			$this->data['meta'] = getMetaContent('instructor_mystudents');
	
	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['my_courses']=$this->courses_model->get_student_courses($member_id, $semester_id);
						
			$this->data['available_courses']=$this->courses_model->get_available_courses($member_id, $semester_id);
					
			$this->data['body'] = 'front/student/mycourse_add';
	
	        $this->load->view('front/structure', $this->data);			
		}
		else
		{
			$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You do not have permission to add courses!</h2>");
			
			redirect('/student/courses/');		
		}

    }

/*
|-----------------------------------------------------------------------------
| add course from dropdown multilist and return the current course list to load
|-----------------------------------------------------------------------------
*/	
    function ajax_add_course() 
	{
		if (isAjax()) 
		{
			// $this->load->model('notification_model');
						
			$new_courses=$this->input->post('new_courses');
			
			$member_id=$this->session->userdata('member_id');

			$semester_id=$this->session->userdata('semester_id');
			
			foreach($new_courses as $semester_course_id)
			{
				$this->courses_model->enrole_student($member_id, $semester_course_id,0);
				
			//	$this->notification_model->instructor_has_new_enrole($member_id, $semester_course_id); /* create notification */
				
			}
			
			$this->data['available_courses']=$this->courses_model->get_available_courses($member_id, $semester_id);
			
			$this->data['my_courses']=$this->courses_model->get_student_courses($member_id, $semester_id);
			
			echo $this->load->view('front/student/mycourse_list_ajx', $this->data);
			
			
		} else 
		
		{
	        show_error('Sorry, direct access is not allowed');
	    }
		
	}
	
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ABOVE ARE ACTIVE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/	




/*
|-----------------------------------------------------------------------------
| New add/Edit a Notice Post
|-----------------------------------------------------------------------------
*/	

    function edit($noticewall_id=0) 
	{
		$member_id=$this->session->userdata('member_id');
		
		$this->data['add_edit']='add';
		
		if($noticewall_id>0)
		{
			$this->data['add_edit']='edit';
			
			$this->data['my_notice']=$this->noticewall_model->get_my_notice($noticewall_id, $member_id);
		}
		
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
		$semester_id=$this->session->userdata('semester_id');
		
		$this->data['my_courses']=$this->courses_model->get_instructor_courses($member_id,$semester_id);
	
		$this->data['body'] = 'front/instructor/noticewall_new';

        $this->load->view('front/structure', $this->data);		
    }


/*
|-----------------------------------------------------------------------------
| View an individual Notice
|-----------------------------------------------------------------------------
*/	

    function view($noticewall_id=0) 
	{
		$member_id=$this->session->userdata('member_id');
		
		if($this->noticewall_model->is_my_post($member_id, $noticewall_id))
		{
			$this->data['notice_post']=$this->noticewall_model->get_notice_post($noticewall_id);
			
			$this->data['notice_courses']=$this->noticewall_model->get_notice_courses($noticewall_id);
			
			$this->data['meta'] = getMetaContent('instructor_mystudents');

	        $this->data['content'] = $this->data['meta']['data'];	
		
			$this->data['body'] = 'front/instructor/noticewall_view';
	
	        $this->load->view('front/structure', $this->data);				
		}
		else {
			redirect('/instructor/noticewall');
		}	
    }

/*
|-----------------------------------------------------------------------------
| Save/Post a New Notice on the wall
|-----------------------------------------------------------------------------
*/		
	function post($noticewall_id=0)
	{
		$member_id=$this->session->userdata('member_id');
		
		$post=$this->input->post();
	
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('notice_body', 'Notice Body', 'trim|xss_clean|required');

        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}
		else 
		{
			$this->noticewall_model->save($post, $member_id, $noticewall_id);			
			
			$this->data['confirmation_content']="Notice has been added to the wall for posting";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/instructor/noticewall_post_confirmation', $this->data, true));

        	redirect('/instructor/noticewall');
		}
		
	}
	
	function empty_msg()
	{
		$this->data['heading']="Forgot Something?";
		
		$this->data['content']='<p><span style="color:blue;">Notice Body</span> cannot be left empty.</p>';
		
		$this->load->view('front/warning_msg',$this->data);
	}
	


}
