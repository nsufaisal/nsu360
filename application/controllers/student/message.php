<?php
class Message extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		if ($this->session->userdata('member_id')==FALSE)
		{
			redirect(site_url());
		}
		
		$this->load->model('members_model');
		
		$this->load->model('messages_model');

		$this->load->model('outbound_email_model');
		
        $this->load->library('form_validation');  
		
		$this->data = array();  
		
		$this->data['sel'] = 'message';
	}
	
/*
|-----------------------------------------------------------------------------
| Display Member inbox message as the default message center page
|-----------------------------------------------------------------------------
*/		
	function index()
	{
		$this->data['sub_sel'] = 'inbox';
		 
		$this->data['meta']  = getMetaContent('member_message','meta');
		
		$this->data['body']='front/member/message';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Display Message Send form 
|-----------------------------------------------------------------------------
*/		
	function send()
	{
		$this->data['sub_sel'] = 'new_msg';
		 
		$this->data['meta']  = getMetaContent('member_message_send','meta');
		
		$this->data['body']='front/member/message_send';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Send the message to the desired member and show confirmation
|-----------------------------------------------------------------------------
*/		
	function send_confirmation()
	{
		$this->form_validation->set_rules('to', 'Receipient', 'trim|required|xss_clean');

        $this->form_validation->set_rules('subject', 'Subject', 'trim|xss_clean|required');

        $this->form_validation->set_rules('message', 'Message', 'trim|xss_clean|required');
		
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->send();
		}
		else 
		{
			$receipient=$this->members_model->GetIfMember($this->input->post('to'));
			
			$receipient_id=$receipient['member_id'];
			
			$sender_id=$this->session->userdata('member_id');
			
			$msg_data=array(
			
				'subject'=>$this->input->post('subject'),
				
				'message'=>$this->input->post('message'),
				
				'sender_id'=>$sender_id,
				
				'receipient_id'=>$receipient_id,				
			);
			
			$this->messages_model->send_message($msg_data);
			
			/* outbound email notification sending */
			
			$first_name=$receipient['first_name'];
			
			$last_name=$receipient['last_name'];
		
			$email=$receipient['email'];
			
			$result =$this->outbound_email_model->get('Message_center_new_notification');			//Model Function calling for get email content
			
			$message= $result['content'];
			
			$message=str_replace('{NAME}',$first_name.' '.$last_name,$message);

			$this->email->from($result['from'], $result['from_name']);
			
			$this->email->to($email);
			
			$this->email->subject($result['subject']);
			
			$this->email->message($message);
			
			$this->email->send();			
			
			$this->data['sub_sel'] = 'new_msg';
			
			$this->data['receipient']=$receipient;
			 
			$this->data['meta']  = getMetaContent('member_message_send','meta');
			
			$this->data['body']='front/member/message_send_confirmation';
			
			$this->load->view('front/structure',$this->data);
				
			
		}

	}

/*
|-----------------------------------------------------------------------------
| Display list of sent messages
|-----------------------------------------------------------------------------
*/		
	function sent()
	{
		$this->data['sub_sel'] = 'sent_msgs';
		 
		$this->data['meta']  = getMetaContent('member_message_send','meta');
		
		$this->data['body']='front/member/message_send';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Display a reply message form
|-----------------------------------------------------------------------------
*/		
	function reply()
	{
		$this->data['meta']  = getMetaContent('member_message_reply','meta');
		
		$this->data['body']='front/member/message_reply';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Remove a message form the inbox
|-----------------------------------------------------------------------------
*/		
	function remove()
	{ 
		$this->data['meta']  = getMetaContent('member_message_remove_message','data');
		
		$this->data['content']  = $this->data['meta']['data'];
		
		$this->load->view('front/member/message_remove',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| View a message detail
|-----------------------------------------------------------------------------
*/		
	function view()
	{ 
		$this->data['meta']  = getMetaContent('member_message_view','meta');
		
		$this->data['body']='front/member/message_view';
		
		$this->load->view('front/structure',$this->data);
	}

	
	function credit()
	{ 
		$this->data['meta']  = getMetaContent('member_message_credit_option','meta');
		
		$this->data['body']='front/member/message_credit_option';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function credit_confirmation()
	{ 
		$this->data['meta']  = getMetaContent('member_message_credit_option_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/message_credit_option_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function payment()
	{ 
		$this->data['meta']  = getMetaContent('member_message_payment','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/member/message_payment',$this->data);
	}
	
	function payment_confirmation()
	{ 
		$this->data['meta']  = getMetaContent('member_message_payment_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/message_payment_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
}

?>