<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Myaccount extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		if ($this->session->userdata('user_type')!='ST')
		{
			redirect(site_url());
		}
		$this->data = array();  
		
		$this->data['sel'] = 'myaccount';
		
		$this->load->model(array('students_model', 'general_model', 'members_model'));

	}

/*
|-----------------------------------------------------------------------------
| Change profile photo
|-----------------------------------------------------------------------------
*/	
	function change_photo()
	{
		$member_id = $this->session->userdata('member_id');
		 
		$this->data['member_id']=$member_id;
				
		$this->data['meta']  = getMetaContent('member_myaccount','meta');
		
		$this->data['my_info']=$this->students_model->get_student($member_id);
		
		$this->data['body']='front/student/myaccount_change_photo';
		
		$this->load->view('front/structure',$this->data);		
	}
	
/*
|-----------------------------------------------------------------------------
| Save Change profile photo
|-----------------------------------------------------------------------------
*/	
	function save_photo_confirmation()
	{
		$member_id = $this->session->userdata('member_id');
		$valid_exts = array('jpeg', 'jpg');
		$max_file_size = 500 * 1024; #200kb
		$nw = 300;
		$nh = 350; # image with # height
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if ( isset($_FILES['image']) ) {
				if (! $_FILES['image']['error'] && $_FILES['image']['size'] < $max_file_size) {
					$ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
					if (in_array($ext, $valid_exts)) {
						
						$filename=uniqid() . '.' . $ext;
						
						$dir='./uploads/student/'.$member_id.'/';
			
						if (!is_dir($dir))
				        {
				            mkdir($dir, 0755, true);
	        			}
							$path = $dir . $filename;
							$size = getimagesize($_FILES['image']['tmp_name']);
							
							$adjust=(double) $size[0]/300;
		
							$x = (int) $_POST['x']*$adjust;
							$y = (int) $_POST['y']*$adjust;
							$w = (int) $_POST['w'] ? $_POST['w']*$adjust : $size[0];
							$h = (int) $_POST['h'] ? $_POST['h']*$adjust : $size[1];
		
							$data = file_get_contents($_FILES['image']['tmp_name']);
							$vImg = imagecreatefromstring($data);
							$dstImg = imagecreatetruecolor($nw, $nh);
							imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
							imagejpeg($dstImg, $path);
							imagedestroy($dstImg);
							
							if($this->students_model->change_photo($member_id,$filename))
							{
								$meta = getMetaContent('student_signupconfirmcontent', 'data');
			        
						        $this->data['content'] = $meta['data'];
				
								$this->session->set_flashdata('slider_content', $this->load->view('front/student/myaccount_change_photo_confirmation',$this->data,true));
								
								redirect('/student/myaccount/');
							}
		
						} else {
							//echo 'unknown problem!';
							$this->session->set_flashdata('slider_content', "<h2>Oopse! May be you are trying to upload file other than jpg/jpeg extension!</h2>");
							redirect('/student/myaccount/change_photo/');
						} 
				} else {
					//echo 'file is too small or large';
					$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You forgot to select a photo or your file size is too large for upload (file limit is 500KB)!</h2>");
					redirect('/student/myaccount/change_photo/');
				}
			} else {
				//echo 'file not set';
				$this->session->set_flashdata('slider_content', "<h2>Aah! File not set!</h2>");
				redirect('/student/myaccount/change_photo/');
			}
		} else {
			//echo 'bad request!';
			$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You made a very bad request!</h2>");
			redirect('/student/myaccount/change_photo/');
		}
	}	
	
	
	function index()
	{
		$member_id = $this->session->userdata('member_id');
		 
		$this->data['meta']  = getMetaContent('member_myaccount','meta');
		
		$this->data['member_id']=$member_id;
		
		$this->data['my_info']=$this->students_model->get_student($member_id);
		
		$this->data['body']='front/student/myaccount';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Display Member my account Edit form with current info
|-----------------------------------------------------------------------------
*/	
	function edit()
	{
		$member_id=$this->session->userdata('member_id');
		
		$this->load->model('departments_model');
		 
		$this->data['meta']  = getMetaContent('member_myaccount_edit','meta');
		
		$this->data['departments'] = $this->departments_model->getAllDepartments();

		$this->data['degree_majors'] = $this->departments_model->get_all_degree_major();
		
		$this->data['body']='front/student/myaccount_edit';
		
		$this->data['my_info']=$this->students_model->get_student($member_id);
		
		$this->load->view('front/structure',$this->data);
	}


	function Save($member_id=0)
	{
		$member_id=$this->session->userdata('member_id');
		
	  	$this->form_validation->set_rules('email', 'Email Address', 'trim|xss_clean|required|valid_email|callback_new_unique_email');

		if($this->input->post('password', TRUE)!='' || $this->input->post('new_password', TRUE)!='')	
		{
			$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required|callback_check_old_pass');
			
			$this->form_validation->set_rules('new_password', 'Change Password', 'trim|xss_clean|required');

			$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|xss_clean|required|matches[new_password]');
		}

		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() != FALSE)
		{
			$post=$this->input->post();
			
			$member_data=array(
				
				'phone'=>$post['phone'],
				
				'email'=>$post['email'],
				
				'username'=>$post['student_id'],				
			);
			
			if ($this->input->post('new_password')!='') 
			{
			  $member_data['password'] = md5($post['new_password']);
			} 			
						
			if($this->members_model->update_member($member_id,$member_data))
			{
				$std_data=array(
				
					'first_name'=>$post['first_name'],
				
					'last_name'=>$post['last_name'],
				
					'department_id'=>$post['department_id'],
					
					'degree_major'=>$post['degree_major'],
				
					'blood_group'=>$post['blood_group'],
				
					'gender'=>$post['gender'],
				
					'curr_address'=>$post['curr_address'],	
					
					'student_id'=>$post['student_id'],			
				);
										
				if($this->students_model->update_student($member_id, $std_data))
				{                
			        $meta = getMetaContent('student_signupconfirmcontent', 'data');
			        
			        $this->data['content'] = $meta['data'];
	
					$this->session->set_flashdata('slider_content', $this->load->view('front/student/myaccount_edit_confirmation',$this->data,true));
					
					redirect('/student/myaccount/');
				}
			}				
			

		}		
		else{
			$this->edit();
		}
	}	


/*
|-----------------------------------------------------------------------------
| validation call back: check whether the new email is available
|-----------------------------------------------------------------------------
*/
	function new_unique_email($email)
	{
		$member_id=$this->session->userdata('member_id');
		
		$result=$this->members_model->CheckEmailExcept($member_id, $email);
		
		if(count($result)==0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('new_unique_email', "This email is used by another member");

			return FALSE;
		}

	}
	
/*
|-----------------------------------------------------------------------------
| validation call back: check whether member enters existing password correct
|-----------------------------------------------------------------------------
*/
	function check_old_pass($password)
	{
		$member_id=$this->session->userdata('member_id');
		
		$result=$this->members_model->CheckOldPassword($member_id, $password);
		
		if(count($result)==0)
		{
			$this->form_validation->set_message('check_old_pass', "Old password is not correct");

			return FALSE;	
			
		}
		else
		{
			return TRUE;
		}

	}

	function confirmation()
	{ 
		$this->data['meta']  = getMetaContent('member_myaccount_edit_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/member/myaccount_edit_confirmation',$this->data);
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/member/myaccount_edit_confirmation', $this->data,true));
	
	    redirect('/member/myaccount');
	}
	
/*
|-----------------------------------------------------------------------------------------
| returns dropdownlist of cities based on a country to a ajax call in myaccount_edit form
|-----------------------------------------------------------------------------------------
*/	
	function AjxGetMyStates($type=0)
	{
		$country=$this->input->post('country');
		
		$member_id=$this->session->userdata('member_id');
		
		$member=$this->members_model->GetMember($member_id);
		
		$states=$this->members_model->getMyStates($country);
		
		$default=0;
		
		if($member['state']>0)
		{
			$default=$member['state'];
		}		
		
		$options=array();
		
		$options[0]='';
		
		foreach($states as $state)
		{
			$options[$state['state_id']]=$state['state_name'];				
		}		
		
		if($type==0)
			echo form_dropdown('state', $options, $default, 'class="input_field" id="state"');
		
		else {
			echo form_dropdown('state', $options, $default, 'id="state" class="input_field validate[required]" style="width:222px"');
		}		
		
	}
}

?>