<?php
class Download_file extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('download');
	}
	
	function model_contract($type='')
	{
		if($type=='word')
		{
			$data = file_get_contents(base_url()."data/docs/ModelAgreementContract.docx"); // Read the file's contents
			
			$name = 'ModelContractAgreement.docx';
		}
		else 
		{	
			$data = file_get_contents(base_url()."data/docs/ModelAgreementContract.pdf"); // Read the file's contents
			
			$name = 'ModelContractAgreement.pdf';
		}
		
		

		force_download($name, $data);
	}
	
	function studio_contract()
	{
		$data = file_get_contents(base_url()."data/docs/StudioAgreementContract.pdf"); // Read the file's contents
		
		$name = 'StudioContractAgreement.pdf';

		force_download($name, $data);
	}
}