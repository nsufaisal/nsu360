<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Instructor extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'footer_teacher';
        
        $this->load->model('departments_model');
		
		$this->load->model('members_model');
		
		$this->load->model('teachers_model');
	}

/*
|--------------------------------------------------------------------------
| Instructor Activates her account by code given to her email address.
|--------------------------------------------------------------------------
*/
	function activation()
	{           
        $meta = getMetaContent('instructor_activation', 'data');
		
        $this->data['content'] = $meta['data'];
		
		$this->data['body']='front/footer/instructor_activation';
		
		$this->load->view('front/structure',$this->data);
		
	}	
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('footer_teacher');
                
		$this->data['content'] = $this->data['meta']['data'];
                
		$this->data['body']='front/footer/footer_instructor';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function teacher_signup()
	{ 
		$this->data['meta']  = getMetaContent('teacher_signup');
		
       $this->data['content'] = $this->data['meta']['data'];
       
       $this->data['departments'] = $this->departments_model->getAllDepartments();
                
        $meta = getMetaContent('teacher_signupcontent', 'data');
		
        $this->data['signupcontent'] = $meta['data'];
		
		$this->load->view('front/footer/instructor_signup',$this->data);

	}
	
		
	/*
|--------------------------------------------------------------------------
| User Login Form validation & Authentication
|--------------------------------------------------------------------------
*/
	 function register(){
	     
         $this->load->model('instructor_model');
	  		
	  	$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[members.username]|xss_clean');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|xss_clean');
		
		$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|required|matches[password]|xss_clean');
		
		$this->form_validation->set_error_delimiters('<span class="rtn-error">', '</span>');
	
		if($this->form_validation->run()==FALSE) {
			
			$this->index();				
		}
		else{
		    
            $post=$this->input->post();
                
                $member_data = array(
                
                'username' =>$post['username'],
                
                'email'=>$post['email'],
                
				'phone'=>$post['phone'],
                
               'password'=>md5($post['password']),  
               
               'user_type'=>'IN',
               
               'isactive'=>0,
                
                );
            $member_id = $this->members_model->Save($member_data,0);
            
            $instructor_data = array(
                'member_id' => $member_id,
      
                'first_name'=>$post['first_name'],
                
                'last_name'=>$post['last_name'],
                     
                 'initial'=>$post['initial'],
                     
                'department_id' => $post['department_id'],
                
            );
            
			$confirm = $this->instructor_model->save($instructor_data, 0);
				

        
        $this->data['signupconfirmcontent'] = "Sign up Success";
		
		$this->session->set_flashdata('slider_content', "<h2>Instructor Registered Successfully. Please check email for account activation!</h2>");
		
		redirect('/footer/instructor/activation/');		
		
		}
  	
  }

	
    function teacher_signupconfirmation()
	{ 
		$this->data['meta']  = getMetaContent('teacher_signupconfirmation');
		
        $this->data['content'] = $this->data['meta']['data'];
                
        $meta = getMetaContent('teacher_signupconfirmcontent', 'data');
        
        $this->data['signupconfirmcontent'] = $meta['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/footer/instructor_signupconfirmation',$this->data,true));
	
	    redirect('/home');
	}
       

        
        
        
	
}

?>