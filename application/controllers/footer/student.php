<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();  
		
		$this->data['sel'] = 'footer_student';
		
		$this->load->model('members_model');
		
		$this->load->model('students_model');
		
		$this->load->model('departments_model');
		
		$this->load->model('courses_model');
	}
	
	function show_avatar_crop_popup()
	{	
        $this->data['meta'] = getMetaContent('model_message_remove_message','data');

        $this->data['content'] = $this->data['meta']['data'];

        $this->load->view('front/footer/avatar_crop_preview', $this->data);
	}

/* displays student sign up link page for becoming a student */
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('footer_student');
                
		$this->data['content'] = $this->data['meta']['data'];
                
		$this->data['body']='front/footer/footer_student';
		
		$this->load->view('front/structure',$this->data);
	}

/*  Slides down the sign up page for becoming a student */
	
	function student_signup()
	{ 
		$this->data['meta']  = getMetaContent('student_signup');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['departments'] = $this->departments_model->getAllDepartments();

		$this->data['degree_majors'] = $this->departments_model->get_all_degree_major();
		
		$this->load->view('front/footer/student_signup',$this->data);
	}

/*
|--------------------------------------------------------------------------
| User Login Form validation & Authentication
|--------------------------------------------------------------------------
*/
	 function register(){
	  		
	  	$this->form_validation->set_rules('first_name', 'First Name', 'trim|xss_clean')
		
              ->set_rules('last_name','Last Name', 'trim|required|xss_clean')
			  
			  ->set_rules('student_id','Student ID', 'trim|required|xss_clean')
			  
			  ->set_rules('phone','Phone', 'trip|required|xss_clean')
			  
			  ->set_rules('email','Email', 'trim|required|xss_clean')
			  
			  ->set_rules('password','Password', 'trim|required|xss_clean')
			 
	//		  ->set_rules('file_name_box_1','Student Photo', 'trim|required|xss_clean')
			  ;

		$this->form_validation->set_error_delimiters('<span class="rtn-error">', '</span>');
	
		if($this->form_validation->run()==FALSE) {
			
			$this->index();				
		}
		else{
			
			$activation_key=$this->_randomPassword();
			
			$post=$this->input->post();
			
			$member_data = array(
				
				'username'=>$post['student_id'],
				
				'phone'=>$post['phone'],
				
				'email'=>$post['email'],
				
				'password'=>md5($post['password']),	
				
				'activation_key'=>$activation_key,
				
				'user_type'=>'ST',		
			);
			
			$member_id = $this->members_model->Save($member_data,0);
			
			$student_photo='';
			
			$this->load->library('upload');
			
			$dir='./uploads/student/'.$member_id;
			
			if (!is_dir($dir))
	        {
	            mkdir($dir, 0755, true);
	        }
         
			
			if (!empty($_FILES['userfile_1']['name']))
	        {			
	            $config['upload_path'] = $dir;
				
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
				
	            $config['max_size'] = '1024';
				
				$config['encrypt_name']=TRUE;
				
	            $this->upload->initialize($config);			 
	           
	            if ($this->upload->do_upload('userfile_1'))
	            {
	                $upload_data_1 = $this->upload->data();
					
					$student_photo=$upload_data_1['file_name'];
	            }			
	        }
						
			$student_data=array(
				
				'member_id' => $member_id,
			
				'student_id'=>$post['student_id'],
				
				'first_name'=>$post['first_name'],
				
				'last_name'=>$post['last_name'],
				
				'photo' => $student_photo,
				
				'department_id' => $post['department_id'],
				
				'degree_major' =>$post['degree_major'],
			);
			
			$confirm = $this->students_model->Save($student_data,0);
			
			if($confirm)
			{
				$this->load->model('outbound_email_model');
				
				$content =$this->outbound_email_model->get('footer_student_signup_success');			//Model Function calling for get email content
				
				//
				if(!empty($content))
				{
					$content_body=$content['content'];
				}
				else 
				{
					exit();
				}
				$content_body =str_replace('{FIRST_NAME}',$post['first_name'],$content_body);
				
				$content_body=str_replace('{LAST_NAME}',$post['last_name'],$content_body);
				
				$content_body=str_replace('{USERNAME}',$post['email'],$content_body);
				
				$content_body=str_replace('{PASSWORD}',$post['password'],$content_body);
				
				$content_body=str_replace('{ACTIVATION_KEY}',$activation_key,$content_body);
				
				$content_body=str_replace('{ACTIVATION_LINK}',site_url('login/account_activation'),$content_body);
				
				$data_mail['content_body']=$content_body;
					
				$msg = $this->load->view('front/email_format',$data_mail,TRUE);
				
				$subject = $content['subject'];
				
				$to=$post['email'];
				
				$this->_emailer($to, $msg, $subject);							
			}
			
			$this->data['confirmation_title']="Sign Up Confirmation";
        	
        	$this->data['confirmation_content'] = "<p>Great! Your have submitted student sign-up form successfully. Now, follow these steps:</p>";

    		$this->data['confirmation_content'].="<ul><li>Check your email, the one you filled during sign-up. A confirmation email has been sent with activation code.</li>";
    		
    		$this->data['confirmation_content'].="<li>If you do not find the email in inbox, check other places (e.g junk mail box etc.)</li>";
    		
    		$this->data['confirmation_content'].="<li>Copy the activation code from you email and submit on activation page given in the link.</li>";
			
			$this->data['confirmation_content'].="<li>Once your account is activated, you are ready to start use NSU360!</li></ul>";
			
			$this->data['confirmation_content'].="<p>Happy Journey!</p>";

			
			$this->session->set_flashdata('slider_content', $this->load->view('front/common_confirmation',$this->data,true));
						
			redirect('/login/account_activation/');
		}
  	
  }

           
	function _randomPassword() 
	{
    	$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		
	    $pass = array(); //remember to declare $pass as an array
	    
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    
	    for ($i = 0; $i < 8; $i++) {
	    	
	        $n = rand(0, $alphaLength);
			
	        $pass[] = $alphabet[$n];
	    }
		
    return implode($pass); //turn the array into a string
}
        
	public function ajx_student_id_available()
	{
		$student_id=$this->input->post('student_id');
		
		$result=$this->students_model->check_studentId($student_id);
		
		if(count($result)==0)
			echo TRUE;		
		else
			echo FALSE;		
	} 

/*
|--------------------------------------------------------------------------
| returns list of degree major when a department is chosen in ajx req
|--------------------------------------------------------------------------
*/
	function ajx_load_majors()
	{
		if (isAjax()) 
		{			
			$department_id=$this->input->post('department_id');
			
			if($department_id>0)
			{
				$degree_majors=$this->departments_model->get_degree_major($department_id);				
			}
			else 
			{
				$degree_majors=$this->departments_model->get_all_degree_major();
			}				
			
			echo "<option value=\"\">Select</option>";
			
			foreach($degree_majors as $major)
			{		
				echo "<option value=".$major['major_id'].">".$major['name']." (".$major['initial'].")</option>";
				
			}
		} else {
	        show_error('Sorry, direct access is not allowed');
	    }
		
	}       

/*
|-----------------------------------------------------------------------------
| Send an email according to parameters
|-----------------------------------------------------------------------------
*/	
	function _emailer($to, $html, $subject, $file = '') {

		$config['protocol'] = 'mail';

		$config['wordwrap'] = TRUE;

		$config['crlf'] = "\r\n";

		$config['newline'] = "\r\n";

		$config['mailtype'] = 'html';

		$config['charset'] = 'utf-8';

		$this -> email -> initialize($config);
		
		$this->email ->to($to);
		
		$this -> email -> from('noreply@nsu360.com', 'NSU360 Support');

		$this -> email -> subject($subject);
		
		$this -> email -> message($html);
		
		if ($file!='') 
		{
			$this -> email -> attach('uploads/attachments/' . $file);
		}
		$this -> email -> send();

	}

	
}