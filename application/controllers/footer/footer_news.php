<?php

class Footer_news extends CI_Controller {

    function __construct() {
        parent::__construct();

        // Your own constructor code

        $this->data = array();

        $this->data['sel'] = 'footer_news';
    }

    function index() {
        $this->data['meta'] = getMetaContent('footer_news');
        $this->data['content'] = $this->data['meta']['data'];

        $meta = getMetaContent('footer_news_content', 'data');
        $this->data['footer_news_content'] = $meta['data'];


        $meta = getMetaContent('footer_news_recent_post', 'data');
        $this->data['footer_news_recent_post'] = $meta['data'];

        $meta = getMetaContent('footer_news_recent_comments', 'data');
        $this->data['footer_news_recent_comments'] = $meta['data'];




        $this->data['body'] = 'front/footer/news';
        $this->load->view('front/structure', $this->data);
    }

    function news_detail() {
        $this->data['meta'] = getMetaContent('footer_news_detail');
        $this->data['content'] = $this->data['meta']['data'];


        $meta = getMetaContent('footer_news_article_content', 'data');
        $this->data['footer_news_article_content'] = $meta['data'];


        $meta = getMetaContent('footer_news_article_recent_post', 'data');
        $this->data['footer_news_article_recent_post'] = $meta['data'];

        $meta = getMetaContent('footer_news_article_recent_comments', 'data');
        $this->data['footer_news_article_recent_comments'] = $meta['data'];


        $this->data['body'] = 'front/footer/news_article_detail';

        $this->load->view('front/structure', $this->data);
    }

    function comment() {
        $this->data['meta'] = getMetaContent('comment', 'meta');

        $this->load->view('front/footer/comments', $this->data);
    }

    function mainstream() {
        $this->data['meta'] = getMetaContent('footer_mainstream','meta');
		
        $this->data['content'] = $this->data['meta']['data'];

        $this->data['body'] = 'front/footer/mainstream';

        $this->load->view('front/structure', $this->data);
    }

    function mainstream_newsdetail() {
        $this->data['meta'] = getMetaContent('footer_mainstream_detail');
        $this->data['content'] = $this->data['meta']['data'];



        $meta = getMetaContent('footer_mainstream_article_content', 'data');
        $this->data['footer_mainstream_article_content'] = $meta['data'];


        $meta = getMetaContent('footer_mainstream_article_recent_post', 'data');
        $this->data['footer_mainstream_article_recent_post'] = $meta['data'];

        $meta = getMetaContent('footer_mainstream_article_recent_comments', 'data');
        $this->data['footer_mainstream_article_recent_comments'] = $meta['data'];


        $this->data['body'] = 'front/footer/mainstream_detail';

        $this->load->view('front/structure', $this->data);
    }

    function upgrade() {
        $this->data['meta'] = getMetaContent('member_upgrade_account');

        $this->data['content'] = $this->data['meta']['data'];

        $this->data['body'] = 'front/member/upgrade_account';

        $this->load->view('front/structure', $this->data);
    }

    function mainstream_comment() {
        $this->data['meta'] = getMetaContent('mainstream_comment', 'meta');

        //$this->data['content'] = $this->data['meta']['data'];

        $this->data['body'] = 'front/footer/mainstream_comment';

        $this->load->view('front/structure', $this->data);
    }


}

?>