<?php

class Courses extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();

        $this->data['sel'] = 'courses_admin';
        $this->data['display_menu'] = 'yes';
        $this->load->model(array('departments_model', 'general_model'));
        $this->data['departments'] = $this->departments_model->getAllDepartments();
    }

    function index() {
        $this->load->model('courses_model');

        $this->data['message'] = $this->session->flashdata('message');

        $this->data['courses'] = $this->courses_model->get_courses();

        $this->data['body'] = 'admin/courses/view';

        $this->load->view('admin/structure', $this->data);
    }

    function edit($course_id = 0) {
        $this->load->model('courses_model');
        $this->data['semesters'] = $this->general_model->get_ALL('semesters');
        if ($course_id != 0) {

            $this->data['course'] = $this->courses_model->get_my_course($course_id);

            $this->data['edit'] = 'edit';
        } else {
            $this->data['edit'] = 'new';
        }

        $this->data['body'] = 'admin/courses/edit';

        $this->load->view('admin/structure', $this->data);
    }

    function save($course_id = 0) {
        $this->load->model('courses_model');
        $data = array();
        if (isset($_POST['initial']))
            $data['initial'] = $_POST['initial'];
        if (isset($_POST['title']))
            $data['title'] = $_POST['title'];
        if (isset($_POST['description']))
            $data['description'] = $_POST['description'];
        if (isset($_POST['department_id']))
            $data['department_id'] = $_POST['department_id'];
        if (isset($_POST['credit_no']))
            $data['credit_no'] = $_POST['credit_no'];

        $this->form_validation->set_rules('credit_no', 'Number Of Credits', 'trim|required|xss_clean');
        if ($course_id == 0) {
            $this->form_validation->set_rules('initial', 'Initial', 'trim|required|is_unique[courses.initial]|xss_clean');
        }
        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('department_id', 'Department Name', 'trim|required|xss_clean');

        $this->form_validation->set_error_delimiters('<span class="error" style="color:red;">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->edit($course_id);
        } else {

            $this->courses_model->save($data, $course_id);

            redirect('/admin/courses/index');
        }
    }

    function delete($id) {
        $this->load->model('courses_model');
        if (is_numeric($id)) {
            $this->courses_model->delete($id);

            $this->session->set_flashdata('message', 'Course has been deleted');

            redirect('admin/courses');
        }
    }

    function ajx_course_available_admin() {
        $this->load->model('courses_model');
        $course_initial = $this->input->post('initial');
        $result = $this->courses_model->check_course($course_initial);

        if (count($result) == 0)
            echo TRUE;
        else
            echo FALSE;
    }

    function ajx_load_courses() {
        $this->load->model('courses_model');
        if (isAjax()) {
            $department_id = $this->input->post('department_id');
            if ($department_id > 0) {
                $courses = $this->courses_model->get_department_courses($department_id);
            } else {
                $courses = $this->courses_model->get_courses();
            }

            $n = 1;
            foreach ($courses as $course) {
                $initial = $course['initial'];
                $title = $course['title'];
                $id = $course['course_id'];
                $edit = '<a href="' . site_url('admin/courses/edit/' . $course['course_id']) . '"><img src="' . base_url() . 'images/edit_icon.png" width="21" height="21" alt=" " /></a>';
                $delete = '<a class="confirm_action" title="Are you sure you want to delete this Course?" href="' . site_url('admin/courses/delete/' . $course['course_id']) . '"><img src="' . base_url() . 'images/delete_icon.jpg" width="21" height="21" alt=" " /></a>';

                echo <<<END
<tr>
<td>$n</td>
<td>$initial</td>
<td>$title</td>
<td>$edit $delete</td>
</tr>
END;
                $n++;
            }
        } else {
            show_error('Sorry, direct access is not allowed');
        }
    }

}