<?php
class Blog_category extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_id')=='')
			redirect('admin/login');
               
		$this->load->library('form_validation');											//load form validation library		 

		$this->load->model('blog_categories_model');													//load admin user model
	    
		$this->data['sel']='blog_categories';

		$this->data['display_menu']='yes';
	}

/*
|-----------------------------------------------------------------------------
| Default list view of all the categories/ sub/ sub sub categories of blog
|-----------------------------------------------------------------------------
*/	
	function index($fields='')
	{
		if($fields!='')
		{
			if($this->session->userdata('sorttype')=='')
				$this->session->set_userdata(array('sorttype'=>'asc'));
			else{ 

				if($this->session->userdata('sorttype')=='asc') {

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'desc'));
				} else 	{

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'asc'));

				}	

			}

		}

	$this->data['blog_categories'] =  $this->blog_categories_model->getBlogCategories();
	
	$this->data['body']='admin/blog_categories/list';
                    
	$this->load->view('admin/structure',$this->data);
		
	}

/*
|-----------------------------------------------------------------------------
| Delete a Category form blog categories
|-----------------------------------------------------------------------------
*/	
	function delete($id)
	{
		if(is_numeric($id))
		{
			$this->blog_categories_model->delete($id);

			$this->session->set_flashdata('message', 'Category has been deleted');
			
			redirect('admin/blog_category');
		}
	}
	
	function new_fancy()
	{
		$this->data['body']='admin/blog_categories/add';
	                    
		$this->load->view('admin/structure',$this->data);
	} 

/*
|-----------------------------------------------------------------------------
| Update or shows a new form for blog category
|-----------------------------------------------------------------------------
*/	
	function edit($category_id='')
	{	  	

		if ($category_id!=0){			
                   
			$this->data['blog_category'] = $this->blog_categories_model->getCategoryByID($category_id);

			$this->data['edit']='edit';  

		}else

		$this->data['edit']='new';
              
	   	$this->data['categories_id']=$category_id;
		
		$this->data['parent_categories']=$this->blog_categories_model->getParentCategories();

	   	$this->data['body']='admin/blog_categories/edit';
        
		$this->load->view('admin/structure',$this->data);

	}

        
/*
|-----------------------------------------------------------------------------
| Update or create a new blog category
|-----------------------------------------------------------------------------
*/	
	function save($category_id)
	{
	
	  	$this->form_validation->set_rules('parent_id', 'Parent', 'trim|xss_clean');
		
		$this->form_validation->set_rules('category', 'Category', 'trim|xss_clean|required');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() != FALSE)
		{
			$parent_id=$this->input->post('parent_id', TRUE);
			
			$parent= $this->blog_categories_model->getCategoryByID($parent_id);
			$type=1;
			if(isset($parent['type']))
			{
				if($parent['type']==1)
					$type=2;
				else {
					$type=3;
				}
			}

		$data=array(
	  			'parent_id'=>$this->input->post('parent_id',true),
	  			'category'=>$this->input->post('category',true),
	  			'type'=>$type,
                 );
			
			if($this->blog_categories_model->save($category_id,$data))
			{
				if ($category_id==0){			

				$this->data['edit']='new';

				$this->session->set_flashdata('message', 'Category added successfully');

				redirect('admin/blog_category');
	
		  		}else 	
					$this->session->set_flashdata('message', 'Category updated successfully');
				
			}

			

			redirect('admin/blog_category');
	  	} else {

			$this->edit($category_id);

		}
	}  
}