<?php
class Payment_method extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_id')=='')
			redirect('admin/login');
               
		$this->load->library('form_validation');											//load form validation library		 

		$this->load->model('payment_methods_model');													//load admin user model
	    
		$this->data['sel']='payment_methods';

		$this->data['display_menu']='yes';
	}

/*
|-----------------------------------------------------------------------------
| displays list of payment methods in the admin controller page
|-----------------------------------------------------------------------------
*/
	function index($fields='')
	{
		if($fields!='')
		{

			if($this->session->userdata('sorttype')=='')
				$this->session->set_userdata(array('sorttype'=>'asc'));
			else{ 

				if($this->session->userdata('sorttype')=='asc') {

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'desc'));
				} else 	{

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'asc'));

				}	

			}

		}

	$this->data['payment_methods'] =  $this->payment_methods_model->getAllMethods();

	$this->data['body']='admin/payment_method/list';
                    
	$this->load->view('admin/structure',$this->data);
		
	}

/*
|-----------------------------------------------------------------------------
| create a new row in the payment method table or update a row by given id
|-----------------------------------------------------------------------------
*/
	function edit($payment_method_id)
	{	 
		if ($payment_method_id!=0){			
                   
			$this->data['payment_method'] = $this->payment_methods_model->getMethodsByID($payment_method_id);

			$this->data['edit']='edit';  
		}	
		else
		{
			$this->data['edit']='new';			
		}           
	   	
	   	$this->data['payment_method_id']=$payment_method_id;

	  	$this->data['body']='admin/payment_method/edit';
        
  		$this->load->view('admin/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| display a detail view of a payment method
|-----------------------------------------------------------------------------
*/
	function view($payment_method_id)
	{
		$this->data['payment_method'] = $this->payment_methods_model->getMethodsByID($payment_method_id);

	   	$this->data['payment_method_id']=$payment_method_id;

	   	$this->data['body']='admin/payment_method/view';

	   	$this->load->view('admin/structure',$this->data);	  

	}

/*
|-----------------------------------------------------------------------------
| save a as a new row or update a new row depending on the $id
|-----------------------------------------------------------------------------
*/	
	function save($payment_method_id)
	{
		//Set Form validation rules
		
	  	$this->form_validation->set_rules('name', 'Payment Method', 'trim|xss_clean|required');
		
		$this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');
		
		$this->form_validation->set_rules('payment_processor', 'Payment Processor', 'trim|xss_clean|required');
		
		$this->form_validation->set_rules('payment_type', 'Payment Type', 'trim|xss_clean|required');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() != FALSE)
		{
			
			$data=array(
	  			
	  			'name'=>$this->input->post('name',true),
	  			
				'description'=>$this->input->post('description',true),
				
				'payment_processor'=>$this->input->post('payment_processor',true),
				
				'payment_type'=>$this->input->post('payment_type',true),
			
                 );
				 
			$this->payment_methods_model->save($payment_method_id,$data);

			if ($payment_method_id==0)
			{
				$this->data['edit']='new';

				$this->session->set_flashdata('message', 'Payment method added successfully');
	  		}
	  		else 
	  		{
				$this->session->set_flashdata('message', 'Payment method updated successfully');	  			
	  		}
			redirect('admin/payment_method');
			
	  	}else {

			$this->edit($payment_method_id);
		}
	}

/*
|-----------------------------------------------------------------------------
| Removes a row from the payment method table
|-----------------------------------------------------------------------------
*/	
	function delete($payment_method_id)
	{
		$this->payment_methods_model->delete($payment_method_id);

		$this->session->set_flashdata('message', 'Payment method has been deleted');
		
		redirect('admin/payment_method');
	} 
    
}
?>
