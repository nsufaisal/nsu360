<?php
class Semester_Courses extends CI_Controller {

     function __construct()
     {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();
        
        $this->data['sel']='semester_courses';
        $this->data['display_menu']='yes';
        $this->load->model(array('departments_model', 'general_model', 'courses_model', 'semesters_model', 'semester_course_model', 'rooms_model', 'instructor_model'));
        $this->data['semester_courses'] = $this->semester_course_model->getAllSemesterCourses();
        $this->data['instructors'] = $this->instructor_model->get_all_instructors();
        $this->data['departments'] = $this->departments_model->getAllDepartments();
        $this->data['courses'] = $this->courses_model->get_courses();
        $this->data['rooms'] = $this->rooms_model->getAllRooms();
        $this->data['semesters'] = $this->semesters_model->getAllSemesters();
    }
    
    function index() {

        $this->data['message'] = $this->session->flashdata('message');

        $this->data['body'] = 'admin/semester_courses/list';

        $this->load->view('admin/structure', $this->data);
    }
        function edit($id = 0) {
            $this->data['room_types'] = $this->rooms_model->getAllRoomTypes();
            if ($room_id != 0) {

            $this->data['MYroom'] = $this->rooms_model->get_my_room($room_id);

            $this->data['edit'] = 'edit';
        } else {
            $this->data['edit'] = 'new';
        }
        $this->data['body'] = 'admin/rooms/edit';

        $this->load->view('admin/structure', $this->data);
    }
}
