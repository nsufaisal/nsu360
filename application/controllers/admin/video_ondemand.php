<?php

class Video_ondemand extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if ($this->session->userdata('admin_id') == '')
            redirect('admin/login');

        $this->load->library('form_validation');           //load form validation library	
        
        $this->load->library('upload'); 	 
    
        $this->load->model('videos_ondemand_model');
		
		$this->load->model('models_model');
		
		$this->load->model('studios_model');	
		
		$this->load->model('categories_model');	
		
		$this->load->model('settings_model');	 

        $this->data = array();

        $this->data['sel'] = 'video_ondemand';

        $this->data['display_menu'] = 'yes';
    }

    function index($fields = '') {
        if ($fields != '') {

            if ($this->session->userdata('sorttype') == '')
                $this->session->set_userdata(array('sorttype' => 'asc'));
            else {

                if ($this->session->userdata('sorttype') == 'asc') {

                    $this->session->unset_userdata(array('sorttype' => ''));

                    $this->session->set_userdata(array('sorttype' => 'desc'));
                } else {

                    $this->session->unset_userdata(array('sorttype' => ''));

                    $this->session->set_userdata(array('sorttype' => 'asc'));
                }
            }
        }
		
		$this->data['trial_video']=$this->settings_model->getSettingByName('trial_video');
		
		$this->data['trailer_pre_post']=$this->settings_model->getSettingByName('trailer_pre_post');
		
        $this->data['videos'] = $this->videos_ondemand_model->get_videos($fields);

        $this->data['body'] = 'admin/video_ondemand/list';

        $this->load->view('admin/structure', $this->data);
    }
/*
|-----------------------------------------------------------------------------
| Add or update a new videos
|-----------------------------------------------------------------------------
*/
    function edit($video_id) {

        if ($video_id != 0) {

            $this->data['video'] = $this->videos_ondemand_model->getVideoByID($video_id);

            $this->data['edit'] = 'edit';

	        $this->session->set_userdata('edit_video_id', $video_id);
        } 
        else
            $this->data['edit'] = 'new';

        $this->data['video_id'] = $video_id;
		
		$this->data['categories']=$this->categories_model->getCategories();
		
		$this->data['models']=$this->models_model->getModelList();
		
		$this->data['studios']=$this->studios_model->getStudioList();

        $this->data['body'] = 'admin/video_ondemand/edit';

        $this->load->view('admin/structure', $this->data);
    }

/*
|-----------------------------------------------------------------------------
| Create or update a new video  on demand
|-----------------------------------------------------------------------------
*/
    function save($video_id=0) 
    {   	    	
		ob_start();	
			
		log_message('error', print_r($_FILES, TRUE));		
				
		$this->form_validation->set_rules('file_name_box_1', 'Video File', 'trim|xss_clean|required');

        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');

        $this->form_validation->set_rules('model_id', 'Performer', 'trim|required|xss_clean');

        $this->form_validation->set_rules('studio_id', 'Studio', 'trim|xss_clean|required');

        $this->form_validation->set_rules('price', 'Price', 'trim|xss_clean|required|nemeric');

        $this->form_validation->set_rules('category_id', 'Genre', 'trim|xss_clean|required');
		
        $this->form_validation->set_rules('length', 'Length', 'trim|xss_clean|nemeric|required');
		
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit($video_id);
		}
		else 
		{
			if($video_id !=0)
			{
				$video_file=$this->input->post('file_name_box_1');
			}
			else {
				$video_file='';
			}			
			$this->load->helper('form');
						
			$this->load->library('upload'); 
			
	        if (!empty($_FILES['userfile_1']['name']))
	        {
	            $config['upload_path'] = './uploads/videos/';
	            $config['allowed_types'] = '*';
	            $config['max_size'] = '51200';
				$config['encrypt_name']=TRUE;
	            $this->upload->initialize($config);		 
	            if ($this->upload->do_upload('userfile_1'))
	            {
	            	if($video_id !=0)
					{
						if($video_file !='')
							unlink('./uploads/videos/'.$video_file);
					}
	                $upload_data_1= $this->upload->data();
					$video_file=$upload_data_1['file_name'];
	            }
	            else
	            {
	            	$upload_data['userfile_1']='';
	            }
	        }					
	 
			/* populating array for create/update to database */
						
			$data=array(
			
				'title'=>$this->input->post('title'),
				
				'model_id'=>$this->input->post('model_id'),
				
				'description'=>$this->input->post('description'),
				
				'studio_id'=>$this->input->post('studio_id'),
				
				'price'=>$this->input->post('price'),
				
				'category_id'=>$this->input->post('category_id'),
				
				'length'=>$this->input->post('length'),
				
				'video_file'=>$video_file,
	
			);
			
			$this->videos_ondemand_model->save($data, $video_id);
		
			if($tutorial_id==0)
	        	$this->session->set_flashdata('message', 'Admin video on demand added successfully');
			else {
				$this->session->set_flashdata('message', 'Admin video on demand updated successfully');
			}
	
	       redirect('admin/video_ondemand');

		}
	}

/*
|-----------------------------------------------------------------------------
| display detail of a video on deman on the admin page
|-----------------------------------------------------------------------------
*/
    function view($video_id){

		$this->data['video']=$this->videos_ondemand_model->getVideoByID($video_id);
		
        $this->data['body'] = 'admin/video_ondemand/view';

        $this->load->view('admin/structure', $this->data);
    }

/*
|-----------------------------------------------------------------------------
| delete a video on demand row from the database table
|-----------------------------------------------------------------------------
*/
    function delete($video_id) {

        if (is_numeric($video_id)) {

           	$this->videos_ondemand_model->delete($video_id);

	       	$this->session->set_flashdata('message', 'Video has been deleted');
			
          redirect('admin/video_ondemand');
        }
    }

/*
|-----------------------------------------------------------------------------
| ajx call for change a settings
|-----------------------------------------------------------------------------
*/
    function ajx_change_settings() {
    	
		$name=$this->input->post('name');
		
		$state_value=$this->input->post('state_value');
    	
		$this->settings_model->updateSettingByName($name,$state_value);

        echo TRUE;
    }

}

?>
