<?php

class Tutorials extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if ($this->session->userdata('admin_id') == '')
            redirect('admin/login');

        $this->load->library('form_validation');           //load form validation library	
        
        $this->load->library('upload'); 	 

        $this->load->model('admin_users');             //load admin user model
        
        $this->load->model('tutorials_model'); 

        $this->data = array();

        $this->data['sel'] = 'tutorials';

        $this->data['display_menu'] = 'yes';
    }

    function index($fields = '') {
        if ($fields != '') {

            if ($this->session->userdata('sorttype') == '')
                $this->session->set_userdata(array('sorttype' => 'asc'));
            else {

                if ($this->session->userdata('sorttype') == 'asc') {

                    $this->session->unset_userdata(array('sorttype' => ''));

                    $this->session->set_userdata(array('sorttype' => 'desc'));
                } else {

                    $this->session->unset_userdata(array('sorttype' => ''));

                    $this->session->set_userdata(array('sorttype' => 'asc'));
                }
            }
        }

        $this->data['tutorials'] = $this->tutorials_model->get_tutorials($fields);

        $this->data['body'] = 'admin/tutorials/list';

        $this->load->view('admin/structure', $this->data);
    }

    function delete_pp($user_id) {

        $this->data['user_id'] = $user_id;

        $this->load->view('admin/users/remove', $this->data);
    }

    function delete($tutorial_id) {

        if (is_numeric($tutorial_id)) {

           	$this->tutorials_model->del_tutorial($tutorial_id);

	       	$this->session->set_flashdata('message', 'Tutorial has been deleted');
			
          redirect('admin/tutorials');
        }
    }

    function edit($tutorial_id) {

        if ($tutorial_id != 0) {

            $this->data['tutorial'] = $this->tutorials_model->get_tutorial_by_id($tutorial_id);

            $this->data['edit'] = 'edit';

            $this->session->set_userdata('edit_tutorial_id', $tutorial_id);
        } 
        else
            $this->data['edit'] = 'new';

        $this->data['tutorial_id'] = $tutorial_id;
		
		$this->data['tut_categories']=$this->tutorials_model->get_tut_categories();

        $this->data['body'] = 'admin/tutorials/add';

        $this->load->view('admin/structure', $this->data);
    }

    function view($tutorial_id) {

		$this->data['tutorial']=$this->tutorials_model->get_tutorial($tutorial_id);
		
        $this->data['body'] = 'admin/tutorials/view';

        $this->load->view('admin/structure', $this->data);
    }

    function save($tutorial_id=0) {    	
		ob_start();		
		log_message('error', print_r($_FILES, TRUE));

        $this->form_validation->set_rules('category', 'Category', 'trim|required|xss_clean');

        $this->form_validation->set_rules('name', 'Name', 'trim|xss_clean|required');

        $this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');
		
		$this->form_validation->set_rules('file_name_box_1', 'Video File', 'trim|xss_clean|required');
		
		$this->form_validation->set_rules('file_name_box_2', 'Video Image', 'trim|xss_clean|required');
		
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit($tutorial_id);
		}
		else 
		{
			if($tutorial_id !=0)
			{
				$video_file=$this->input->post('file_name_box_1');
				
				$video_image=$this->input->post('file_name_box_2');
			}
			else {
				$video_file='';
				
				$video_image='';
			}

			
			$this->load->helper('form');
			
			$this->load->library('upload'); 
			
			if (!empty($_FILES['userfile_2']['name']))
	        {
			
	            $config['upload_path'] = './uploads/tutorials/';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            $config['max_size'] = '2048';
				$config['encrypt_name']=TRUE;
	            $this->upload->initialize($config);			 
	           
	            if ($this->upload->do_upload('userfile_2'))
	            {
	            	if($tutorial_id !=0)
					{
						if($video_image !='')
							unlink('./uploads/tutorials/'.$video_image);
					}					
	                $upload_data_2 = $this->upload->data();
					$video_image=$upload_data_2['file_name'];
	            }
	            else
	            {
	            	$upload_data['userfile_2']='';
	            }			
	        }					
	        if (!empty($_FILES['userfile_1']['name']))
	        {
	            $config['upload_path'] = './uploads/tutorials/';
	            $config['allowed_types'] = 'avi|wmv|mov|flv|mp4';
	            $config['max_size'] = '25600';
				$config['encrypt_name']=TRUE;
	            $this->upload->initialize($config);		 
	            if ($this->upload->do_upload('userfile_1'))
	            {
	            	if($tutorial_id !=0)
					{
						if($video_file !='')
							unlink('./uploads/tutorials/'.$video_file);
					}
						
						
	                $upload_data_1	 = $this->upload->data();
					$video_file=$upload_data_1['file_name'];
	            }
	            else
	            {
	            	$upload_data['userfile_1']='';
	            }
	        }					
						
			$data=array(
			
			'category_id'=>$this->input->post('category'),
			
			'name'=>$this->input->post('name'),
			
			'description'=>$this->input->post('description'),
			
			'video_file'=>$video_file,
			
			'video_image'=>$video_image,
		
			);
			
			$this->tutorials_model->SaveAdminTutorial($data, $tutorial_id);
		
			if($tutorial_id==0)
	        	$this->session->set_flashdata('message', 'Admin tutorial added successfully');
			else {
				$this->session->set_flashdata('message', 'Admin tutorial updated successfully');
			}
	
	       redirect('admin/tutorials');

		}
	}
}

?>
