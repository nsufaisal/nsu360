<?php

class Settings extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();

        $this->data['sel'] = 'settings_admin';
        $this->data['display_menu'] = 'yes';
        $this->load->model(array('semesters_model', 'general_model'));
    }

    function index() {

        $this->data['message'] = $this->session->flashdata('message');

        //$this->data['settings'] = $this->general_model->get_All();

        $this->data['body'] = 'admin/settings/view';

        $this->load->view('admin/structure', $this->data);
    }
    function ajx_generate_current_semster($year='')
    {
        
    }
}