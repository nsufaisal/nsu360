<?php
class Semesters extends CI_Controller {

     function __construct()
     {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();
        
        $this->data['sel']='semesters_admin';
        $this->data['display_menu']='yes';
         $this->load->model(array('semesters_model', 'general_model'));
        $this->data['semesters'] = $this->semesters_model->getAllSemesters();
        $this->data['current_semester'] = $this->semesters_model->current_semester();
    }
    
    function index()
    {        
        $this->data['body']='admin/semesters/list';
        
        $this->data['message']=$this->session->flashdata('message');
        
        $this->load->view('admin/structure',$this->data);
    }
    
    function edit($semester_id=0)
    {
        $this->data['semesters'] = $this->general_model->get_ALL('semesters');
        if ($semester_id != 0) {

            $this->data['semester'] = $this->semesters_model->get_my_semester($semester_id);

            $this->data['edit'] = 'edit';
        } else {
            $this->data['edit'] = 'new';
        }

        $this->data['body'] = 'admin/semesters/edit';

        $this->load->view('admin/structure', $this->data);
    }
    
    
    
    function save($semester_id = 0) {
        $data = array();
        if (isset($_POST['initial']))
            $data['initial'] = $_POST['initial'];
        if (isset($_POST['title']))
            $data['title'] = $_POST['title'];
        if (isset($_POST['start_time']))
            $data['start_date'] = $_POST['start_time'];
        if (isset($_POST['end_time']))
            $data['end_date'] = $_POST['end_time'];
        $this->form_validation->set_rules('initial', 'Semester Initial', 'trim|required|xss_clean');
            $this->form_validation->set_rules('initial', 'Initial', 'trim|required|is_unique[courses.initial]|xss_clean');
        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_error_delimiters('<span class="error" style="color:red;">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->edit($semester_id);
        } else {

            $this->semesters_model->save($data, $semester_id);

            redirect('/admin/semesters/index');
        }
    }
    function delete($semester_id) {
        if (is_numeric($semester_id)) {
            $this->semesters_model->delete($semester_id);

            $this->session->set_flashdata('message', 'Course has been deleted');

            redirect('admin/semesters');
        }
    }
    function activate($semester_id=0)
    {
        $this->semesters_model->activate_semester($semester_id);

        redirect('/admin/semesters/index');
    }
    function deactivate($semester_id=0)
    {
        $this->semesters_model->deactivate_semester($semester_id);
        redirect('/admin/semesters/index');
    }
    function deactivate_activate($semester_id){
        $this->semesters_model->activate_semester($semester_id);
         $current = $this->semesters_model->current_semester();
        //if (($current!=null)) {
            $current_id = $current['semester_id'];
            $this->semesters_model->deactivate_semester($current_id);
        //}
    redirect('/admin/semesters/index');
    }
}