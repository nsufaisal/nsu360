<?php
class Blog extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_id')=='')
			redirect('admin/login');
               
		$this->load->library('form_validation');
		
		$this->load->library('upload'); 
		
		$this->load->model('blogs_model');		 

		$this->load->model('blog_categories_model');
	    
		$this->data['sel']='blog';

		$this->data['display_menu']='yes';
	}

/*
|-----------------------------------------------------------------------------
| Default list view of all the categories/ sub/ sub sub categories of blog
|-----------------------------------------------------------------------------
*/	
	function index($fields='')
	{
		if($fields!='')
		{
			if($this->session->userdata('sorttype')=='')
				$this->session->set_userdata(array('sorttype'=>'asc'));
			else{ 

				if($this->session->userdata('sorttype')=='asc') {

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'desc'));
				} else 	{

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'asc'));

				}
			}
		}

	$this->data['blogs'] =  $this->blogs_model->getAllBlogs();
	
	$this->data['body']='admin/blogs/list';
                    
	$this->load->view('admin/structure',$this->data);
		
	}

/*
|-----------------------------------------------------------------------------
| Update or shows a new form for a blog 
|-----------------------------------------------------------------------------
*/	
	function edit($blog_id=0)
	{	  	
		if ($blog_id!=0){			
                   
			$this->data['blog'] = $this->blogs_model->getBlogByID($blog_id);

			$this->data['edit']='edit';  

		}else

		$this->data['edit']='new';
              
	   	$this->data['blog_id']=$blog_id;
		
		$this->data['parent_categories']=$this->blog_categories_model->getParentCategories();

	   	$this->data['body']='admin/blogs/edit';
        
		$this->load->view('admin/structure',$this->data);

	}
	
/*
|-----------------------------------------------------------------------------
| Update or create a new blog category
|-----------------------------------------------------------------------------
*/	
	function save($blog_id)
	{
		ob_start();		
		
		log_message('error', print_r($_FILES, TRUE));
	  		
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('author', 'Author', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('category_id', 'Category', 'trim|required|xss_clean');
		
	  	$this->form_validation->set_rules('post_date', 'Date', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('content', 'Content', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() == FALSE)
		{
			$this->edit($blog_id);
		}
		else 
		{
		
			if($blog_id !=0)
			{
				$image_file=$this->input->post('file_name_box_1');
				
			}
			else {
				$image_file='';
			}
			
			$this->load->helper('form');
			
			if (!empty($_FILES['userfile_1']['name']))
	        {
	            $config['upload_path'] = './uploads/blog_images/';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            $config['max_size'] = '2048';
				$config['encrypt_name']=TRUE;
	            $this->upload->initialize($config);			 
	           
	            if ($this->upload->do_upload('userfile_1'))
	            {
	            	if($blog_id !=0)
					{
						if($image_file !='')
							unlink('./uploads/blog_images/'.$image_file);
					}					
	                $upload_data_1 = $this->upload->data();
					$image_file=$upload_data_1['file_name'];
	            }
	            else
	            {
	            	$upload_data['userfile_1']='';
	            }			
	        }		
			$data=array(
		
	  			'title'=>$this->input->post('title',true),
	  			
				'author'=>$this->input->post('author',true),
				
				'post_date'=>$this->input->post('post_date',true),
				
				'video_link'=>$this->input->post('video_link',true),
				
				'image_file'=>$image_file,
				
	  			'category_id'=>$this->input->post('category_id',true),
	  			
				'content'=>$this->input->post('content',true),

                 );
			
			if($this->blogs_model->save($blog_id,$data))
			{
				if ($blog_id==0){			

				$this->data['edit']='new';

				$this->session->set_flashdata('message', 'Blog added successfully');

				redirect('admin/blog');
	
		  		}else 	
					$this->session->set_flashdata('message', 'Blog updated successfully');	
			}	
			redirect('admin/blog');
		}  
	}

/*
|-----------------------------------------------------------------------------
| View a blog detail
|-----------------------------------------------------------------------------
*/	
    function view($blog_id) {

		$this->data['blog']=$this->blogs_model->getBlogByID($blog_id);
		
        $this->data['body'] = 'admin/blogs/view';

        $this->load->view('admin/structure', $this->data);
    }

/*
|-----------------------------------------------------------------------------
| Delete a Blog form blogs table
|-----------------------------------------------------------------------------
*/	
	function delete($blog_id)
	{
		if(is_numeric($blog_id))
		{
			$this->blogs_model->delete($blog_id);

			$this->session->set_flashdata('message', 'Blog has been deleted');
			
			redirect('admin/blog');
		}
	}	
	
/******************************************* */

}