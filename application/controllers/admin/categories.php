<?php
class Categories extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_id')=='')
			redirect('admin/login');
               
		$this->load->library('form_validation');											//load form validation library		 

		$this->load->model('categories_model');													//load admin user model
	    
		$this->data['sel']='categories';

		$this->data['display_menu']='yes';
	}

	function index($fields='')
	{
		if($fields!='')
		{

			if($this->session->userdata('sorttype')=='')
				$this->session->set_userdata(array('sorttype'=>'asc'));
			else{ 

				if($this->session->userdata('sorttype')=='asc') {

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'desc'));
				} else 	{

					$this->session->unset_userdata(array('sorttype'=>''));

					$this->session->set_userdata(array('sorttype'=>'asc'));

				}	

			}

		}

	$this->data['categories'] =  $this->categories_model->getCategories();

	$this->data['body']='admin/categories/list';
                    
	$this->load->view('admin/structure',$this->data);
		
	}

	function delete_pp($user_id) 	   
	{

		$this->data['user_id'] = $user_id;

		$this->load->view('admin/users/remove',$this->data);
	}

	function delete($id)
	{

		if(is_numeric($id))
		{

			$this->categories_model->delete($id);

			$this->session->set_flashdata('message', 'Category has been deleted');
			
			redirect('admin/categories');
		}
	} 

	function edit($categories_id)
	{	  	

		if ($categories_id!=0){			
                   
			$this->data['category'] = $this->categories_model->getCategoriesByID($categories_id);

			$this->data['edit']='edit';  

		}	else

		$this->data['edit']='new';
              
	   $this->data['categories_id']=$categories_id;

	   $this->data['body']='admin/categories/edit';
        
  		$this->load->view('admin/structure',$this->data);	  

	}

	function view($user_id)
	{	  	

		$this->data['publicusers'] = $this->public_users_model->get_user_by_id($user_id);

	   	$this->data['user_id']=$user_id;

	   	$this->data['body']='admin/publicusers/view';

	   	$this->load->view('admin/structure',$this->data);	  

	}
            
       function export_data(){
        $this->data['exportdata'] = $this->public_users_model->getexportdetails();
         $this->load->view('admin/publicusers/export_dataview',$this->data);
       } 
        
	function save($categories_id)
	{
		//Set Form validation rules
		
	  	$this->form_validation->set_rules('name', 'Category', 'trim|xss_clean|required');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() != FALSE)
		{
			
			$data=array(
	  			'name'=>$this->input->post('name',true)
                       );
			$this->categories_model->save($categories_id,$data);

			if ($categories_id==0){			

				$this->data['edit']='new';

				$this->session->set_flashdata('message', 'Category added successfully');

			redirect('admin/categories');

	  		}else 

				$this->session->set_flashdata('message', 'Category updated successfully');

			redirect('admin/categories');
	  	} else {

			$this->edit($categories_id);

		}
	}

		function name_check()
	{
			$this->form_validation->set_message('name_check', "The Category is required");
	}

        
    
      
      
      
      
      
}
?>
