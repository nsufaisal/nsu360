<?php
class Departments extends CI_Controller {

     function __construct()
     {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();
        
        $this->data['sel']='departments_admin';
        $this->data['display_menu']='yes';
         $this->load->model(array('departments_model', 'general_model'));
        $this->data['departments'] = $this->departments_model->getAllDepartments();
            
    }
    
    function index()
    {        
        $this->data['body']='admin/departments/list';
        
        $this->data['message']=$this->session->flashdata('message');
        
        $this->load->view('admin/structure',$this->data);
    }
    
    function edit($department_id=0)
    {
        $this->data['parent_schools'] = $this->departments_model->get_parent_school();
        if ($department_id!=0){
                   
            $this->data['department'] = $this->departments_model->get_my_department($department_id);
            
            $this->data['edit']='edit';  

        }else{
                    $this->data['edit']='new';
        }

        $this->data['body']='admin/departments/edit';
        
        $this->load->view('admin/structure',$this->data);
    }
    
    
            function delete($id)
    {
        $this->load->model('departments_model');                   
        if(is_numeric($id))
        {
            $this->departments_model->delete($id);

            $this->session->set_flashdata('message', 'Department has been deleted');
            
            redirect('admin/departments');
        }
    }
    
    
    
    function save($department_id=0)
    {
        $this->load->model('departments_model');
        $data = array();
        if(isset($_POST['name'])) $data['name'] = $_POST['name'];
        if(isset($_POST['initial'])) $data['initial'] = $_POST['initial'];
         if(isset($_POST['phone'])) $data['phone'] = $_POST['phone'];
         if(isset($_POST['director_name'])) $data['director_name'] =  $_POST['director_name'];
         if(isset($_POST['parent_school'])) $data['parent_school'] =  $_POST['parent_school'];
        if(isset($_POST['location'])) $data['location'] = $_POST['location'];
         
         
        /*$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');

        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->edit($course_id);
        }
        else 
        {*/
        
            $this->departments_model->save($data, $department_id);   

            redirect('/admin/departments/index');
        //}
        
    }
}