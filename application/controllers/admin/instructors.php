<?php
class Instructors extends CI_Controller {

     function __construct()
     {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();
        
        $this->data['sel']='instructors_admin';
        $this->data['display_menu']='yes';
        $this->load->model(array('departments_model', 'general_model', 'courses_model', 'instructor_model'));
        $this->data['departments'] = $this->departments_model->getAllDepartments();
    }
    
    function index()
    {
         
        $this->data['message']=$this->session->flashdata('message');
        
        $this->data['instructors'] = $this->instructor_model->get_all_instructors();
        
          $this->data['body']='admin/instructors/list';
        
        $this->load->view('admin/structure',$this->data);
    }
    
    function edit($instructor_id=0)
    {
        if ($instructor_id!=0){
                   
            $this->data['instructor'] = $this->instructor_model->get_instructor($instructor_id);

            $this->data['edit']='edit';  

        }else{
                    $this->data['edit']='new';
        }

        $this->data['body']='admin/instructors/edit';
        
        $this->load->view('admin/structure',$this->data);
    }
    function activate($instructor_id=0)
    {
        $this->instructor_model->activate_instructor($instructor_id);
        redirect('/admin/instructors/index');
    }
    function deactivate($instructor_id=0)
    {
        $this->instructor_model->deactivate_instructor($instructor_id);
        redirect('/admin/instructors/index');
    }
    function save($instructor_id=0)
    {  
        $data = array();
        if(isset($_POST['first_name'])) $data['first_name'] = $_POST['first_name'];
        if(isset($_POST['last_name'])) $data['last_name'] = $_POST['last_name'];
         if(isset($_POST['initial'])) $data['initial'] = $_POST['initial'];
         if(isset($_POST['department_id'])) $data['department_id'] =  $_POST['department_id'];
         //if(isset($_POST['email'])) $data['email'] =  $_POST['email'];
    
        /*$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');

        $this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');

        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->edit($course_id);
        }
        else 
        {*/
        
            $this->instructor_model->save($data, $instructor_id);   

            redirect('/admin/instructors/index');
        //}
        
    }
     function delete($id)
    {        
        if(is_numeric($id))
        {
            $this->instructor_model->delete_instructor($id);

            $this->session->set_flashdata('message', 'Instructor has been deleted');
            
            redirect('admin/instructors');
        }
    }
     function ajx_load_instructors()
    {
        if (isAjax()) 
        {           
            $department_id=$this->input->post('department_id');
            if($department_id>0)
            {
                $instructors=$this->instructor_model->get_department_instructors($department_id);
            }
            else 
            {
                $instructors=$this->instructor_model->get_all_instructors();
            }               
            
            $n = 1;
            foreach ($instructors as $instructor) {
            
                echo "<tr>
                <td>" . $n++ . '</td><td>' . $instructor['initial'] . "</td><td>" . $instructor['first_name'] . ' ' . $instructor['last_name'] . "</td><td></td></tr>";
        
                } 
            } else {
                show_error('Sorry, direct access is not allowed');
            }
        
    }      
    
    
}