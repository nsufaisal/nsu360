<?php
class Room extends CI_Controller {

     function __construct()
     {
        parent::__construct();
        if ($this->session->userdata('admin_id') == FALSE)
            redirect('admin/login');
        $this->header_data = array('system_message' => $this->session->flashdata('message'));
        $this->load->library('form_validation');
        $this->data = array();
        
        $this->data['sel']='rooms_admin';
        $this->data['display_menu']='yes';
         $this->load->model(array('rooms_model', 'general_model', 'departments_model'));
         $this->data['departments'] = $this->departments_model->getAllDepartments();
        $this->data['rooms'] = $this->rooms_model->getAllRooms();
            
    }
    
    function index()
    {
        $this->data['room_types'] = $this->rooms_model->getAllRoomType();
                
        $this->data['body']='admin/rooms/list';
        
        $this->data['message']=$this->session->flashdata('message');
        
        $this->load->view('admin/structure',$this->data);
    }
    function delete($id)
    {            
        if(is_numeric($id))
        {
            $this->rooms_model->delete($id);

            $this->session->set_flashdata('message', 'Department has been deleted');
            
            redirect('admin/room');
        }
    }
        function edit($room_id = 0) {
            $this->data['room_types'] = $this->rooms_model->getAllRoomTypes();
        if ($room_id != 0) {

            $this->data['MYroom'] = $this->rooms_model->get_my_room($room_id);

            $this->data['edit'] = 'edit';
        } else {
            $this->data['edit'] = 'new';
        }
        $this->data['body'] = 'admin/rooms/edit';

        $this->load->view('admin/structure', $this->data);
    }
        function save($room_id = 0) {
        $this->load->model('rooms_model');
        $data = array();
        if (isset($_POST['initial']))
            $data['initial'] = $_POST['initial'];
        if (isset($_POST['type_id']))
            $data['type_id'] = $_POST['type_id'];
        if (isset($_POST['description']))
            $data['description'] = $_POST['description'];
        if (isset($_POST['department_id']))
            $data['department_id'] = $_POST['department_id'];

        if ($room_id == 0) {
            $this->form_validation->set_rules('initial', 'Initial', 'trim|required|is_unique[room.initial]|xss_clean');
        }
          $this->form_validation->set_rules('type_id', 'Type', 'trim|required|xss_clean');

        $this->form_validation->set_rules('department_id', 'Department Name', 'trim|required|xss_clean');

        $this->form_validation->set_error_delimiters('<span class="error" style="color:red;">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->edit($room_id);
        } else {

            $this->rooms_model->save($data, $room_id);

            redirect('/admin/room/index');
        }
    }
}