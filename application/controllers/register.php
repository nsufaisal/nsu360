<?php
class Register extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();

    // Your own constructor code

    $this->data = array();  

    $this->data['sel'] = 'register';
	
	$this->load->model('members_model');
	
	$this->load->model('outbound_email_model');
	
  }

  function index()
  {  		
		$this->data['meta']  = getMetaContent('join_4_free');
	
		$this->data['content'] = $this->data['meta']['data'];
		
	    $this->data['body']='front/register';
		
		$this->data['security_qs']=$this->members_model->getSecurityQuestions();
	
	    $this->load->view('front/structure',$this->data);
    
  }
  
  function check(){
	  		
	  	$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[members.username]|xss_clean');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|xss_clean');
		
		$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|required|matches[password]|xss_clean');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[members.email]|xss_clean');
		
		$this->form_validation->set_rules('security_ans1', 'Security Ans 1', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('security_ans2', 'Security Ans 2', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('security_ans3', 'Security Ans 3', 'trim|required|xss_clean');	
		
		$this->form_validation->set_error_delimiters('<span class="rtn-error">', '</span>');
	
		if($this->form_validation->run()==FALSE) {
			
			$this->index();				
			
		}
		else{
			$member_id = $this->members_model->Save();
			
			$first_name=$this->input->post('first_name');
			
			$last_name=$this->input->post('last_name');
			
			$email=$this->input->post('email');
			
			$result =$this->outbound_email_model->get('User_confirmation');			//Model Function calling for get email content
			
			$message= $result['content'];
			
			$message=str_replace('{first_name}',$first_name,$message);
			
			$message=str_replace('{last_name}',$last_name,$message);
			
			$message=str_replace('{email}',$email,$message);
	
			$this->email->from($result['from'], $result['from_name']);
			
			$this->email->to($email);
			
			$this->email->subject($result['subject']);
			
			$this->email->message($message);
			
			$this->email->send();			
				
			redirect('/register/confirmation');
		}
  	
  }
  function confirmation()
  {  	 
    $this->data['meta']  = getMetaContent('join_4_free_confrimation');
	
	$this->data['content'] = $this->data['meta']['data'];

	$this->session->set_flashdata('slider_content',  $this->load->view('front/register_confrimation',$this->data,TRUE));
	
	redirect();
  }
 
}

?>