<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Noticewall extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
			
        $this->data = array();

        $this->data['sel']='noticewall';
		
		$this->load->model(array('courses_model','general_model', 'noticewall_model'));

    }

/*
|-----------------------------------------------------------------------------
| Edit form of a student
|-----------------------------------------------------------------------------
*/	

    function index()
	{	
		$semester_id = $this->session->userdata('semester_id');
		
		$this->data['meta'] = getMetaContent('noticewall_public');		

        $this->data['content'] = $this->data['meta']['data'];

		$this->data['public_notices']=$this->noticewall_model->get_public_notices($semester_id);
		
		$this->data['body'] = 'front/noticewall';

        $this->load->view('front/structure', $this->data);		
    }

/*
|-----------------------------------------------------------------------------
| View an individual Public Notice
|-----------------------------------------------------------------------------
*/	

    function view($noticewall_id=0) 
	{
		if($this->noticewall_model->is_public_post($noticewall_id))
		{
			$this->data['notice_post']=$this->noticewall_model->get_notice_post($noticewall_id);
			
			$this->data['notice_courses']=$this->noticewall_model->get_notice_courses($noticewall_id);
			
			$this->data['meta'] = getMetaContent('noticewall_public');
			
			$this->data['notice_id']=$noticewall_id;

	        $this->data['content'] = $this->data['meta']['data'];	
		
			$this->data['body'] = 'front/noticewall_view';
	
	        $this->load->view('front/structure', $this->data);				
		}
		else {
			redirect('/home/');
		}	
    }

}
