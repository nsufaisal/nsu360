<?php
class Terms extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();

    // Your own constructor code

    $this->data = array();  

    
  }

  function index()
  {
  	$this->data['sel'] = 'terms';
	
    $this->data['meta']  = getMetaContent('terms');
	
	$this->data['content'] = $this->data['meta']['data'];
    
    $this->data['body']='front/terms';

    $this->load->view('front/structure',$this->data);
  }
  
  function agreed()
  {
  	$this->session->set_userdata('usage_terms', TRUE);		
  }
  

 }

?>