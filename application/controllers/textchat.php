<?php
class Textchat extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'textchat';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('textchat','meta');
		
		$this->data['body']='front/textchat';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function search_filter()
	{
		$this->data['meta']  = getMetaContent('textchat_search_filter','meta');
	
		$this->data['body']='front/textchat_search_filter';
	
		$this->load->view('front/structure',$this->data);
	}

}

?>