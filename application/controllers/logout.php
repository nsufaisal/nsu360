<?php
class Logout extends CI_Controller {

   function __construct()
   {
		parent::__construct();

		$this->data = array();
   }

   function index(){

		$this->session->unset_userdata(array('admin_id'=>''));

		$this->session->sess_destroy();

		redirect('/home');
   }

   function log_out(){

		$this->session->unset_userdata(array('member_id'=>''));

		$this->session->sess_destroy();

		$this->session->set_flashdata('message', 'You have been logged out.');

		redirect('front/logout');

   }

}
?>
