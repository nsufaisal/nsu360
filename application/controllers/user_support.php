<?php
class User_support extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();  
		
		$this->data['sel'] = 'customer_services';
		
		$this->load->model('contacts_model');
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('customer_services','meta');
		
		$technical  = getMetaContent('customer_services_technical_support','data');
		
		$this->data['technical_content'] = $technical['data'];
		
		$feedback  = getMetaContent('customer_services_feedback','data');
		
		$this->data['feedback_content'] = $feedback['data'];
		
		$this->data['body']='front/user_support';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function technical_support()
	{
		$this->data['meta']  = getMetaContent('technical_support');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/technical_support';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| Send a message to admin contacts and show confirmation msg
|-----------------------------------------------------------------------------
*/	
	function technical_support_confirmation()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|xss_clean');
		
		$this->form_validation->set_rules('phone','Phone', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('message', 'Message','trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if($this->form_validation->run() ==FALSE)
		{
			$this->technical_support();
		}

		else {
			
			$name=$this->input->post('name');
			
			$words = explode(' ', $name);
			
			$last_name = array_pop($words);
			
			$first_name = implode(' ', $words);
			
			$contact = $this->contacts_model->getContectByEmail($this->input->post('email'));
			
			$reply=0;
			
			if(count($contact) > 0)
			{
				$reply =$contact['contact_id'];				
			}	
						
			$msg_data=array(
			
				'first_name'=>$first_name,
				
				'last_name'=>$last_name,
				
				'email'=>$this->input->post('email'),
				
				'phone'=>$this->input->post('phone'),
				
				'subject'=>'Technical Support - '.$this->input->post('subject'),
				
				'message'=>$this->input->post('message'),	
				
				'date'=>date('Y-m-d H:i:s'),
				
				'reply'=>$reply,
				
				'type'=>1,			
			);
			
			$this->contacts_model->saveMessage($msg_data);
			
			$this->data['meta']  = getMetaContent('technical_support');
						
			$this->data['content'] = $this->data['meta']['data'];
			
			$meta  = getMetaContent('technical_support_confirmation','data');
			
			$this->data['content_1'] = $meta['data'];
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/technical_support_confirmation',$this->data,true));
		
		    redirect('/customer_services/technical_support/');
			
		}

	}
	
	function feedback_support()
	{
		$this->data['meta']  = getMetaContent('feedback_support');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/feedback_support';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function feedback_support_confirmation()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|xss_clean');
		
		$this->form_validation->set_rules('phone','Phone', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('message', 'Message','trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if($this->form_validation->run() ==FALSE)
		{
			$this->technical_support();
		}

		else 
		{
			
			$name=$this->input->post('name');
			
			$words = explode(' ', $name);
			
			$last_name = array_pop($words);
			
			$first_name = implode(' ', $words);
			
			$contact = $this->contacts_model->getContectByEmail($this->input->post('email'));
			
			$reply=0;
			
			if(count($contact) > 0)
			{
				$reply =$contact['contact_id'];				
			}	
						
			$msg_data=array(
			
				'first_name'=>$first_name,
				
				'last_name'=>$last_name,
				
				'email'=>$this->input->post('email'),
				
				'phone'=>$this->input->post('phone'),
				
				'subject'=>'Feedback - '.$this->input->post('subject'),
				
				'message'=>$this->input->post('message'),	
				
				'date'=>date('Y-m-d H:i:s'),
				
				'reply'=>$reply,
				
				'type'=>2,			
			);
			
			$this->contacts_model->saveMessage($msg_data);
			
			$this->data['meta']  = getMetaContent('feedback_support');
			
			$this->data['content'] = $this->data['meta']['data'];
			
			$meta  = getMetaContent('feedback_support_confirmation','data');
			
			$this->data['content_1'] = $meta['data'];
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/feedback_support_confirmation',$this->data,true));
		
		    redirect('/customer_services/feedback_support/');
		
		}
	}
	
	function billing_support()
	{
		$this->data['meta']  = getMetaContent('billing_support');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/billing_support';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function billing_support_confirmation()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|xss_clean');
		
		$this->form_validation->set_rules('phone','Phone', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('message', 'Message','trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if($this->form_validation->run() ==FALSE)
		{
			$this->technical_support();
		}

		else 
		{			
			$name=$this->input->post('name');
			
			$words = explode(' ', $name);
			
			$last_name = array_pop($words);
			
			$first_name = implode(' ', $words);
			
			$contact = $this->contacts_model->getContectByEmail($this->input->post('email'));
			
			$reply=0;
			
			if(count($contact) > 0)
			{
				$reply =$contact['contact_id'];				
			}	
						
			$msg_data=array(
			
				'first_name'=>$first_name,
				
				'last_name'=>$last_name,
				
				'email'=>$this->input->post('email'),
				
				'phone'=>$this->input->post('phone'),
				
				'subject'=>'Billing Support - '.$this->input->post('subject'),
				
				'message'=>$this->input->post('message'),	
				
				'date'=>date('Y-m-d H:i:s'),
				
				'reply'=>$reply,
				
				'type'=>3,
							
			);
			
			$this->contacts_model->saveMessage($msg_data);
			
			$this->data['meta']  = getMetaContent('billing_support');
			
			$this->data['content'] = $this->data['meta']['data'];
			
			$meta  = getMetaContent('billing_support_confirmation','data');
			
			$this->data['content_1'] = $meta['data'];
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/billing_support_confirmation',$this->data,true));
		
		    redirect('/customer_services/billing_support/');
		
		}
	}
}

?>