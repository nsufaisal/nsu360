<?php
 if ( ! defined('BASEPATH')) 
 	exit('No direct script access allowed');
class Home extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();  
		
		$this->data['sel'] = 'home';
		
	}

	function index()
	{
		$this->data['meta']  = getMetaContent('home','meta');
		
		$this->data['body']='front/home';
		
		$this->load->model('noticewall_model');
		
		$this->data['course_notice']=$this->noticewall_model->get_public_course_notice(8);
		
		$this->load->view('front/structure',$this->data);
	}
	
	function splash()
	{
		$meta  = getMetaContent('splash_18_up','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->load->view('front/splash_18_up',$this->data);
	}
	
	function search_filter()
	{
		$this->data['meta']  = getMetaContent('livecams_search_filter','meta');
	
		$this->data['body']='front/search_filter';
	
		$this->load->view('front/structure',$this->data);
	}
	function list_view()
	{ 
		$this->data['meta']  = getMetaContent('home_list_view','meta');
		
		$this->data['body']='front/home_list_view';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function tipme($member_id='', $amount='')
	{
		$this->data['meta']  = getMetaContent('home','meta');
		
		$this->data['body']='front/home';
		
		$this->load->view('front/structure',$this->data);
	}
    
    function order_software()
    {
        
        $this->load->model('contacts_model');
        
        
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|xss_clean|required|valid_email');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('institution_name', 'Instituion Name', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
        if ($this->form_validation->run() != FALSE)
        {
                $post=$this->input->post();
                
                $order_data=array(
            
                'first_name'=>$post['first_name'],
                
                'last_name'=>$post['last_name'],
                
                'phone'=>$post['mobile'],
                
                'email'=>$post['user_email'],
                
                'institution_name'=>$post['institution_name'],
                
                'type'=>'3',
                );
        }
        else {
            redirect('/home/');
        }
          if($this->contacts_model->add_new_contact($order_data)){
        
            $this->data['meta']  = getMetaContent('home','meta');
    
            $this->session->set_flashdata('slider_content', $this->load->view('order_confirmation',$this->data,true));
    
            redirect('/home/');
        }
    }

}

?>