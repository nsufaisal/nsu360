<?php
class Content_Store extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'content_store';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('content_store','meta');
		
		$this->data['body']='front/store/content_store';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function detail()
	{
		$this->data['meta']  = getMetaContent('content_store_detail','meta');
		
		$this->data['body']='front/store/content_store_detail';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function checkout()
	{
		$this->data['meta']  = getMetaContent('content_store_checkout','meta');
		
		$this->data['body']='front/store/checkout';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function checkout_confirmation()
	{
		$this->data['meta']  = getMetaContent('content_store_checkout','meta');
		
		$meta  = getMetaContent('content_store_checkout_confirmation','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->data['body']='front/store/checkout_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function payment()
	{
		$this->data['meta']  = getMetaContent('content_store_payment','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/store/payment',$this->data);
	}
	
	function payment_confirmation()
	{
		$this->data['meta']  = getMetaContent('content_store_detail','meta');
		
		$meta  = getMetaContent('content_store_payment_confirmation','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->data['body']='front/store/payment_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}

}

?>