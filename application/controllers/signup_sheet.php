<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class Noticewall extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
			
        $this->data = array();

        $this->data['sel']='signup_sheet';
		
		$this->load->model(array('courses_model','general_model'));

    }

/*
|-----------------------------------------------------------------------------
| View list of signup sheets
|-----------------------------------------------------------------------------
*/	

    function index()
	{	
		$semester_id = $this->session->userdata('semester_id');
		
		$this->data['meta'] = getMetaContent('noticewall_public');		

        $this->data['content'] = $this->data['meta']['data'];

		$this->data['signup_sheet']=$this->noticewall_model->get_signup_sheets($semester_id);
		
		$this->data['body'] = 'front/signup_sheet/all';

        $this->load->view('front/structure', $this->data);		
    }



}
