<?php
class Compliance extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();

    // Your own constructor code

    $this->data = array();  

    $this->data['sel'] = 'compliance';
	//
  }

  function index()
  { 
    $this->data['meta']  = getMetaContent('2257_compliance');
	
	$this->data['content'] = $this->data['meta']['data'];

    $this->data['body']='front/compliance';

    $this->load->view('front/structure',$this->data);
  }
  

 }

?>