<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Myaccount extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		if ($this->session->userdata('user_type')!='IN')
		{
			redirect(site_url());
		}
		
		$this->data = array();  
		
		$this->data['sel'] = 'myaccount';
		
		$this->load->model(array('instructor_model', 'general_model'));
		
		$this->load->model('members_model');
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('instructor_myaccount','meta');
		
		$member_id=$this->session->userdata('member_id');

		$this->data['member_id']=$member_id;
		
		$this->data['my_info']=$this->instructor_model->get_instructor($member_id);
		
		$this->data['body']='front/instructor/myaccount';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function edit()
	{
		$member_id=$this->session->userdata('member_id');
		 
		$this->data['meta']  = getMetaContent('studio_myaccount_edit','meta');

		$this->load->model('departments_model');

		$this->data['departments'] = $this->departments_model->getAllDepartments();
		
		$this->data['my_info']=$this->instructor_model->get_instructor($member_id);
		
		$this->data['body']='front/instructor/myaccount_edit';
		
		$this->load->view('front/structure',$this->data);
	}

 /*
|-----------------------------------------------------------------------------
| Save/Update an instructor's information
|-----------------------------------------------------------------------------
*/	
	function Save($member_id=0)
	{
		$member_id=$this->session->userdata('member_id');
		
	  	$this->form_validation->set_rules('email', 'Email Address', 'trim|xss_clean|required|valid_email|callback_new_unique_email');

		if($this->input->post('password', TRUE)!='' || $this->input->post('new_password', TRUE)!='')	
		{
			$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required|callback_check_old_pass');
			
			$this->form_validation->set_rules('new_password', 'Change Password', 'trim|xss_clean|required');

			$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|xss_clean|required|matches[new_password]');
		}

		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

	  	if ($this->form_validation->run() != FALSE)
		{
			$post=$this->input->post();
			
			$member_data=array(
				
				'phone'=>$post['phone'],
				
				'email'=>$post['email'],
				
				'username'=>$post['inst_school_id'],				
			);
			
			if ($this->input->post('new_password')!='') 
			{
			  $member_data['password'] = md5($post['new_password']);
			} 			
						
			if($this->members_model->update_member($member_id,$member_data))
			{
				$inst_data=array(
				
					'first_name'=>$post['first_name'],
				
					'last_name'=>$post['last_name'],
				
					'department_id'=>$post['department_id'],
					
					'initial'=>$post['instructor_initial'],
				
					'blood_group'=>$post['blood_group'],
				
					'gender'=>$post['gender'],
				
					'curr_address'=>$post['curr_address'],	
					
					'inst_school_id'=>$post['inst_school_id'],			
				);
				
				$this->instructor_model->update_instructor($member_id, $inst_data);
				
				$meta = getMetaContent('instructor_myaccount_update_confirm', 'data');
				
				$this->data['flash_message'] = "Your account has been updated successfully";
				
				$this->data['title']="Account Update Confirmation";
									
				$this->session->set_flashdata('slider_content',$this->load->view('front/confirmation_common',$this->data,TRUE));									
					
				redirect('/instructor/myaccount/');
			}
		}		
		else{
			$this->edit();
		}
	}	


/*
|-----------------------------------------------------------------------------
| Change profile photo
|-----------------------------------------------------------------------------
*/	
	function change_photo()
	{
		$member_id = $this->session->userdata('member_id');
		 
		$this->data['member_id']=$member_id;
				
		$this->data['meta']  = getMetaContent('member_myaccount','meta');
		
		$this->data['my_info']=$this->instructor_model->get_instructor($member_id);
		
		$this->data['body']='front/instructor/myaccount_change_photo';
		
		$this->load->view('front/structure',$this->data);		
	}
	
/*
|-----------------------------------------------------------------------------
| Save Change profile photo
|-----------------------------------------------------------------------------
*/	
	function save_photo_confirmation()
	{
		$member_id = $this->session->userdata('member_id');
		$valid_exts = array('jpeg', 'jpg');
		$max_file_size = 500 * 1024; #200kb
		$nw = 300;
		$nh = 350; # image with # height
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if ( isset($_FILES['image']) ) {
				if (! $_FILES['image']['error'] && $_FILES['image']['size'] < $max_file_size) {
					$ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
					if (in_array($ext, $valid_exts)) {
						
						$filename=uniqid() . '.' . $ext;
						
						$dir='./uploads/instructor/'.$member_id.'/';
			
						if (!is_dir($dir))
				        {
				            mkdir($dir, 0755, true);
	        			}
							$path = $dir . $filename;
							$size = getimagesize($_FILES['image']['tmp_name']);
							
							$adjust=(double) $size[0]/300;
		
							$x = (int) $_POST['x']*$adjust;
							$y = (int) $_POST['y']*$adjust;
							$w = (int) $_POST['w'] ? $_POST['w']*$adjust : $size[0];
							$h = (int) $_POST['h'] ? $_POST['h']*$adjust : $size[1];
		
							$data = file_get_contents($_FILES['image']['tmp_name']);
							$vImg = imagecreatefromstring($data);
							$dstImg = imagecreatetruecolor($nw, $nh);
							imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
							imagejpeg($dstImg, $path);
							imagedestroy($dstImg);
							
							if($this->instructor_model->change_photo($member_id,$filename))
							{
								$meta = getMetaContent('student_signupconfirmcontent', 'data');
			        
						        $this->data['content'] = $meta['data'];
				
								$this->session->set_flashdata('slider_content', $this->load->view('front/instructor/myaccount_change_photo_confirmation',$this->data,true));
								
								redirect('/instructor/myaccount/');
							}
		
						} else {
							//echo 'unknown problem!';
							$this->session->set_flashdata('slider_content', "<h2>Oopse! May be you are trying to upload file other than jpg/jpeg extension!</h2>");
							redirect('/instructor/myaccount/change_photo/');
						} 
				} else {
					//echo 'file is too small or large';
					$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You forgot to select a photo or your file size is too large for upload (file limit is 500KB)!</h2>");
					redirect('/instructor/myaccount/change_photo/');
				}
			} else {
				//echo 'file not set';
				$this->session->set_flashdata('slider_content', "<h2>Aah! File not set!</h2>");
				redirect('/instructor/myaccount/change_photo/');
			}
		} else {
			//echo 'bad request!';
			$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You made a very bad request!</h2>");
			redirect('/instructor/myaccount/change_photo/');
		}
	}	



//////////////////////////useful till up////////////////////////////////////////////




 
 /*
|-----------------------------------------------------------------------------
| Do update the Studio based on provided info and show confirmation
|-----------------------------------------------------------------------------
*/	
	function update()
	{
		$member_id=$this->session->userdata('member_id');
		
		$post=$this->input->post();
		
		//var_dump($post);
		
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|xss_clean|required');
			
		if($this->input->post('password', TRUE)!='')	
		{
			$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required|callback_check_old_pass');
			
			//$this->form_validation->set_rules('new_password', 'Change Password', 'trim|xss_clean|required');

			//$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|xss_clean|required|matches[new_password]');
		}
		if($this->input->post('new_password', TRUE)!='')
		{
			$this->form_validation->set_rules('new_password', 'Change Password', 'trim|xss_clean|required');

			$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|xss_clean|required|matches[new_password]');
		
			
		}
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

		if ($this->form_validation->run() != FALSE)
		{
			$password=$post['password'];
			
			$new_password=$post['new_password'];
			
			if($new_password=='')
			{
				$change_password=$password;
				
			}
			else 
			{
				$change_password=$new_password;
			}
			

				$member_data=array(
				
				'first_name'=>$post['first_name'],
				
				'last_name'=>$post['last_name'],
				
				'email'=>$post['email'],
				
				'phone'=>$post['phone'],
				
				'nid'=>$post['nid'],
				
				'password'=>md5($change_password)
				
				);
				
				$is_update=$this->members_model->update($member_id,$member_data);
				
				if($is_update)
				{
					redirect('sadmin/myaccount/confirmation');
					
				}
				else 
				{
					echo "Failed to Edit account";
					
					redirect('sadmin/myaccount');
				}

				
		}		
		else{
			$this->edit();
		}

	}

/*
|-----------------------------------------------------------------------------
| Displays studio's myaccount update confirmation 
|-----------------------------------------------------------------------------
*/
	
	function confirmation()
	{ 
		$this->data['meta']  = getMetaContent('studio_myaccount_edit_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/studio/myaccount_edit_confirmation',$this->data,true));
	
	    redirect('/sadmin/myaccount');
	}
		
	function upgrade()
	{ 
		$this->data['meta']  = getMetaContent('member_upgrade_account');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/upgrade_account';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|-----------------------------------------------------------------------------
| validation call back: check whether member enters existing password correct
|-----------------------------------------------------------------------------
*/
	function check_old_pass($password)
	{
		$member_id=$this->session->userdata('member_id');
		
		$result=$this->members_model->CheckOldPassword($member_id, $password);
		
		if(count($result)==0)
		{
			$this->form_validation->set_message('check_old_pass', "Old password is not correct");

			return FALSE;	
			
		}
		else
		{
			return TRUE;
		}

	}

/*
|--------------------------------------------------------------------------
| password check in the ajax call for validation 
|--------------------------------------------------------------------------
*/	
	function ajx_check_password()
	{
		$pass=$this->input->post('password');
		
		$new_pass=$this->input->post('new_password');
		
		$conf_pass=$this->input->post('conf_password');
		
		$flag=FALSE;
		
		if(strlen($new_pass)>1)
		{
			if(strlen($pass)>1)
			{
				$email=$this->session->userdata['email'];
				
				$result=$this->members_model->CheckAuthentication($email, $pass);
				
				if(count($result)!=0){
					
					$flag=TRUE;
				}
			}
		}
		else {
			$flag=TRUE;
		}
		echo $flag;
	}
        
        
        
        
        
}

?>