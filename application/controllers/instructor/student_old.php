<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Student extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
		
		if ($this->session->userdata('user_type')!='IN')
		{
			redirect(site_url());
		}
		
		$member_id = $this->session->userdata('member_id');
				
        $this->data = array();

        $this->data['sel'] = 'my_students';
		
		$this->load->model(array('students_model','courses_model','general_model'));

    }

    function index() 
	{
		$selected_course=0;
			
		$selected_semester=0;
		
		$member_id=$this->session->userdata('member_id');
		
		$post=$this->input->post();
		
		if(isset($post["courses"]))
			$selected_course = $post["my_courses"];
		
		if(isset($post["semesters"]))
				$selected_semester=$post["semesters"];	
			
		$this->data['course_id']=$selected_course;
		
		$this->data['semester_id']=$selected_semester;
		
//		$this->data['my_semesters']=$this->courses_model->getMySemesters($member_id);
		
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
	//	$this->data['my_students']=$this->students_model->get_my_students($member_id, $selected_course, $selected_semester);
				
		$this->data['body'] = 'front/instructor/mystudents';

        $this->load->view('front/structure', $this->data);		
    }
	

/*
|-----------------------------------------------------------------------------
| Edit form of a student
|-----------------------------------------------------------------------------
*/	
    function edit($student_id=0) 
    {	
        $this->data['meta'] = getMetaContent('studio_add_model');
		
        $this->data['content'] = $this->data['meta']['data'];
		
		if($student_id>0)
		{
			$this->data['student']=$this->students_model->GetById($student_id);
			
			$this->data['mode']='edit';		
			
		}
		else 
			
		$this->data['mode']='new';	
		
		$this->data['departments'] = $this->general_model->get_record('departments','result',array('is_active'=>1),'initial');

		$curr_semester = $this->general_model->get_record('settings','row',array('name'=>'curr_semester_id'));
		
		$this->data['courses'] = $this->courses_model->get_semester_courses($curr_semester['value']);
	
        $this->data['body'] = 'front/instructor/mystudent_edit';
		
        $this->load->view('front/structure', $this->data);
    }
	

/*
|-----------------------------------------------------------------------------
| Save a student to students database
|-----------------------------------------------------------------------------
*/	
	function Save($student_id=0) 
    {
		$post=$this->input->post();
		if($student_id==0)
		{
			$this->form_validation->set_rules('email', 'Email', 'trim|is_unique[members.email]|xss_clean');
			
			$mode=0;
		}
		else {
			$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean');
			
			$mode=1;
		}

	  	if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}
		else 
		{

			$this->load->helper(array('form', 'url'));
			$this->load->library('upload'); 
							
			$photo_file='';	
			if (!empty($_FILES['userfile_1']['name']))
	        {
	        	$dir='./uploads/student/photo/';
	        	if(!is_dir($dir))
                	mkdir($dir, 0755, true);
	            $config['upload_path'] = $dir;
	            $config['allowed_types'] = 'gif|jpg|png|jpeg';
	            $config['max_size'] = '2048';
				$config['encrypt_name']=TRUE;
	            $this->upload->initialize($config);			 
	           
	            if ($this->upload->do_upload('userfile_1'))
	            {
	                $upload_data_1 = $this->upload->data();
					$photo_file=$upload_data_1['file_name'];
	            }
	        }
			else {
				$photo_file=$post['file_name_box_1'];
			}
			
			
			
			
			$username=$post['username'];
			
			//if username field is blank then student_id would be used as default username
			if($post['username']=='')
				$username=$post['student_id'];
			
			$password=md5($post['password']);			
			//if password field is blank then default password would be 123456
			if($post['password']=='')
				$password=md5('123456');				
			
			$member_data=array(
				
			
				'username'=>$username,
				
				'password'=>$password,
				
				'type'=>'S',
				
				'email'=>$post['email'],
				
				'first_name'=>$post['first_name'],
				
				'last_name'=>$post['last_name'],	
			);
			
			if($student_id==0)
					$member_id= $this->members_model->AddMember($member_data,0); // add a member
			else {
					$member_id=$this->students_model->GetMemberId($student_id);
					$member_id= $this->members_model->AddMember($member_data,$member_id);//edit member
					
				}
			
				
			/*---------------------------member insert complete now student entry------------*/
		
			$day=$post['day'];
			
			$month=$post['month'];
			
			$year=$post['year'];
			
			$birthday = $year.'-'.$month.'-'.$day;
			
			$student_data=array(
				
				'member_id'=>$member_id,
				
				'std_school_id'=>$post['std_school_id'],
				
				'last_name'=>$post['last_name'],
				
				'first_name'=>$post['first_name'],
				
				'email'=>$post['email'],
				
				'username'=>$post['username'],
				
				'birthday'=>$birthday,
				
				'gender'=>$post['gender'],
				
				'blood_group'=>$post['blood_group'],
				
				'curr_address'=>$post['curr_address'],
				
				'perm_address'=>$post['perm_address'],
				
				'admission_year'=>$post['admission_year'],
				
				'admission_class'=>$post['admission_class'],
				
				'curr_class'=>$post['curr_class'],
				
				'curr_section'=>$post['curr_section'],
				
				'shift'=>$post['shift'],
				
				'transport'=>$post['transport'],				
				
				'bus_stand'=>$post['bus_stand'],
				
				'father_name'=>$post['father_name'],
				
				'father_occupation'=>$post['father_occupation'],
				
				'father_phone'=>$post['father_phone'],
				
				'mother_name'=>$post['mother_name'],
				
				'mother_occupation'=>$post['mother_occupation'],
				
				'mother_phone'=>$post['mother_phone'],
				
				'guardian_name'=>$post['guardian_name'],
				
				'guardian_relation'=>$post['guardian_relation'],
				
				'guardian_address'=>$post['guardian_address'],
				
				'guardian_phone'=>$post['guardian_phone'],
				
				'student_photo'=>$photo_file,
				
				'added_by'=>$this->session->userdata('member_id'),				
			);
			
			$student_id=$this->students_model->Save($student_data,$student_id);
			
			if($student_id)
			{
				if($mode==0)
					redirect('sadmin/mystudents/add_confirmation');
				else
					redirect('sadmin/mystudents/edit_confirmation');
				
			} 
			else {
				
				if($mode==0)
					echo "Failed to add a student";
				else
					echo "Failed to edit a student";
			}
			
		} //end else 
	
	}

/*
|-----------------------------------------------------------------------------
| Show Confirmation message after adding a new students
|-----------------------------------------------------------------------------
*/	
	function add_confirmation() {
	    	$this->data['meta']  = getMetaContent('studio_add_modelconfirmation');
			
			$this->data['content'] = $this->data['meta']['data'];
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/sadmin/add_student_confirmation',$this->data,true));
		
		    redirect('/sadmin/mystudents/');
    	}

    function edit_confirmation() {

    	$this->data['meta']  = getMetaContent('studio_edit_model');
		
		$this->data['confirmation_content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/sadmin/edit_student_confirmation',$this->data,true));
	
	    redirect('/sadmin/mystudents');
    }

/*
|-----------------------------------------------------------------------------
| Admin can view a student details
|-----------------------------------------------------------------------------
*/	
    function view($id) {
			
		$member_id=$this->session->userdata('member_id');
		
		if(is_numeric($id))
		{
			$this->data['meta'] = getMetaContent('studio_view_model');

	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['student']=$this->students_model->GetById($id);
			
	        $this->data['body'] = 'front/sadmin/mystudent_view';
	
	        $this->load->view('front/structure', $this->data);
		}				
		
		else {
			redirect('/sadmin/mystudents/');
		}  
    }

	function load_section($class_id)
	{
		echo "Loading sections";
		
		$this->data['classes']=$this->classes_model->getClasses();
		
		$this->data['sections']=$this->classes_model->getSections($class_id);
		
		$this->data['bus_stoppages']=$this->general_model->getAll('bus_stoppages','name');
			
        $this->data['meta'] = getMetaContent('studio_add_model');
		
        $this->data['content'] = $this->data['meta']['data'];
		
		//$this->data['mode']='new';

        //$this->data['body'] = 'front/sadmin/mystudent_add';

        //$this->load->view('front/structure', $this->data);
		
	}

/*
|-----------------------------------------------------------------------------
| Disable a student
|-----------------------------------------------------------------------------
*/	
    function remove($member_id) {
    	//echo "remove".$member_id;
		
        $this->data['meta'] = getMetaContent('studio_view_model');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['member_id']=$member_id;

        $this->load->view('front/sadmin/mystudent_remove', $this->data);
    }

    function remove_confirmation($member_id) {
    	
		//echo $member_id;
    	
		$this->members_model->delete($member_id);
		
		$this->students_model->delete($member_id) ;
		
		$this->data['meta']  = getMetaContent('studio_view_model');
		
		$this->data['confirmation_content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/sadmin/mystudent_remove_confirmation',$this->data,true));
	
	    redirect('/sadmin/mystudents');

    }
	
////////////////////////ACTIVELY USED ABOVE//////////////////////////////////////////

    function edit_model($member_id) { // member_id of the model
    	
		$studio_member_id=$this->session->userdata('member_id');
		
		$studio=$this->studios_model->GetStudio($studio_member_id);
			
    	$is_my_model=$this->models_model->IsMyModel($studio['studio_id'], $member_id);
		
		if(count($is_my_model)==0 )		
		{
			echo 'Sorry! You lack proper permission!! Plz go back to <a href="'.base_url('studio/mymodels').'">My Models Page</a>';
		}
		else 
		{
	        $this->data['meta'] = getMetaContent('studio_edit_model');
	
	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['model']=$this->models_model->GetModel($member_id);
			
	        $this->data['body'] = 'front/studio/edit_model';
	
	        $this->load->view('front/structure', $this->data);
        }
    }

/*
|---------------------------------------------------------------------------------
| Update a Model of a studio by the studio. On update redirect to confirm message
|---------------------------------------------------------------------------------
*/	
	function update($member_id)  // member_id of the model
	{
		$studio_member_id=$this->session->userdata('member_id');
		
		$studio=$this->studios_model->GetStudio($studio_member_id);
			
    	$is_my_model=$this->models_model->IsMyModel($studio['studio_id'], $member_id);
		
		if(count($is_my_model)==0 )		
		{
			echo 'Sorry! You lack proper permission!! Plz go back to <a href="'.base_url('studio/mymodels').'">My Models Page</a>';
		}
		else 
		{
			$this->form_validation->set_rules('last_name', 'Last name', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			
			if($this->input->post('conf_password', TRUE)!='' || $this->input->post('new_password', TRUE)!='')	
			{						
				$this->form_validation->set_rules('new_password', 'Change Password', 'trim|xss_clean|required');
	
				$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|xss_clean|required|matches[new_password]');
			}
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
			
			if ($this->form_validation->run() != FALSE)
			{			
				$this->models_model->update_studio_model($member_id);
					
				redirect('/studio/mymodels/edit_confirmation');	
	
			}		
			else{
				$this->edit_model($member_id);
			}
				
		} //end else is_my_model
		
		
		
	}

/*
|-----------------------------------------------------------------------------
| shows confirmation  of A model edit by her studio
|-----------------------------------------------------------------------------
*/	


/*
|-----------------------------------------------------------------------------
| Studio can view a model details of its own models only
|-----------------------------------------------------------------------------
*/	
    function view_model($model) {
			
		$member_id=$this->session->userdata('member_id');
		
		$studio=$this->studios_model->GetStudio($member_id);
			
    	$is_my_model=$this->models_model->IsMyModel($studio['studio_id'], $model);
		
		if(count($is_my_model)==0 )		
		{
			echo 'Sorry! You lack proper permission!! Plz go back to <a href="'.base_url('studio/mymodels').'">My Models Page</a>';
		}
		else 
		{
			$this->data['meta'] = getMetaContent('studio_view_model');

	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['model']=$this->models_model->GetModel($model);
			
			$this->data['country']=$this->members_model->GetCountry($model);
			
	        $this->data['body'] = 'front/studio/view_model';
	
	        $this->load->view('front/structure', $this->data);
		}
       
    }

    function remove_model($member_id) {
        $this->data['meta'] = getMetaContent('studio_view_model');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['member_id']=$member_id;

        $this->load->view('front/studio/remove_model', $this->data);
    }

    function remove_model_confirmation($member_id) {
    	
		$this->members_model->delete($member_id);
		
		$this->models_model->delete($member_id);
		
		$this->data['meta']  = getMetaContent('studio_view_model');
		
		$this->data['confirmation_content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/studio/remove_model_confirmation',$this->data,true));
	
	    redirect('/studio/mymodels');

    }
	
	function ajx_change_model_status()
	{
		$is_active = $this->input->post('is_active');
		
		$member_id=$this->input->post('member_id');
		
		$result = $this->models_model->change_model_status($member_id, $is_active);
		
		echo $result;
	}

}

?>