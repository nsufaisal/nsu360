<?php
class Message extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'message';
		
		$this->load->model('students_model');
		
		$this->load->model('classes_model');
		
		$this->load->model('general_model');
		
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('member_message','meta');
		
		$this->data['body']='front/instructor/message';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function send()
	{ 
		$this->data['meta']  = getMetaContent('member_message_send','meta');
		
		$this->data['body']='front/sadmin/message_send';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function send_all_students()
	{

		$classes=$this->classes_model->getClasses();
		
		foreach($classes AS $class)
		{
			$row[$class['class_id']]=$this->classes_model->getSections($class['class_id']);
			
		}
		
		$this->data['classes']=$classes;	
		
		$this->data['class_section']=$row;
			
		$this->data['meta']  = getMetaContent('member_message_send','meta');
		
		$this->data['body']='front/sadmin/all_students_view';
		
		$this->load->view('front/structure',$this->data);		
		
	}

	function send_sms()
	{
		echo"sms sending form";
		
		$post=$this->input->post();var_dump($post);
		
		if(isset($post['students']))
		{
			var_dump($post['students']);
			
		}
		
	}
	
	function send_all_teachers()
	{
		
		
	}
		
	
	function reply()
	{ 
		$this->data['meta']  = getMetaContent('member_message_reply','meta');
		
		$this->data['body']='front/member/message_reply';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function remove()
	{ 
		$this->data['meta']  = getMetaContent('member_message_remove_message','data');
		
		$this->data['content']  = $this->data['meta']['data'];
		
		$this->load->view('front/member/message_remove',$this->data);
	}
	
	function view()
	{ 
		$this->data['meta']  = getMetaContent('member_message_view','meta');
		
		$this->data['body']='front/member/message_view';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function credit()
	{ 
		$this->data['meta']  = getMetaContent('member_message_credit_option','meta');
		
		$this->data['body']='front/member/message_credit_option';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function credit_confirmation()
	{ 
		$this->data['meta']  = getMetaContent('member_message_credit_option_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/message_credit_option_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function payment()
	{ 
		$this->data['meta']  = getMetaContent('member_message_payment','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/member/message_payment',$this->data);
	}
	
	function payment_confirmation()
	{ 
		$this->data['meta']  = getMetaContent('member_message_payment_confirmation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/message_payment_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
}

?>