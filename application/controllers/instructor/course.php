<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course extends CI_Controller {

    function __construct() {
    	
        parent::__construct();
		
		if ($this->session->userdata('user_type')!='IN')
		{
			redirect(site_url());
		}
		
        $this->data = array();

        $this->data['sel'] = 'my_courses';
		
		$this->load->model(array('courses_model','general_model',));

    }

/*
|-----------------------------------------------------------------------------
| show courses taken by an instructor
|-----------------------------------------------------------------------------
*/	

    function index() 
	{
		$member_id = $this->session->userdata('member_id');
		
		$semester_id = $this->session->userdata('semester_id');
				
		$this->data['meta'] = getMetaContent('instructor_mystudents');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['semesters']=$this->general_model->get_select_record('semester_id,title, is_current','semesters', 'result',array('is_active'=>1,),'');
		
		$this->data['advising_approval']=TRUE; // if an instructor can add a course by himself
				
		$this->data['my_courses']=$this->courses_model->get_detail_instructor_courses($member_id, $semester_id);
				
		$this->data['body'] = 'front/instructor/mycourses';

        $this->load->view('front/structure', $this->data);		
    }

/*
|-----------------------------------------------------------------------------
| New Course add / Remove 
|-----------------------------------------------------------------------------
*/	

    function add($semester_course_id=0) 
	{
		$advising_approval=TRUE;

		$member_id = $this->session->userdata('member_id');
		
		$semester_id = $this->session->userdata('semester_id');
		
		if($advising_approval)
		{				
			$this->data['meta'] = getMetaContent('instructor_mystudents');
	
	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['my_courses']=$this->courses_model->get_detail_instructor_courses($member_id, $semester_id);
						
			$this->data['available_courses']=$this->courses_model->get_instructor_available_courses($member_id, $semester_id);
			
			$this->data['body'] = 'front/instructor/mycourse_add';
	
	        $this->load->view('front/structure', $this->data);			
		}
		else
		{
			$this->session->set_flashdata('slider_content', "<h2>Slow down cowboy! You do not have permission to add courses!</h2>");
			
			redirect('/instructor/course/');		
		}

    }


/*
|-----------------------------------------------------------------------------
| Remove a course from add course page redirection is different
|-----------------------------------------------------------------------------
*/		
	function remove($semester_course_id=0) 
	{
        	$this->data['semester_course_id']=$semester_course_id;
			
            $this->data['meta'] = getMetaContent('model_message_remove_message','data');

            $this->data['content'] = $this->data['meta']['data'];

            $this->load->view('front/instructor/mycourse_remove', $this->data);
	}
	
	
	function remove_confirmation($semester_course_id=0) 
	{
        $member_id = $this->session->userdata('member_id');

        if ($semester_course_id>0) /* check if this message ownership matches */
        {
        	$this->courses_model->remove_instructor_course($member_id, $semester_course_id);
			
        	$this->data['confirmation_content']="Course has been removed successfully";
			
			$this->session->set_flashdata('slider_content', $this->load->view('front/student/mycourse_remove_confirmation', $this->data, true));

        	redirect('/instructor/course/add');
        }
	}
			

/*
|-----------------------------------------------------------------------------
| add course from dropdown multilist and return the current course list to load
|-----------------------------------------------------------------------------
*/	
    function ajax_add_course() 
	{
		if (isAjax()) 
		{
			// $this->load->model('notification_model');
						
			$new_courses=$this->input->post('new_courses');
			
			$member_id=$this->session->userdata('member_id');

			$semester_id=$this->session->userdata('semester_id');
			
			foreach($new_courses as $semester_course_id)
			{
				$this->courses_model->assign_instructor_course($member_id, $semester_course_id);
				
			//	$this->notification_model->instructor_has_new_enrole($member_id, $semester_course_id); /* create notification */
				
			}
			
			$this->data['available_courses']=$this->courses_model->get_instructor_available_courses($member_id, $semester_id);
			
			$this->data['my_courses']=$this->courses_model->get_detail_instructor_courses($member_id, $semester_id);
			
			echo $this->load->view('front/instructor/mycourse_list_ajx', $this->data);
			
			
		} else 
		
		{
	        show_error('Sorry, direct access is not allowed');
	    }
		
	}


}
