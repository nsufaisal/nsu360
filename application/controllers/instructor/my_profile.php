<?php
class My_Profile extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		if ($this->session->userdata('member_id')==FALSE)
		{
			redirect(site_url());
		}		
		$this->data = array();  
		
		$this->data['sel'] = 'my_profile';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('model_my_profile','meta');
		
		$this->data['body']='front/model/my_profile';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function edit()
	{
		$this->data['meta']  = getMetaContent('model_my_profile_edit','meta');
		
		$this->data['body']='front/model/my_profile_edit';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function confirmation()
	{ 
		$this->data['meta']  = getMetaContent('model_my_profile_edit','meta');
		
		$meta  = getMetaContent('model_my_profile_edit_confirmation','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->data['body']='front/model/my_profile_edit_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
}

?>