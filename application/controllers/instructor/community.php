<?php
class Community extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		if ($this->session->userdata('user_type')!='M')
		{
			redirect(site_url());
		}

		$this->load->model('models_model');
		
		$this->load->model('members_model');
		
		$this->load->model('messages_model');
		
		$this->data = array();  
		
		$this->data['sel'] = 'community';	
		
	}
	
	function index()
	{
		$member_id=$this->session->userdata('member_id');
			
		$this->data['meta']  = getMetaContent('model_community','meta');

		$this->data['new_messages']=$this->messages_model->getNewMessages($member_id);
				
		$this->data['body']='front/model/community';
		
		$this->load->view('front/structure',$this->data);
	}
	
	
	function my_appointment()
	{

				 
		$this->data['meta']  = getMetaContent('model_community','meta');
		
		$this->data['body']='front/model/my_appointment';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function my_customer()
	{
		$this->data['meta']  = getMetaContent('model_community','meta');
		
		$this->data['body']='front/model/my_customer';
		
		$this->load->view('front/structure',$this->data);
	}

}

?>