<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Exam extends CI_Controller {

    function __construct() {
        parent::__construct();
		if ($this->session->userdata('user_type')!='IN')
		{
			redirect(site_url());
		}
        $this->data = array();

        $this->data['sel'] = 'my_exams';
		
		$this->load->model('exams_model');
		
		$this->load->model('general_model');
		
		
    }

/*
|-----------------------------------------------------------------------------
| displays exams list of courses taken by the instructor
|-----------------------------------------------------------------------------
*/    
    function index($course_id=0) {
    	
		$this->load->model('courses_model');
		
		$member_id=$this->session->userdata('member_id');
		
		$semester_id=$this->session->userdata('semester_id');
		
	   	$this->data['meta'] = getMetaContent('studio_myclasses');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['semesters']=$this->general_model->get_select_record('semester_id,title, is_current','semesters', 'result',array('is_active'=>1,),'');
				
		$this->data['my_courses']=$this->courses_model->get_detail_instructor_courses($member_id, $semester_id);

		$this->data['exams']=$this->exams_model->get_all_exams($member_id, $semester_id, $course_id);
		
		$this->data['body'] = 'front/instructor/myexams';

       	$this->load->view('front/structure', $this->data);
		
    }

/*
|-----------------------------------------------------------------------------
| Add a new exam to a course
|-----------------------------------------------------------------------------
*/    	
	function add() 
    {
		$member_id=$this->session->userdata('member_id');
		
		$semester_id=$this->session->userdata('semester_id');
		
    	$this->load->model('courses_model');
		
        $this->data['meta'] = getMetaContent('studio_add_model');
		
        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['mode']='new';

		$this->data['semesters']=$this->general_model->get_select_record('semester_id,title, is_current','semesters', 'result',array('is_active'=>1,),'');
				
		$this->data['my_courses']=$this->courses_model->get_detail_instructor_courses($member_id, $semester_id);

        $this->data['body'] = 'front/instructor/myexam_add';

        $this->load->view('front/structure', $this->data);
    }
	
	function edit($exam_id=0) 
    {

    	$this->data['classes']=$this->classes_model->getClasses();
		
		$this->data['sections']=$this->general_model->getAll('sections','title');
			
        $this->data['meta'] = getMetaContent('studio_add_model');
		
        $this->data['content'] = $this->data['meta']['data'];
		
		if($exam_id){
		
		$this->data['exam']=$this->exams_model->GetById($exam_id);

		$this->data['mode']='edit';
		}
		else {
			$this->data['mode']='new';
		}
		

        $this->data['body'] = 'front/sadmin/add_exam';

        $this->load->view('front/structure', $this->data);
    }

	function view($id) 
	{
			
		
		if(is_numeric($id))
		{
			$this->data['meta'] = getMetaContent('studio_view_model');

	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['exam']=$this->exams_model->GetById($id);
			
	        $this->data['body'] = 'front/sadmin/exam_view';
	
	        $this->load->view('front/structure', $this->data);
		}				
		
		else {
			redirect('/sadmin/myexams/');
		}  
    }
    	
	function save($exam_id=0) 
    {
			
			$post=$this->input->post();
			
			$day=$post['day'];
			
			$month=$post['month'];
			
			$year=$post['year'];
			
			$examday = $year.'-'.$month.'-'.$day;	
			
			$class_title=$post['class_title'];	
			
			$exam_data=array(
				
				'class_id'=>$class_title,
				
				'exam_title'=>$post['exam_title'],
				
				'marks'=>$post['marks'],
				
				'weight'=>$post['weight'],
				
				'date'=>$examday,			
							
			);
			
			$exam_id=$this->exams_model->Save($exam_data,$exam_id);
			
			if($exam_id)
			 {
			 	
			 	redirect('sadmin/myexams/');
			 }
			
			else {
			
			 redirect('sadmin/myexams/add');
			}
		
		} //end else 
	
	

		function add_marks($exam_id)
		{
			//echo "Add Marks controller";
			
			$this->data['classes']=$this->classes_model->getClasses();
			
			$this->data['sections']=$this->general_model->getAll('sections','title');
			
			$this->data['meta'] = getMetaContent('studio_add_model');
			
	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['exam']=$this->exams_model->GetById($exam_id);
			
			$this->data['students']=$this->exams_model->GetStudentsById($exam_id);
			
			$this->data['body'] = 'front/sadmin/add_marks';
	
	       $this->load->view('front/structure', $this->data);
			
			//redirect('front/sadmin/add_marks');
			
		}
		
		function save_marks($exam_id)
		{
			echo "save marks for".$exam_id ;
			
			$post=$this->input->post();
			
			$marks=$post['marks'];
			
			$students=$post['students'];
			
			$course_id=$post['course_id'];
			
			
			for($i=0;$i<count($marks);$i++)
			{
				$entryday = date('Y-m-d');
				
				$grade_data=array(
			
					'student_id'=>$students[$i],
			
					'exam_id'=>$exam_id,
			
					'marks'=>$marks[$i],
					
					'entry_date'=>$entryday,
					
					'course_id'=>$course_id[$i]
			
					);
					
					$grade_id=$this->exams_model->save_grades($grade_data);
					
						if(!$grade_id)
			
			 				redirect('sadmin/myexams/add_marks'.$exam_id);

				
			}

			
			//var_dump($marks);
			//var_dump($students);
			
			redirect('sadmin/myexams/');
			
		}
		
		function viewclasses()
		{
			$classes=$this->classes_model->getClasses();
			
			//var_dump($classes);
			
			foreach($classes AS $class)
			{
				$sections[$class['class_id']]=$this->classes_model->getSections($class['class_id']);
				
				
			}
			//$this->data['sections']=$sections;
			
			$this->data['classes']=$classes;
			
			$this->data['meta'] = getMetaContent('studio_add_model');
			
	        $this->data['content'] = $this->data['meta']['data'];
			
			$this->data['body'] = 'front/sadmin/view_classes';

        	$this->load->view('front/structure', $this->data);
			
		}

    }

?>