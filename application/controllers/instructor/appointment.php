<?php
class Appointment extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'my_appointment';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('member_my_appointment','meta');
		
		$this->data['body']='front/member/my_appointment';
		
		$this->load->view('front/structure',$this->data);
	}
	
	
	function view()
	{ 
		$this->data['meta']  = getMetaContent('member_view_my_appointment','meta');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/member/view_my_appointment';
		
		$this->load->view('front/structure',$this->data);
	}
}

?>