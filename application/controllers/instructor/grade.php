<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mygrades extends CI_Controller {

    function __construct() {
        parent::__construct();
		if ($this->session->userdata('user_type')!='A')
		{
			redirect(site_url());
		}
        $this->data = array();

        $this->data['sel'] = 'my_exams';
		
		$this->load->model('students_model');
		
		$this->load->model('classes_model');
		
		$this->load->model('exams_model');
		
		$this->load->model('courses_model');
		
		$this->load->model('general_model');
		
		
    }
    
    function index() {
    	
		
    	$this->data['meta'] = getMetaContent('studio_myclasses');

        $this->data['content'] = $this->data['meta']['data'];
		

       $this->data['body'] = 'front/sadmin/view_grades';

      	$this->load->view('front/structure', $this->data);
		
    }

	function viewgrades($class_id)
	{
		
		//echo "here this is the view";
		
		$this->data['meta'] = getMetaContent('studio_myclasses');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['exams']=$this->exams_model->getExamsByClassId($class_id);
		
		//$this->data['grades']=$this->exams_model->getGrades($class_id);
			
		$this->data['courses']=$this->courses_model->getCoursesByClassId($class_id);
		
		$this->data['students']=$this->exams_model->GetStudentsByClassId($class_id);
			
		$this->data['class_id']=$class_id;
				
		//$this->data['course']=$selected_course;

		$this->data['body'] = 'front/sadmin/view_grades';

        $this->load->view('front/structure', $this->data);
		
		
	}
	
	function viewgradesbycourse($course_id=1)
	{
		echo "here";
		$post=$this->input->post();
		
		$class_id=$post['class_id'];
		
		$course_id=$post['courses'];
		
		var_dump($class_id);
		
		var_dump($course_id);
		
		$this->data['meta'] = getMetaContent('studio_myclasses');

        $this->data['content'] = $this->data['meta']['data'];
		
		$this->data['exams']=$this->exams_model->getExamsByClassCourse($class_id,$course_id);
		
		//$this->data['grades']=$this->exams_model->getGrades($class_id);
			
		$this->data['courses']=$this->courses_model->getCoursesByClassId($class_id);
		
		$this->data['students']=$this->exams_model->GetStudentsByClassId($class_id);
			
		$this->data['class_id']=$class_id;
				
		//$this->data['course']=$selected_course;

		//$this->data['body'] = 'front/sadmin/view_grades';

        //$this->load->view('front/structure', $this->data);
		
	}

		


}

?>