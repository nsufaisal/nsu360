<?php
class Search extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		// Your own constructor code
		
		$this->data = array();  
		
		$this->data['sel'] = 'search';
	}
	
	function index()
	{ 
		$this->data['meta']  = getMetaContent('search','meta');
		
		$this->data['body']='front/search/home';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function model_detail()
	{
		$this->data['meta']  = getMetaContent('model_detail','meta');
		
		$this->data['body']='front/search/model_detail';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function favorites_confirmation()
	{
		$this->data['meta']  = getMetaContent('model_detail','meta');
		
		$meta = getMetaContent('add_favorites_confirmation','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->data['body']='front/search/favorites_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function chat_appointment()
	{ 
		$this->data['meta']  = getMetaContent('chat_appointment','meta');
		
		$this->data['body']='front/search/chat_appointment';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function chat_appointment_confirmation()
	{ 
		$this->data['meta']  = getMetaContent('chat_appointment','meta');
		
		$meta  = getMetaContent('chat_appointment_confirmation','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->data['body']='front/search/chat_appointment_confirmation';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function transition_video()
	{ 
		$this->data['meta']  = getMetaContent('transition_video');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/search/transition_video';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function my_camera()
	{ 
		$this->data['meta']  = getMetaContent('my_camera','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/search/my_camera',$this->data);
	}
	
	function camera_activate()
	{ 
		$this->data['meta']  = getMetaContent('camera_activate','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/search/camera_activate',$this->data);
	}
	
	function camera_deactivate()
	{ 
		$this->data['meta']  = getMetaContent('camera_deactivate','data');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->load->view('front/search/camera_deactivate',$this->data);
	}
	
	function content_store()
	{ 
		$this->data['meta']  = getMetaContent('search_content_store','meta');
		
		$this->data['body']='front/search/content_store';
		
		$this->load->view('front/structure',$this->data);
	}
	
	function store_detail()
	{ 
		$this->data['meta']  = getMetaContent('content_store_detail','meta');
		
		$this->data['body']='front/search/content_store_detail';
		
		$this->load->view('front/structure',$this->data);
	}
}

?>