<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('members_model');
		
		$this->load->model('outbound_email_model');
				
		$this->data = array();  
		
		$this->data['sel'] = 'login';			
	}

/*
|-----------------------------------------------------------------------------
| member authentication
|-----------------------------------------------------------------------------
*/
	public function ajx_user_credential_check()
	{
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		
		$result=$this->members_model->CheckAuthentication($email,$password);			
		
		if(count($result)>0)
			echo TRUE;
		
		else
			echo FALSE;
	}	
	function index()
	{
		$member_id=$this->session->userdata('member_id');
		
		if(!$member_id) 
		{
			$meta  = getMetaContent('login','data');
			
			$this->data['content'] = $meta['data'];
	                
			$this->data['body']='front/login_normal_signin';
			
			$this->load->view('front/structure',$this->data);			
		}
		else {
			redirect('/home/');
		}

	}

/*
|--------------------------------------------------------------------------
| User Login Form validation & Authentication
|--------------------------------------------------------------------------
*/
	function account_activation()
	{ 
		$this->data['meta']  = getMetaContent('student_account_activation');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['body']='front/account_activation';
		
		$this->load->view('front/structure',$this->data);
	}

/*
|--------------------------------------------------------------------------
| User Login Form validation & Authentication
|--------------------------------------------------------------------------
*/
	function activation_confirm()
	{
		$this->form_validation->set_rules('activ_username', 'Username', 'trim|reauired|xss_clean');
		
		$this->form_validation->set_rules('activation_key', 'Activation Key', 'trim|reauired|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{			
			$this->account_activation();
		}
		else  /* form is validated */
		{
			$username = $this->input->post('active_username',TRUE);
			
			$activation_key = $this->input->post('activation_key', TRUE);
			
			if($this->members_model->activate_account($username, $activation_key))
			{				
				$this->data['meta']  = getMetaContent('student_activation_confirmation');
			
				$this->data['content'] = $this->data['meta']['data'];
				
				$this->session->set_flashdata('slider_content', $this->load->view('front/activation_confirmation',$this->data,true));
	
	    		redirect('/home');
			}
			else // wrong information given
			{				
				$this->data['meta']  = getMetaContent('student_signup');
			
				$this->data['content'] = $this->data['meta']['data'];
				
				$this->data['error_msg']=TRUE;
				
				$this->data['body']='front/account_activation';
			
				$this->load->view('front/structure',$this->data);					
			}
		}
	}

/*
|--------------------------------------------------------------------------
| Login In button calls this method
|--------------------------------------------------------------------------
*/
	function fancy_login()
	{
		$meta  = getMetaContent('login','data');
		
		$this->data['content'] = $meta['data'];
		
		$this->load->view('front/login',$this->data);
	}
	
/*
|--------------------------------------------------------------------------
| User Login Form validation & Authentication
|--------------------------------------------------------------------------
*/
	function user_auth()
	{ 
		$this->form_validation->set_rules('email', 'Username', 'trim|reauired|xss_clean');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|reauired|xss_clean|callback_user_check');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{			
			$this->index();
		}
		else
		{
			$email=$this->input->post('email', TRUE);
			
			$password=$this->input->post('password', TRUE);
			
			$result=$this->members_model->verify_authentication($email,$password);	
			
			$curr_semester=$this->members_model->get_curr_semester();	
						
			$this->session->set_userdata('member_id', $result['member_id']);
			
			if(isset($curr_semester))
			{
				$this->session->set_userdata('semester_id',$curr_semester);	
			}			
			$this->session->set_userdata('user_type', $result['user_type']);
			
			$this->session->set_userdata('email', $result['email']);
			
			$this->session->set_userdata('username',$result['username']);
			
			$this->members_model->Update_Activity_Date($result['member_id']);
			
			if($result['user_type']=='U')
				redirect('/member/myaccount');
			
			else if($result['user_type']=='PA')
				redirect('/sadmin/myaccount/');
				
			else if($result['user_type']=='IN')
				redirect('/instructor/myaccount/');
							
			else if($result['user_type']=='ST')
				redirect('/student/myaccount/');
			
			else {
				redirect(site_url());
			} 
		}
	}


/*
|-----------------------------------------------------------------------------
| check member email and returns json for ajax validation engine in login page
|-----------------------------------------------------------------------------
*/
	public function ajax_login_user_checkl()
	{
		$email=$this->input->post('fieldValue');
		
		$result=$this->members_model->checklogin_l($email);
		
		if(count($result)==0){
			$arrayToJs= array();
			$arrayToJs[0] = "email";
			$arrayToJs[1] = FALSE;
			
		}
		else{
			$arrayToJs = array();
			$arrayToJs[0] = "email";
			$arrayToJs[1] = TRUE;
			$this->session->set_userdata('temp', $email);
		}	
			
			echo json_encode($arrayToJs);
	}
/*
|-----------------------------------------------------------------------------
| check member email and returns json for ajax validation engine in login page
|-----------------------------------------------------------------------------
*/
	public function ajax_check_email()
	{
		$email=$this->input->post('fieldValue');
		
		$result=$this->members_model->check_email($email);
		
		if(count($result)==0){
			$arrayToJs= array();
			$arrayToJs[0] = "email";
			$arrayToJs[1] = FALSE;
			
		}
		else{
			$arrayToJs = array();
			$arrayToJs[0] = "email";
			$arrayToJs[1] = TRUE;
			$this->session->set_userdata('temp', $email);
		}	
			
			echo json_encode($arrayToJs);
	}
	
/*
|---------------------------------------------------------------------------------
| check member password and returns json for ajax validation engine in login page
|---------------------------------------------------------------------------------
*/	
	public function ajax_check_password()
	{
		$password=$this->input->post('fieldValue');
		$email= $this->session->userdata('temp');
		
		$result=$this->members_model->CheckAuthentication($email,$password);
		if (count($result)==0){
			$arrayToJs= array();
			$arrayToJs[0] = "password";
			$arrayToJs[1] = FALSE;			
		}
		else{
			$arrayToJs = array();
			$arrayToJs[0] = "password";
			$arrayToJs[1] = TRUE;
		}	
			
			echo json_encode($arrayToJs);
	}
	

/*
|--------------------------------------------------------------------------
| Callback Validation function for checking user credential
|--------------------------------------------------------------------------
*/
	public function user_check($password)
	{
		$email=$this->input->post('email', TRUE);
		
		$result=$this->members_model->CheckAuthentication($email,$password);
			
		if (count($result)==0)
		{
			$this->form_validation->set_message('user_check', "Sorry, but the user credentials you provided is not correct. Please try again.");
			
			return FALSE;			
		}
		else
		{
			return TRUE;
		}
	}
	
	
	/* show register form for a new user */
	
	function register()
	{
		$this->data['meta']  = getMetaContent('register');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['security_qs']=$this->members_model->getAllSecurityQuestions();

    	$this->load->view('front/register',$this->data);		
		
	}

/*
|-----------------------------------------------------------------------------
| check whether username is available
|-----------------------------------------------------------------------------
*/
	public function ajx_username_available(){
		
		$username=$this->input->post('username');
		
		$result=$this->members_model->check_username($username);
		
		if(count($result)==0)
			echo TRUE;
		
		else
			echo FALSE;
			
	}

/*
|-----------------------------------------------------------------------------
| check whether email is available
|-----------------------------------------------------------------------------
*/	
	public function ajx_email_available(){
		
		$email=$this->input->post('email');
		
		$result=$this->members_model->check_email($email);
		
		if(count($result)==0)
			echo TRUE;
		
		else
			echo FALSE;
	}

/*
|-----------------------------------------------------------------------------
| check whether username is available
|-----------------------------------------------------------------------------
*/
	public function ajx_password_check()
	{
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		
		$result=$this->members_model->CheckAuthentication($email,$password);			
		
		if(count($result)==0)
			echo TRUE;
		
		else
			echo FALSE;
	}
	
	public function ajax_user_already_exist()
	{
		$username=$this->input->post('fieldValue');
		
		$result=$this->members_model->GetRowIfExist('username', $fieldValue);
		
		if(count($result)==0){
			$arrayToJs= array();
			$arrayToJs[0] = "username";
			$arrayToJs[1] = FALSE;
			
		}
		else{
			$arrayToJs = array();
			$arrayToJs[0] = "username";
			$arrayToJs[1] = TRUE;			
		}	
			
			echo json_encode($arrayToJs);
	}

/*
|-----------------------------------------------------------------------------
| Member Forgot Passwor Form
|-----------------------------------------------------------------------------
*/	
	function forgot()
	{
		$this->data['meta']  = getMetaContent('forgotpassword');
		
		$this->data['content'] = $this->data['meta']['data'];
		
    	$this->load->view('front/forgetpassword',$this->data);
	}
	
/*
|-----------------------------------------------------------------------------
| process a new password for user and send it through email 
|-----------------------------------------------------------------------------
*/

	function forgot_password_confirmation(){
	
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');
	   
		if ($this->form_validation->run() != FALSE)
		{
			$member=$this->members_model->check_email($this->input->post('email'));
			
			$forget_email=$this->input->post('email');
			
			$member=$this->members_model->check_email($forget_email);
			
			$new_pass=random_string('alnum', 16);
			
			$this->members_model->UpdatePassword($forget_email, md5($new_pass));  				
		   
			//Confirmation Email Sending
			
			$content =$this->outbound_email_model->get('User_forgot_password');			//Model Function calling for get email content
			
			if(!empty($content))
			{
				$content_body=$content['content'];
			}
			else 
			{
				exit();
			}
			
			$content_body= $content['content'];
			
			$content_body=str_replace('{PASSWORD}',$new_pass,$content_body);

			$data_mail['content_body']=$content_body;
				
			$msg = $this->load->view('front/email_format',$data_mail,TRUE);
			
			$subject = $content['subject'];
			
			$to=$forget_email;
			
			$this->_emailer($to, $msg, $subject);							
						
			$this->data['confirmation_title']="Password Recovery Confirmation";
        	
        	$this->data['confirmation_content'] = "<p>Great! Your request has been accepted. A new password has been created for your account.Please check your email.</p>";

			$this->data['confirmation_content'].="<p>You may now Sign in with your new password. Have a great day!</p>";

			$this->session->set_flashdata('slider_content', $this->load->view('front/common_confirmation',$this->data,true));
						
			}

			redirect('/home/');
	}

/*
|-----------------------------------------------------------------------------
| user can recovery username/password by answering 3 security question
|-----------------------------------------------------------------------------
*/
	function forgot_user_email()
	{
		$this->data['meta']  = getMetaContent('forgot_user_email');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->data['security_qs']=$this->members_model->getSecurityQuestions();

    	$this->load->view('front/forget_user_email',$this->data);
	}
	
	
	function failed_recovery()
	{
		$this->data['meta'] = getMetaContent('forgot_failed_recovery');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/forget_failed_recovery',$this->data, TRUE));
		
    	
				
		redirect();
	}

/*
|-----------------------------------------------------------------------------
| Email username recovry confirmation
|-----------------------------------------------------------------------------
*/	
	function recovery()
	{
		$question_id_1 = $this->input->post('question_id_1');
			
		$security_ans1 = $this->input->post('security_ans1');
		
		$question_id_2 = $this->input->post('question_id_2');
		
		$security_ans2 = $this->input->post('security_ans2');
		
		$result=$this->members_model->GetSecurityAnswers($question_id_1,$security_ans1, $question_id_2,$security_ans2);
		
		if(count($result)==0)
		{
			redirect('/login/failed_recovery');
		}
		

		$this->data['recovery'] =$result;
		
		$this->data['meta'] = getMetaContent('forgot_recovery');
		
		$this->data['content'] = $this->data['meta']['data'];
		
		$this->session->set_flashdata('slider_content', $this->load->view('front/forget_recovery',$this->data,true));

    	redirect();
	}
	
	

/*
|-----------------------------------------------------------------------------
| checks whether security answers matches user's previous answers
|-----------------------------------------------------------------------------
*/	
	function ajx_security_q_check()
	{
		$security_q1=$this->input->post('security_q1');
				
		$security_ans1=$this->input->post('security_ans1');
		
		$security_q2=$this->input->post('security_q2');
		
		$security_ans2=$this->input->post('security_ans2');
		
		$result=$this->members_model->GetSecurityAnswers($security_q1,$security_ans1, $security_q2,$security_ans2);
		
		if(count($result)==0)
			echo FALSE;
		
		else 
			echo TRUE;

	}
	
	function ajx_check_pass_with_id()
	{
		$member_id=$this->input->post('member_id');
		
		$password=$this->input->post('password');
		
		$result=$this->members_model->check_pass_id($member_id, $password);
		
		if(count($result)==0)
			echo FALSE;
		
		else 
			echo TRUE;
	}
	
/*
|-----------------------------------------------------------------------------
| Send an email according to parameters
|-----------------------------------------------------------------------------
*/	
	function _emailer($to, $html, $subject, $file = '') {

		$config['protocol'] = 'mail';

		$config['wordwrap'] = TRUE;

		$config['crlf'] = "\r\n";

		$config['newline'] = "\r\n";

		$config['mailtype'] = 'html';

		$config['charset'] = 'utf-8';

		$this -> email -> initialize($config);
		
		$this->email ->to($to);
		
		$this -> email -> from('noreply@nsu360.com', 'NSU360 Support');

		$this -> email -> subject($subject);
		
		$this -> email -> message($html);
		
		if ($file!='') 
		{
			$this -> email -> attach('uploads/attachments/' . $file);
		}
		$this -> email -> send();

	}

	
}

?>