<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course extends CI_Controller {

    function __construct() {
        
        parent::__construct();
            
        $this->data = array();

        $this->data['sel']='course_list';
        
        $this->load->model(array('courses_model','general_model', 'instructor_model', 'departments_model'));

    }

/*
|-----------------------------------------------------------------------------
| Edit form of a student
|-----------------------------------------------------------------------------
*/  

    function index()
    {   
        $semester_id = $this->session->userdata('semester_id');
        
        $this->data['meta'] = getMetaContent('');      

        $this->data['content'] = $this->data['meta']['data'];

        $this->data['departments'] = $this->departments_model->getAllDepartments();
        
        $this->data['courses'] = $this->courses_model->get_courses();
        
        $this->data['body'] = 'front/courses';

        $this->load->view('front/structure', $this->data);      
    }

}
