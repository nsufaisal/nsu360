<?php 
class Students_model extends CI_Model
{

	function __construct()
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| remove an enroled student from enrole table
|-----------------------------------------------------------------------------
*/
	function remove_enrole_student($enrole_id)
	{
		$this->db->where('enrole_id',$enrole_id);
		
		return $this->db->delete('course_enrole');
		
	}
/*
|-----------------------------------------------------------------------------
| checks if an enroled student belong to an instructor's course
|-----------------------------------------------------------------------------
*/
	function is_my_enroled_student($enrole_id=0, $member_id=0)
	{
		$query=$this->db->select('cn.student_id')
						->from('course_enrole AS cn')
						->join('semester_course AS sc','sc.id=cn.semester_course_id','inner')
						->where('cn.enrole_id',$enrole_id)
						->where('sc.instructor_id',$member_id)
						->get();
		
		if($query->num_rows()>0)
			return TRUE;
		else {
			return FALSE;
		}
		
		
	}
/*
|-----------------------------------------------------------------------------
| instructor approve student enrole
|-----------------------------------------------------------------------------
*/
	function approve_enrole($enrole_id)
	{
		$this->db->where('enrole_id', $enrole_id);
		
		if($this->db->update('course_enrole', array('approval'=>1,)))
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
		
	}
	
/*
|-----------------------------------------------------------------------------
| check whether a student belong to an instructor's course 
|-----------------------------------------------------------------------------
*/
	function is_my_student($member_id, $student_id)
	{
		$query=$this->db->select('cn.student_id')
						->from('course_enrole AS cn')
						->join('semester_course AS sc','sc.id=cn.semester_course_id','inner')
						->where('cn.student_id',$student_id)
						->where('sc.instructor_id',$member_id)
						->get();
		
		if($query->num_rows()>0)
			return TRUE;
		else {
			return FALSE;
		}
		
	}

/*
|-----------------------------------------------------------------------------
| return students list of an instructor of his first taken course of semester
|-----------------------------------------------------------------------------
*/	
	function get_instructor_students($member_id, $semester_id, $course_id=0)
	{
	//	$this->db->distinct('st.member_id');
		
		$this->db->select('st.member_id, cn.approval,cn.enrole_id, st.student_id, st.last_name, st.first_name,cs.initial AS course, sc.section_no, 
							
							dm.initial AS major, m.phone, m.email')			 
		
		 ->from('students AS st')
		 
		 ->join('members AS m', 'm.member_id=st.member_id')
		 
		 ->join('course_enrole AS cn','st.member_id=cn.student_id','inner')
		 
		 ->join('semester_course AS sc', 'sc.id=cn.semester_course_id','inner')
		 
		 ->join('courses AS cs','sc.course_id=cs.course_id')
		 
		 ->join('degree_majors AS dm','st.degree_major=dm.major_id','left' )
		 
		 ->where('sc.instructor_id',$member_id)
		 		 
		 ->where('sc.semester_id',$semester_id);
		
		if($course_id>0)
		{
			$this->db->where('sc.course_id', $course_id);
		} 
		$this->db->order_by('cn.approval','asc');
		
		$this->db->order_by('cs.initial','asc');
		
		return $this->db->get()->result_array();
		
	}
/*
|-----------------------------------------------------------------------------
| update student profile photo
|-----------------------------------------------------------------------------
*/	
	function change_photo($member_id,$filename)
	{
		return $this->db->where('member_id', $member_id)
				->update('students', array('photo'=>$filename));
		
	}
/*
|-----------------------------------------------------------------------------
| update student from myaccount edit page
|-----------------------------------------------------------------------------
*/		
	function update_student($member_id,$std_data)
	{
		return $this->db->where('member_id', $member_id)

						->update('students', $std_data);	
		
	}

/*
|-----------------------------------------------------------------------------
| Get an individual student info for myaccount page or anywhere
|-----------------------------------------------------------------------------
*/		
	function get_student($member_id='')
	{
		return $this->db->select('m.member_id, m.username, m.nid, m.email, m.phone, s.student_id, s.last_name,
									s.photo, s.first_name,s.curr_address, s.gender, s.degree_major, s.department_id, s.blood_group')
				 		->from('members AS m')
				 		->join('students AS s', 'm.member_id=s.member_id')
						->join('degree_majors AS dm', 's.degree_major = dm.major_id','left')
						->where(array('m.member_id'=>$member_id, 'm.isactive'=>1))->get()->row_array();
	}
	
		
/*
|-----------------------------------------------------------------------------
| Get students of an instructor enrolled current semester
|-----------------------------------------------------------------------------
*/		
	function get_my_students($member_id='', $selected_course='', $selected_semester='')
	{
		return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
				 ->from('students AS s')
				 ->join('classes AS c', 's.curr_class=c.class_id')
				 ->join('members AS m', 's.member_id=m.member_id')
				 ->order_by('student_id', 'desc')->get()->result_array();
	}
		
/*
|-----------------------------------------------------------------------------
| Delete a student
|-----------------------------------------------------------------------------
*/	
	function check_studentId($student_id,$skip_id=0)
	{
		if($skip_id > 0)	
			$this->db->where('member_id <>',$skip_id);
			
		return $this->db->where('student_id', $student_id)->get('students')->row_array();	
	}	
/*
|-----------------------------------------------------------------------------
| Delete a student
|-----------------------------------------------------------------------------
*/		
	function delete($member_id)
	{
		$this->db->where('member_id',$member_id)->delete('students');
	}	
/*
|-----------------------------------------------------------------------------
| create/update a new student
|-----------------------------------------------------------------------------
*/	
	function Save($student_data, $student_id)
	{
		if($student_id==0)
		{
			$this->db->insert('students', $student_data);
				
			return $this->db->insert_id();		
		}			

		else {
			$this->db->where('student_id', $student_id);
			
			$this->db->update('students',$student_data);
			
			return TRUE;
		}
	}	
/*
|-----------------------------------------------------------------------------
| Get a student data given id
|-----------------------------------------------------------------------------
*/		
	function GetById($student_id)
	{
		return $this->db->select('s.*, c.title AS class ')
				 ->where('student_id', $student_id)
				 ->from('students AS s')
				 ->join('classes AS c', 's.curr_class=c.class_id')
				 ->get()->row_array();	
	}
	


/*
|-----------------------------------------------------------------------------
| Get students data given by limit offset
|-----------------------------------------------------------------------------
*/		
	function getStudents($limit=30, $offset=0)
	{
		return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
				 ->from('students AS s')
				 ->join('classes AS c', 's.curr_class=c.class_id')
				 ->join('members AS m', 's.member_id=m.member_id')
				 ->order_by('student_id', 'desc')->get()->result_array();
	}
	
	/*
|-----------------------------------------------------------------------------
| Get students data given by limit offset
|-----------------------------------------------------------------------------
*/		
	function disable($member_id)
	{
		return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
				 ->from('students AS s')
				 ->join('classes AS c', 's.curr_class=c.class_id')
				 ->join('members AS m', 's.member_id=m.member_id')
				 ->order_by('student_id', 'desc')->get()->result_array();
	}
	
	
	function GetMemberId($student_id)
	{
		 $this->db->select('s.member_id')
			->where('student_id',$student_id)
			->from('students AS s');
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->row()->member_id;
		
		
	}

}