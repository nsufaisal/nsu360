<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Purpose : Function to get records from database
     * Input : @param table_name and conditions
     * Return : return_type array
     * Created on : 22-Feb-2013
     */
    function get_record($table_name = '', $number = 'result', $where = '', $sort = '') {
        if ($sort) {
            $this->db->order_by($sort);
        }
        $qry = $this->db->get_where($table_name, $where);

        if ($qry->num_rows()) {
            if ($number == 'result') {
                return $qry->result_array();
            } else {
                return $qry->row_array();
            }
        } else {
            return false;
        }
    }
    function get_All($table_name = '', $number = 'result') {

        $qry = $this->db->get_where($table_name);
        if ($qry->num_rows()) {
            if ($number == 'result') {
                return $qry->result_array();
            } else {
                return $qry->row_array();
            }
        } else {
            return false;
        }
    }

    function get_select_record($select='',$table_name = '', $number = 'result', $where = '', $sort = '') {
        if($select)
		{
			$this->db->select($select);
		}
        if ($sort) {
            $this->db->order_by($sort);
        }
        $qry = $this->db->get_where($table_name, $where);

        if ($qry->num_rows()) {
            if ($number == 'result') {
                return $qry->result_array();
            } else {
                return $qry->row_array();
            }
        } else {
            return false;
        }
    }


    /**
     * Purpose : Function to get records from database
     * Input : @param table_name and conditions
     * Return : return_type array
     * Created on : 22-Feb-2013
     */
    function count_record($table_name = '', $number = 'result', $where = '', $sort = '') {
        if ($sort) {
            $this->db->order_by($sort);
        }
        $qry = $this->db->get_where($table_name, $where);

        if ($qry->num_rows()) {
            return $qry->num_rows();
        } else {
            return "0";
        }
    }

    /**
     * Purpose : Function to delete records from database
     * Input : @param table_name and condition
     * Return : return_type TRUE and FALSE
     * Created on : 22-Feb-2013
     */
    function delete_record($table_name = '', $where = '') {
        if ($this->db->delete($table_name, $where)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Purpose : function to insert or update records in database
     * param string $table_name, $fields and $where condition
     * return TRUE and FALSE
     * Created on : 22-Feb-2013
     */
    function update_records($table_name = '', $fields = array(), $where = array()) {

        if ($where) {
            if ($this->db->update($table_name, $fields, $where)) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($this->db->insert($table_name, $fields)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
    }

    function get_appointments($search_array = array()) {
        $sort = $search_array['sort'];
        $receipient_id = $search_array['receipient_id'];

        if ($sort) {
            $this->db->order_by($sort);
        }

        $this->db->select('messages.message_id, messages.subject, messages.description, messages.message, messages.sender_id, messages.receipient_id, messages.create_date, messages.appointment_date, messages.appointment_time1 as appointment_time,messages.appointment_time2, messages.duration, messages.appoint_status, members.member_id, members.first_name, members.last_name');
        $this->db->join('members', 'members.member_id = messages.sender_id ', 'inner');

        $this->db->where(array('messages.receipient_id' => $receipient_id, 'messages.message_type' => '2'));

        if (isset($search_array['where']) && $search_array['where'] != '') {
            $this->db->where($search_array['where']);
        }

        $qry = $this->db->get('messages');

        if ($qry->num_rows()) {
            return $qry->result_array();
        } else {
            return false;
        }
    }

    function get_appointment_detail($message_id = 0, $receipient_id = 0) {

        $this->db->select('messages.message_id, messages.subject, messages.description, messages.message, messages.sender_id, messages.receipient_id, messages.create_date, messages.appointment_date, messages.appointment_time1 as appointment_time,messages.appointment_time2, messages.duration, messages.appoint_status, members.member_id, members.first_name, members.last_name, members.email');
        $this->db->join('members', 'members.member_id = messages.sender_id ', 'inner');

        $this->db->where(array('messages.message_id' => $message_id, 'messages.receipient_id' => $receipient_id));
        $qry = $this->db->get('messages');

        if ($qry->num_rows()) {
            return $qry->row_array();
        } else {
            return false;
        }
    }

    function get_member_appointments($search_array = array()) {
        $sort = $search_array['sort'];
        $sender_id = $search_array['sender_id'];

        if ($sort) {
            $this->db->order_by($sort);
        }

        $this->db->select('messages.message_id, messages.subject, messages.description, messages.message, messages.sender_id, messages.receipient_id, messages.create_date, messages.appointment_date, messages.appointment_time1 as appointment_time,messages.appointment_time2, messages.duration, messages.appoint_status, members.member_id, members.first_name, members.last_name');
        $this->db->join('members', 'members.member_id = messages.receipient_id ', 'inner');

        $this->db->where(array('messages.sender_id' => $sender_id, 'messages.message_type' => '2'));

        if (isset($search_array['where']) && $search_array['where'] != '') {
            $this->db->where($search_array['where']);
        }

        $qry = $this->db->get('messages');

        if ($qry->num_rows()) {
            return $qry->result_array();
        } else {
            return false;
        }
    }

    function get_member_appointment_detail($message_id = 0) {

        $this->db->select('messages.message_id, messages.subject, messages.description, messages.message, messages.sender_id, messages.receipient_id, messages.create_date, messages.appointment_date, messages.appointment_time1 as appointment_time,messages.appointment_time2, messages.duration, messages.appoint_status, members.member_id, members.first_name, members.last_name, members.email');
        $this->db->join('members', 'members.member_id = messages.receipient_id ', 'inner');

        $this->db->where(array('messages.message_id' => $message_id));
        $qry = $this->db->get('messages');

        if ($qry->num_rows()) {
            return $qry->row_array();
        } else {
            return false;
        }
    }

    function get_my_favorite($params = array()) {
        $member_id = $params['member_id'];
        $page = $params['page'];
        $perpage = $params['perpage'];
        $sort = $params['sort'];
        $all = $this->db->get_where('my_favorites', array('member_id' => $member_id, 'status' => '1'));
        $num_rows = $all->num_rows;
        //if ( $sort )
        //{
        //	$this->db->order_by($sort);
        //}

        $this->db->select('my_favorites.*, members.username, members.username, members.first_name, members.last_name ');

        $this->db->join('members', 'members.member_id = my_favorites.model_id ', 'inner');

        $this->db->where(array('my_favorites.member_id' => $member_id, 'my_favorites.status' => '1'));

        $qry = $this->db->get('my_favorites', $perpage, ($page - 1) * $perpage);

        if ($qry->num_rows()) {
            return array($qry->result_array(), $num_rows);
        } else {
            return false;
        }
    }

    /**
     * <p text-align="justify">
     * Get models
     * </p>
     * @author Pronab Saha <pranab.su@gmail.com>
     * @param mixed $search Search array
     * @param boolean $phone_cam Phone with cam chat
     * @param string $sort_by Sort field
     * @param mixed 
     */
    function get_all_models_pc($search = array(), $phone_cam= FALSE, $sort_by=''){
        $offset = $search['offset'];
        $perpage = $search['per_page'];

        if (isset($search['country']) && $search['country'] != '') {
            $this->db->where('m.country', $search['country']);
        }

        if (isset($search['age_group']) && $search['age_group'] != '') {
            $this->db->where('d.age_group', $search['age_group']);
        }

        if (isset($search['ethnicity']) && $search['ethnicity'] != '') {
            $this->db->where('d.ethnicity', $search['ethnicity']);
        }

        if (isset($search['hair']) && $search['hair'] != '') {
            $this->db->where('d.hair', $search['hair']);
        }

        if (isset($search['eyes']) && $search['eyes'] != '') {
            $this->db->where('d.eyes', $search['eyes']);
        }

        if (isset($search['build']) && $search['build'] != '') {
            $this->db->where('d.build', $search['build']);
        }

        if (isset($search['status']) && $search['status'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        } else if (isset($search['status']) && $search['status'] == 'offline') {
            $this->db->where('m.online_status', 'NO');
        }

        if ($phone_cam) {
            $this->db->where('d.phone_chat_support', '1');
            $this->db->where('d.cam_support', '1');
        } else {
            $this->db->where('d.phone_chat_support', '1');
        }

        if ((isset($search['isOnline']) && $search['isOnline'] == 'Y') || isset($search['filterby']) && $search['filterby'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        }
        
        if (isset($search['search_text']) && $search['search_text'] != '' && trim($search['search_text']) != 'Search Models' && trim($search['search_text']) != 'Keyword') {
            $this->db->where("(m.username like '%" . $search['search_text'] . "%' OR m.first_name like '%" . $search['search_text'] . "%' OR m.last_name like '%" . $search['search_text'] . "%')");
        }
        
        
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        $all = $this->db->get('members AS m');
        $num_rows = $all->num_rows;

        if (isset($search['country']) && $search['country'] != '') {
            $this->db->where('m.country', $search['country']);
        }

        if (isset($search['age_group']) && $search['age_group'] != '') {
            $this->db->where('d.age_group', $search['age_group']);
        }

        if (isset($search['ethnicity']) && $search['ethnicity'] != '') {
            $this->db->where('d.ethnicity', $search['ethnicity']);
        }

        if (isset($search['hair']) && $search['hair'] != '') {
            $this->db->where('d.hair', $search['hair']);
        }

        if (isset($search['eyes']) && $search['eyes'] != '') {
            $this->db->where('d.eyes', $search['eyes']);
        }

        if (isset($search['build']) && $search['build'] != '') {
            $this->db->where('d.build', $search['build']);
        }

        if (isset($search['search_text']) && $search['search_text'] != '' && trim($search['search_text']) != 'Search Models' && trim($search['search_text']) != 'Keyword') {
            $this->db->where("(m.username like '%" . $search['search_text'] . "%' OR m.first_name like '%" . $search['search_text'] . "%' OR m.last_name like '%" . $search['search_text'] . "%')");
        }

        if (isset($search['status']) && $search['status'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        } else if (isset($search['status']) && $search['status'] == 'offline') {
            $this->db->where('m.online_status', 'NO');
        }

        if ($phone_cam) {
            $this->db->where('d.phone_chat_support', '1');
            $this->db->where('d.cam_support', '1');
        } else {
            $this->db->where('d.phone_chat_support', '1');
        }

        if ((isset($search['isOnline']) && $search['isOnline'] == 'Y') || isset($search['filterby']) && $search['filterby'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        }
	

        switch ($sort_by){
            case 'num_session':
                $this->db->order_by('d.sessions','desc');
                break;
            
            case 'num_followers':
                $this->db->order_by('d.followers','desc');
                break;
            
            case 'site_earnings':
                $this->db->order_by('d.site_earnings','desc');
                break;
            
            case 'random':
                $this->db->order_by('d.phone_chat_support','desc');
                break;
            
            default:
                break;
        }
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        //$qry = $this->db->get('members AS m', $perpage, ($page-1)*$perpage);
        $qry = $this->db->get('members AS m', $perpage, $offset);
        //echo $this->db->last_query();exit;
        
        if ($qry->num_rows()) {
            return array($qry->result_array(), $num_rows);
        } else {
            return false;
        }
    }
    
    function getAllModels($search = array(), $phone_chat = FALSE, $sort_by='') {
        
     //$this->session->unset_userdata('myvar');    
       
     
     ///print_r($search);
        $offset = $search['offset'];
        $perpage = $search['per_page'];
    
   
session_start();
if($_REQUEST[img])
  {
    
    $_SESSION['myvar'] = $_REQUEST[img];
    $this->session->set_userdata('myimg',$_REQUEST[img]);
    $myimg = $this->session->all_userdata();
    $CamImg = $myimg['myimg'];
  }  

  //echo $_SESSION['myvar'];
  //echo $CamImg;
if($_SESSION['myvar'] == 'active')
  {
    $search['phone'] = 'yes';
    
  }
  
  /*$this->load->library('session');
  $this->session->set_flashdata('item', $_REQUEST[img]);
 echo $this->session->keep_flashdata('item');
echo $this->session->flashdata('item');*/


        if (isset($search['country']) && $search['country'] != '') {
            $this->db->where('m.country', $search['country']);
        }

        if (isset($search['age_group']) && $search['age_group'] != '') {
            $this->db->where('d.age_group', $search['age_group']);
        }

        if (isset($search['ethnicity']) && $search['ethnicity'] != '') {
            $this->db->where('d.ethnicity', $search['ethnicity']);
        }

        if (isset($search['hair']) && $search['hair'] != '') {
            $this->db->where('d.hair', $search['hair']);
            
        }

        if (isset($search['eyes']) && $search['eyes'] != '') {
            $this->db->where('d.eyes', $search['eyes']);
            
        }

        if (isset($search['build']) && $search['build'] != '') {
            $this->db->where('d.build', $search['build']);
        }

        if (isset($search['status']) && $search['status'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        } else if (isset($search['status']) && $search['status'] == 'offline') {
            $this->db->where('m.online_status', 'NO');
        }

        //	parth code print_r($search);
        
          
			if (( isset($search['phone']) && $search['phone'] == 'yes'))
			{
				$this->db->where('m.phone !=', '0');
				$this->db->where('m.online_status', 'YES');
				$this->db->where('m.camara', 'YES');
				
			}
			
			if ( (isset($search['isOnline']) && $search['isOnline'] == 'Y') || isset($search['filterby']) && $search['filterby'] == 'online' )
			{
				$this->db->where('m.online_status', 'YES');
				
			}
		// end parth code	
        if (isset($search['search_text']) && $search['search_text'] != '' && trim($search['search_text']) != 'Search Models' && trim($search['search_text']) != 'Keyword') {
            $this->db->where("(m.username like '%" . $search['search_text'] . "%' OR m.first_name like '%" . $search['search_text'] . "%' OR m.last_name like '%" . $search['search_text'] . "%')");
        }
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        $all = $this->db->get('members AS m');
        $num_rows = $all->num_rows;

        if (isset($search['country']) && $search['country'] != '') {
            $this->db->where('m.country', $search['country']);
        }

        if (isset($search['age_group']) && $search['age_group'] != '') {
            $this->db->where('d.age_group', $search['age_group']);
        }

        if (isset($search['ethnicity']) && $search['ethnicity'] != '') {
            $this->db->where('d.ethnicity', $search['ethnicity']);
        }

        if (isset($search['hair']) && $search['hair'] != '') {
            $this->db->where('d.hair', $search['hair']);
            
        }

        if (isset($search['eyes']) && $search['eyes'] != '') {
            $this->db->where('d.eyes', $search['eyes']);
            
        }

        if (isset($search['build']) && $search['build'] != '') {
            $this->db->where('d.build', $search['build']);
        }

        if (isset($search['search_text']) && $search['search_text'] != '' && trim($search['search_text']) != 'Search Models' && trim($search['search_text']) != 'Keyword') {
            $this->db->where("(m.username like '%" . $search['search_text'] . "%' OR m.first_name like '%" . $search['search_text'] . "%' OR m.last_name like '%" . $search['search_text'] . "%')");
        }

        if (isset($search['status']) && $search['status'] == 'online') {
            $this->db->where('m.online_status', 'YES');
        } else if (isset($search['status']) && $search['status'] == 'offline') {
            $this->db->where('m.online_status', 'NO');
        }
        		//parth code 
            //print_r($search);
            
        
			if ( (isset($search['phone']) && $search['phone'] == 'yes')  )
			{
				$this->db->where('m.phone !=', '0');
				$this->db->where('m.online_status', 'YES');
				$this->db->where('m.camara', 'YES');
				
			}
			
		
			if ( (isset($search['isOnline']) && $search['isOnline'] == 'Y') || isset($search['filterby']) && $search['filterby'] == 'online' )
			{
				$this->db->where('m.online_status', 'YES');
			
			}
			
		// end parth code	
        if($phone_chat){
            $this->db->where('d.phone_chat_support', '1');
        }
        
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        //$qry = $this->db->get('members AS m', $perpage, ($page-1)*$perpage);
        $qry = $this->db->get('members AS m', $perpage, $offset);
      	//echo $this->db->last_query();exit;
        if ($qry->num_rows()) {
            return array($qry->result_array(), $num_rows);
        } else {
            return false;
        }
    }

    function top_rated_model() {
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        $this->db->order_by('rating DESC');
        $qry = $this->db->get('members AS m', 4, 0);

        if ($qry->num_rows()) {
            return $qry->result_array();
        } else {
            return false;
        }
    }

    function newest_model() {
        $this->db->where('m.isactive', 1);
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        $this->db->order_by('m.date DESC');
        $qry = $this->db->get('members AS m', 4, 0);

        if ($qry->num_rows()) {
            return $qry->result_array();
        } else {
            return false;
        }
    }

    function featured_model() {
        $this->db->where('m.isactive', 1);
        $this->db->where('d.featured', 'YES');
        $this->db->select('d.*, m.*');
        $this->db->join('models AS d', 'm.member_id = d.member_id', 'INNER');
        $this->db->order_by('m.date DESC');
        $qry = $this->db->get('members AS m', 4, 0);

        if ($qry->num_rows()) {
            return $qry->result_array();
        } else {
            return false;
        }
    }

}