<?php 
class Categories_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }
	function get($fields=''){
		if($fields!='')
			$this->db->order_by($fields,$this->session->userdata('sorttype'));
			
		return $this->db->get('categories')->result_array();
	}
	function getCategories(){
		
			return $this->db->get('categories')->result_array();
	}
	
	function getCategoriesByID($categories_id='')
	{
		return $this->db->where('categories_id',$categories_id)->get('categories')->row_array();
	}
	
	function save($categories_id,$data)
	{
			if ($categories_id!=0){
				$this->db->where('categories_id',$categories_id);	
				$this->db->update('categories',$data);
			} else 
				$this->db->insert('categories',$data);
			return TRUE;			
		
	}
	function delete($category_id){
		$this->db->where('categories_id',$category_id)->delete('categories');
		return TRUE;
	}
}
?>
