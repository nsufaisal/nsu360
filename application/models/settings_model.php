<?php 
class Settings_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| Returns a single setting by setting name
|-----------------------------------------------------------------------------
*/
	function getSettingByName($name='')
	{
		return $this->db->where('name',$name)->get('settings')->row_array();
	}
/*
|-----------------------------------------------------------------------------
| set a new state by name
|-----------------------------------------------------------------------------
*/
	function updateSettingByName($name='',$state_value='', $description='')
	{
		$this->db->where('name',$name)
		                  ->set('state_value',$state_value)
                          ->set('description',$description)
		                  ->update('settings');

		return true;
	}	
	



}