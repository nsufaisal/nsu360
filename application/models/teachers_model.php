<?php 
class Teachers_model extends CI_Model
{

	function __construct()
    {
        parent::__construct();
    }
/*
|-----------------------------------------------------------------------------
| create/update a new student
|-----------------------------------------------------------------------------
*/	
	function Save($teacher_data, $teacher_id)
	{
		//echo $teacher_id;
		//var_dump($teacher_data);
		 
		if($teacher_id==0){

			$this->db->insert('teachers', $teacher_data);
				
			return $this->db->insert_id();		
		}			

		else {
			$this->db->where('teacher_id', $teacher_id);
			
			$this->db->update('teachers',$teacher_data);
			
			return $teacher_id;
		}
	}	
	
	function getTeachers($limit=30, $offset=0)
	{
		
				return $this->db->select('t.*,c.title AS class_title, m.email, m.phone')
				 ->from('teachers AS t')
				 ->join('classes AS c', 't.class_teacher=c.class_id')
				 ->join('members AS m', 't.member_id=m.member_id')
				 ->order_by('teacher_id', 'desc')->get()->result_array();
		
		
	}
	/*
|-----------------------------------------------------------------------------
| Get a teacher data given id
|-----------------------------------------------------------------------------
*/		
	function GetById($teacher_id)
	{
		return $this->db->select('t.*,m.email AS email, m.phone AS phone')
				 ->where('teacher_id', $teacher_id)
				 ->from('teachers AS t')
				 ->join('members AS m', 't.member_id=m.member_id')
				 ->get()->row_array();	
	}


	function GetMemberId($teacher_id)
	{
		 $this->db->select('t.member_id')
			->where('teacher_id',$teacher_id)
			->from('teachers AS t');
			$query=$this->db->get();
			if($query->num_rows()>0)
			return $query->row()->member_id;
		
		
	}

}