<?php 
class Users_model extends CI_Model
{

	function __construct()
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| create a new free user/member, can also be used for update
|-----------------------------------------------------------------------------
*/	
	function Save($member_id=0, $type='U')
	{
		$this->db->set('username',$this->input->post('username', TRUE));
		
		$this->db->set('email',$this->input->post('email', TRUE));
		
		$this->db->set('password',md5($this->input->post('password', TRUE)));
		
		$this->db->set('security_ans1',$this->input->post('security_ans1', TRUE));
		
		$this->db->set('security_ans2',$this->input->post('security_ans2', TRUE));
		
		$this->db->set('security_ans3',$this->input->post('security_ans3', TRUE));
		

		if($member_id==0){
			
			$this->db->set('type',$type);
			
			$this->db->set('date',date('Y-m-d H:i:s'));
			
			$this->db->set('isactive',1);
			
			$this->db->insert('members');
			
			$member_id = $this->db->insert_id();
			
		}else
			
			$this->db->where('member_id',$member_id)->update('members');
				
		return $member_id;
	}

/*************************************************Bellow not used yet *********************************/	
	function GetAllMembers()
	{	
		return $this->db->get('members')->result_array();
	}
	
	function GetExportMembers($member_id=0)
	{
		if($member_id !=0)
			$this->db->where_in('member_id',$member_id);
		return $this->db->get('members')->result_array();	
	}

/*
|-----------------------------------------------------------------------------
| check member existe provided a particular field value, be careful to call it
|-----------------------------------------------------------------------------
*/
	function GetRowIfExist($field, $value)
	{
		return $this->db->where($field, $value)->get('members')->row_array();
	}
	
	function GetMember($member_id)
	{
		return $this->db->where('member_id',$member_id)->get('members')->row_array();
	}
	
	function GetMemberDetail($member_id)
	{
		$this->db->select('last_name, first_name, username, member_id, email, 
		
			phone, type, card_on_file, address, c.city_name, cn.country_name, s.state_name')
			
			->from('members AS m')			
			
			->join('countries AS cn','m.country=cn.contry_id', 'left')
			
			->join('states AS s','cn.country_id=s.country_id','left')
			
			->join('cities AS c','s.state_id=c.state_id','left')
			
			->where('m.member_id',$member_id);
			
		return $this->db->get()->row_array();
		
	//	return $this->db->where('member_id',$member_id)->get('members')->row_array();
	}
	
	function GetCity($member_id)
	{
		$this->db->select('m.city, c.city_name')
			->from('members AS m')
			->join('cities AS c','m.city=c.city_id','left')
			->where('m.member_id', $member_id);	
		return $this->db->get()->row_array();
	}
	
	function GetState($member_id)
	{	
		$this->db->select('m.state, s.state_name')
			->from('members AS m')
			->join('states AS s','m.state=s.state_id','left')
			->where('m.member_id', $member_id);	
		return $this->db->get()->row_array();
	}

	function GetCountry($member_id)
	{	
		$this->db->select('m.country, cn.country_name')
			->from('members AS m')
			->join('countries AS cn','m.country=cn.country_id','left')
			->where('m.member_id', $member_id);	
		return $this->db->get()->row_array();
	}
	
	
	function CheckActivateKey($key)
	{
		return $this->db->where('activation_key',$key)->get('members')->row_array();
	}
	
	function check_username($username)
	{		
		return $this->db->where('username',$username)->get('members')->row_array();
	}
	
	function check_email($email,$skip_id=0)
	{
		if($skip_id > 0)	
			$this->db->where('member_id <>',$skip_id);		
			
		return $this->db->where('email',$email)->get('members')->row_array();	
	}
	
/*
|-----------------------------------------------------------------------------
| check member existe with username/email
|-----------------------------------------------------------------------------
*/	
	
	function check_login_username($username,$skip_id=0)
	{
		if($skip_id > 0)	
			$this->db->where('member_id <>',$skip_id);
			
		$this->db->where('email', $username);
		
		$this->db->or_where('username', $username);	
			
		return $this->db->get('members')->row_array();	
	}
	
	function delete($member_id)
	{
		$this->db->where('member_id',$member_id)->delete('members');
	}
	
	function UpdatePassword($email_to,$new_pass)
	{
		return $this->db->set('password',$new_pass)->where('email',$email_to)->update('members');
	}
	
	function Update_Activity_Date($member_id)
	{
		return $this->db->set('last_activity_date',date('Y-m-d H:i:s'))->where('member_id',$member_id)->update('members');
	}


/*
|-----------------------------------------------------------------------------
| authenticate login info
|-----------------------------------------------------------------------------
*/
	function CheckAuthentication($email,$password)
	{
		$this->db->where('password', md5($password));
		
		$this->db->where('isactive',1);
		
		$this->db->where('email', $email);
		
//		$this->db->or_where('username', $email);
		
		$this->db->from('members');
		
		return $this->db->get()->row_array();
		
				
	//	return $this->db->where(array('email'=>$email,'password'=>md5($password),'isactive'=>1))->from('members')->get()->row_array();
	}
	
	function BanMember($member_id)
	{
		return $this->db->set('ban',1)->where('member_id',$member_id)->update('members');
	}
	
	function AllowMember($member_id)
	{
		return $this->db->set('ban',0)->where('member_id',$member_id)->update('members');
	}

/*
|-----------------------------------------------------------------------------
| create a new free user/member, can also be used for update
|-----------------------------------------------------------------------------
*/	
	function Save($member_id=0, $type='U')
	{
		$this->db->set('username',$this->input->post('username', TRUE));
		
		$this->db->set('email',$this->input->post('email', TRUE));
		
		$this->db->set('password',md5($this->input->post('password', TRUE)));
		
		$this->db->set('security_ans1',$this->input->post('security_ans1', TRUE));
		
		$this->db->set('security_ans2',$this->input->post('security_ans2', TRUE));
		
		$this->db->set('security_ans3',$this->input->post('security_ans3', TRUE));
		

		if($member_id==0){
			
			$this->db->set('type',$type);
			
			$this->db->set('date',date('Y-m-d H:i:s'));
			
			$this->db->set('isactive',1);
			
			$this->db->insert('members');
			
			$member_id = $this->db->insert_id();
			
		}else
			
			$this->db->where('member_id',$member_id)->update('members');
				
		return $member_id;
	}

/*
|-----------------------------------------------------------------------------
| Edit member information from member myaccount page
|-----------------------------------------------------------------------------
*/		
	function SaveMember($member_id, $password='')
	{
		$this->db->set('email',$this->input->post('email', TRUE));
		
		$this->db->set('first_name',$this->input->post('first_name', TRUE));
		
		$this->db->set('last_name',$this->input->post('last_name', TRUE));
		
		$this->db->set('phone',$this->input->post('phone', TRUE));
		
		$this->db->set('state',$this->input->post('state', TRUE));
		
		$this->db->set('country',$this->input->post('country', TRUE));
		
		$this->db->set('city',$this->input->post('city', TRUE));
		
		$this->db->set('zip',$this->input->post('zip', TRUE));
		
		$this->db->set('address',$this->input->post('address', TRUE));
		
		$this->db->set('card_on_file',$this->input->post('card_on_file', TRUE));
		
		if($password !='')
		{
			$this->db->set('password',md5($password));
		}
		
		$this->db->where('member_id',$member_id)->update('members');	
	}
	

	
	function UpdateActivation($member_id,$isactive)
	{
		$this->db->set('isactive',$isactive);
		return $this->db->where('member_id',$member_id)->update('members');
	}
	
	function AdminSaveMember($member_id=0)
	{
		$this->db->set('first_name',$this->input->post('first_name', TRUE));
		$this->db->set('last_name',$this->input->post('last_name', TRUE));
		$this->db->set('email',$this->input->post('email', TRUE));
		if($this->input->post('password', TRUE)!='')
			$this->db->set('password',md5($this->input->post('password', TRUE)));
		$this->db->set('company_name',$this->input->post('company_name', TRUE));
		$this->db->set('phone',$this->input->post('phone', TRUE));	
		if($member_id==0) {
			$this->db->set('date',date('Y-m-d H:i:s'));
			$this->db->set('isactive',1);
			$this->db->insert('members');
			$member_id = $this->db->insert_id();
		}
		else
			$this->db->where('member_id',$member_id)->update('members');		
		
		return $member_id;
	} 
	
	function SendMsgToAdmin($type, $member_id)
	{
		$this->db->set('first_name',$this->input->post('first_name', TRUE));

		$this->db->set('last_name',$this->input->post('last_name', TRUE));

		$this->db->set('email',$this->input->post('email',TRUE));
		
		if($type=='M')
			$this->db->set('subject','New manufacturer added');
		$link = site_url('admin/members/views/'.$member_id);
		$this->db->set('message','New manufacturer added to view details on link <br>'.$link);

		$this->db->set('date',date('Y-m-d H:i:s'));
		$this->db->insert('contacts');
	}
	
	function getMemberByType($type)
	{
		return $this->db->where('type',$type)->get('members')->result_array();
	}
	
/*
|-----------------------------------------------------------------------------
| get cities, countries & states functions accordingly
|-----------------------------------------------------------------------------
*/

	function GetAllCities($member_id)
	{	
		$this->db->select('cities.city_id, cities.city_name')
				->from('cities')
				->join('members','cities.country_id=members.country')
				->where('members.member_id',$member_id)
				->group_by('cities.city_name');	
		return $this->db->get()->result_array();
	}
	
	function GetMemberStates($member_id)
	{
			$query =$this->db->query('select state_id, state_name from states where country_id=(select country from members where member_id='.
										$member_id.' )');
			return $query->result_array();
		
	}
	
	function GetAllCountries()
	{	
		return $this->db->get('countries')->result_array();
	}

/*
|-----------------------------------------------------------------------------
| Check whether excep the given member anyone else has this email
|-----------------------------------------------------------------------------
*/	
	function CheckEmailExcept($skip_id, $email)
	{
		if($skip_id > 0)	
			$this->db->where('member_id <>',$skip_id);
			
		$this->db->where('email', $email);
		
		return $this->db->get('members')->row_array();	
	}
	
	
	
/*
|-----------------------------------------------------------------------------
| Check whether member id & passwor is correct
|-----------------------------------------------------------------------------
*/	
	function CheckOldPassword($member_id, $password)
	{
		$this->db->where('member_id',$member_id);
			
		$this->db->where('password', md5($password));
		
		return $this->db->get('members')->row_array();	
	}

/*
|-----------------------------------------------------------------------------
| returns list of cities based on a particular country id from countries table
|-----------------------------------------------------------------------------
*/		
	function getMyStates($country)
	{
		return $this->db->where('country_id',$country)->get('states')->result_array();
	}
	
	function getSecurityQuestions()
	{
		$this->db->where('is_active',1)
				 ->order_by('RAND()')
				 ->limit(2);
		
		return	$this->db->get('security_questions')->result_array();

	}
	
	function getAllSecurityQuestions()
	{
		$this->db->where('is_active',1);		
		return	$this->db->get('security_questions')->result_array();

	}

/*
|-----------------------------------------------------------------------------
| checks whether security answer are correct if 2 out of 3 correct is fine
|-----------------------------------------------------------------------------
*/	
	function GetSecurityAnswers($q_id_1='', $ans_1='', $q_id_2='', $ans_2='')
	{
		$first='security_ans'.$q_id_1;
		
		$second='security_ans'.$q_id_2;
		
		$this->db->where($first,$ans_1);
		
		$this->db->where($second,$ans_2);
		
		return $this->db->get('members')->row_array();		

	}

	function check_pass_id($member_id, $password)
	{
		$this->db->where('member_id',$member_id);
		
		$this->db->where('password', md5($password));
		
		return $this->db->get('members')->row_array();
	}


/*
|-----------------------------------------------------------------------------
| Update a member with the given array data set values
|-----------------------------------------------------------------------------
*/	
	function update($member_id, $data)
	{
		$this->db->where('member_id', $member_id);
		
		$this->db->update('members', $data);
		
		return TRUE;
	}

/*
|-----------------------------------------------------------------------------
| returns a member if matches with username/ email
|-----------------------------------------------------------------------------
*/	
	function GetIfMember($user)
	{
		$this->db->where('email', $user)
				 ->or_where('username', $user);	
		
		return $this->db->get('members')->row_array();
	}
}



?>
