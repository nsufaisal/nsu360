<?php

class Instructor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function delete_instructor($instructor_id) {
        $member = $this->db->select('i.member_id')
                        ->where('i.instructor_id', $instructor_id)
                        ->from('instructors AS i')
                        ->get()->row_array();
        $member_id = $member['member_id'];
        $this->db->where('member_id', $member_id)->delete('members');
        $this->db->where('instructor_id', $instructor_id)->delete('instructors');

        return TRUE;
    }

    function save($data, $instructor_id = 0) {
        if ($instructor_id > 0) { // update existing notice
            // checks whethere valid noticewall id is provided
            $query = $this->db->get_where('instructor_id', array('instructor_id' => $instructor_id,))->row_array();

            if ($query) {
                $this->db->where('instructor_id', $instructor_id);

                $this->db->update('instructors', $data);
            }

            return TRUE;
        } else {  // add new notice post
            $this->db->insert('instructors', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    function activate_instructor($instructor_id = 0) {
        $inst_data = array(
            'is_active' => 1,
            'added_by' => 1
        );
        $inst_data2 = array(
            'isactive' => 1
        );
        $this->db->where('instructor_id', $instructor_id)
                ->update('instructors', $inst_data);
        $member = $this->db->select('i.member_id')
                        ->where('i.instructor_id', $instructor_id)
                        ->from('instructors AS i')
                        ->get()->row_array();
        $member_id = $member['member_id'];
        $this->db->where('member_id', $member_id)
                ->update('members', $inst_data2);
    }

    function deactivate_instructor($instructor_id = 0) {
        $inst_data = array(
            'is_active' => 0,
            'added_by' => 1
        );
        $inst_data2 = array(
            'isactive' => 0
        );
        $this->db->where('instructor_id', $instructor_id)
                ->update('instructors', $inst_data);
        $member = $this->db->select('i.member_id')
                        ->where('i.instructor_id', $instructor_id)
                        ->from('instructors AS i')
                        ->get()->row_array();
        $member_id = $member['member_id'];
        $this->db->where('member_id', $member_id)
                ->update('members', $inst_data2);
    }

    /*
      |-----------------------------------------------------------------------------
      | Get an individual instructor info for myaccount page or anywhere
      |-----------------------------------------------------------------------------
     */

    function get_department_instructors($department_id = 0) {
        return $this->db->select('i.first_name, i.last_name, i.department_id, i.initial')
                        ->from('instructors AS i')
                        ->join('members AS m', 'm.member_id=i.member_id')
                        ->join('departments AS d', 'i.department_id=d.department_id', 'left')
                        ->where(array('i.department_id' => $department_id))->get()->result_array();
    }

    function get_all_instructors() {
        return $this->db->select('i.first_name, i.last_name, i.department_id, i.initial, i.is_active, i.instructor_id')
                        ->from('instructors AS i')
                        //->join('members AS m', 'm.member_id=i.member_id')
                        ->join('departments AS d', 'i.department_id=d.department_id', 'left')
                        ->get()->result_array();
    }

    function get_instructor($member_id = '') {
        return $this->db->select('m.member_id, m.username, m.nid, m.email, m.phone, i.inst_school_id, i.last_name,
									i.photo, i.first_name,i.initial AS instructor_initial,i.curr_address, i.gender, d.name AS department_name,
									 d.initial AS department_initial, i.department_id, i.blood_group')
                        ->from('members AS m')
                        ->join('instructors AS i', 'm.member_id=i.member_id')
                        ->join('departments AS d', 'i.department_id=d.department_id', 'left')
                        ->where(array('m.member_id' => $member_id, 'm.isactive' => 1))->get()->row_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | update student from myaccount edit page
      |-----------------------------------------------------------------------------
     */

    function update_instructor($member_id, $inst_data) {
        return $this->db->where('member_id', $member_id)
                        ->update('instructors', $inst_data);
    }

    /*
      |-----------------------------------------------------------------------------
      | update instructor profile photo
      |-----------------------------------------------------------------------------
     */

    function change_photo($member_id, $filename) {
        return $this->db->where('member_id', $member_id)
                        ->update('instructors', array('photo' => $filename));
    }

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ACTIVE ABOVE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */













    /*
      |-----------------------------------------------------------------------------
      | Get students of an instructor enrolled current semester
      |-----------------------------------------------------------------------------
     */

    function get_my_students($member_id = '', $selected_course = '', $selected_semester = '') {
        return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
                        ->from('students AS s')
                        ->join('classes AS c', 's.curr_class=c.class_id')
                        ->join('members AS m', 's.member_id=m.member_id')
                        ->order_by('student_id', 'desc')->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Delete a student
      |-----------------------------------------------------------------------------
     */

    function check_studentId($student_id, $skip_id = 0) {
        if ($skip_id > 0)
            $this->db->where('member_id <>', $skip_id);

        return $this->db->where('student_id', $student_id)->get('students')->row_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Delete a student
      |-----------------------------------------------------------------------------
     */

    function delete($member_id) {
        $this->db->where('member_id', $member_id)->delete('students');
    }

    /*
      |-----------------------------------------------------------------------------
      | create/update a new student
      |-----------------------------------------------------------------------------
     */
    /* function Save($instructor_data, $instructor_id=0)
      {
      if($student_id==0)
      {
      $this->db->insert('instructors', $instructor_data);

      return $this->db->insert_id();
      }

      else {
      $this->db->where('instructor_id', $instructor_id);

      $this->db->update('instructors',$instructor_data);

      return TRUE;
      }
      } */
    /*
      |-----------------------------------------------------------------------------
      | Get a student data given id
      |-----------------------------------------------------------------------------
     */

    function GetById($student_id) {
        return $this->db->select('s.*, c.title AS class ')
                        ->where('student_id', $student_id)
                        ->from('students AS s')
                        ->join('classes AS c', 's.curr_class=c.class_id')
                        ->get()->row_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Get students data given by limit offset
      |-----------------------------------------------------------------------------
     */

    function getStudents($limit = 30, $offset = 0) {
        return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
                        ->from('students AS s')
                        ->join('classes AS c', 's.curr_class=c.class_id')
                        ->join('members AS m', 's.member_id=m.member_id')
                        ->order_by('student_id', 'desc')->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Get students data given by limit offset
      |-----------------------------------------------------------------------------
     */

    function disable($member_id) {
        return $this->db->select('s.student_id, s.last_name,s.student_photo, s.first_name,s.curr_section, c.title AS class, s.father_name, s.father_phone, m.email ')
                        ->from('students AS s')
                        ->join('classes AS c', 's.curr_class=c.class_id')
                        ->join('members AS m', 's.member_id=m.member_id')
                        ->order_by('student_id', 'desc')->get()->result_array();
    }

    function GetMemberId($student_id) {
        $this->db->select('s.member_id')
                ->where('student_id', $student_id)
                ->from('students AS s');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->row()->member_id;
    }

}