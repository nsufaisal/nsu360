<?php 
class Semesters_model extends CI_Model
{

    function __construct() // model construc
    {
        parent::__construct();
    }
    
    function getAllSemesters()
    {
            return $this->db->get('semesters')->result_array();
        
    }
    function get_my_semester($semester_id){
        return $this->db->select('s.*')
                        ->from('semesters AS s')
                        ->where('s.semester_id', $semester_id)
                        ->get()->row_array();
    }
    function current_semester(){
        $current = $this->db->select('s.*')
                        ->from('semesters AS s')
                        ->where('s.is_current', 1)
                        ->get()->row_array();
            return isset($current) ? $current : null;
        }
    
        function save($data, $semester_id = 0) {
        if ($semester_id > 0) { 

            $query = $this->db->get_where('semesters', array('semester_id' => $semester_id,))->row_array();

            if ($query) {
                $this->db->where('semester_id', $semester_id);

                $this->db->update('semesters', $data);
            }

            return TRUE;
        } else {  // add new notice post
            $this->db->insert('semesters', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
    function delete($semester_id) {

        $this->db->where('semester_id', $semester_id)->delete('semesters');

        return TRUE;
    }
        function activate_semester($semester_id = 0) {
        $inst_data = array(
            'is_current' => 1,
        );
        $this->db->where('semester_id', $semester_id)
                ->update('semesters', $inst_data);
        $title = $this->get_my_semester($semester_id);
        $sem_title = $title['title'];
        $this->load->model('settings_model');
        $this->settings_model->updateSettingByName('curr_semester_id', 1, $sem_title);
    }

    function deactivate_semester($semester_id = 0) {
        $inst_data = array(
            'is_current' => 0,
        );
        $this->db->where('semester_id', $semester_id)
                ->update('semesters', $inst_data);
        $this->load->model('settings_model');
        $this->settings_model->updateSettingByName('curr_semester_id', 0, "");
    }
}