<?php

class Courses_model extends CI_Model {

    function __construct() { // model construc
        parent::__construct();
    }

    function delete($course_id) {

        $this->db->where('course_id', $course_id)->delete('courses');

        return TRUE;
    }
	function getCourseListTitle()
	{
		return   $this->db->select('course_id, c.initial AS course_initial, c.title AS course_title, c.department_id,d.initial AS department')
				 ->from('courses AS c')
				 ->join('departments AS d', 'c.department_id=d.department_id')
				 ->where('c.is_active', 1)
                 ->order_by('c.title', 'asc')
                 ->get()->result_array();
    }


    function get_courses() {

        return $this->db->get('courses')->result_array();
    }

    function save($data, $course_id = 0) {
        if ($course_id > 0) { // update existing notice
            // checks whethere valid noticewall id is provided

            $query = $this->db->get_where('courses', array('course_id' => $course_id,))->row_array();

            if ($query) {
                $this->db->where('course_id', $course_id);

                $this->db->update('courses', $data);
            }

            return TRUE;
        } else {  // add new notice post
            $this->db->insert('courses', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of courses conducted by an instructor of a semester
      |-----------------------------------------------------------------------------
     */

    function get_detail_instructor_courses($member_id, $semester_id) {
        return $this->db->select('sc.id, sc.section_no,sc.room_no, c.initial AS course_initial, c.title AS course_title,
						 ins.initial AS instructor_initial,ins.member_id AS instructor_id, ct.class_day, ct.class_time')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->join('instructors AS ins', 'ins.instructor_id=sc.instructor_id', 'left')
                        ->join('class_times AS ct', 'sc.class_time_id=ct.time_id', 'left')
                        ->where('sc.semester_id', $semester_id)
                        ->where('sc.instructor_id', $member_id)
                        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->order_by('sc.section_no', 'asc')
                        ->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of avaiable courses to be conducted by an instructor
      | exclude the courses he already taken
      |-----------------------------------------------------------------------------
     */

    function get_instructor_available_courses($member_id, $semester_id) {
        return $this->db->select('sc.id, sc.section_no,sc.instructor_id, c.initial AS course_initial, c.title AS course_title,
						 ct.class_day, ct.class_time')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->join('class_times AS ct', 'sc.class_time_id=ct.time_id', 'left')
                        ->where('sc.instructor_id', NULL)
                        ->where('sc.semester_id', $semester_id)
                        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->order_by('sc.section_no', 'asc')
                        ->get()->result_array();
    }

    function remove_instructor_course($member_id, $semester_course_id) {
        $data = array('instructor_id' => NULL,);

        $this->db->where('id', $semester_course_id);

        $this->db->update('semester_course', $data);

        return TRUE;
    }

    /*
      |-----------------------------------------------------------------------------
      | assign instructor to a course
      |-----------------------------------------------------------------------------
     */

    function assign_instructor_course($member_id, $semester_course_id) {
        $data = array('instructor_id' => $member_id,);

        $this->db->where('id', $semester_course_id);

        return $this->db->update('semester_course', $data);
    }

    /*
      |-----------------------------------------------------------------------------
      | remove a course from course_enrole table
      |-----------------------------------------------------------------------------
     */

    function remove_enrole_course($member_id, $semester_course_id) {
        return $this->db->delete('course_enrole', array('student_id' => $member_id, 'semester_course_id' => $semester_course_id));
    }

    /*
      |-----------------------------------------------------------------------------
      | Enrole a student to a course
      |-----------------------------------------------------------------------------
     */

    function enrole_student($member_id = 0, $semester_course_id = 0, $approval = 0) {
        $data = array(
            'student_id' => $member_id,
            'semester_course_id' => $semester_course_id,
            'approval' => $approval,
        );

        return $this->db->insert('course_enrole', $data);
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of avaiable courses to be take by a student
      | exclude the courses he already taken
      |-----------------------------------------------------------------------------
     */

    function get_available_courses($member_id, $semester_id) {
        return $this->db->select('sc.id, sc.section_no, c.initial AS course_initial, c.title AS course_title,
						 ins.initial AS instructor_initial,ct.class_day, ct.class_time')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->join('instructors AS ins', 'ins.instructor_id=sc.instructor_id', 'left')
                        ->join('class_times AS ct', 'sc.class_time_id=ct.time_id', 'left')
                        ->where('sc.id NOT IN (SELECT `semester_course_id` FROM `course_enrole`  WHERE `student_id` =' . $member_id . ')', NULL, FALSE)
                        ->where('sc.semester_id', $semester_id)
                        ->where('sc.student_limit >', 'sc.total_enrole')
                        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->order_by('sc.section_no', 'asc')
                        ->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of courses taken by a student of a semester
      |-----------------------------------------------------------------------------
     */

    function get_student_courses($member_id, $semester_id) {
        return $this->db->select('sc.id, sc.section_no,sc.room_no, c.initial AS course_initial, c.title AS course_title,
						 ins.initial AS instructor_initial,ins.member_id AS instructor_id, ct.class_day, ct.class_time, en.approval')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->join('instructors AS ins', 'ins.instructor_id=sc.instructor_id', 'left')
                        ->join('class_times AS ct', 'sc.class_time_id=ct.time_id', 'left')
                        ->join('course_enrole AS en', 'sc.id=en.semester_course_id', 'left')
                    //    ->where('sc.id IN (SELECT `semester_course_id` FROM `course_enrole`  WHERE `student_id` =' . $member_id . ')', NULL, FALSE)
                  //     ->where('sc.semester_id', $semester_id)
                 //       ->where('sc.student_limit >', 'sc.total_enrole')
                //        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->order_by('sc.section_no', 'asc')
                        ->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of courses of a semester
      |-----------------------------------------------------------------------------
     */

    function get_semester_courses($semester_id) {
        return $this->db->select('sc.*, c.*')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->where('sc.semester_id', $semester_id)
                        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of courses of a DEPARTMENT
      |-----------------------------------------------------------------------------
     */

    function get_department_courses($department_id = 0) {
        return $this->db->select('c.*')
                        ->from('courses c')
                        ->join('departments d', 'd.department_id=c.department_id', 'left')
                        ->where('c.department_id', $department_id)
                        ->get()->result_array();
    }

    function getCoursesByClassId($class_id) {
        //echo"course model";
        return $this->db->select('c.*')
                        ->from('courses AS c')
                        ->where('c.class_id', $class_id)
                        ->order_by('c.course_id', 'asc')
                        ->get()->result_array();
    }

    /*
      |-----------------------------------------------------------------------------
      | Return list of courses of an instructor for a semester
      |-----------------------------------------------------------------------------
     */

    function get_instructor_courses($member_id, $semester_id) {
        return $this->db->select('sc.*, c.*')
                        ->from('semester_course AS sc')
                        ->join('courses AS c', 'sc.course_id=c.course_id', 'inner')
                        ->where('sc.semester_id', $semester_id)
                        ->where('sc.instructor_id', $member_id)
                        ->where('sc.is_active', 1)
                        ->order_by('c.initial', 'asc')
                        ->get()->result_array();
    }

    function get_my_course($course_id) {
        return $this->db->select('c.*')
                        ->from('courses AS c')
                        ->where('c.course_id', $course_id)
                        ->get()->row_array();
    }

    function check_course($course_initial) {
        return $this->db->where('initial', $course_initial)->get('courses')->row_array();
    }

}

?>