<?php 
class Public_Users_Model extends CI_Model
{

	function __construct()
    {
        parent::__construct();
    }
	
	function get_user_by_id($id){
		if(is_numeric($id)){
			return $this->db->where('member_id',$id)->get('members')->row_array();
		}
	}
	
	function get_publicusers(){
            
		
		return $this->db->get('members')->result_array();
                
	}
	function del_user($id){
		if(is_numeric($id)){
                          $this->db->where('member_id',$id)->delete('personal_info');
                          $this->db->where('member_id',$id)->delete('personal_exp');
			return $this->db->where('member_id',$id)->delete('members');
		}
	}
        
        
     function  getexportdetails(){
         $this->load->dbutil();
        $this->db->select('M.first_name as FirstName,M.last_name as LastName,M.company_name as CompanyName,M.type as Type,M.phone as PhoneNumber,M.email as Email');
        $this->db->from('members as M');
        $query=$this->db->get();
         $delimiter = ",";
        $newline = "\r\n";



        $result = $this->dbutil->csv_from_result($query, $delimiter, $newline);

        return $result;
     }
        
        
        
	
	function CheckAdminAuthentication($email, $password)
	{
		return $this->db->where(array('email'=>$email,'password'=>md5($password),'active'=>1))->from('admin_users')->get()->row_array();
	}
	
	function CheckAdminUser($username, $user_id=0)
	{
		return $this->db->where(array('email'=>$username, 'user_id <>'=>$user_id))->from('admin_users')->get()->row_array();
	}
	
	function UpdateAdminPassword($email,$password)
	{
		$this->db->set('password',md5($password));
		$this->db->where('email',$email);
		$this->db->update('admin_users');
		return true;
	}
	
	function SaveAdminUser($first_name, $last_name, $email , $password, $user_id, $active)
	{
		$this->db->set('first_name',$first_name);
		$this->db->set('last_name',$last_name);
		$this->db->set('email',$email);
		if($password!='')
			$this->db->set('password',md5($password));
			
		$this->db->set('active',$active);
		
		if ($user_id==0){
	  		$this->db->insert('admin_users');
		} else {
	  		$this->db->where('user_id',$user_id);
			$this->db->update('admin_users');
		}
		
		return TRUE;
	}
        
        
        
        
}
?>
