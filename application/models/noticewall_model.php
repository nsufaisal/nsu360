<?php

class Noticewall_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| checks whether a post is posted by a given author
|-----------------------------------------------------------------------------
*/	
	function is_my_course_notice($member_id, $noticewall_id)
	{
		$query=$this->db->select('nc.noticewall_id')
						->from('notice_course AS nc')
						->join('course_enrole AS cn','nc.semester_course_id=cn.semester_course_id','inner')
						->where('cn.student_id',$member_id)
						->where('nc.noticewall_id',$noticewall_id)
						->get();
		
		if($query->num_rows()>0)
			return TRUE;
		else {
			return FALSE;
		}
		
	}
/*
  |-----------------------------------------------------------------------------
  | returns list of notices of the courses taken by student in given semester
  |-----------------------------------------------------------------------------
 */	
	function get_student_course_notices($member_id, $semester_id)
	{
		return $this->db->select('nw.noticewall_id, nw.title, nw.notice_body,nw.post_date, nw.semester_course_ids, 
									sc.section_no, cs.initial AS course_initial')
						->from('noticewall AS nw')
						->join('notice_course AS nc', 'nc.noticewall_id=nw.noticewall_id','inner')
						->join('semester_course AS sc', 'sc.id=nc.semester_course_id')
						->join('courses AS cs', 'sc.course_id=cs.course_id','inner')
						->join('course_enrole AS cn', 'sc.id=cn.semester_course_id','inner')
						->join('instructors AS ins', 'sc.instructor_id=ins.member_id','left')
						->where('cn.student_id', $member_id)
						->where('nw.semester_id',$semester_id)
						->group_by('nc.noticewall_id')
						->order_by('nw.post_date', 'desc')
						->get()->result_array();
		
	}

/*
  |-----------------------------------------------------------------------------
  | remove/disable a  notice by noticewall id
  |-----------------------------------------------------------------------------
 */

    function remove($noticewall_id)
    {
        $this->db->where('noticewall_id', $noticewall_id);

        return $this->db->delete('noticewall');
    }

/*
  |-----------------------------------------------------------------------------
  | Returns a notice conten by the notice_id
  |-----------------------------------------------------------------------------
 */

    function is_my_notice($noticewall_id, $member_id)
    {
        $this->db->where('author_id', $member_id);

        $this->db->where('noticewall_id', $noticewall_id);

        return $this->db->get('noticewall')->row_array();
    }
		
/*
|-----------------------------------------------------------------------------
| get an instructor's notice for edit etc
|-----------------------------------------------------------------------------
*/	
	function get_my_notice($noticewall_id, $member_id)
	{
		return $this->db->select('nw.*')
                ->from('noticewall nw')
				->where('nw.noticewall_id', $noticewall_id)
				->where('nw.author_id', $member_id)
				->get()->row_array();
}
/*
|-----------------------------------------------------------------------------
| get public notices for front page
|-----------------------------------------------------------------------------
*/	
	function get_public_notices($semester_id)
	{
		return $this->db->select('nw.title,nw.post_date,nw.notice_body, nw.noticewall_id')
	            ->from('noticewall nw')
				->where('nw.is_public_post', 1)
				->order_by('nw.post_date','desc')
				->get()->result_array();
	}
/*
|-----------------------------------------------------------------------------
| Check if a notice is for public view
|-----------------------------------------------------------------------------
*/	
	function is_public_post($noticewall_id=0)
	{
		$query=$this->db->select('is_public_post')->get_where('noticewall', array('noticewall_id'=>$noticewall_id))->row_array();
		
		if($query['is_public_post']==1)
			return TRUE;
		else {
			return FALSE;
		}
	}
/*
|-----------------------------------------------------------------------------
| get top course notices for home page public notice
|-----------------------------------------------------------------------------
*/	
	function get_public_course_notice($limit=10)
	{
		return $this->db->select('nw.title,nw.post_date, nw.noticewall_id')
				->limit($limit)
	            ->from('noticewall nw')
				->where('nw.is_public_post', 1)
				->order_by('nw.post_date','desc')
				->get()->result_array();
	}	
/*
|-----------------------------------------------------------------------------
| get an individual notice post
|-----------------------------------------------------------------------------
*/		
	function get_notice_post($noticewall_id)
	{
		return $this->db->select('nw.title, nw.post_date, nw.notice_body, nw.noticewall_id')
		                ->from('noticewall nw')
						->where('nw.noticewall_id', $noticewall_id)
						->get()->row_array();
	}

/*
|-----------------------------------------------------------------------------
| get courses that are covered by the notice post
|-----------------------------------------------------------------------------
*/		
	function get_notice_courses($noticewall_id)
	{
		return $this->db->select('sc.id AS semester_course_id,sc.section_no, cs.initial, cs.title')
						->from('semester_course AS sc')
						->join('courses AS cs', 'sc.course_id=cs.course_id')
						->join('notice_course AS nc', 'nc.semester_course_id=sc.id')
						->where('nc.noticewall_id', $noticewall_id)
						->order_by('cs.initial', 'aesc')
						->order_by('sc.section_no', 'aesc')
						->get()->result_array();
	}
/*
|-----------------------------------------------------------------------------
| checks whether a post is posted by a given author
|-----------------------------------------------------------------------------
*/	
	function is_my_post($member_id, $noticewall_id)
	{
		$query=$this->db->select('noticewall_id')->get_where('noticewall', array('noticewall_id'=>$noticewall_id, 'author_id'=>$member_id));
		
		if($query->num_rows()>0)
			return TRUE;
		else {
			return FALSE;
		}
		
	}
/*
|-----------------------------------------------------------------------------
| Get Notices written by instructors/sadmin
|-----------------------------------------------------------------------------
*/		
	function get_my_notices($member_id=0, $semester_id=0)
	{
		return $this->db->select('nw.noticewall_id,nw.title, nw.notice_body, nw.semester_course_ids,nw.post_date, nw.is_public_post')
		 ->from('noticewall AS nw')
		 ->where('nw.semester_id',$semester_id)
		 ->where('nw.author_id',$member_id)
		 ->where('nw.is_active',1)
		 ->order_by('nw.post_date','desc')
		 ->get()->result_array();		
	}
		
/*
|-----------------------------------------------------------------------------
| Save or update a notice
|-----------------------------------------------------------------------------
*/		
	function save($post=array(), $member_id=0, $noticewall_id=0)
	{
		$is_public=0;
		$courses='';
		if(isset($post['courses']))
		{
			foreach($post['courses'] as $course)
			if($course=='public_wall')
				$is_public=1;
			else {
				$courses .=$course.';';
			}
		}
			{
		$semester_id = $this->session->userdata('semester_id');
		
		date_default_timezone_set('Asia/Dacca');
		
		$data=array(
			'title'=> $post['title'],
			'author_id' =>$member_id,
			'notice_type' =>$post['notice_type'],
			'notice_body' =>$post['notice_body'],
			'semester_id' =>$semester_id,
			'semester_course_ids'=>$courses,
			'post_date'	=>time(),
			'is_public_post'=>$is_public,		
		);
		if($noticewall_id>0) // update existing notice
		{
			// checks whethere valid noticewall id is provided
			
			$query=$this->db->get_where('noticewall', array('noticewall_id'=> $noticewall_id,))->row_array();
			
			if($query)
			{
				$this->db->where('noticewall_id', $noticewall_id);
			
				$this->db->update('noticewall', $data);
				
				$this->db->select('semester_course_ids');
				
				if($query['semester_course_ids']!=$courses) // course selection chaged
				{
					$this->db->where('noticewall_id', $noticewall_id);
				
					$this->db->delete('notice_course');
					
					if(isset($post['courses']))
					{		
						foreach($post['courses'] as $course)
						if($course !='public_wall')
						{
							$data= array(
								'noticewall_id'=>$noticewall_id,
								'semester_course_id'=>$course,					
							);
							
							$this->db->insert('notice_course',$data);
						}
					 }
				}
				
				
				
				
			}
			
			return TRUE;
		}
		else{  // add new notice post
			
			$this->db->insert('noticewall', $data);
		
			$insert_id=$this->db->insert_id();
			
			if(isset($post['courses']))
			{
				foreach($post['courses'] as $course)
				if($course !='public_wall')
				{
					$data= array(
						'noticewall_id'=>$insert_id,
						'semester_course_id'=>$course,					
					);
					
					$this->db->insert('notice_course',$data);
				}
			}
			return $insert_id;
		}		
		
		
	}
}
}