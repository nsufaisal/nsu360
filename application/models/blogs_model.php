<?php 
class Blogs_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| Returns a single blog from the blogs table
|-----------------------------------------------------------------------------
*/
	function getBlogByID($blog_id='')
	{
		$this->db->select('b.*, c.category AS blog_category')
		 ->where('blog_id', $blog_id)
		 ->from('blogs AS b')
		 ->join('blog_categories as c', 'b.category_id=c.category_id');

		return $this->db->get()->row_array();
	}

/*
|-----------------------------------------------------------------------------
| returns list of blogs
|-----------------------------------------------------------------------------
*/	
	function getAllBlogs(){
		$this->db->select('b.*, c.category AS blog_category')
		 ->from('blogs AS b')
		 ->join('blog_categories as c', 'b.category_id=c.category_id')
		 ->order_by('b.post_date', 'desc');

		return $this->db->get()->result_array();
	}

/*
|-----------------------------------------------------------------------------
| Insert or update a new blog 
|-----------------------------------------------------------------------------
*/	
	function save($blog_id,$data)
	{
			if ($blog_id!=0){
				$this->db->where('blog_id',$blog_id);	
								
				$this->db->update('blogs',$data);
			} else 				
				$this->db->insert('blogs',$data);			
			return TRUE;			
		
	}

/*
|-----------------------------------------------------------------------------
| Delete a blog in the admin blog  controller
|-----------------------------------------------------------------------------
*/	
	function delete($blog_id){
		
		$this->db->where('blog_id',$blog_id)->delete('blogs');
		
		return TRUE;
	}

}