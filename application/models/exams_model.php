<?php 
class Exams_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| Return list of a course(s) exams conducted by an instructor of a semester
|-----------------------------------------------------------------------------
*/
	function get_all_exams($member_id, $semester_id, $course_id)
	{
	   $this->db->select('ex.exam_id, ex.title,ex.weight,ex.venue, ex.description,ex.exam_mark, ex.exam_date, cs.initial AS course, sc.section_no')
				->from('exams AS ex')
				->join('semester_course AS sc','sc.id=ex.semester_course_id','inner')
				->join('courses AS cs','cs.course_id=sc.course_id','inner')
				->where('sc.instructor_id',$member_id)
				->where('sc.semester_id',$semester_id);
						
		if($course_id>0)
		{
			$this->db->where('sc.course_id',$course_id);			
		}				
		
		return $this->db->order_by('ex.exam_date','desc')
						->get()->result_array();						
		
	}



}

