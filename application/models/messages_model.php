<?php 
class Messages_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }

	function send_message($msg_data)
	{
		$this->db->insert('messages',$msg_data);
		
		return true;
	}

/*
|-----------------------------------------------------------------------------
| Returns all new messages message conten by the message_id
|-----------------------------------------------------------------------------
*/	
	function getNewMessages($member_id)
	{
		$this->db->where('receipient_id',$member_id);
		
		$this->db->where('is_read',0); /* only unread messages */
		
		return $this->db->get('messages')->result_array();
	}

/*
|-----------------------------------------------------------------------------
| Returns a message conten by the message_id
|-----------------------------------------------------------------------------
*/	
	function isMyMessage($message_id, $member_id)
	{
		$this->db->where('receipient_id', $member_id);
		
		$this->db->where('message_id', $message_id);
		
		return $this->db->get('messages')->row_array();
	}

/*
|-----------------------------------------------------------------------------
| Returns a message conten by the message_id
|-----------------------------------------------------------------------------
*/	
	function getMessageById($message_id)
	{
		return $this->db->select('msg.*, m.first_name sFirstName, m.last_name sLastName')
				 ->from('messages AS msg')
				 ->join('members AS m','msg.sender_id=m.member_id')
				 ->where('msg.message_id',$message_id)
				 ->get()->row_array();
	}
	
/*
|-----------------------------------------------------------------------------
| Returns all inbox messages message conten by the message_id
|-----------------------------------------------------------------------------
*/	
	function getInboxMessages($member_id)
	{
		return $this->db->select('message_id,subject,create_date,is_read, m.last_name sLastName, m.first_name sFirstName')
				 ->from('messages')
				 ->join('members AS m','sender_id=m.member_id','left')
				 ->where('receipient_id',$member_id)
				 ->where('message_type',1)
				 ->where('is_active',1)
				 ->order_by('create_date','desc')
				 ->get()->result_array();
	}

/*
|-----------------------------------------------------------------------------
| set a message as read
|-----------------------------------------------------------------------------
*/	
	function setReadStatus($message_id)
	{
		$this->db->where('message_id',$message_id)->set('is_read',1)->update('messages');
		
		return TRUE;
	}

/*
|-----------------------------------------------------------------------------
| set a message as read
|-----------------------------------------------------------------------------
*/	
	function setIsActive($message_id, $is_active)
	{
		if(!empty($message_id))
		{
			$this->db->where('message_id', $message_id)
					 ->set('is_active',$is_active)
					 ->update('messages');			
			return TRUE;
		}
	}

}
?>
