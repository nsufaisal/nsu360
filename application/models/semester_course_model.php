<?php 
class Semester_Course_Model extends CI_Model
{

    function __construct() // model construc
    {
        parent::__construct();
    }
        function delete($department_id){
        
        $this->db->where('department_id',$department_id)->delete('departments');
        
        return TRUE;
    }
    function getAllSemesterCourses()
    {
            return $this->db->get('semester_course')->result_array();
        
    }
    function get_my_department($department_id)
    {
            return $this->db->select('d.*')
                ->from('departments AS d')
                ->where('d.department_id', $department_id)
                ->get()->row_array();
    }
/*
|--------------------------------------------------------------------------
| returns list of degree majors associated with a department_id
|--------------------------------------------------------------------------
*/  
    function get_degree_major($department_id)
    {
        return $this->db->select('dm.major_id, dm.name, dm.initial')
                        ->from('degree_majors AS dm')
                        ->where(array('dm.department_id'=>$department_id, 'dm.is_active'=>1))
                        ->get()->result_array();
    }

/*
|--------------------------------------------------------------------------
| returns list of all degree majors
|--------------------------------------------------------------------------
*/  
    function get_all_degree_major()
    {
        return $this->db->select('dm.major_id, dm.name, dm.initial')
                        ->from('degree_majors AS dm')
                        ->where('dm.is_active',1)
                        ->get()->result_array();
    }
    
    function get_parent_school(){
        return $this->db->distinct()
                                        ->select('d.parent_school')
                                        ->from('departments AS d')
                                        ->where('d.is_active', 1)
                                        ->get()->result_array();
    }
    
    
     function save($data, $department_id=0)
    {        
        if($department_id>0) 
        {
            
            $query=$this->db->get_where('departments', array('department_id'=> $department_id,))->row_array();
            
            if($query)
            {
                $this->db->where('department_id', $department_id);
            
                $this->db->update('departments', $data);              
            }
            
            return TRUE;
        }
        else{  // add new notice post
            
            $this->db->insert('departments', $data);
            $insert_id=$this->db->insert_id();
            return $insert_id;
        }       
        
        
    }
}

?>