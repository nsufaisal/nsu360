<?php 
class faq_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }
	function get($fields=''){
		if($fields!='')
			$this->db->order_by($fields,$this->session->userdata('sorttype'));
			
		return $this->db->get('faq')->result_array();
	}
	function get_record_by_id($faq_id){
		if($faq_id==0)
			$faq = array_fill_keys($this->db->list_fields('faq'), '');
		else
			$faq = $this->db->where('faq_id',$faq_id)->get('faq')->row_array();
		 
		 return $faq;
	}
	
	function Save($faq_id)
	{
		$this->form_validation->set_rules('question', 'Question', 'required|xss_clean|trim');
		$this->form_validation->set_rules('response', 'Response', 'required|xss_clean|trim');
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		
		if ($this->form_validation->run() != FALSE){
			$this->db->set('question', $this->input->post('question')); 
			$this->db->set('response', $this->input->post('response')); 

			if ($faq_id!=0){
				$this->db->where('faq_id',$faq_id);	
				$this->db->update('faq');
			} else 
				$this->db->insert('faq');
			return TRUE;			
		}
		else
		{
			$this->data['message'] = validation_errors();
			return FALSE;
		}
	}
	function delete($faq_id){
		$this->db->where('faq_id',$faq_id)->delete('faq');
		return TRUE;
	}
}
?>
