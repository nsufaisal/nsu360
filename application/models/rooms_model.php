<?php 
class Rooms_model extends CI_Model
{

    function __construct() // model construc
    {
        parent::__construct();
    }

    function getAllRooms()
    {
            return $this->db->get('room')->result_array();
        
    }
    function getAllRoomType(){
                return $this->db->select('rt.*')
                        ->from('room AS r')
                        ->join('room_type AS rt', 'r.type_id=rt.room_type_id', 'inner')
                        ->order_by('r.initial', 'asc')
                        ->get()->result_array();
    }
        function getAllRoomTypes(){
                return $this->db->select('rt.*')
                        ->from('room AS r')
                        ->join('room_type AS rt', 'r.type_id=rt.room_type_id', 'right')
                        ->order_by('r.initial', 'asc')
                        ->get()->result_array();
    }
     function delete($room_id){
        
        $this->db->where('room_id',$room_id)->delete('room');
        
        return TRUE;
    }
     function get_my_room($room_id)
    {
            return $this->db->select('r.*')
                ->from('room AS r')
                ->where('r.room_id', $room_id)
                ->get()->row_array();
    }
        function save($data, $room_id = 0) {
        if ($room_id > 0) { 
            $query = $this->db->get_where('room', array('room_id' => $course_id,))->row_array();

            if ($query) {
                $this->db->where('room_id', $room_id);

                $this->db->update('room', $data);
            }

            return TRUE;
        } else {  // add new notice post
            $this->db->insert('room', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
}