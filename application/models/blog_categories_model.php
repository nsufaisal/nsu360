<?php 
class Blog_categories_model extends CI_Model
{

	function __construct() // model construc
    {
        parent::__construct();
    }

/*
|-----------------------------------------------------------------------------
| Returns all the parent Categories & Parent Subcategories but not ssc
|-----------------------------------------------------------------------------
*/
	function getParentCategories()
	{		
		return $this->db->not_like('type',3)->order_by('type')->get('blog_categories')->result_array();
	}
	function getBlogCategories(){
		
		$q_str1 ='SELECT	c.category as category,sc.category as sub_category,ssc.category as subsub_category,
						c.category_id AS c_id, sc.category_id AS sc_id, ssc.category_id AS ssc_id
						from (  select  category,category_id
						        from    blog_categories
						        where   type = 1
						        order by parent_id
						        ) as c
						join    blog_categories as sc on c.category_id=sc.parent_id
						left join    blog_categories as ssc
						on      sc.category_id=ssc.parent_id
						order by c.category';

		$q_str2='SELECT category, category_id AS c_id FROM `blog_categories` WHERE parent_id=0 
					and category_id not in(select distinct parent_id from blog_categories where
					parent_id !=0)';
 
		$resutl_arr= $this->db->query($q_str1)->result_array();
		
		$result_arr2=$this->db->query($q_str2)->result_array();
		$arr_add=array();
		$i=0;
		foreach($result_arr2 as $row)
		{
			$arr_add[$i]['category']=$row['category'];
			$arr_add[$i]['sub_category']=NULL;
			$arr_add[$i]['subsub_category']=NULL;
			$arr_add[$i]['c_id']=$row['c_id'];
			$arr_add[$i]['sc_id']=NULL;
			$arr_add[$i]['ssc_id']=NULL;
		
			$i++;			
		}
		$result = array_merge((array)$resutl_arr, (array)$arr_add);
		
		return $result;
	}
	
	function getCategoryByID($category_id='')
	{
		return $this->db->where('category_id',$category_id)->get('blog_categories')->row_array();
	}

/*
|-----------------------------------------------------------------------------
| Insert or update a new blog category
|-----------------------------------------------------------------------------
*/	
	function save($category_id,$data)
	{
			if ($category_id!=0){
				$this->db->where('category_id',$category_id);	
								
				$this->db->update('blog_categories',$data);
			} else 
				
				$this->db->insert('blog_categories',$data);
			
			return TRUE;			
		
	}

/*
|-----------------------------------------------------------------------------
| Delete a blog category in the admin blog categories controller
|-----------------------------------------------------------------------------
*/	
	function delete($category_id){
		
		$this->db->where('category_id',$category_id)->delete('blog_categories');
		
		return TRUE;
	}
}