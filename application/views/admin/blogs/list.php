<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">Admin Blog Controller</h1>

<a href="<?=site_url('admin/blog/edit/0')?>" class="top_btn right" style="margin-left:25px;">Create New</a>

<div class="right top_search">

<div class="left cat_div" id="cat_select_bttn">
Category
<a href="#"><img src="<?=base_url('images/admin/p5_drop_down_bg.jpg')?>" width="10" height="7" alt=" " /></a>
</div>
<!--
<input name="" type="text" value="Search" />
-->
<select class="input-field" style="width:240px; display: inline-block;background:transparent;border:none; appearance:none; -webkit-appearance:none; -moz-appearance:none; 
	 cursor:pointer;" onclick="runThis()" name="blog_category" id="fire">
	<option value="">Select</option>
	<option value="1">Option 1</option>
	<option value="2">Option 2</option>
</select>

<input name="" type="submit" value="&nbsp;" class="srch_sbmt" />

<div class="clear"></div>
</div>

<div class="clear"></div>
</div>

<div class="ymp_table">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="100" scope="col">Date</th>
    <th width="200" scope="col">Title</th>
    <th width="120" scope="col">Author</th>
    <th width="300" scope="col">Excerpt</th>
    <th width="115" scope="col">Category</th>
    <th scope="col">Actions</th>
  </tr>
 
<?php  foreach($blogs as $blog){ ?>	
  <tr>
    <td style="padding-left:30px;"><?php echo $blog['post_date']; ?></td>
    <td><?php echo $blog['title']; ?></td>
    <td><?php echo $blog['author']; ?></td>
    <td><?php echo $blog['content']; ?></td>
    <td><?php echo $blog['blog_category']; ?></td>
    <td class="action"><a href="<?php echo site_url('admin/blog/view/'.$blog['blog_id']);?>">view</a>  |<a href="<?php echo site_url('admin/blog/edit/'.$blog['blog_id']);?>">Edit</a>  |  
			<a href="<?php echo site_url('admin/blog/delete/'.$blog['blog_id'])?>" title="Are you sure you want to delete this blog?" class="confirm_action">Remove</a></td>
  </tr>
       <?php } ?>

</table>

  </div>

</div>

</div>	<!-- end page body -->
<script>
	showDropdown = function (element) {
    var event;
    event = document.createEvent('MouseEvents');
    event.initMouseEvent('mousedown', true, true, window);
    element.dispatchEvent(event);
};

/* This isn't magic.*/
window.runThis = function () { 
    var dropdown = document.getElementById('dropdown');
    showDropdown(dropdown);
};
</script>
<script>
	$("document").ready(function() {
		//alert("good");
		$("#cat_select_bttn").click(function() {
			$("#blog_category").focus();
			$("#blog_category").click();
		});
	});
</script>