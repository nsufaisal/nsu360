
<div id="page_body">

<div class="center">

<div class="admin_title">
<h1>
	<?=($edit=='new')?'Admin Create New Blog Entry':'Admin Edit Blog Engry'?>
</h1>

</div>


<div class="admin_cont admin_form acnb_form">
<form action="<?=site_url('admin/blog/save/'.$blog_id)?>" enctype="multipart/form-data" method="post" name="pageForm" id="pageForm">

<p>
	<label>Title:</label> 
    <input type="text" name="title" id="title" class="input_field " value="<?=isset($blog['title'])?$blog['title']:''?>" />
</p>
<?=form_error('title')?>
<p>
	<label>Author:</label> 
    <input type="text" name="author" id="author" class="input_field " value="<?=isset($blog['author'])?$blog['author']:''?>" />
</p>
<?=form_error('author')?>
<p>
	<label>Category:</label> 
	<select name="category_id" id="category_id" class="input_field">
	<option value="">Select</option>
	
	<?php

foreach($parent_categories as $parent)
{
	if(isset($blog))
	{
		if($parent['category_id']==$blog['category_id']){
			$selected='selected="selected"';
			echo '<option  value="'.$parent['category_id'].'"'.$selected.'>'.$parent['category'].'</option>';
		}
		else{
			echo '<option  value="'.$parent['category_id'].'">'.$parent['category'].'</option>';
		}	
	}
	else {
			echo '<option  value="'.$parent['category_id'].'">'.$parent['category'].'</option>';
	}
	
	
}
?>
	</select>
</p>
<?=form_error('category_id')?>
<p>
	<label>Date:</label> 
    <input type="text" name="post_date" id="datepicker" class="input_field datepicker" value="<?=isset($blog['post_date'])?$blog['post_date']:''?>" />
</p>
<?=form_error('post_date')?>
<p>
	<label>Video (link):</label> 
    <input type="text" name="video_link" id="video_link" class="input_field" value="<?=isset($blog['video_link'])?$blog['video_link']:''?>" /> 
</p>
<?=form_error('video_link')?>
<p>
	<label>Images:</label> 
	<input type="text"   name="file_name_box_1" onfocus="javascript:openBrowse1();" id="file_name_box_1" style="width:343px;" class="input_field" value="<?=isset($blog['image_file'])?$blog['image_file']:''?>">&nbsp;
	<input type="button" onclick="javascript:openBrowse1();" name="uploadButton" id="uploadButton" class="btn" value="Browse"> 
	<?PHP if (form_error('file_name_box_1')) { ?><div><?php echo form_error('file_name_box_1'); ?></div><?PHP } ?>
	</p>
<div id="err_userfile_1"></div>
<p>
	<label>Content:</label> 
    <span class="left">
	
    <textarea name="content" style="width:100%">
    	<?=isset($blog['content'])?$blog['content']:''?>
    </textarea>
	
</p>
</br>
<?=form_error('content')?>
<div class="clear"></div>
<!-- hidden file uploaders -->
<input type="file" name="userfile_1" id="userfile_1" style="display:none;"/>
<div class="sbmt_dv" style="padding-left:430px;">

	<input type="submit" name="submit" class="btn" value="Save" />&nbsp;
	<input type="button" name="cancl_bttn"  onclick=" window.location .href ='<?=site_url ('admin/blog' )?> '" class="btn" value="Cancel" />
    
</div>

</form>

<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>	<!-- end page body -->

<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ],
});
</script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
</script>

<script>
 	function openBrowse1(){
		document.getElementById("userfile_1").click();
	}
</script>
<!--instance the validator engine ::rtn:: -->    

<script>		
	$(document).ready(function(){  
      $("#userfile_1").change(function(){
	        var filename=$(this).val();	       
	        var size = this.files[0].size;
	        var name=this.files[0].name;
	        var type=this.files[0].type;
	        if(size>1048576){
	        	$("#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
	        	 $("#file_name_box_1").val('');
	        }
	        else if(!(/\.(gif|jpg|jpeg|png)$/i).test(filename))
	        {
	        	$("#err_userfile_1").html('<span style="color:red;">*Only Image file accepted (gif|jpg|jpeg|png)</span><br/><br/>');	        	
	        	$("#file_name_box_1").val('');
	  	        	 
	        }
	        else{
	        	$("#err_userfile_1").html('');
	        	$("#file_name_box_1").val(name);
	        }	         
	    });	    
	});
</script>
