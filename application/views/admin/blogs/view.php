<style>
	#tut_video{
		display:block;
		width:417px;
		background-color:#ebf0ef;
		border:2px solid white;
		margin:0 auto;
		  -webkit-box-shadow: 2px 2px 2px -2px grey;
	   -moz-box-shadow: 2px 2px 2px -2px grey;
	        box-shadow: -1px 2px 2px -2px grey;
	}
</style>
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1>Admin View Blog Entry	</h1>

</div>
<div class="admin_cont">

<div class="blog_entry_detail">

<h3 class="left"><?=$blog['title']?></h3>
<span class="right"><?=$blog['post_date']?></span>
<div class="clear"></div>

<div class="author"><span>Author:</span> <?=$blog['author']?> </div>

<div class="bed_img">

	<img class="img_wth_brdrsdw" src="<?=base_url('uploads/blog_images/'.$blog['image_file'])?>" max-width="752" alt=" " /> 
	<br/>
	<a href="<?=$blog['video_link']?>" target="_blank"><?=isset($blog['video_link'])?$blog['video_link']:''?></a>
</div>

<?=$blog['content']?>
</div>

<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>	<!-- end page body -->
