
<div id="page_body">

    <div class="center">

        <div class="admin_title">
            <h1>
                <?= ($edit == 'new') ? 'Admin Add New Room Entry' : 'Admin Edit Room Entry' ?>
            </h1>

        </div>
        <?php
        $link = '';
        if (isset($MYroom['room_id'])) {
            $link = '/' . $MYroom['room_id'];
        }
        ?>
        <div class="admin_cont admin_form acnb_form">
            <form id="pageForm" action="<?= site_url('admin/room/save' . $link) ?>" enctype="multipart/form-data" method="post" name="pageForm">
                <p>
                    <label>Initial:</label> 
                    <input type="text" name="initial" id="initial" class="input_field validate[required]" value="<?= isset($MYroom['initial']) ? $MYroom['initial'] : '' ?>" />
                <div id="rtn-initial"></div>
                </p>
                <?= form_error('initial') ?>
                <p>
                    <label>Room Type:</label> 
                    <select name="type_id" id="type_id" class="input_field validate[required]">
                        <option value="">Select</option>
                        <?php foreach ($room_types as $rt) : ?>
                            <option <?php echo (isset($MYroom['type_id']) && $MYroom['type_id'] == $rt['room_type_id']) ? 'selected="selected"' : ''; ?> value="<?php echo $rt['room_type_id']; ?>"><?php echo $rt['type']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <?= form_error('type_id') ?>

                <p>
                    <label>Department:</label> 
                    <select name="department_id" id="department_id" class="input_field validate[required]">
                        <option value="">Select</option>
                        <?php foreach ($departments as $dep) : ?>
                            <option <?php echo (isset($MYroom['department_id']) && $MYroom['department_id'] == $dep['department_id']) ? 'selected="selected"' : ''; ?> value="<?php echo $dep['department_id']; ?>"><?php echo $dep['name'] . '(' . $dep['initial'] . ')'; ?></option>   
                        <?php endforeach; ?>
                    </select>
                </p>

                <?= form_error('department_id') ?>
                <p>
                    <label>Description:</label> 
                    <span class="left">

                        <textarea name="description" style="width:100%">
                            <?= isset($MYroom['description']) ? $MYroom['description'] : '' ?>
                        </textarea>

                </p>
                </br>
                <?= form_error('description') ?>
                <div class="clear"></div>
                <div align="center" style = "padding: 10px 0 0;">

                    <input type="submit" name='submit'  class="btn" value="<?= ($edit == 'new') ? 'Add' : 'Update' ?>" />
                    <input type="button" class="btn" value="Cancel" name="cancel_btn"  onclick="window.location.href = '<?= site_url('admin/room') ?>'" />
                </div>
            </form>
            <div class="clear"></div>
        </div>
        <!-- end admin content -->

    </div>

</div>  <!-- end page body -->
<!--instance the validator engine ::rtn:: -->   
<script type="text/javascript">
                        tinymce.init({
                            selector: "textarea",
                            theme: "modern",
                            plugins: [
                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                "emoticons template paste"
                            ],
                            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                            toolbar2: "print preview media | forecolor backcolor emoticons",
                            templates: [
                                {title: 'Test template 1', content: 'Test 1'},
                                {title: 'Test template 2', content: 'Test 2'}
                            ],
                        });
</script>
<script>
    $(document).ready(function() {
        $("#initial").blur(function() {
            var initial = $('#initial').val();
            if (initial == "") {
                alert("You have not entered any initial");
            } else {
                $("#rtn-initial").html('<b>Checking... please wait..</b>');
                $.ajax({
                    type: "post",
                    data: {'initial': initial},
                    url: "<?= site_url('admin/room/ajx_room_available_admin') ?>",
                    success: function(result)
                    {
                        if (result) {
                            $("#rtn-initial").html('<span style="color:green"><b>room Available</b></span>');
                        }
                        else {
                            $("#rtn-initial").html('<span style="color:red"><b>Sorry, room is not available, please try another</b></span>');
                        }
                    }
                }).fail(function() {
                    alert("error");
                });
            }

            return false;
        });
    });
</script>