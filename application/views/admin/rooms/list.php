
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">View The Rooms </h1>
<div class="right top_link_dv">

<a href="<?php echo site_url('admin/room/edit'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove Room</span></a>

     
</div>
<div class="clear"></div>
</div>

<div class="tabs_contents">
    
<div class="filter-list">
      <label><strong>Department:</strong> </label>

<select name="department" id="department_id" style="width: 300px;" onchange="page_search_submit()" class="input_field" >
<option value="0">Select</option>
<?php foreach($departments as $dep) :?>
        <option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option> 
    <?php  endforeach;?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Room..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
<div class="clear"></div>

<div class="ymp_table">
    
      <table width="100%" border="0" cellspacing="0"  style="margin: 20px 0 0 0px;">
  <thead><tr>
    <th width="50" scope="col">#</th>
    <th width="190" scope="col">Initial</th>
    <th width="190" scope="col">Type</th>
    <th width="320" scope="col">Department</th>
    <th scope="col">Actions</th>
  </tr>
    </thead>
    <tbody>
        <?php $n = 1; ?>
    <?php foreach ($rooms as $room) { ?>
    
    <tr>
    <td><?php echo $n++ ;?></td>
    <td><?php echo $room['initial'];?></td>
    <td><?php   foreach ($room_types as $rt)  {
        if($rt['room_type_id']==$room['type_id']){
            echo $rt['type'];
            break;
        }        
        } ?>
    </td>
    <td>
        <?php foreach($departments as $dep) {
        if($dep['department_id']==$room['department_id']){
            echo $dep['name'];
        }        
    } ;?>
    </td>
    <td>
        <a href="<?= site_url('admin/room/edit/'.$room['room_id'])?>"><img src="<?=base_url();?>images/edit_icon.png" width="21" height="21" alt=" " /></a>&nbsp;&nbsp;
        <a class="confirm_action" title="Are you sure you want to delete this Room?" href ="<?=site_url('admin/room/delete/'.$room['room_id'])?>"><img src="<?=base_url();?>images/delete_icon.jpg" width="21" height="21" alt=" " /></a>
    </td>
    <td></td>
    </tr>

 <?php }  ?>
    </tbody>
           
</table>


</div>
</div>
</div>
</div>

</div>  <!-- end page body -->

