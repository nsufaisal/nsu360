
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">View Semester's Courses </h1>
<div class="right top_link_dv">

<a href="<?php echo site_url('admin/semester_courses/edit'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove Course</span></a>

     
</div>
<div class="clear"></div>
</div>

<div class="tabs_contents">
    
<div class="filter-list">
      <label><strong>Department:</strong> </label>

<select name="department" id="department_id" style="width: 300px;" onchange="page_search_submit()" class="input_field" >
<option value="0">Select</option>
<?php foreach($departments as $dep) :?>
        <option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option> 
    <?php  endforeach;?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Course..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
<div class="clear"></div>

<div class="ymp_table">
    
      <table width="100%" border="0" cellspacing="0"  style="margin: 20px 0 0 0px;">
  <thead><tr>
    <th width="50" scope="col">#</th>
    <th width="150" scope="col">Course</th>
    <th width="150" scope="col">Section</th>
    <th width="150" scope="col">Room</th>
    <th width="190" scope="col">Instructor</th>
    <th scope="col">Actions</th>
  </tr>
    </thead>
    <tbody>
        <?php $n = 1; ?>
    <?php foreach ($semester_courses as $semester_course) { ?>
    
    <tr>
    <td><?php echo $n++ ;?></td>
    <td>
        <?php   foreach ($courses as $c)  {
        if($c['course_id']==$semester_course['course_id']){
            echo $c['initial'];
            break;
        }        
        } ?>
    </td>
    <td><?= $semester_course['section_no'] ?></td>
    <td>
        <?php   foreach ($rooms as $r)  {
        if($r['room_id']==$semester_course['room_id']){
            echo $r['initial'];
            break;
        }
        } ?>
    </td>
    <td>
        <?php   foreach ($instructors as $i)  {
        if($i['instructor_id']==$semester_course['instructor_id']){
            echo $i['first_name']. ' '. $i['last_name'];
            break;
        }
        } ?>
    </td>
    <td>
        <a href="<?= site_url('admin/semester_courses/edit/'.$semester_course['id'])?>"><img src="<?=base_url();?>images/edit_icon.png" width="21" height="21" alt=" " /></a>&nbsp;&nbsp;
        <a class="confirm_action" title="Are you sure you want to delete this Room?" href ="<?=site_url('admin/room/delete/'.$semester_course['room_id'])?>"><img src="<?=base_url();?>images/delete_icon.jpg" width="21" height="21" alt=" " /></a>
    </td>
    <td></td>
    </tr>

 <?php }  ?>
    </tbody>
           
</table>


</div>
</div>
</div>
</div>

</div>  <!-- end page body -->

