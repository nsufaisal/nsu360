<div id="page_body">
    <div class="inner">
      <div class="page-title">
        <h1><span>Admin</span> <?php if($user_id == 0) echo "Add New User"; else echo "Edit User"; ?></h1>
      </div>
      <div class="content-box">
      <form action="<?php echo site_url('admin/users/save/'.$user_id);?>" method="post">
	  <?PHP if(form_error('first_name')) { ?><div><?php echo form_error('first_name');?></div><?PHP } ?>
      <p><label class="width-118">First Name:</label> <input name="first_name" type="text" class="input-field" value="<?php echo set_value('first_name',($edit=='edit')? $user['first_name'] : '');?>" /> </p>
	  	<?PHP if(form_error('last_name')) { ?><div><?php echo form_error('last_name');?></div><?PHP } ?>
      <p><label class="width-118">Last Name:</label><input name="last_name" type="text" class="input-field"     value="<?php echo set_value('last_name',($edit=='edit')? $user['last_name'] : '');?>"     /></p>
      <p><label class="width-118">Email Address:</label><input name="email" type="text" class="input-field"  value="<?php echo set_value('email',($edit=='edit')? $user['email'] : '');?>" /></p>
      	<?PHP if(form_error('password')) { ?><div><?php echo form_error('password');?></div><?PHP } ?>
	  <p><label class="width-118">Password:</label><input name="password" type="password" class="input-field" /></p>
	  <?PHP if(form_error('confirm')) { ?><div> <?php echo form_error('confirm');?></div><?PHP } ?>
      <p><label class="width-118">Confirm Password:</label><input name="confirm" type="password" class="input-field" /></p>
	    <p><label class="width-118">Active:</label><input name="active" type="checkbox"  <?PHP if(isset($user['active']) && $user['active']==1) echo 'checked="checked"'; echo 'value="1"'?> /></p>
      <p style="padding-top: 10px;"><input name="save" type="submit" class="btn" value="Save" style="margin-left: 230px; margin-right: 5px;" /><input name="cancel" id="btn_cancel" type="button" class="btn" value="Cancel"   /></p>
      
      </form>
        
      </div>
      <!-- end table container here --> 
      
    </div>
  </div>
		
<script type="text/javascript">
  
$("#btn_cancel").click(function(){
   var   url = "<?php echo site_url('admin/users')?>";

   //  $(window.location).attr('href', url); 
     window.location.href=url;
       
       
   
});

</script>		
		
		
		
		
		
		
		
		
		

