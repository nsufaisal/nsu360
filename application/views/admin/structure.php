<!DOCTYPE html>
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>Welcome to Our Site!</title>
		
		<link href="<?=base_url()?>css/admin/reset.css" rel="stylesheet" type="text/css" />
		
		<link href="<?=base_url()?>css/admin/my_styles.css" rel="stylesheet" type="text/css" />
		
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>images/admin/favicon.ico" />
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>js/jquery/jquery-ui-1.8.10.custom.css">
		
	   <!--	<script src="<?php echo base_url()?>js/jquery/jquery.1.5.2.min.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url()?>js/jq/jquery.js"></script>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/calendar.css"/>

		<script src="<?php echo base_url()?>js/jquery/jquery-ui-1.8.10.custom.min.js"></script>
		
		<script src="<?php echo base_url()?>js/jquery.simplemodal.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url()?>js/admin.js" type="text/javascript" ></script>
		
		<script type="text/javascript" src="<?php echo base_url()?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url()?>js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url()?>js/jquery.tablesorter-update.js"></script>
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

		<script type="text/javascript" src="<?=base_url('js/video-js/video.js')?>"></script>

		<link rel="stylesheet" type="text/css" href="<?=base_url('js/video-js/video-js.css')?>" media="screen"/>
		
		<script type='text/javascript' src='<?php echo base_url()?>js/osx/osx.js'></script>
		
		<link rel="stylesheet" href="<?php echo base_url()?>css/validationEngine.jquery.css" type="text/css"/>

		<script src="<?php echo base_url()?>js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

		<script src="<?php echo base_url()?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		
		<!-- file uploader -->
		
		<script src="<?php echo base_url()?>js/fileuploader/fileuploader.js" type="text/javascript" charset="utf-8"></script>
		
		<link rel="stylesheet" href="<?php echo base_url()?>js/fileuploader/fileuploader.css" type="text/css"/>
	
		<link type='text/css' href='<?php echo base_url()?>js/osx/osx.css' rel='stylesheet' media='screen' />
		
		<script language="javascript"> var base_url = '<?=base_url();?>';</script>
		
		<!-- tinymce -->
		<script type="text/javascript" src="<?=base_url('js/tinymce/tinymce.min.js')?>"></script>
	</head>
	
	<body>
	
		<div id="gray_bak" style="display:none; position:absolute; margin: auto; top: 0; left: 0; width: 100%; height: 100%; z-index: 9;background-color: #7f7f7f; opacity:0.8; filter: alpha(opacity=80);overflow:auto" onclick="closePP();">&nbsp;</div>
		
		<div id="content" style="display:none">
			<div id="basic-modal-content" style="display:none;padding:8px; "></div>
		</div>
		
		<div style="display:none">
			<div id="modal_message"><?PHP echo $this->session->flashdata('message');?></div>
		</div>
		<div id="wrapper">
		  <!-- header starts here -->
		  <?php $this->load->view('admin/header');?>
		  <!-- header ends here -->
		
		  <!--content-->
		  <?php $this->load->view($body);?>
		  <div class="clear"></div>	
		  <!--/content-->
		
		  <!--footer-->
		  <?php $this->load->view('admin/footer');?>
			<!-- footer ends here -->
		</div>
		<?php 	
			$msg = $this->session->flashdata('message'); 
			if($msg){
		?>
			<script type="text/javascript">
				$('#modal_message').dialog({
						modal: true
				});
			</script>
	<script>
		$(document).ready(coolSliderLink());
	</script>
		<?php } ?>
		
	</body>
</html>
