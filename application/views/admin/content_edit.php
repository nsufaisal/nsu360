<?php if($content['tiny_enabled']==1) { ?>
	<script type="text/javascript" src="<?php echo base_url()?>js/tiny_mce_new/tiny_mce.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>js/tiny_mce_new/init_tiny.js"></script>
<?php } 
?>

<div id="page_body">
    <div class="center">
      <div class="admin_title">
        <h1>Admin Edit Content and Meta</h1>
      </div>
      <!--Edit outbound email -->
      <div class="admin_cont admin_form">
      <form action="<?php echo site_url('admin/content/save/'.$content_id)?>" method="post">
	  	 <?php if($content['type']!='data') { ?>
  	<p>
		<label>Page Name:</label> 
	<input name="name" id="name"  type="text" class="input-field"  value="<?php echo $content['name'];?>" /> 
	</p>	
	<?php if(form_error('data')) echo '<p>'.form_error('data').'</p>';?>
	<?php if($content['type']!='meta') { ?>
	<p>
		<label>Content:</label>
		<textarea  name="data" rows="" cols="" class="input-field" style="height: 154px;"><?php echo $content['data'];?></textarea>
	</p>
	<?php } ?>
	<?php if(form_error('title')) echo '<p>'.form_error('title').'</p>'?>
	<p>
            <label>Title:</label><input name="title" id="title" type="text" class="input-field" style="width:432px;" value="<?php echo $content['title'];?>" />
        </p>
	<?php if(form_error('description')) echo '<p>'.form_error('description').'</p>'?>
	<p><label>Description:</label> <input name="description" id="description" type="text" class="input-field" style="width:432px;" value="<?php echo $content['description'];?>" /> </p>
	<?php if(form_error('keywords')) echo '<p>'.form_error('keywords').'</p>'?>
	<p><label>Keywords:</label> <input name="keywords" id="keywords" type="text" class="input-field" style="width:432px;" value="<?php echo $content['keywords'];?>" /> </p>
	<?php  }else { ?>
		<p>
		<label>Page Name:</label> 
		<input name="name" id="name" readonly="" type="text" class="input-field" value="<?php echo $content['name'];?>" /> 
	</p>
		<p><label>Content:</label><textarea  name="data" rows="" cols="" class="input-field" style="height: 154px;"><?php echo $content['data'];?></textarea></p>
		<input name="title"  value="0" type="hidden" class="ad_inpt" />
		<input name="description"  value="0" type="hidden" class="ad_inpt" />
		<input name="keywords"  value="0" type="hidden" class="ad_inpt" />
	<?php } ?> 	
	
       
      <p>
          <input type="submit" class="btn" style="margin: 0 6px 0 180px;" value="Save" />
          <input type="button" class="btn" value="Cancel" onclick="location.href='<?=site_url('admin/content');?>'"/>
      </p>
      </form>
      </div>
       <!--/Edit outbound email -->
    </div>
  </div>
  <script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ],
});
</script>