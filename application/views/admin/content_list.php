

<div id="page_body">

<div class="center">

<div class="admin_title">

<h1>Admin Content and META Controller</h1>

</div>

<div class="ymp_table">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="225" scope="col">Page Type</th>
    <th width="585" scope="col">Content</th>
    <th scope="col">Actions</th>
  </tr>
   <?php foreach ($contents as $content) : ?>
  <tr>
    <td><?php echo $content['name'];?></td>
    <td><?php if(trim(strip_tags($content['data']))!='') echo substr(strip_tags($content['data']),0, 60); else echo '&nbsp;';?></td>
    <td class="action"><a href="<?php echo site_url('admin/content/view/'.$content['content_id'])?>">View</a>  |  <a href="<?php echo site_url('admin/content/edit/'.$content['content_id'])?>">Edit</a></td>
  </tr>
    <?php endforeach; ?>
  
  
</table>

  </div>

</div>

</div>	<!-- end page body -->