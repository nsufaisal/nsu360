<div class="heading_cont">
<h2 class="left"><span>Admin</span> Export Users List</h2>
<div class="right"><a href="<?PHP echo site_url('admin/members/export_list');?>" class="addnew" style="background:url(<?PHP echo base_url();?>images/admin/add_bg2.png);width:108px;">Export Users List</a></div>
<div class="clear"></div>
</div>	<!-- end heading container here -->

<div class="tabular_cont">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="90" class="tabheader">First Name</td>
    <td width="90" class="tabheader">Last Name</td>
    <td width="180" class="tabheader">Company Name</td>
    <td width="120" class="tabheader">Phone Number</td>
    <td width="120" class="tabheader">Registration Date</td>
    <td width="120" class="tabheader">Last Activity</td>
    <td class="tabheader">Email Address</td>
  </tr>
   <?php foreach($members as $crt_member):?>
  <tr class="strong">
    <td><?php echo $crt_member['first_name']?></td>
    <td><?php echo $crt_member['last_name']?></td>
    <td><?php echo $crt_member['company_name']?></td>
    <td><?php echo $crt_member['phone']?></td>
    <td><?php echo date('m-d-Y',strtotime($crt_member['date']));?></td>
    <td><?php echo date('m-d-Y',strtotime($crt_member['last_activity_date']));?></td>
    <td><?php echo $crt_member['email']?></td>
  </tr>
  <?php endforeach;?>
  </tr>
  </table>
</div>	<!-- end tabular container here -->

</div>	<!-- end content bg container here -->
