<div class="heading_cont">
<h2 class="left"><span>Admin</span> Public Users Controller</h2>
<div class="right"><a href="<?php echo site_url('admin/members/edit/0')?>" class="addnew">Add New User</a></div>
<div class="clear"></div>
</div>


<div class="tabular_cont">
  <form name="memlistfrm" id="memlistfrm" method="post" action="<?=site_url('admin/member/export_list/');?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>

  <td width="110" class="tabheader">First Name</td>
    <td width="110" class="tabheader">Last Name</td>
    <td width="220" class="tabheader">Company Name</td>
    <td width="120" class="tabheader">Phone Number</td>
    <td width="250" class="tabheader">Email</td>
    <td class="tabheader">Actions</td>

  </tr>

  <?php foreach($members as $crt_member):?>

  <tr>

    <td class="strong"><?php echo $crt_member['first_name']?></td>

    <td class="strong"><label><?php echo $crt_member['last_name']?> </label><?php /*?><input name="member_id[]" id="member_id" type="checkbox" value="<?=$crt_member['member_id'];?>" /><?php */?></td>

    <td class="strong"><?php echo $crt_member['company_name'];?></td>

    <td class="strong"><?php echo $crt_member['phone'];?></td>

    <td class="strong"><?php echo $crt_member['email']?></td>

	<td class="actions"><a href="<?php echo site_url('admin/members/view/'.$crt_member['member_id'])?>">view</a> | 

	<a href="<?php echo site_url('admin/members/edit/'.$crt_member['member_id'])?>">edit</a> | 

	<a href="<?php echo site_url('admin/members/delete/'.$crt_member['member_id'])?>" title="Are you sure you want to delete this public user" class="confirm_action" >delete</a></td>

  </tr>

  <?php endforeach;?>

</table>
 </form>
 <div class="right" style="padding-top:25px;"> <a href="<?php echo site_url('admin/members/export')?>" class="addnew">Export User</a></div>
</div>	<!-- end tabular container here -->

