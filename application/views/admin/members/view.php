<div class="heading_cont">
<h2 class="left"><span>Admin</span> View Account Detail</h2>
<div class="clear"></div>
</div>	<!-- end heading container here -->

<div class="site_contents">
<table width="437" border="0" cellspacing="0" cellpadding="0" class="add_cont">
  <tr>
    <td width="115" class="strong">First Name:</td>
    <td><?PHP echo $member['first_name'];?></td>
  </tr>
  <tr>
    <td class="strong">Last Name:</td>
    <td><?PHP echo $member['last_name'];?></td>
  </tr>
  <tr>
    <td class="strong">Company Name:</td>
    <td><?PHP echo $member['company_name'];?></td>
  </tr>
  <tr>
    <td class="strong">Phone:</td>
    <td><?PHP echo $member['phone'];?></td>
  </tr>
  <tr>
    <td class="strong">Email address:</td>
    <td><?PHP echo $member['email'];?></td>
  </tr>
  <tr>
    <td class="strong">Password:</td>
    <td>********</td>
  </tr>
 
  <tr>
    <td height="40"></td>
    <td>
		<?PHP if($member['isactive']==0) { ?>
		<a href="<?php echo site_url('admin/members/approve/'.$member['member_id'])?>" title="Are you sure you want to approve this public user" class="confirm_action" ><input name="" type="button" class="sm_btn" value="Approved"/></a> &nbsp; <?PHP } elseif($member['ban']==0){ ?> <a href="<?php echo site_url('admin/members/ban/'.$member['member_id'])?>" title="Are you sure you want to ban this public user" class="confirm_action" ><input name="" type="button" class="sm_btn" value="Ban"/></a> <?PHP } elseif($member['ban']==1){?><a href="<?php echo site_url('admin/members/allow/'.$member['member_id'])?>" title="Are you sure you want to allow this public user" class="confirm_action" ><input name="" type="button" class="sm_btn" value="Allow"/></a> <?PHP } ?><input name="" type="button" class="sm_btn" value="Edit"  onclick="location.href='<?PHP echo site_url('admin/members/edit/'.$member['member_id']);?>';"/> &nbsp; <a href="<?php echo site_url('admin/members/delete/'.$member['member_id'])?>" title="Are you sure you want to delete this public user" class="confirm_action" ><input name="" type="button" class="sm_btn" value="Delete"/></a> &nbsp;<input name="" type="button" class="sm_btn" value="Back"  onclick="location.href='<?PHP echo site_url('admin/members');?>';"/></td>
  </tr>
  </table>
</div>	<!-- end site contents container here -->
