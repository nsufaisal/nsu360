
<div id="page_body">

<div class="center">

<div class="admin_title">
<h1>
    <?=($edit=='new')?'Admin Add New Instructor Entry':'Admin Edit Instructor Entry'?>
</h1>

</div>
<?php $link ='';
if (isset($instructor['instructor_id'])) {
    $link = '/' . $instructor['instructor_id'];
}
?>
<div class="admin_cont admin_form acnb_form">
<form action="<?=site_url('admin/instructors/save'. $link)?>" enctype="multipart/form-data" method="post" name="pageForm" id="pageForm">
<p>
    <label>First Name:</label> 
    <input type="text" name="first_name" id="first_name" class="input_field " value="<?php isset($instructor['first_name'])?$instructor['first_name']:''?>" />
</p>
<p>
    <label>Last Name:</label> 
    <input type="text" name="last_name" id="last_name" class="input_field " value="<?=isset($instructor['last_name'])?$instructor['last_name']:''?>" />
</p>

<p>
    <label>Initial:</label> 
    <input type="text" name="initial" id="initial" class="input_field " value="<?=isset($instructor['initial'])?$instructor['initial']:''?>" />
</p>


<p>
    <label>Department:</label> 
    <select name="department_id" id="department_id" class="input_field">
    <option value="">Select</option>
    <?php foreach($departments as $dep) :?>
        <option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option> 
    <?php  endforeach;?>
    </select>
</p>

<p>
    <label>Email:</label> 
    <input type="text" name="email" id="email" class="input_field " value="<?=isset($instructor['email'])?$instructor['email']:''?>" />
</p>

</br>
<div class="clear"></div>
<div align="center" style = "padding: 10px 0 0;">

<input type="submit" name='submit'  class="btn" value="Add" />
<input type="button" class="btn" value="Cancel" name="cancel_btn"  onclick="window.location.href='<?=site_url('admin/instructors')?>' " />
</div>
</form>
<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>  <!-- end page body -->