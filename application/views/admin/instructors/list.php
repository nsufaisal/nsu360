
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">View The Instructors </h1>
<!--<div class="right top_link_dv">

<a href="<?php echo site_url('admin/instructors/edit'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove Instructor</span></a>

     
</div>-->
<div class="clear"></div>
</div>

<div class="tabs_contents">

<div class="filter-list">
      <label><strong>Department:</strong> </label>

<select name="department" id="department_id" style="width: 350px;" onchange="page_search_submit()" class="input_field" >
<option value="0">Select</option>
<?php foreach($departments as $dep) :?>
        <option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option> 
    <?php  endforeach;?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Instructor..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="ymp_table">
    
      <table width="100%" border="0" cellspacing="0"  style="margin: 20px 0 0 0px;">
  <thead><tr>
      <th width="50" scope="col">#</th>
    <th width="90" scope="col">Instructor Initial</th>
    <th width="220" scope="col">Instructor Full name</th>
    <th scope="col">Department</th>
    <th scope="col">Approve</th>
    <th scope="col">Actions</th>
  </tr>
    </thead>
    <tbody id='instructors_show_admin'>
        <?php $n = 1; ?>
    <?php foreach ($instructors as $instructor) { ?>
    
    <tr>
    <td><?php echo $n++ ;?></td>
    <td><?php echo $instructor['initial'];?></td>
    <td><?php echo $instructor['first_name']. ' ' . $instructor['last_name']; ?></td>
    <td>
    <?php foreach($departments as $dep) {?>
         <?php if ($instructor['department_id']==$dep['department_id']) {
             echo $dep['name'].'('.$dep['initial'].')';
         } ?>
     <?php  }?>
    </td>
    <td><?php if ( $instructor['is_active']==0){ ?>
        <a class="confirm_action" title="Are you sure you want to ACTIVATE this Instructor?" href="<?= site_url('admin/instructors/activate/'.$instructor['instructor_id'])?>"><img src="<?=base_url();?>images/deactive.jpg" width="21" height="21" alt=" " /></a>
        <?php  } else { ?>
         <a class="confirm_action" title="Are you sure you want to DEACTIVATE this Instructor?" href="<?= site_url('admin/instructors/deactivate/'.$instructor['instructor_id'])?>"><img src="<?=base_url();?>images/active.jpg" width="21" height="21" alt=" " /></a>
       <?php }?>
    </td>
    <td>
        <a class="confirm_action" title="Are you sure you want to delete this Course?" href ="<?=site_url('admin/instructors/delete/'.$instructor['instructor_id'])?>"><img src="<?=base_url();?>images/delete_icon.jpg" width="21" height="21" alt=" " /></a>
    </td>
    </tr>

 <?php }  ?>
</tbody>
</table>
</div>
  </div>

</div>

</div>  <!-- end page body -->

<script>
    $('select#department_id').change(function() {
    var department_id=this.value;
 //  alert(department_id);
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('admin/instructors/ajx_load_instructors'); ?>",
        data: {'department_id': department_id}, 
        success: function (data) {
            //alert(data);
            $("#instructors_show_admin").html(data);
        },
    });
     
});
</script>

