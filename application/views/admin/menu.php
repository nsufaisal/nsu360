<div id="nav">

<div class="center">

<ul>

<li <?PHP if(isset($sel) && $sel=='content') echo 'class="active"'; ?>><a href="<?php  echo site_url('admin/content');?>">Content</a></li>


<li <?PHP if(isset($sel) && $sel=='public_users') echo ' class="active"';?>><a href="<?php  echo site_url('admin/public_users'); ?>">Public Users</a></li>

<li <?PHP if(isset($sel) && $sel=='email') echo ' class="active"';?>><a href="<?php  echo site_url('admin/email');?>">Email</a></li>

<li <?PHP if(isset($sel) && $sel=='contacts') echo ' class="active"';?>><a href="<?php  echo site_url('admin/contacts'); ?>">Contacts</a></li>

<li <?PHP if(isset($sel) && $sel=='blog') echo ' class="active"';?>><a href="<?php  echo site_url('admin/blog'); ?>">Blog</a></li>

<li <?PHP if(isset($sel) && $sel=='blog_categories') echo ' class="active"';?>><a href="<?php  echo site_url('admin/blog_category'); ?>">Blog Categories</a></li>

<li<?PHP if(isset($sel) && $sel=='settings_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/settings'); ?>">Settings</a></li>

<li<?PHP if(isset($sel) && $sel=='categories') echo ' class="active"';?>><a href="<?php  echo site_url('admin/categories'); ?>">Categories</a></li>

<li <?PHP if(isset($sel) && $sel=='courses_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/courses'); ?>">Courses</a></li>

<li <?PHP if(isset($sel) && $sel=='departments_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/departments'); ?>">Departments</a></li>

<li <?PHP if(isset($sel) && $sel=='semesters_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/semesters'); ?>">Semesters</a></li>

<li <?PHP if(isset($sel) && $sel=='instructors_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/instructors'); ?>">Instructors</a></li>

<li <?PHP if(isset($sel) && $sel=='semester_courses') echo ' class="active"';?>><a href="<?php  echo site_url('admin/semester_courses'); ?>">Semester Courses</a></li>

<li <?PHP if(isset($sel) && $sel=='rooms_admin') echo ' class="active"';?>><a href="<?php  echo site_url('admin/room'); ?>">Rooms</a></li>
</ul>

<div class="clear"></div>

</div>

</div>	<!-- end navigation -->