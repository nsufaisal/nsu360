
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1>Admin Add New User	</h1>

</div>


<div class="admin_cont admin_form">
<form action="<?php echo site_url('admin/users/save/' . $user_id); ?>" method="post">
 
<p><?PHP if (form_error('first_name')) { ?><div><?php echo form_error('first_name'); ?></div><?PHP } ?>
	<label>First Name:</label> 
    <input type="text" name="first_name" class="input_field " value="<?php echo set_value('first_name', ($edit == 'edit') ? $user['first_name'] : ''); ?>" />
</p>

<p><?PHP if (form_error('last_name')) { ?><div><?php echo form_error('last_name'); ?></div><?PHP } ?>
	<label>Last Name:</label> 
    <input type="text" name="last_name" class="input_field " value="<?php echo set_value('last_name', ($edit == 'edit') ? $user['last_name'] : ''); ?>" />
</p>
<p> <?PHP if (form_error('email')) { ?><div><?php echo form_error('email'); ?></div><?PHP } ?>
	<label>Email Address:</label> 
    <input type="text" name="email" class="input_field " value="<?php echo set_value('email', ($edit == 'edit') ? $user['email'] : ''); ?>"/>
</p>

<p> <?PHP if (form_error('ip_address')) { ?><div><?php echo form_error('ip_address'); ?></div><?PHP } ?>
	<label>IP Address:</label> 
    <input type="text" name="ip_address" class="input_field " value="<?php echo set_value('ip_address', ($edit == 'edit') ? $user['ip_address'] : ''); ?>" />
</p>

<p><?PHP if (form_error('password')) { ?><div><?php echo form_error('password'); ?></div><?PHP } ?>
	<label>Password:</label> 
    <input name="password" type="password" class="input_field " value="" />
</p>

<p><?PHP if (form_error('confirm')) { ?><div> <?php echo form_error('confirm'); ?></div><?PHP } ?>
	<label>Confirm Password:</label> 
    <input name="confirm" type="password" class="input_field " value="" />
</p>

<div class="clear"></div>

<div class="sbmt_dv">

	<input type="submit" name="" class="btn" value="Save" />&nbsp;
	<input type="button" name="" class="btn" value="Cancel" id="btn_cancel" />
    
</div>

</form>

<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>	<!-- end page body -->



<script type="text/javascript">
  
    $("#btn_cancel").click(function(){
        var   url = "<?php echo site_url('admin/users') ?>";

        //  $(window.location).attr('href', url); 
        window.location.href=url;
       
       
   
    });

</script>







