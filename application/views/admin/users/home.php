<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">Admin Users Controller</h1>

<a href="<?php  echo site_url('admin/users/edit/0'); ?>" class="right top_btn">Add New User</a>

<div class="clear"></div>
</div>


<div class="ymp_table ymp_table_single">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="130" scope="col">First Name  </th>
    <th width="230" scope="col">Last Name  </th>
    <th width="230" scope="col">Email</th>
    <th width="250" scope="col">IP Address</th>
    <th scope="col">Actions</th>
  </tr>
   <?php  foreach($users as $user){ ?>	
  <tr>
    <td><?php echo $user['first_name']; ?></td>
    <td><?php echo $user['last_name']; ?></td>
    <td><?php echo $user['email']; ?></td>
    <td><?php echo $user['ip_address']; ?></td>
    <td class="action"><a href="<?php echo site_url('admin/users/edit/'.$user['user_id']);?>">Edit</a>  |  <a href="<?php echo site_url('admin/users/delete/'.$user['user_id'])?>" title="Are you sure you want to delete this user?" class="confirm_action">Remove</a></td>
  </tr>
       <?php } ?>
  
  
</table>

  </div>

</div>

</div>	<!-- end page body -->
