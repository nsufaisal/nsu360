<div id="page_body">
<div class="center">

<div class="admin_title">
<h1 class="left">Admin Categories Controller</h1>
<a href="<?=site_url('admin/categories/edit/0')?>" class="right top_btn">Add Category</a>
<div class="clear"></div>
</div>

<div class="admin_cont">

<div class="category_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr class="tab_head">
    <td width="800"><strong>Category Name</strong></td>
    <td align="center"><strong>Actions</strong></td>
  </tr>
<?php $i=1; foreach ($categories as $category):?> 
  <tr>
    <td <?php if($i%2==0) echo 'class="alter_row"'?>><?=$category['name']?></td>
    <td align="center" class="action"><a href="<?= site_url('admin/categories/edit/'.$category['categories_id'])?>">Edit</a>  |  <a class="confirm_action" title="Are you sure you want to delete this category?" href="<?=site_url('admin/categories/delete/'.$category['categories_id'])?>">Remove</a></td>
  </tr>
  <?php $i++; endforeach ?>
  
  </tbody></table>
</div>
</div>
</div>
</div>