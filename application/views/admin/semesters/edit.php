<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>js/juitime/jquery-ui-timepicker-addon.css"/>
<script src="<?php echo site_url('js/juitime/jquery-ui-timepicker-addon.js') ?>"></script>
<script src="<?php echo site_url('js/juitime/jquery-ui-sliderAccess.js') ?>"></script>
<div id="page_body">

<div class="center">

<div class="admin_title">
<h1>
    <?=($edit=='new')?'Admin Add New Semester Entry':'Admin Edit Semester Entry'?>
</h1>

</div>
<?php $link ='';
if (isset($semester['semester_id'])) {
    $link = '/' . $semester['semester_id'];
}
?>
<div class="admin_cont admin_form acnb_form">
<form action="<?=site_url('admin/semesters/save'. $link)?>" enctype="multipart/form-data" method="post" name="pageForm" id="pageForm">
<p>
<label>Semester Initial:</label> 
<input type="text" name="initial" id="initial" class="input_field " value="<?php echo isset($semester['initial'])?$semester['initial']:''?>" />
</p>
<?=form_error('initial')?>
<p>
    <label>Semester Title:</label> 
    <input type="text" name="title" id="title" class="input_field " value="<?php echo isset($semester['title'])?$semester['title']:''?>" />
</p>
<?=form_error('title')?>
<p><label>Start Date:</label>
    <span class="date-inputs"> 
         <?php date_default_timezone_set('asia/dhaka'); ?>
        <input name="start_time" id="start_time" type="text" class="input_field" 
        value="<?php echo isset($semester['start_date'])?$semester['start_date']:''?>" />
        <a href="#" class="date-icon" onclick="$('#start_time').focus(); return false;" >
            <img src="<?= base_url(); ?>images/date-input-icon.gif" width="16" height="13" alt="" /></a>
    </span>  </p>

<p><label>End Date:</label>
    <span class="date-inputs"> 
         <?php date_default_timezone_set('asia/dhaka'); ?>
        <input name="end_time" id="end_time" type="text" class="input_field" 
        value="<?php echo isset($semester['end_date'])?$semester['end_date']:''?>" />
        <a href="#" class="date-icon" onclick="$('#end_time').focus(); return false;" >
            <img src="<?= base_url(); ?>images/date-input-icon.gif" width="16" height="13" alt="" /></a>
    </span>  </p>
    
</br>
<div class="clear"></div>
<div align="center" style = "padding: 10px 0 0;">

<input type="submit" name='submit'  class="btn" value="<?=($edit=='new')?'Add':'Update'?>" />
<input type="button" class="btn" value="Cancel" name="cancel_btn"  onclick="window.location.href='<?=site_url('admin/semesters')?>' " />
</div>
</form>
<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>  <!-- end page body -->
<script type="text/javascript">
$(document).ready(function() {

 $('#start_time,#end_time').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });

});
</script>