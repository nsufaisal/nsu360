
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">View The Semesters </h1>
<div class="right top_link_dv">

<a href="<?php echo site_url('admin/semesters/edit'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove Semester</span></a>

<?php $current = isset($current_semester['initial']) ? $current_semester['initial'] : null?>     
</div>
<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Semester..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
<div style="float: right; margin-right: 10px">
<label><strong>Year:</strong> </label>

<select name="year" id="year" style="width: 250px;" onchange="page_search_submit()" class="input_field" >
<option value="0">Select</option>
<?php for($i=0; $i<20; $i++){ $year = 2000;?>
<option value="<?php echo $i;?>" <?php if($i==14) echo 'selected';; ?>><?php echo $year = $year + $i;?></option> 
<?php  } ?>
</select>
</div>
<div class="clear"></div>

<div class="ymp_table">
    
      <table width="100%" border="0" cellspacing="0"  style="margin: 20px 0 0 0px;">
  <thead><tr>
    <th width="50" scope="col">#</th>
    <th width="150" scope="col">Initial</th>
    <th width="150" scope="col">Title</th>
    <th width="150" scope="col">Start Date</th>
    <th width="150" scope="col">End Date</th>
    <th width="150" scope="col">Current</th>
    <th scope="col">Actions</th>
  </tr>
    </thead>
    <tbody>
        <?php $n = 1; ?>
    <?php foreach ($semesters as $semester) { ?>
    
    <tr>
    <td><?php echo $n++ ;?></td>
    <td><?php echo $semester['initial'];?></td>
    <td><?php echo $semester['title'];?></td>
    <td><?php echo $semester['start_date']; ?></td>
    <td><?php echo $semester['end_date']; ?></td>
    <td><?php if ( $semester['is_current']==0){ ?>
            <?php if (isset($current)) { ?>
            <a class="confirm_action" title="<?php echo "Deactivate $current as current to ACTIVATE this semester as current" ?>" href="<?= site_url('admin/semesters/deactivate_activate/'.$semester['semester_id'])?>"><img src="<?=base_url();?>images/deactive.jpg" width="21" height="21" alt=" " /></a>
            <?php } else { ?>
            <a class="confirm_action" title="Are you sure you want to ACTIVATE THIS Semester as current?" href="<?= site_url('admin/semesters/activate/'.$semester['semester_id'])?>"><img src="<?=base_url();?>images/deactive.jpg" width="21" height="21" alt=" " /></a>
            <?php }?>
         <?php  } else { ?>
         <a class="confirm_action" title="Are you sure you want to DEACTIVATE THIS Semester as current?" href="<?= site_url('admin/semesters/deactivate/'.$semester['semester_id'])?>"><img src="<?=base_url();?>images/active.jpg" width="21" height="21" alt=" " /></a>
       <?php }?>
    </td>
    <td>
        <a href="<?= site_url('admin/semesters/edit/'.$semester['semester_id'])?>"><img src="<?=base_url();?>images/edit_icon.png" width="21" height="21" alt=" " /></a>&nbsp;&nbsp;
        <a class="confirm_action" title="Are you sure you want to delete this Course?" href ="<?=site_url('admin/semesters/delete/'.$semester['semester_id'])?>"><img src="<?=base_url();?>images/delete_icon.jpg" width="21" height="21" alt=" " /></a>
    </td>
    <td></td>
    </tr>

 <?php }  ?>
    </tbody>
           
</table>


</div>
</div>

</div>

</div>  <!-- end page body -->

