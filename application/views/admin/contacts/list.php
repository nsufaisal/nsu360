<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="">Admin Contacts Controller</h1>

</div>


<div class="ymp_table ymp_table_single">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="" scope="col">Date  </th>
    <th width="" scope="col">Email </th>
    <th width="" scope="col">Name</th>
     <th width="" scope="col">Subject</th>
    <th scope="col">Actions</th>
  </tr>
   <?php $i=0; foreach($contacts as $crt_contact): ?>	 
  <tr>
        <td><?php echo date('m-d-Y',strtotime($crt_contact['date']));?></td>
     <td><?php echo $crt_contact['email'];?></td>
    <td><?php echo $crt_contact['first_name'].' '.$crt_contact['last_name'];?></td>
    <td><?php 
    echo $crt_contact['subject'];
    ?></td>
      <td class="action"><a href="<?php echo site_url('admin/contacts/view/'.$crt_contact['contact_id'])?>">View</a>  | <a href="<?php echo site_url('admin/contacts/reply/'.$crt_contact['contact_id'])?>">Reply</a>  |  
        <a href="<?php echo site_url('admin/contacts/delete/'.$crt_contact['contact_id'])?>" title="Are you sure you want to delete this message?" class="confirm_action">Remove</a></td>
  </tr>
  <?php $i++; endforeach;?>

  

  

  
</table>

  </div>

</div>

</div>	<!-- end page body -->

