
<div id="page_body">

    <div class="center">

        <div class="admin_title">

            <h1>Admin Reply</h1>

        </div>


        <div class="admin_cont edit_form_y">
            <form action="<?php echo site_url('admin/contacts/send/' . $contact['contact_id']) ?>"  method="post">


                <div class="admin_form acd_cont">

                    <p>
                        <label>Date Received:</label> 
                        <span>   <?php echo date('m-d-Y', strtotime($contact['date'])); ?></span>
                    </p>

                    <p>
                        <label>From:</label> 
                        <span> <?php echo $contact['first_name']; ?></span>
                    </p>

                    <p>
                        <label>To:</label> 
                        <span><?php echo $contact['email']; ?></span>
                    </p>

                </div>

                <p>
                    <strong>Subject:</strong>
                    <input name="subject" id="subject" style="width:432px;" type="text" class="input-field" value=""/>
                </p>

                <p>
                    <strong>Message:</strong>

                    <textarea name="message" cols="" rows="" class="input-field" style="width:742px; height:105px;">
                    </textarea>
                </p>

                <div class="clear"></div>

                <div class="sbmt_dv" style="padding:10px 0 0 300px;">


                    <input name="save" type="submit" class="btn" value="Send" />&nbsp; 
                    <input type="button" class="btn" value="Cancel" onclick="location.href='<?php echo site_url('admin/contacts') ?>'"/>

                </div>

            </form>

            <div class="clear"></div>
        </div>
        <!-- end admin content -->

    </div>

</div>	<!-- end page body -->

