
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="">Admin View Contact Detail	</h1>

</div>
<!--end admin title-->

<div class="admin_form acd_cont">

<p>
	<label>Date Received:</label> 
	<span><?php echo date('m-d-Y',strtotime($contact['date']));?></span>
</p>

<p>
	<label>From:</label> 
	<span><?php echo $contact['email'];?></span>
</p>

<p>
	<label>Subject:</label> 
	<span><?php echo $contact['subject'];?>
</span>
</p>

<p>
	<label>Questions/Info:</label> 
	<span><?php echo $contact['message'];?> </span>
</p>

<div class="sbmt_dv" style="padding:10px 0 0;">

	<input type="button" name="" class="btn" value="Reply" onclick="location.href='<?php echo site_url('admin/contacts/reply/'.$contact['contact_id'])?>'"/>&nbsp;
	<input type="button" name="" class="btn" value="Delete" onclick="location.href='<?php echo site_url('admin/contacts/delete/'.$contact['contact_id'])?>'"/>&nbsp;
	<input type="button" name="" class="btn" value="Cancel" onclick="location.href='<?php echo site_url('admin/contacts')?>'"/>
    
</div>

</div>
<!--end view cont detail txt-->


<div class="admin_title" style="padding-top:30px;">

<h1 class="">Admin Contacts History</h1>

</div>
<!--end admin title-->


<div class="ymp_table ymp_table_single" style="min-height:130px;;">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="130" scope="col">Date  </th>
    <th width="420" scope="col">Email </th>
    <th width="290" scope="col">Name</th>
    <th scope="col">Actions</th>
  </tr>
    <?php $i=0; foreach($replys as $crt) : ?>
  <tr>
     <td><?php echo date('m/d/Y',strtotime($crt['date']));?></td>
    <td><?php echo $crt['email']?></td>
   <td><?php echo $crt['first_name'].' '.$crt['last_name'];?></td>
   <td class="action"><a href="<?php echo site_url('admin/contacts/history/'.$crt['contact_id'])?>">View</a>  | <a href="<?php echo site_url('admin/contacts/reply/'.$crt['contact_id'])?>">Reply</a>  |  <a href="<?php echo site_url('admin/contacts/delete/'.$crt['contact_id'])?>" title="Are you sure you want to delete this message?" class="confirm_action">Remove</a></td>
  </tr>
   <?PHP $i++; endforeach;?>

  
</table>

  </div>

</div>

</div>	<!-- end page body -->
