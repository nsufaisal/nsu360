<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/my_styles.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />

<div class="popup-wrapper">

<!--popup -->
<div class="popup sign_in_pp">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?=base_url();?>"><img src="<?=base_url();?>images/pp_logo.gif" width="188" height="59" alt=" " /></a></h1>
<a href="<?=site_url()?>" class="close-button">Close (x)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content ymp_pp_cont">
<h2>Create a New Category</h2>
<?= validation_errors()?>
<div class="sign_in_form edit_form">
<form action="<?= site_url('admin/blog_category/save/0')?>" method="post" id="pageForm" accept-charset="utf-8" >
<p>
<strong>Parent:</strong>
<select name="" class="input_field">
<option>Lorem Ipsum</option>
</select>
</p>

<p>
<strong>Category Name:</strong>
<input name="password" id="password" type="password" class="input_field validate[required]" data-prompt-position="topLeft:280,-2" />
<span id="rtn-password"></span>
</p>

<!--submit buttons -->
<div class="pp_sbmt" style="padding-top:5px;">

<div class="right_inpt">
    <input name="submit" id="submit" type="submit" class="btn" value="Save" />
    <input name="cancel" onclick="window.location.href='<?= site_url()?>'" type="button" class="btn" value="Cancel" />
</div>

<div class="clear"></div>
</div>
<!--/submit buttons -->

</form>
</div>

</div>
<!--/popup content -->
</div>

</div>
<!--/popup -->

<!--instance the validator engine ::rtn:: -->    
<script>              
       $("#pageForm").validationEngine( 'attach', {promptPosition : "topRight", scroll: false},
       {focusFirstField : true });              
</script>

	<script>
		$(document).ready(coolSliderLink());
	</script>

