<div id="page_body">
<div class="center">

<div class="admin_title">
<h1 class="left">
	<?=$edit=='edit'?'Admin Edit a Category':'Crete a New Category'?></h1>
<div class="clear"></div>
</div>

<div class="admin_cont edit_form_y">
<?php
	if(isset($blog_category))
	{
		$id=$blog_category['category_id'];
	}
	else {
		$id=0;
	}
?>
<form action="<?=site_url('admin/blog_category/save/'.$id)?>" method="post" id="pageForm">
	<?=validation_errors()?>
<p>
<strong>Parent:</strong>
<select name="parent_id" id="parent_id" class="input_field validate[required]">
<option value="">Select</option>
<?php
foreach($parent_categories as $parent)
{
	if(isset($blog_category))
	{
		if($parent['category_id']==$blog_category['parent_id']){
			$selected='selected="selected"';
			echo '<option  value="'.$parent['category_id'].'"'.$selected.'>'.$parent['category'].'</option>';
		}
		else{
			echo '<option  value="'.$parent['category_id'].'">'.$parent['category'].'</option>';
		}	
	}
	else {
			echo '<option  value="'.$parent['category_id'].'">'.$parent['category'].'</option>';
	}
	
	
}
?>
</select>
</p>

<p>
<strong>Category Name:</strong>
<input type="text" name="category" id="category" style="width:432px;" class="input_field " value="<?=isset($blog_category['category'])?$blog_category['category']:''?>" />
</p>
<div class="clear"></div>

<div class="sbmt_dv" style="padding:10px 0 0 132px;">
<input type="submit" id="submit" name="submit" class="btn" value="Save" />&nbsp;
<input type="button" name="cancel"  onclick=" window.location .href ='<?=site_url ('admin/blog_category' )?> '" class="btn" value="Cancel" />    
</div>
</form>
<div class="clear"></div>
</div>
</div>
</div>	<!-- end page body -->

<!--instance the validator engine ::rtn:: -->    
<script>              
               $("#pageForm").validationEngine( 'attach', {promptPosition : "topRight", scroll: false},
               {focusFirstField : true });              
</script>

