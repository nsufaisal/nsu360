<div id="page_body">
<div class="center">

<div class="admin_title">
<h1 class="left">Admin Blog Categories Controller</h1>
<a href="<?=site_url('admin/blog_category/edit/0')?>" class="fancybox right top_btn">Add New Category</a>
<div class="clear"></div>
</div>
<div class="ymp_table ymp_table_single">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="200" scope="col">Category</th>
    <th width="320" scope="col">Sub Category</th>
    <th width="320" scope="col">Sub Sub Category</th>
    <th scope="col">Actions</th>
  </tr>
  
<?php foreach ($blog_categories as $blog):?> 
  <tr>
    <td><?=$blog['category']?></td>
    <td><?=$blog['sub_category']?></td>
    <td><?=$blog['subsub_category']?></td>
    <?php
    	$id=0;
    	if($blog['ssc_id']!='')
			$id=$blog['ssc_id'];
		else if($blog['sc_id']!='')
			$id=$blog['sc_id'];
		else 
			$id=$blog['c_id'];
    ?>
    <td><a href="<?= site_url('admin/blog_category/edit/'.$id)?>">Edit</a>  |  <a class="confirm_action" title="Are you sure you want to delete this Blog Category?" href ="<?=site_url('admin/blog_category/delete/'.$id)?>">Remove</a></td>
 
  </tr>
<?php endforeach ?>

  </table>
</div>
</div>
</div>	<!-- end page body -->
