
<div id="page_body">

<div class="center">

<div class="admin_title">
<h1>
    <?=($edit=='new')?'Admin Add New Department Entry':'Admin Edit Department Entry'?>
</h1>

</div>
<?php $link ='';
if (isset($department['department_id'])) {
    $link = '/' . $department['department_id'];
}
?>
<div class="admin_cont admin_form acnb_form">
<form action="<?=site_url('admin/departments/save'. $link)?>" enctype="multipart/form-data" method="post" name="pageForm" id="pageForm">
<p>
<label>Department Name:</label> 
<input type="text" name="name" id="name" class="input_field " value="<?php echo isset($department['name'])?$department['name']:''?>" />
</p>
<p>
    <label>Department Initial:</label> 
    <input type="text" name="initial" id="initial" class="input_field " value="<?php echo isset($department['initial'])?$department['initial']:''?>" />
</p>
<p>
    <label>Phone:</label> 
    <input type="text" name="phone" id="phone" class="input_field " value="<?=isset($department['phone'])?$department['phone']:''?>" />
</p>
<?=form_error('title')?>

<p>
    <label>Director Name:</label> 
    <input type="text" name="director_name" id="director_name" class="input_field " value="<?=isset($department['director_name'])?$department['director_name']:''?>" />
</p>
<?php if(isset($department['parent_school'])){ ?>
<p>
    <label>Parent School:</label> 
    <!--<input type="text" name="parent_school" id="parent_school" class="input_field " value="<?=isset($department['parent_school'])?$department['parent_school']:''?>" /> -->
    <select name="parent_school" id="parent_school" class="input_field">
        <option value="">Select</option>
        <?php $n=1; foreach($parent_schools as $ps) :?>
        <option <?php echo (isset($department['parent_school']) && $department['parent_school']==$ps['parent_school'])?'selected="selected"':'';?>><?php echo $ps['parent_school'];?></option> 
        <?php  endforeach;?>
    </select>
</p>
<?php } else { ?>
<p>
    <label>Parent School:</label> 
    <input type="text" name="parent_school" id="parent_school" class="input_field " value="<?=isset($department['parent_school'])?$department['parent_school']:''?>" />
</p>
<?php } ?>
<p>
    <label>Location:</label> 
    <input type="text" name="location" id="location" class="input_field " value="<?=isset($department['location'])?$department['location']:''?>" />
</p>

</br>
<div class="clear"></div>
<div align="center" style = "padding: 10px 0 0;">

<input type="submit" name='submit'  class="btn" value="<?=($edit=='new')?'Add':'Update'?>" />
<input type="button" class="btn" value="Cancel" name="cancel_btn"  onclick="window.location.href='<?=site_url('admin/departments')?>' " />
</div>
</form>
<div class="clear"></div>
</div>
<!-- end admin content -->

</div>

</div>  <!-- end page body -->


