
<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">View The Departments </h1>
<div class="right top_link_dv">

<a href="<?php echo site_url('admin/departments/edit'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove Department</span></a>

     
</div>
<div class="clear"></div>

<div class="ymp_table">
    
      <table width="100%" border="0" cellspacing="0"  style="margin: 20px 0 0 0px;">
  <thead><tr>
      <th width="50" scope="col">#</th>
    <th width="150" scope="col">Department Name</th>
    <th width="150" scope="col">Initial</th>
    <th width="150" scope="col">Director</th>
    <th width="150" scope="col">Parent School</th>
    <th width="150" scope="col">Location</th>
    <th scope="col">Actions</th>
  </tr>
    </thead>
    <tbody'>
        <?php $n = 1; ?>
    <?php foreach ($departments as $department) { ?>
    
    <tr>
    <td><?php echo $n++ ;?></td>
    <td><?php echo $department['name'];?></td>
    <td><?php echo $department['initial']; ?></td>
    <td><?php echo $department['director_name']; ?></td>
    <td><?php echo $department['parent_school']; ?></td>
    <td><?php echo $department['location']; ?></td>
    <td><a href="<?= site_url('admin/departments/edit/'.$department['department_id'])?>"><img src="<?=base_url();?>images/edit_icon.png" width="21" height="21" alt=" " /></a>&nbsp;&nbsp;<a class="confirm_action" title="Are you sure you want to delete this Course?" href ="<?=site_url('admin/departments/delete/'.$department['department_id'])?>"><img src="<?=base_url();?>images/delete_icon.jpg" width="21" height="21" alt=" " /></a></td>
    </tr>

 <?php }  ?>
</tbody>
           
</table>


</div>
</div>

</table>


</div>

</div>  <!-- end page body -->

