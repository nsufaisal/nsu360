






<div id="page_body">

<div class="center">

<div class="admin_title">

<h1 class="left">Admin Tutorials Controller</h1>

<a href="<?php  echo site_url('admin/tutorials/edit/0'); ?>" class="right top_btn">Add Tutorial</a>

<div class="clear"></div>
</div>


<div class="ymp_table ymp_table_single">
  <table width="100%" border="0" cellspacing="0">
  <tr>
    <th width="200" style="padding-left:30px;" scope="col">Category  </th>
    <th width="200" scope="col">Name  </th>
    <th width="400" scope="col">Description </th>
    <th scope="col">Actions</th>
  </tr>
   <?php  foreach($tutorials as $tutorial){ ?>	
  <tr>
    <td style="padding-left:30px;"><?php echo $tutorial['category']; ?></td>
    <td><?php echo $tutorial['name']; ?></td>
    <td><?php echo $tutorial['description']; ?></td>
    <td class="action"><a href="<?php echo site_url('admin/tutorials/view/'.$tutorial['tutorial_id']);?>">view</a>  |<a href="<?php echo site_url('admin/tutorials/edit/'.$tutorial['tutorial_id']);?>">Edit</a>  |  <a href="<?php echo site_url('admin/tutorials/delete/'.$tutorial['tutorial_id'])?>" title="Are you sure you want to delete this tutorial?" class="confirm_action">Remove</a></td>
  </tr>
       <?php } ?>
  
  
</table>

  </div>

</div>

</div>	<!-- end page body -->
