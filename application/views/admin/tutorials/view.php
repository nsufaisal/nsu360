<style>
	#tut_video{
		display:block;
		width:417px;
		background-color:#ebf0ef;
		border:2px solid white;
		margin:0 auto;
		  -webkit-box-shadow: 2px 2px 2px -2px grey;
	   -moz-box-shadow: 2px 2px 2px -2px grey;
	        box-shadow: -1px 2px 2px -2px grey;
	}
</style>

<div id="page_body">
<div class="center">

<div class="admin_title">
<h1 class="left">Admin View Tutorial</h1>
<div class="clear"></div>
</div>

<div class="admin_cont">
        <!--view tutorial page -->
        <div class="view-tutorial">
        <h2><strong><?=$tutorial['name']?></strong></h2>
        <p><?=$tutorial['description']?></p>
    </div>
    
    <video id="tut_video" class="video-js vjs-default-skin" controls
	  preload="auto" width="417" height="249" poster="<?=base_url('uploads/tutorials/'.$tutorial['video_image'])?>"
	  data-setup="{}">
	  <source src="<?=base_url('uploads/tutorials/'.$tutorial['video_file'])?>" type='video/mp4'>
<!--	  <source src="my_video.webm" type='video/webm'> -->
	</video>
</div>
</div>
</div>	<!-- end page body -->
