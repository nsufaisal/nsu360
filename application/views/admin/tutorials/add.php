
<div id="page_body">
<div class="center">

<div class="admin_title">
<h1 class="left">Admin Add Tutorial</h1>
<div class="clear"></div>
</div>
<?php
	$id=0;
	if(isset($tutorial['tutorial_id']))
	{
		$id=$tutorial['tutorial_id'];
	}
?>
<div class="admin_cont edit_form_y">
<form  method="post" id="pageForm"  enctype="multipart/form-data"  action="<?=site_url('admin/tutorials/save/'.$id)?>">
<p>
<strong>Category:</strong>
<select name="category" id="category" class="input_field">
	<option value="" >Select</option>
<?php
	$selected='';
	foreach($tut_categories as $row)
	{
		if(isset($tutorial['category_id']))
		{
			if($tutorial['category_id']==$row['category_id'])
			{
				$selected='selected="selected"';
				
				echo '<option value="'.$row['category_id'].'" '.$selected.'>'.$row['name'].'</option>';
			}
			else {
				echo '<option value="'.$row['category_id'].'">'.$row['name'].'</option>';
			}
		}
		else {
			echo '<option value="'.$row['category_id'].'">'.$row['name'].'</option>';
		}
		
	}
?>
</select><?PHP if (form_error('category')) { ?><div><?php echo form_error('category'); ?></div><?PHP } ?>
</p>

<p>
<strong>Name:</strong>
<input type="text" name="name" id="name" class="input_field" value="<?=isset($tutorial['name'])?$tutorial['name']:''?>"/>
<?PHP if (form_error('name')) { ?><div><?php echo form_error('name'); ?></div><?PHP } ?>
</p>

<p>
<strong>Description:</strong>
<textarea name="description" id="description" cols="" rows="" class="input_field validate[required]" style="width:530px;height:75px;"><?=isset($tutorial['description'])?$tutorial['description']:''?>
</textarea>
<?PHP if (form_error('description')) { ?><div><?php echo form_error('description'); ?></div><?PHP } ?>
</p>
<p>
<strong>Upload Video:</strong>
<input type="text"   name="file_name_box_1" onfocus="javascript:openBrowse1();" id="file_name_box_1" style="width:445px;" class="input_field" value="<?=isset($tutorial['video_file'])?$tutorial['video_file']:''?>">&nbsp;
<input type="button" onclick="javascript:openBrowse1();" name="" id="uploadButton" class="btn" value="Browse"> 
<?PHP if (form_error('file_name_box_1')) { ?><div><?php echo form_error('file_name_box_1'); ?></div><?PHP } ?>
</p>
<div id="err_userfile_1"></div>
<div class="clear"></div>

<p>
<strong>Upload Front Image:</strong>
<input type="text" name="file_name_box_2" onfocus="javascript:openBrowse2();" id="file_name_box_2" style="width:445px;" class="input_field" value="<?=isset($tutorial['video_image'])?$tutorial['video_image']:''?>">&nbsp;
<input type="button" name="" id="uploadImageButton"  onclick="javascript:openBrowse2();" class="btn" value="Browse"> 
<?PHP if (form_error('file_name_box_2')) { ?><div><?php echo form_error('file_name_box_2'); ?></div><?PHP } ?>
</p>
<div id="err_userfile_2"></div>
<div class="clear"></div>
<!-- hidden file uploaders -->
<input type="file" name="userfile_1" id="userfile_1" style="display:none;"/>

<input type="file" name="userfile_2" id="userfile_2" style="display:none;"  />  

<!-- hidden file uploaders end -->
<div class="clear"></div>
<div class="sbmt_dv" style="padding:10px 0 0 182px;">
<div id="loading_img"></div>
<input type="submit" name="submit" id="submit" class="btn" value="Save" style="width:80px;" />&nbsp;
<input type="button" name="" class="btn" onclick="window.location='<?=site_url('admin/tutorials')?>'" value="Cancel" style="width:80px;" />    
</div>

</form>


<div class="clear"></div>
</div>
<!-- end admin content -->
</div>

</div>	<!-- end page body -->


<script>
	 	function openBrowse1(){
    		document.getElementById("userfile_1").click();
		}
		function openBrowse2(){
		    document.getElementById("userfile_2").click();
		}
 	
</script>
<!--instance the validator engine ::rtn:: -->    

<script>		
	$(document).ready(function(){  
      $("#userfile_1").change(function(){
	        var filename=$(this).val();	       
	        var size = this.files[0].size;
	        var name=this.files[0].name;
	        var type=this.files[0].type;
	        if(size>26214400){
	        	$("#err_userfile_1").html('<span style="color:red;">*File size exceeds 25MB limit</span><br/><br/>');
	        	 $("#file_name_box_1").val('');
	        }
	        else if(!(/\.(avi|wmv|mov|flv|mp4)$/i).test(filename))
	        {
	        	$("#err_userfile_1").html('<span style="color:red;">*Only video file accepted (avi/wmv/mov/flv/mp4)</span><br/><br/>');	        	
	        	$("#file_name_box_1").val('');
	  	        	 
	        }
	        else{
	        	$("#err_userfile_1").html('');
	        	$("#file_name_box_1").val(name);
	        }
	         
	         
	    });
	    $("#userfile_2").change(function(){
	        var filename=$(this).val();
	        var size = this.files[0].size;
	        var name=this.files[0].name;
	        var type=this.files[0].type;
	        if(size>1048576){
	        	$("#err_userfile_2").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
	        	 $("#file_name_box_2").val('');
	        }
	        else if(!(/\.(gif|jpg|jpeg|png)$/i).test(filename))
	        {
	        	$("#err_userfile_2").html('<span style="color:red;">*Only image file accepted (gif/jpg/png/jpeg)</span><br/><br/>');
	        	 $("#file_name_box_2").val('');
	        }
	        else{
	        	$("#err_userfile_2").html('');
	        	$("#file_name_box_2").val(name);
	        }
	         
	    });	
	    
	});
</script>
<script>
	$("#submit").click(function(){
		$("#loading_img").html('<img src="<?=base_url('images/loading.gif')?>"/>');
	});
</script>


