<div id="page_body">

<div class="center">

<h1 class=""> My Appointments</h1>

<!-- acc_tabs loading-->

<?php $this->load->view('front/member/page_menu');?>

<div class="tabs_contents">

<div class="ymp_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <th scope="col" width="95">Model</th>
    <th scope="col" width="160">Date <a href="#"><img src="<?=base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="115">Duration</th>
    <th scope="col" width="370">Message</th>
    <th scope="col" width="110">Status <a href="#"><img src="<?=base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col">Actions</th>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td class="tbl_red_clr">Pending</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td class="tbl_red_clr">Pending</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td class="tbl_red_clr">Pending</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td class="tbl_red_clr">Pending</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td>Closed</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td>Declined</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td>Closed</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td>Declined</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>02-18-2013</td>
    <td>30 minutes</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
    <td>Closed</td>
    <td><a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
</table>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>