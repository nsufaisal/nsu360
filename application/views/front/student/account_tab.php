<div class="acc_tabs">

<ul>

<li <?php echo (isset($sel) && $sel=='myaccount')?'class="active"':''; ?>><a href="<?php echo site_url('student/myaccount');?>">My Account</a></li>

<li <?php echo (isset($sel) && $sel=='my_courses')?'class="active"':''; ?>><a href="<?php echo site_url('student/courses');?>">My Courses</a></li>

<li <?php echo (isset($sel) && $sel=='exam_grades')?'class="active"':''; ?>><a href="<?php echo site_url('student/content');?>">Exams &amp; Grades</a></li>

<li <?php echo (isset($sel) && $sel=='noticewall')?'class="active"':''; ?>><a href="<?php echo site_url('student/noticewall');?>">My Notice Wall</a></li>

<li <?=(isset($sel) && $sel=='content')?'class="active"':'' ?>><a href="<?=site_url('student/content')?>">My Content</a></li>

<li <?=(isset($sel) && $sel=='message')?'class="active"':'' ?>><a href="<?=site_url('student/message')?>">Message Center</a></li>
<!--

<li <?=(isset($sel) && $sel=='my_appointment')?'class="active"':'' ?>><a href="<?=site_url('student/appointment')?>">My Appointments</a></li>

<li <?=(isset($sel) && $sel=='transaction')?'class="active"':'' ?>><a href="<?=site_url('student/transaction')?>">Transaction History</a></li>
-->

</ul>

<div class="clear"></div>

</div>	<!-- end account tabs -->