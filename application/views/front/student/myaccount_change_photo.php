<link rel="stylesheet" type="text/css" href="<?php echo base_url('js/jquery-image-crop/css/imgareaselect-animated.css');?>" />

<script type="text/javascript" src="<?php echo base_url('js/jquery-image-crop/js/jquery.imgareaselect.pack.js');?>"></script>

<style>
	.wrap{
		margin: 10px auto;
		padding: 10px 15px;
		background: white;
		border: 2px solid #DBDBDB;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		text-align: center;
		overflow: hidden;
	}
	img#uploadPreview{
		border: 0;
		border-radius: 3px;
		-webkit-box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		margin-bottom: 30px;
		overflow: hidden;
	}
	input[type="submit"]{
		border-radius: 10px;
		background-color: #61B3DE;
		border: 0;
		color: white;
		font-weight: bold;
		font-style: italic;
		padding: 6px 15px 5px;
		cursor: pointer;
	}

	input[type="submit"]:hover{
		background-color: white ;
		color: #61B3DE;
		padding: 4px 13px 3px;
		border:2px solid #cdd3d5;
	}	
		.myaccount_avatar{
		overflow:none;
		margin:0 auto;
		padding:5px;
		float:right;
	}
	.polaroid {
  position: relative;
  max-height:300px;
}

.polaroid img {
  border: 8px solid #fff;
  -webkit-box-shadow: 2px 2px 2px #777;
     -moz-box-shadow: 2px 2px 2px #777;
          box-shadow: 2px 2px 2px #777;
}

.change_photo{
	margin-top:15px;	
	text-align:right;

}
.change_photo a{
	text-decoration:none;
	color:#638593;
	font-weight:bold;
	padding:5px;
	background:#E1E1E8;
}
.change_photo a:hover{
	color:#E1E1E8;
	background:#638593;
}

#crop_head{
	font-size:1.4em;
	color:#666666;
	margin:10px;
}

#file_instructions{
	text-align:left;
	padding:10px;
	margin-bottom:20px;
	
}
#file_instructions h2{
	text-align:left;
	font-size:1.4em;
	color:#EA8111;
	
}
#file_instructions ul{
	margin-top:10px;
}
#file_instructions ul li{
	list-style:square;
	font-size:1em;
	color:#555555;
	margin-left:20px;
	margin-top:5px;
	
}

</style>

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> Change Profile Photo <sup style="color:green; font-size:.5em;font-style: italic;" title="Student"> student</sup></h1>

<div class="right top_link_dv">


</div>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/student/account_tab');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac" style="height:450px;">

<div class="left">

	<h2 id="crop_head" style="display: none;">You can select &amp; crop a portion</h2>


<div class="myaccount_avatar polaroid">

<?php 
	$avatar_name='unknown_avatar.png';
	
	if($my_info['gender']=='M')
		$avatar_name='unknown_avatar_male.png';
?>
	<img id="old_photo" src="<?php echo base_url('uploads/student/'.$member_id.'/'.$my_info['photo']);?>" 
        onerror="this.src=' <?php echo base_url(); ?>images/<?php echo $avatar_name;?>'" width="300" alt="Avatar" />

	
	<img id="uploadPreview" width="300"  style="display:none;"/>
</div>

</div>	<!-- end left container -->

<div class="right">

<div class="wrap">
	<!-- image preview area-->

	<!-- image uploading form -->
	<div id="file_instructions">
		<h2>File upload criteria:</h2>
	<ul>
		<li>Maximum file size: 500KB</li>
		<li>Allowed file extensions: jpg/jpeg</li>
	</ul>
	
	</div>

	<form action="<?php echo site_url('student/myaccount/save_photo_confirmation');?>" method="post" enctype="multipart/form-data">
		<input id="uploadImage" type="file" accept="image/jpeg" name="image" />
		<input type="submit" value="Save Photo">

		<!-- hidden inputs -->
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
	</form>
</div><!--wrap-->




</div>	<!-- end right container -->

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>


<!--instance the validator engine ::rtn:: -->	
<script>
		function setInfo(i, e) {
			$('#x').val(e.x1);
			$('#y').val(e.y1);
			$('#w').val(e.width);
			$('#h').val(e.height);
		}
		
		$(document).ready(function() {
			$("#uploadImage").val("");
			var p = $("#uploadPreview");
			
			// prepare instant preview
			$("#uploadImage").change(function(){
				// fadeOut or hide preview
				$("img#old_photo").hide();
				p.fadeOut();
						
				// prepare HTML5 FileReader
				var oFReader = new FileReader();
				oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
		
				oFReader.onload = function (oFREvent) {
					p.attr('src', oFREvent.target.result).fadeIn(2000);
					$("#crop_head").show();
			   		
				};
			});
		
			// implement imgAreaSelect plug in (http://odyniec.net/projects/imgareaselect/)
			$('img#uploadPreview').imgAreaSelect({
				// set crop ratio (optional)
				aspectRatio: '3:3.5',
				onSelectEnd: setInfo
			});
		});
		
	</script>