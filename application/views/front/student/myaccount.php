<style>
	.myaccount_avatar{
		max-height: 300px;
		overflow:none;
		margin:0 auto;
		padding:5px;
		float:right;
	}
	.polaroid {
  position: relative;
}

.polaroid img {
  border: 8px solid #fff;
  -webkit-box-shadow: 2px 2px 2px #777;
     -moz-box-shadow: 2px 2px 2px #777;
          box-shadow: 2px 2px 2px #777;
}

.change_photo{
	margin-top:15px;	
	text-align:right;

}
.change_photo a{
	text-decoration:none;
	color:#638593;
	font-weight:bold;
	padding:5px;
	background:#E1E1E8;
}
.change_photo a:hover{
	color:#E1E1E8;
	background:#638593;
}
</style>
<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> My Account <sup style="color:green; font-size:.5em;font-style: italic;" title="Student"> student</sup></h1>

<div class="right top_link_dv">

	<a href="<?= site_url('student/myaccount/edit')?>" class="top_linkt"><span><img src="<?=base_url();?>images/z_pen.png" width="14" height="18" alt=" " /> Edit My Account</span></a>

</div>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/student/account_tab');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac">

<div class="left">

<div class="myaccount_avatar polaroid">

<?php 
	$avatar_name='unknown_avatar.png';
	
	if($my_info['gender']=='M')
		$avatar_name='unknown_avatar_male.png';
?>

<img src="<?php echo base_url('uploads/student/'.$member_id.'/'.$my_info['photo']);?>" 
        onerror="this.src=' <?php echo base_url(); ?>images/<?php echo $avatar_name;?>'" width="220" alt="Avatar" />
		
	<div class="change_photo">
		<a href="<?php echo site_url('student/myaccount/change_photo');?>">Change this photo</a>
	</div>
</div>

</div>	<!-- end left container -->

<div class="right">

<p>

<strong>NSU ID:</strong>

<?php echo isset($my_info['student_id'])?$my_info['student_id']:''; ?>

</p>

<p>

<strong>First Name:</strong>

<?php echo isset($my_info['first_name'])?$my_info['first_name']:''; ?>

</p>



<p>

<strong>Last Name:</strong>

<?php echo isset($my_info['last_name'])?$my_info['last_name']:''; ?>

</p>



<p>

<strong>Email Address:</strong>

<?php echo isset($my_info['email'])?$my_info['email']:''; ?>

</p>

<p>

<strong>Phone Number:</strong>

<?php echo isset($my_info['phone'])?$my_info['phone']:''; ?>

</p>

<p>

<strong>Gender:</strong>

<?php echo isset($my_info['gender'])?$my_info['gender']:''; ?>

</p>

<p>

<strong>Blood Group:</strong>

<?php echo isset($my_info['blood_group'])?$my_info['blood_group']:''; ?>

</p>


<p>

<strong>Address:</strong>

<?php echo isset($my_info['curr_address'])?$my_info['curr_address']:''; ?>

</p>


</div>	<!-- end right container -->

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>


<!--instance the validator engine ::rtn:: -->	
<script>
    $("#pageForm").validationEngine('attach', {promptPosition: "topRight", scroll: false},
    {focusFirstField: true});
</script>