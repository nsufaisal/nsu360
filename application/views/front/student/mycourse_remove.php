<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?php echo base_url();?>"><img height="50" width="188" alt=" " src="<?php echo base_url();?>images/pp_logo.gif" /></a></h1>
<a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2>Remove Course</h2>
<?=$content;?>

<!--submit buttons -->
<div class="submit-buttons">
<a href="<?php echo site_url('student/courses/remove_confirmation/'.$semester_course_id);?>" onClick="closePP();" class="grey-button">OK</a>
<a href="javascript:void(0)" onClick="closePP();" class="grey-button">Close</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>