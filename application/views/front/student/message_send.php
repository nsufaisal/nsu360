<script type="text/javascript" src="<?=base_url()?>js/tinymce/tinymce.min.js"></script>

<div id="page_body">

<div class="center">

<h1 class=""> Send a Message</h1>



<!-- acc_tabs loading-->

<?php $this->load->view('front/member/page_menu');?>



<div class="tabs_contents" style="padding:10px;">

<div class="msg_cntr_tab msg_cntr_tab_not_main">

<div class="acc_tabs left">

<?=$this->load->view('front/member/message_tab')?>

</div>
<!-- end acc tabs -->



<div class="clear"></div>
</div>
<!-- end message center tab -->

<div class="acdivs msg_cntr_tabs_cont_inner">
<form action="<?=site_url('member/message/send_confirmation')?>" name="pageFrom" id="pageForm" method="post">

<?=validation_errors()?>

<div class="edit_form">

<p>

<strong>To:</strong>

<input name="to" id="to" type="text" class="input_field validate[required]" value="" />

</p>
<div id="err_receipient"></div>
<p>

<strong>Subject:</strong>

<input name="subject" id="subject" type="text" class="input_field validate[required]" value="" />

</p>

<p>

<strong>Message:</strong>

<textarea name="message" id="message" cols="" rows="" class="input_field validate[required]" style="height:95px;"></textarea>

</p>


<div class="clear"></div>



<div align="center" style="padding:10px 0 0;">

<input name="submit" type="submit" id="submit" class="btn" value="Send" />&nbsp;

<input name="cancel" id="cancel"  onclick=" window.location .href ='<?=site_url('member/message' )?> '"  type="button" class="btn" value="Cancel" />

</div>

</div>
<!-- end ac div 2 -->


</form>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>

<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});
       $("#to").change(function (){
       	var to = $('#to').val();
	       $.ajax({
	            type : "post" ,
	            data : {'to':to},
	            async :      false,
	            url: "<?=site_url('users_ajx/check_msg_receipient' )?> ",
	            success : function (result ){
		            status =result;
		            if(result)
		            {
		            	$( "#err_receipient").html('' );
		            }            	   
		            else{
		                $( "#err_receipient").html('<span style="color:red"><b>*Invalid receipient</span>');  
		                $("#to").val('');   
		            }                                                                                                                                                           
	          	}
	      });
       });             
 	});
</script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ],
});
</script>