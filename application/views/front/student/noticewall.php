<style>

html {
font-family: sans-serif;
}

body {
font-family: sans-serif;
font-size: 13px;
line-height: 1.4;
color: #252523;
}

table {
	border-collapse: separate;
	border-spacing: 2px;
	border-color: gray;
}

td, th {
display: table-cell;
vertical-align: inherit;
}

.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}

tr.oPending td {
background-color: #f7f7f6;
}


.oPending {
font-style: italic;
}


.oMute, .oPending, .oFormHelp {
color: #7c7c7a;
}
.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}


.oTable tbody tr:first-child td:first-child, .oReportTable tbody tr:first-child td:first-child {
-moz-border-radius-topleft: 7px;
-webkit-border-top-left-radius: 7px;
border-top-left-radius: 7px;
}

.oTable tbody tr:first-child td, .oReportTable tbody tr:first-child td {
border-top-width: 3px;
}

oTable td, .oReportTable td {
border: 1px solid #ededeb;
border-top-width: 0;
border-left-width: 0;
border-right-width: 0;
padding: 8px 11px 7px;

}

.oTable th, .oReportTable th {
color: #252523;
padding: 6px 15px 6px 11px;
font-size: 12px;
font-weight: 700;
}
.oTable td:first-child, .oReportTable td:first-child {
border-left-width: 3px;
}

.oTable td:last-child, .oReportTable td:last-child {
border-right-width: 3px;
}

.oTable tbody tr:last-child {
-moz-border-radius-topright: 7px;
-webkit-border-top-right-radius: 7px;
border-top-right-radius: 7px;
}

.oTable td, .oReportTable td {
	border: 1px solid #ededeb;
	border-top-width: 0;
	border-left-width: 0;
	border-right-width: 0;
	padding: 8px 11px 7px;
}

.nowrap {
white-space: nowrap;
}

.txtRight, table .txtRight td, table .txtRight th, .data .txtRight th, .data .txtRight td, table thead th.txtRight, table tbody td.txtRight {
text-align: right!important;
}

#home_cont{
	padding-top:20px;
}
#home_cont table{
	margin:0 auto;
		
}

	.notice_head .post_date{
		font-style:italic;
		font-size:.9em;
		color:#1873A5;
		float:right;
	} 
.post_date{
		font-style:italic;
		font-size:.9em;
		color:#1c5463;
		float:right;
	}
	
.notice_title{
	font-weight:bold;
	color:#333333;
} 

</style>

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> My Noticewall<sup style="color:green; font-size:.5em;font-style: italic;" title="Student"> student</sup></h1>

<div class="right top_link_dv">
     
</div>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/student/account_tab');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac" style="min-height:330px;">

<div class="tran_history_top">
<div class="clear"></div>
<!-- some thing -->


<div class="filter-list">
      <label><strong>Semester:</strong> </label>

<select name="semester" onchange="page_search_submit()" class="input_field" >
<option value="">Select</option>
<?php
	if(isset($semesters))
	{
		foreach($semesters as $semester){ ?>		
		<option <?php echo ($semester['is_current']=='1')? 'selected':''; ?> value="<?php echo $semester['semester_id'];?>"><?php echo $semester['title'];?></option>
	
	<?php		
		}
	}
	
?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Course..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="clear"></div>
</div>
<div id="home_cont">
<table class="oTable" id="trxTable">

  <thead>
    <tr>
    	<th>Publish Date</th>
    	<th>Course</th>
    	<th>Title</th>
	    <th>Description</th>

        <!--<th class="txtRight">Amount</th> -->
        <th></th>
  </tr>
  </thead>
  <tbody>
  	

<?php if(isset($my_notices) && sizeof($my_notices)>0){ 
$sl_no=1;
function getExcerpt($str , $startPos=0 , $maxLength=100 ) {
        if(strlen ($str ) > $maxLength) {
               $excerpt   = substr ($str , $startPos , $maxLength -3);
               $lastSpace = strrpos ($excerpt , ' ') ;
               $excerpt   = substr ($excerpt , 0 , $lastSpace );
               $excerpt  .= '...';
        } else {
               $excerpt = $str ;
        }       
        return $excerpt;
}
  	foreach($my_notices AS $notice){

	$instructor='TBA';
	
	if(isset($notice['instructor_initial']) && $notice['instructor_initial'] !='')
	{
		$instructor='<a href="'.site_url().'instructors/profile/'.$my_course['instructor_id'].'">'.$my_course['instructor_initial'].'</a>';
	}

 ?>
      <tr class="">  	
		<td class="nowrap"><span class="post_date"><?php echo date('Y-m-d / g:i a',$notice['post_date']);?></span></td>
		<td><?php echo $notice['course_initial'].'('.$notice['section_no'].')';?></td>
  		<td><span class="notice_title"><?php echo getExcerpt(strip_tags($notice['title']),0,50);?></span></td>
  		<td><?php echo getExcerpt(strip_tags($notice['notice_body']),0,60);?></td>

    	<td>
    		<a href="<?php echo site_url('student/noticewall/view/'.$notice['noticewall_id']);?>" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		
    	</td>
    </tr>				
			
  <?php 
		} 
	}	
?>
       
  </tbody>
</table>

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>
</div>


<!--instance the validator engine ::rtn:: -->	
