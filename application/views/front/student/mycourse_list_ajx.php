<div id="home_cont">
	
<table class="oTable" id="trxTable">
<?php if(sizeof($my_courses)>0){ ?>
  <thead>
    <tr>
    	<th>Course Initial</th>
    	<th>Course Title</th>
	    <th>Instructor</th>
        <th>Class Time</th>
        <!--<th class="txtRight">Amount</th> -->
        <th>Class Room</th>
        <th>Approval</th>
        <th></th>
  </tr>
  </thead>
<?php  } ?>
  <tbody>
  	
  <?php
  	foreach($my_courses AS $my_course){

	$instructor='TBA';
	
	if(isset($my_course['instructor_initial']) && $my_course['instructor_initial'] !='')
	{
		$instructor='<a href="'.site_url().'instructors/profile/'.$my_course['instructor_id'].'">'.$my_course['instructor_initial'].'</a>';
	}

  		if($my_course['approval']==1){
  ?>
      <tr class="">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td>Database Design &amp; Management Lab</td>
  		<td><?php echo $instructor;?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	<td>Approved</td>
    	    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		
    	</td>
    </tr>				
			
  <?php 
		} 
		else{
  ?>
  	  	
  	<tr class="oPending">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td><?php echo $my_course['course_title'];?></td>
  		<td><?php echo $instructor;?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	<td>Pending</td>
    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		<a href="javascript:void(0);" onClick="openForm('<?php echo site_url('student/courses/remove2/'.$my_course['id']);?>');" ><img src="<?php echo base_url('images/DeleteRed.png');?>" height="18" alt="delete" /></a>
    	</td>
    </tr>
  			
  <?php	}
	} ?>
       
  </tbody>
</table>

<div class="clear"></div>

<div id="select_course">

<div class="select_region">

<form name="add_course_form" method="post" id="pageForm" action="" >

<strong>Select Course</strong>

<select name="semester_course_id[]" multiple="multiple" id="course_list" style="width:550px;" class="input_field validate[required]">

<option value=""></option>

<?php foreach ($available_courses as $course): ?>
	
<?php 
	$instructor=' [TBA] ';
	
	if(isset($course['instructor_initial']) && $course['instructor_initial'] !='')
	{
		$instructor=' ['.$course['instructor_initial'].'] ';
	}
?>

<option value="<?php echo $course['id'] ?>"><?php echo $course['course_initial'].'('.$course['section_no'].')'.$instructor.' / '.$course['class_day'].' '.$course['class_time'].' ['.$course['course_title'].']'; ?></option>

<?php endforeach; ?>	

</select>

<input type="button" name="submit_btn"  id="submit_btn" style="margin-left: 10px;" class="btn" value="Add" />
	 
</form>		


</div>

</div>
<script>
	$(document).ready(function(){          
      
       $("#course_list").chosen();
       
       $("#submit_btn").click(function(){
       		
       		var course_value=$('#course_list').val();	
       		
       		if(course_value<1)
       		{
       			$(".select_region").effect( "pulsate", {times:2}, 200 );       			
       		}
       		else
       		{
       			$.ajax({
			        type: "POST",
			        async:false,
			        url: "<?php echo site_url('student/courses/ajax_add_course'); ?>",
			        data: {'new_courses': course_value}, 
			        success: function (data) {
			        	$("#home_cont").html(data);
			        },
			    });
      		}      	

       	return false;
       });
            
 	});
</script>
</div>