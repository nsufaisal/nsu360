<div id="page_body">

<div class="center">

<h1 class=""> Message Center</h1>


<!-- acc_tabs loading-->

<?php $this->load->view('front/member/page_menu');?>


<div class="tabs_contents" style="padding:10px;">

<div class="msg_cntr_tab">

<div class="acc_tabs left">

<?=$this->load->view('front/member/message_tab')?>

</div>
<!-- end acc tabs -->

<div class="msgtb_rgt right">

<span>Sort by: <a href="#" class="current">Read</a> <span class="dfrnce">|</span>	<a href="#">Unread</a></span>

<span>Sort by: <a href="#" class="">Name</a> <span class="dfrnce">|</span>	<a href="#">Date/Time</a></span>

<span>New Mesages: 3 <span class="dfrnce">|</span>  Total Messages: 120</span>

</div>

<div class="clear"></div>
</div>
<!-- end message center tab -->

<div class="ymp_table msg_cntr_tabs_cont">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <th scope="col" width="80">Image</th>
    <th scope="col" width="110">Name</th>
    <th scope="col" width="370">Subject</th>
    <th scope="col" width="160">Date/Time</th>
    <th scope="col" width="110">Amount </th>
    <th scope="col">Actions</th>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 1.50</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 2.50</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 2.30</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 5.30</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 1.90</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 5.40</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
  <tr>
    <td><img src="<?=base_url();?>images/x_smal_wmn_img.jpg" width="40" height="31" alt=" " class="img_with_brdr_sdw" /></td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
    <td>02-18-2013 5:30pm ET</td>
    <td class=""><img src="<?=base_url();?>images/x_dlr_cion.gif" class="ngtv_mar"  width="11" height="18" alt=" " /> 1.50</td>
    <td class="action_td">
    	<a href="#"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>
        <a href="#"><img src="<?=base_url();?>images/x_reply_icon.png" width="22" height="22" alt=" " /></a>
        <a href="javascript:void(0);" onClick="openForm('<?=site_url('member/message/remove');?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    </td>
  </tr>
  
</table>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>