<style>

html {
font-family: sans-serif;
}

body {
font-family: sans-serif;
font-size: 13px;
line-height: 1.4;
color: #252523;
}

table {
	border-collapse: separate;
	border-spacing: 2px;
	border-color: gray;
}

td, th {
display: table-cell;
vertical-align: inherit;
}

.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}

tr.oPending td {
background-color: #f7f7f6;
}


.oPending {
font-style: italic;
}


.oMute, .oPending, .oFormHelp {
color: #7c7c7a;
}
.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}


.oTable tbody tr:first-child td:first-child, .oReportTable tbody tr:first-child td:first-child {
-moz-border-radius-topleft: 7px;
-webkit-border-top-left-radius: 7px;
border-top-left-radius: 7px;
}

.oTable tbody tr:first-child td, .oReportTable tbody tr:first-child td {
border-top-width: 3px;
}

oTable td, .oReportTable td {
border: 1px solid #ededeb;
border-top-width: 0;
border-left-width: 0;
border-right-width: 0;
padding: 8px 11px 7px;

}

.oTable th, .oReportTable th {
color: #252523;
padding: 6px 15px 6px 11px;
font-size: 12px;
font-weight: 700;
}
.oTable td:first-child, .oReportTable td:first-child {
border-left-width: 3px;
}

.oTable td:last-child, .oReportTable td:last-child {
border-right-width: 3px;
}

.oTable tbody tr:last-child {
-moz-border-radius-topright: 7px;
-webkit-border-top-right-radius: 7px;
border-top-right-radius: 7px;
}

.oTable td, .oReportTable td {
	border: 1px solid #ededeb;
	border-top-width: 0;
	border-left-width: 0;
	border-right-width: 0;
	padding: 8px 11px 7px;
}

.nowrap {
white-space: nowrap;
}

.txtRight, table .txtRight td, table .txtRight th, .data .txtRight th, .data .txtRight td, table thead th.txtRight, table tbody td.txtRight {
text-align: right!important;
}

#home_cont{
	padding-top:20px;
}
#home_cont table{
	margin:0 auto;
		
}

</style>

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> My Courses<sup style="color:green; font-size:.5em;font-style: italic;" title="Student"> student</sup></h1>

<div class="right top_link_dv">
<?php if(isset($advising_approval) && $advising_approval){ ?>

<a href="<?=site_url('student/courses/add')?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add New Course</span></a>
 
<?php } ?> 
     
</div>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/student/account_tab');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac" style="min-height:330px;">

<div class="tran_history_top">
<div class="clear"></div>
<!-- some thing -->


<div class="filter-list">
      <label><strong>Semester:</strong> </label>

<select name="semester" onchange="page_search_submit()" class="input_field" >
<option value="">Select</option>
<?php
	if(isset($semesters))
	{
		foreach($semesters as $semester){ ?>		
		<option <?php echo ($semester['is_current']=='1')? 'selected':''; ?> value="<?php echo $semester['semester_id'];?>"><?php echo $semester['title'];?></option>
	
	<?php		
		}
	}
	
?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Course..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="clear"></div>
</div>

<div id="home_cont">
<table class="oTable" id="trxTable">
<?php if(sizeof($my_courses)>0){ ?>
  <thead>
    <tr>
    	<th>Course Initial</th>
    	<th>Course Title</th>
	    <th>Instructor</th>
        <th>Class Time</th>
        <!--<th class="txtRight">Amount</th> -->
        <th>Class Room</th>
        <th>Approval</th>
        <th></th>
  </tr>
  </thead>
<?php  } ?>
  <tbody>
  	
  <?php
  
  	foreach($my_courses AS $my_course){

	$instructor='TBA';
	
	if(isset($my_course['instructor_initial']) && $my_course['instructor_initial'] !='')
	{
		$instructor='<a href="'.site_url().'instructors/profile/'.$my_course['instructor_id'].'">'.$my_course['instructor_initial'].'</a>';
	}

  		if($my_course['approval']==1){
  ?>
      <tr class="">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td><?php echo $my_course['course_title'];?></td>
  		<td><?php echo $instructor;?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	<td>Approved</td>
    	    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		
    	</td>
    </tr>				
			
  <?php 
		} 
		else{
  ?>
  	  	
  	<tr class="oPending">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td><?php echo $my_course['course_title'];?></td>
  		<td><?php echo $instructor;?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	<td>Pending</td>
    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		<a href="javascript:void(0);" onClick="openForm('<?php echo site_url('student/courses/remove/'.$my_course['id']);?>');" ><img src="<?php echo base_url('images/DeleteRed.png');?>" height="18" alt="delete" /></a>
    	</td>
    </tr>
  			
  <?php	}
	} ?>
       
  </tbody>
</table>

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>
</div>


<!--instance the validator engine ::rtn:: -->	
