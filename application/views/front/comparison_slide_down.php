<div id="page_body">
<div class="center">
<h1>How It Works</h1>
<?=$content;?>	

<div class="MS_plans">
<a href="javascript:void(0);" onClick="openForm('<?=site_url('howitwork/membership');?>');" class="a_btn"><span class="mg">Membership Guide</span></a>&nbsp;
<a href="<?=site_url('howitwork/comparison')?>" class="a_btn"><span class="mc">Membership Comparison</span></a>
</div>

</div>
</div>

<div id="INL_confirm">
<div class="center">
<h1>Comparison Slide Down</h1>
<?=$comparison_content;?>

<div class="CT_cont">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="header_row">
    <th class="TT_left"></th>
    <th width="420" class="TT_bg">Free Member</th>
    <th class="TT_bg">Paying Member</th>
    <th class="TT_right"></th>
  </tr>
  <tr>
    <td class="TL_bg"></td>
    <td>Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="LB">Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="TR_bg"></td>
  </tr>
  <tr class="alter_row">
    <td class="TL_bg"></td>
    <td>Ipsum Lorem Dolor Sit Amet</td>
    <td class="LB">Ipsum Lorem Dolor Sit Amet</td>
    <td class="TR_bg"></td>
  </tr>
  <tr>
    <td class="TL_bg"></td>
    <td>Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="LB">Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="TR_bg"></td>
  </tr>
  <tr class="alter_row">
    <td class="TL_bg"></td>
    <td>- - -</td>
    <td class="LB">Ipsum Lorem Dolor Sit Amet</td>
    <td class="TR_bg"></td>
  </tr>
  <tr>
    <td class="TL_bg"></td>
    <td>Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="LB">Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="TR_bg"></td>
  </tr>
  <tr class="alter_row">
    <td class="TL_bg"></td>
    <td>- - -</td>
    <td class="LB">Ipsum Lorem Dolor Sit Amet</td>
    <td class="TR_bg"></td>
  </tr>
  <tr>
    <td class="TL_bg"></td>
    <td>Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="LB">Lorem Ipsum Dolor Sit Amet Dilor LormIpsum</td>
    <td class="TR_bg"></td>
  </tr>
  <tr class="alter_row">
    <td class="TL_bg"></td>
    <td>- - -</td>
    <td class="LB">Ipsum Lorem Dolor Sit Amet</td>
    <td class="TR_bg"></td>
  </tr>
  <tr class="lastrow">
    <td class="TL_bg"></td>
    <td></td>
    <td class="LB"></td>
    <td class="TR_bg"></td>
  </tr>
  </table>
</div>	
<!-- end table container -->

<div class="clz_INL"><a href="#"><span>Close</span></a></div>
</div>
</div>