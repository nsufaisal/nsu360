<div id="page_body">
<div class="center">
<h1>Feedback</h1>
<?=$content;?>
<form action="<?=site_url('user_support/feedback_support_confirmation')?>" name="pageForm" id="pageForm" method="post" accept-charset="utf-8">
<div class="support_form">
<p>
<strong>Name:</strong>
<input name="name" id="name" type="text" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p>
<strong>Email:</strong>
<input name="email" id="email" type="text" class="input_field validate[required,custom[email]]" />
</p>

<p>
<strong>Phone Number:</strong>
<input name="phone" id="phone" type="text" class="input_field validate[required]" />
</p>

<p>
<strong>Subject:</strong>
<input name="subject" id="subject" type="text" class="input_field validate[required]" />
</p>

<p>
<strong>Comment / Suggestions:</strong>
<textarea name="message" id="message" cols="" rows="" class="input_field validate[required]"></textarea>
</p>
<div align="center" style="padding-top:15px;">
<input name="submit" id="submit" type="submit" class="btn" value="Send" />&nbsp;
<input name="" onclick=" window.location .href ='<?=site_url ('user_support')?>'" type="button" class="btn" value="Cancel" />
</div>

</div>
</form>

</div>
</div>

<!--instance the validator engine ::rtn:: -->    
<script>              
   $("#pageForm").validationEngine( 'attach', {promptPosition : "topRight", scroll: false},
   {focusFirstField : true });              
</script>
