<!DOCTYPE html>
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<title><?PHP echo @$meta['title']?></title>

		<meta name="description" content="<?PHP echo @$meta['description']?>" />

		<meta name="keywords" content="<?PHP echo @$meta['keywords']?>" />
		
		<link rel="icon" href="<?=base_url()?>/favicon.png" type="image/png">
		
        <link href="<?php echo base_url()?>js/bootstrap/css/bootstrap.css" rel="stylesheet"/>
		
		<link href="<?PHP echo base_url()?>css/reset.css" rel="stylesheet" type="text/css" />
		
		<link href="<?PHP echo base_url()?>css/popup.css" rel="stylesheet" type="text/css" />
        
		<link href="<?PHP echo base_url()?>css/my_styles.css" rel="stylesheet" type="text/css" />
		
		

		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>js/jquery/jquery-ui-1.8.10.custom.css"/>

		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/calendar.css"/>
		
		

		<script type="text/javascript" src="<?php echo base_url()?>js/jq/jquery.js"></script>
		
		<script src="<?php echo base_url()?>js/jquery.simplemodal.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap/js/bootstrap.min.js"></script>
		
        <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap/js/holder.js"></script>

		<script type="text/javascript" src="<?php echo base_url()?>js/jquery/jquery-ui-1.8.10.custom.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url()?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

		<script type="text/javascript" src="<?php echo base_url()?>js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url()?>js/jquery.tablesorter-update.js"></script>

		<script type="text/javascript" src="<?php echo base_url()?>js/front.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

		<link rel="stylesheet" href="<?php echo base_url()?>css/validationEngine.jquery.css" type="text/css"/>

		<script src="<?php echo base_url()?>js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

		<script src="<?php echo base_url()?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	
	</head>

	<body>
		<div id="gray_bak" style="display:none; position:absolute; margin: auto; top: 0; left: 0; width: 100%; height: 100%; z-index: 999;background-color: #7f7f7f; opacity:0.8; filter: alpha(opacity=80);overflow:auto" onclick="closePP();">&nbsp;</div>

		<div id="basic-modal-content" style="display:none;padding:8px; "></div>

		<div style="display:none">

			<div id="modal_message"><?PHP echo $this->session->flashdata('message');?></div>

			<div id="modal_message1"><?PHP echo @$message?></div>

			<a id="message_pp" href="<?PHP echo $this->session->flashdata('url');?>">View Message</a>

			<a id="confirm_pp" href="<?PHP echo $this->session->flashdata('url');?>">View Message</a>

		</div>
		<div id="wrapper">
		<?PHP echo $this->load->view('front/header');?>
		<div class="loadingShow" style="display:none">
			<img src="<?=base_url();?>images/loading-blue.gif" alt="Loading.." title="Loading.." />
		</div>
		<?PHP echo $this->load->view($body);?>
		
		<div id="INL_confirm" style="display:none"><div class="center">
			<div id="INL_content"><?php echo $this->session->flashdata('slider_content');?></div><div class="clz_INL" id="clz_INL"><a href="<?= site_url() ?>"><span>Close</span></a></div> </div> </div>

		<?PHP echo $this->load->view('front/footer');?>
		</div>	

		<script type="text/javascript">
			
			$(document).ready(function() {
		//		$.fancybox.showActivity();
				$(".fancybox").fancybox({padding:0, margin:0});
		//	$.fancybox.hideActivity()  ;
											
			});						
		</script> 

	<script>
		$(document).ready(coolSliderLink());
	</script>


	<script>
		function INT_slideUP(){
			 $("#INL_confirm").slideUp("slow");
		 	 $(".formErrorContent").remove();
		 	 $(".formErrorArrow").remove();			 	 
		}
		
	</script>
	<script>
		$(document).ready(function(){
			if($("#INL_content").html())
			 $("#INL_confirm").slideDown("slow");
			 
			 $(".clz_INL").click(function(){ 
			 	INT_slideUP();
			 	return false;	 	 
			 });
			 
			  $(".cancel_btn").click(function(){ 
			 	INT_slideUP();
			 	return false;	 	 
			 });
		});
	</script>
    <script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'wwwnsu360com'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
    </script>
    	
	</body>

</html>
