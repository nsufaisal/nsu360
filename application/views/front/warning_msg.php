<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/logo.png" /></a></h1>
<a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2><?php echo $heading;?></h2>
<p><?php echo $content;?></p>

<!--submit buttons -->
<div class="submit-buttons">
<a href="javascript:void(0);" onClick="closePP();" class="grey-button">OK, Got it !</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>