<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to our site</title>

<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />
</head>

<body class="popup-wrapper">
<!--popup -->
<div class="popup popup_b">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="#"><img src="<?=base_url();?>images/popup-logo.png" width="189" height="60" alt="" /></a></h1>
<a href="#" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2>Membership Tour</h2>
<?=$content;?>
<div align="center" class="tourvideo"><a href="#"><img src="<?=base_url();?>images/tourvideo.jpg" width="513" height="342" alt=" " /></a></div>
<!--submit buttons -->
<div class="submit-buttons">
<a href="#" class="grey-button" style="min-width:69px;">Close</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>
<!--/popup -->
</body>
</html>
