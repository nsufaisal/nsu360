<!-- end navigation -->
  
  <div id="page_body">
    <div class="center">
      <div id="advancesrch_cont">
        <form action="" method="post">
          <div class="left">
            <div class="select_box4">
              <p id="drop13">Clips</p>
              <select name="" onchange="$('#drop13').html($(this).val());" class="dropdown">
                <option>Clips</option>
              </select>
            </div>
            <div class="select_box4">
              <p id="drop14">Pics</p>
              <select name="" onchange="$('#drop14').html($(this).val());" class="dropdown">
                <option>Pics</option>
              </select>
            </div>
            <div class="select_box4">
              <p id="drop15">Audio</p>
              <select name="" onchange="$('#drop15').html($(this).val());" class="dropdown">
                <option>Audio</option>
              </select>
            </div>
            <div class="select_box4">
              <p id="drop16">Most Popular</p>
              <select name="" onchange="$('#drop16').html($(this).val());" class="dropdown">
                <option>Most Popular</option>
              </select>
            </div>
            <div class="select_box4">
              <p id="drop17">Category</p>
              <select name="" onchange="$('#drop17').html($(this).val());" class="dropdown">
                <option>Category</option>
              </select>
            </div>
          </div>
          <!-- end left area -->
          
          <div class="advmodel_search">
            <input name="" type="text" class="input_field" value="Search Models..." />
            <input name="" type="image" src="<?=base_url();?>images/srch_btn.png" />
            <span><a href="#">Advanced Search</a></span> </div>
          <!-- end right area -->
          
          <div class="clear"></div>
        </form>
      </div>
      <!-- end advance search area -->
      
      <div class="tophead">
        <h1 class="left">Lorem Ipsum&acute;s Store </h1>
        <div class="right"> <a href="#" class="top_linkt"><span>Tip Me</span></a> &nbsp; <a href="#" class="top_linkt"><span>My Wishlist</span></a> &nbsp; <a href="#"><img src="<?=base_url();?>images/lve_icon_1.gif" alt=" " /></a> </div>
        <div class="clear"></div>
      </div>
      <div id="home_cont">
        <div class="left" id="HM_left">
          <div class="sorting_area">
            <form action="" method="post">
              <strong class="left">Sort By:</strong>
              <div class="sortby">
                <p id="drop10">Random</p>
                <select name="" onchange="$('#drop10').html($(this).val());" class="dropdown">
                  <option>Random</option>
                </select>
              </div>
              <div class="shownumber"><strong>Results per Page:</strong> <a href="#" class="">20</a> <a href="#" class="active">35</a> <a href="#" class="">75</a> <a href="#" class="">100</a></div>
              <div class="viewcont"> <a href="#" class="THview THview_sel">Thumb View</a> <a href="#" class="LSview">List View</a> <!-- add this class name LSview_sel when this view is selected --> 
                
              </div>
            </form>
            <div class="clear"></div>
          </div>
          <!-- end sorting area -->
          
          <div class="VOD ymp_vod">
            <ul class="thumbview">
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /> </span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li class="lastchild">
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /> </span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li class="lastchild">
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /> </span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li class="lastchild">
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /> </span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li class="lastchild">
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /> </span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a></div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="right">$5.00</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
              <li class="lastchild">
                <div class="model_img video_preview"><a href="#"><img src="<?=base_url();?>images/modelimg_2.jpg" width="132" height="142" alt=" " /></a>
                  <div class="play"><a href="#">Play</a></div>
                </div>
                <div class="VDT">
                  <h6><strong>Lorem Ipsum - Part 1</strong></h6>
                  <p><strong><span><img src="<?=base_url();?>images/rating_pink.gif" width="62" height="13" alt=" " /></span> 4/5</strong> </p>
                  <p>Uploaded: 14 Feb, 2013</p>
                </div>
                <div class="ymodel_stats"> <span class="left">12:45</span> <span class="right">$25.50</span>
                  <div class="clear"></div>
                </div>
                <!--end model stats--> 
                
              </li>
            </ul>
          </div>
          <div class="clear"></div>
          <div class="pagination_cont">
            <div class="left">Displaying <strong>35</strong> of <strong>356</strong> results</div>
            <div class="shownumber" style="padding:0 0 0 40px;"><strong>Results per Page:</strong> <a href="#" class="">20</a> <a href="#" class="active">35</a> <a href="#" class="">75</a> <a href="#" class="">100</a></div>
            <div class="right pagination">
              <ul>
                <li class="previous_li"><a href="#">Previous</a></li>
                <li class=""><a href="#">1</a></li>
                <li class=""><a href="#">2</a></li>
                <li class="active"><a href="#">3</a></li>
                <li class=""><a href="#">4</a></li>
                <li class=""><a href="#">5</a></li>
                <li class=""><a href="#">6</a></li>
                <li class="next_li"><a href="#">Next</a></li>
              </ul>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end pagination --> 
          
        </div>
        <!-- end left container -->
        
        <div class="right" id="HM_right">
          <div class="TRM_cont">
            <div class="MS_title" style="margin-top:0;"><span><a href="#">Top Sellers</a></span></div>
            <ul class="thumbview">
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
              <li>
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="modelname">12:45</div>
                  <div class="model_status">$25.50</div>
                  <div class="clear"></div>
                </div>
                <div class="model_video_icon"> <a href="#"><img width="52" height="52" alt=" " src="<?=base_url();?>images/w_video_img.png" /></a> </div>
              </li>
              <li class="lastchild">
                <div class="model_img"><a href="#"><img src="<?=base_url();?>images/top_sellers_2.jpg" width="122" height="122" alt=" " /></a></div>
                <div class="model_stats">
                  <div class="model_status">$5.00</div>
                  <div class="clear"></div>
                </div>
              </li>
            </ul>
            <div class="clear"></div>
          </div>
          <!-- end featured cont --> 
          
        </div>
        <!-- end right -->
        
        <div class="clear"></div>
      </div>
      <!-- end home main container --> 
      
    </div>
  </div>
  <!-- end page body -->