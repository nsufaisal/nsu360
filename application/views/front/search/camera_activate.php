<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to our site</title>

<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/my_styles.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />
</head>

<body class="popup-wrapper">
<!--popup -->
<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="#"><img src="<?=base_url();?>images/pp_logo.gif" width="188" height="59" alt=" " /></a></h1>
<a href="#" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2>Camera Active	</h2>
<?=$content;?>

<!--submit buttons -->
<div class="submit-buttons" style="padding-top:20px;">
<a href="#" class="grey-button">Close</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>
<!--/popup -->
</body>
</html>
