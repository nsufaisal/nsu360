<div id="page_body">
    <div class="center">
      <div id="advancesrch_cont">
        <form action="" method="post">
          <div class="left">
            <div class="select_box2">
              <p id="drop1">Girl Alone</p>
              <select name="" onchange="$('#drop1').html($(this).val());" class="dropdown">
                <option>Girl Alone</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop2">Age</p>
              <select name="" onchange="$('#drop2').html($(this).val());" class="dropdown">
                <option>Age</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop3">Race</p>
              <select name="" onchange="$('#drop3').html($(this).val());" class="dropdown">
                <option>Race</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop4">Hair</p>
              <select name="" onchange="$('#drop4').html($(this).val());" class="dropdown">
                <option>Hair</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop5">Eyes</p>
              <select name="" onchange="$('#drop5').html($(this).val());" class="dropdown">
                <option>Eyes</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop6">Chest</p>
              <select name="" onchange="$('#drop6').html($(this).val());" class="dropdown">
                <option>Chest</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop7">Rate</p>
              <select name="" onchange="$('#drop7').html($(this).val());" class="dropdown">
                <option>Rate</option>
              </select>
            </div>
            <div class="select_box">
              <p id="drop8">Misc</p>
              <select name="" onchange="$('#drop8').html($(this).val());" class="dropdown">
                <option>Misc</option>
              </select>
            </div>
            <div class="select_box3">
              <p id="drop9">Country</p>
              <select name="" onchange="$('#drop9').html($(this).val());" class="dropdown">
                <option>Country</option>
              </select>
            </div>
          </div>
          <!-- end left area -->
          
          <div class="advmodel_search">
            <input name="" type="text" class="input_field" value="Search Models..." />
            <input name="" type="image" src="<?=base_url();?>images/srch_btn.png" />
            <span><a href="#">Advanced Search</a></span> </div>
          <!-- end right area -->
          
          <div class="clear"></div>
        </form>
      </div>
      <!-- end advance search area -->
      
      <h1>Chat Host Appointment Scheduling</h1>
      <div class="edit_form chat_appoinment">
        <form action="#" method="get">
          <p> <strong>Requested Date:</strong> <span class="has_date_icon">
            <input name="" type="text" class="input_field" />
            <a class="date_icon_s" href="#"><img width="16" height="13" alt=" " src="<?=base_url();?>images/t_date_icon.gif" /></a> </span> </p>
          <p> <strong>Requested Time:</strong>
            <select class="input_field">
              <option>Hour</option>
            </select>
            <select class="input_field">
              <option>Minute</option>
            </select>
            <select class="input_field">
              <option>AM/PM</option>
            </select>
          </p>
          <p> <strong class="has_more_pad">Appointment Type:</strong>
            <label>
              <input style="margin-left:0;" checked="checked" type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_0" />
              Cam </label>
            <label>
              <input type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_1" />
              Phone </label>
            <label>
              <input type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_2" />
              Cam w/Phone </label>
          </p>
          <div style="padding:10px 0 0 20px;">
            <input type="button" value="Send Request" class="btn" name="" style="width:120px;" />
            &nbsp;
            <input type="button" value="Cancel" class="btn" name="" />
          </div>
        </form>
        <div class="clear"></div>
      </div>
      <!-- end chat appointment --> 
      
    </div>
  </div>