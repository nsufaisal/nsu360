<div id="page_body">
<div class="center">

<div id="advancesrch_cont">
<form action="" method="post">
<div class="left">

<div class="select_box2">
<p id="drop1">Girl Alone</p>
<select name="" onchange="$('#drop1').html($(this).val());" class="dropdown">
<option>Girl Alone</option>
</select>
</div>

<div class="select_box">
<p id="drop2">Age</p>
<select name="" onchange="$('#drop2').html($(this).val());" class="dropdown">
<option>Age</option>
</select>
</div>

<div class="select_box">
<p id="drop3">Race</p>
<select name="" onchange="$('#drop3').html($(this).val());" class="dropdown">
<option>Race</option>
</select>
</div>

<div class="select_box">
<p id="drop4">Hair</p>
<select name="" onchange="$('#drop4').html($(this).val());" class="dropdown">
<option>Hair</option>
</select>
</div>

<div class="select_box">
<p id="drop5">Eyes</p>
<select name="" onchange="$('#drop5').html($(this).val());" class="dropdown">
<option>Eyes</option>
</select>
</div>

<div class="select_box">
<p id="drop6">Chest</p>
<select name="" onchange="$('#drop6').html($(this).val());" class="dropdown">
<option>Chest</option>
</select>
</div>

<div class="select_box">
<p id="drop7">Rate</p>
<select name="" onchange="$('#drop7').html($(this).val());" class="dropdown">
<option>Rate</option>
</select>
</div>

<div class="select_box">
<p id="drop8">Misc</p>
<select name="" onchange="$('#drop8').html($(this).val());" class="dropdown">
<option>Misc</option>
</select>
</div>

<div class="select_box3">
<p id="drop9">Country</p>
<select name="" onchange="$('#drop9').html($(this).val());" class="dropdown">
<option>Country</option>
</select>
</div>

</div>	<!-- end left area -->

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="Search Models..." />
<input name="" type="image" src="<?=base_url();?>images/srch_btn.png" />
<span><a href="#">Advanced Search</a></span>
</div>	<!-- end right area -->

<div class="clear"></div>
</form>
</div>	<!-- end advance search area -->

<div id="home_cont">

<div class="sorting_area">
<form action="" method="post">

<strong class="left">Sort By:</strong>
<div class="sortby LVM">
<p id="drop10">Random</p>
<select name="" onchange="$('#drop10').html($(this).val());" class="dropdown">
<option>Random</option>
</select>
</div>

<div class="shownumber"><strong>Models Per Page:</strong> <a href="#" class="">20</a> <a href="#" class="active">35</a> <a href="#" class="">75</a> <a href="#" class="">100</a></div>

<div class="viewcont">
<a href="#" class="THview">Thumb View</a>	<!-- add this class name THview_sel when this view is selected -->
<a href="#" class="LSview LSview_sel">List View</a>	<!-- adddfdfdf this class name LSview_sel when this view is selected -->
</div>

</form>
<div class="clear"></div>
</div>	<!-- end sorting area -->

<div class="homelistview">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="475">Model Description</th>
    <th width="130">Phone Listings</th>
    <th width="145">Available For</th>
    <th width="125">Rating</th>
    <th>Items For Sale</th>
  </tr>
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
  
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
  
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
  
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
  
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
  
  <tr>
    <td valign="top">
      <div class="model_info">
        <div class="left"><img src="<?=base_url();?>images/modelimg_1.jpg" width="132" height="142" alt=" " />
        	<div class="model_stats">
			<div class="modelname">Carmen Jones</div>
			<div class="model_status"><span class="online">Online</span></div>
			<div class="clear"></div>
			</div>	<!-- end online status -->
        </div>
        <div class="right">
          <h6><strong>Carmen Jones</strong></h6>
          <a href="#">View Profile</a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut metus velit, vitae hendrerit urna. Nullam eget dui sit amet augue consectetur dapibus. Aliquam ante erat, viverra a fermentum in, convallis non libero. Duis dignissim mattis feugiat.</p>
          </div>
        <div class="clear"></div>
        </div>
    </td>
    <td valign="top">502-555-1111<br />
      502-555-2222<br />502-555-3333<br />502-555-4444<br />502-555-5555<br /></td>
    <td valign="top">
    <div class="model_availability">
    <p><span>Free Chat</span><span class="ma_desc">0.00</span></p>
	<p><span>Private Chat</span><span class="ma_desc">15.99</span></p>
	<p><span>Group Chat1</span><span class="ma_desc">3.99</span></p>
	<p><span>Group Chat2</span><span class="ma_desc">2.99</span></p>
	<p><span>Group Chat3</span><span class="ma_desc">2.49</span></p>
	<p><span>Group Chat4+</span><span class="ma_desc">1.99</span></p>
	<p><span>Voyeur</span><span class="ma_desc">9.99</span></p>
    </div>
    </td>
    <td valign="top"><img src="<?=base_url();?>images/model_rating.gif" width="82" height="17" alt=" " /> 4/5</td>
    <td valign="top">
    <div class="msale_items">
    <p><span>Videos</span><span class="ma_desc">5</span></p>
    <p><span>Photos</span><span class="ma_desc">32</span></p>
    <p><span>Rec. Sessions</span><span class="ma_desc">11</span></p>
    <p><span>PDFs</span><span class="ma_desc">6</span></p>
    <p><span>Lorem Ipsum</span><span class="ma_desc">12</span></p>
    <p><span>Lorem</span><span class="ma_desc">10</span></p>
    <p><span>Ipsum</span><span class="ma_desc">4</span></p>
    </div>
    </td>
  </tr>
</table>

<div class="pagination_cont">
<div class="left">Displaying <strong>35</strong> of <strong>356</strong> results</div>
<div class="right pagination">
<ul>
<li class="previous_li"><a href="#">Previous</a></li>
<li class=""><a href="#">1</a></li>
<li class=""><a href="#">2</a></li>
<li class="active"><a href="#">3</a></li>
<li class=""><a href="#">4</a></li>
<li class=""><a href="#">5</a></li>
<li class=""><a href="#">6</a></li>
<li class="next_li"><a href="#">Next</a></li>
</ul>
</div>

<div class="clear"></div>
</div>	<!-- end pagination -->

</div>	<!-- end list view here -->

<div class="clear"></div>
</div>	<!-- end home main container -->
</div>
</div>