

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">Financial Reports</h1>


<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/studio/account_tabs');?>
	
	<!-- end account tabs -->


<div class="tabs_contents">

<div class="tran_history_top fncl_rep_tp_1">
<form action="#" method="get">

<div class="left">

<strong>Filter by:</strong>

<select class="input_field" style="width:85px;"><option>Model</option></select>

<strong>|</strong>

<select class="input_field" style="width:145px;"><option>Customer/Member</option></select>

<strong>|</strong>

<select class="input_field" style="width:120px;"><option>Content Type</option></select>

<strong>|</strong>

<select class="input_field" style="width:70px;"><option>Day</option></select>

<strong>|</strong>

<select class="input_field" style="width:80px;"><option>Week</option></select>

<strong>|</strong>

<select class="input_field" style="width:80px;"><option>Year</option></select>

<strong style="margin-right:3px;">|</strong>

<div class="clear"></div>
</div>


<div class="left">
	<input name="" type="text" class="input_field" value="Begin Date" style="width:95px;" />
    <a href="#" class="date_icon_s"><img src="<?= base_url(); ?>images/t_date_icon.gif" width="16" height="13" alt=" " /></a>
</div>

<strong style="margin:0 3px; float:left; line-height:inherit;">|</strong>

<div class="left">
	<input name="" type="text" class="input_field" value="End Date" style="width:95px;" />
    <a href="#" class="date_icon_s"><img src="<?= base_url(); ?>images/t_date_icon.gif" width="16" height="13" alt=" " /></a>
</div>

<div class="clear"></div>
<!-- end left -->


<div class="clear" style="height:16px;">&nbsp;</div>

<div class="srch_bx">

<span class="left">Transaction <a href="#"><img src="<?= base_url(); ?>images/t_srch_nav.png" width="10" height="7" alt=" " /></a></span>

<input name="" type="text" class="input_field left" value="Search" />

<input name="" type="submit" class="srch_sbmt" value="&nbsp;" />

</div>
<!-- end search box -->



</form>
<div class="clear"></div>
</div>
<!-- end top part -->


<div class="ymp_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <th scope="col" width="80">Item ID <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="225">Time of Purchase <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="170">Transaction ID <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="170">Type of Content <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="170">Model <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col">Price <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    </tr>
  
</table>

</div>

<div class="clear"></div>
</div>

</div>

</div>	<!-- end page body -->