
<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">Add a Model</h1>

<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/studio/account_tabs');?>
	
	<!-- end account tabs -->

<div class="tabs_contents">


<form id="pageForm" method="post"   accept-charset="utf-8" action="<?=site_url('studio/mymodels/register')?>" >

<div class="edit_form">
<div class="left">

<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" class="input_field validate[required,custom[onlyLetterSp]]" value="" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Gender:</strong>
<select name="gender" class="input_field">
<option value="0">Select</option>
<option value="M">Male</option>
<option value="F">Female</option>
</select>
</p>


<p><strong>Birthday:</strong>
<select name="month" id="month" class="input_field validate[required]" style="width:145px;">
	<option value="" selected="selected">Month</option>
	<option value="01">January</option>
	<option value="02">February</option>
	<option value="03">March</option>
	<option value="04">April</option>
	<option value="05">May</option>
	<option value="06">June</option>
	<option value="07">July</option>
	<option value="08">August</option>
	<option value="09">September</option>
	<option value="10">October</option>
	<option value="11">November</option>
	<option value="12">December</option>
</select>&nbsp;

<select name="day" id="day" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Day</option>
<?php
for($i=1;$i<=31;$i++)
{
	if($i<10)
		$day='0'.$i;
	else 
		$day=$i;
	echo '<option value="'.$day.'">'.$day.'</option>';
}
?>
</select>&nbsp;

<select name="year" id="year" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1970;$i--)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
</p>

<p><strong>Country:</strong>
<select name="country" id="country" class="input_field validate[required]">
	<option value="" selected="selected">Country</option>
<?php
	$options=array();
	$default=0;
	$options[]='';
	
	foreach($countries as $row){
		echo "<option value=".$row['country_id'].">".$row['country_name']."</option>";
	}
?>
</select>
</p>

<p><strong>Tax ID/EIN or SSN:</strong>
<input type="text" name="ssn_ein" id="ssn_ein"  value="" class="input_field validate[required]" />
</p>

<p><strong>Account Type:</strong>
<select name="account_type" class="input_field validate[required]">
<option value="">Select</option>
<option value="Type 1">Type 1</option>
<option value="Type 2">Type 2</option>
<option value="Type 3">Type 3</option>
</select>
</p>

<p>

<p><strong>Payment Method:</strong>
<select name="payment_method" class="input_field">
<option>Select</option>
<option value="credit_card">Credit Card</option>
<option value="paypal">Paypal</option>
<option value="other">other</option>
</select>
</p>

<p><strong>Preferred Username:</strong>
<input type="text" value="" class="input_field validate[required]" name="username" id="username" />
<div id="rtn-username"></div>
</p>


</div>	<!-- end left container -->



<div class="right">

<p>

<strong>Download Model Contract:</strong>
</p>

<div style="padding:7px 0 10px;">

<img src="<?=base_url()?>images/pdf_small.gif" width="16" height="16" alt=" " />&nbsp; <a href="<?=site_url('download_file/model_contract')?>" target="_blank" class="tuca">PDF</a>
<img src="<?=base_url()?>images/word_small.gif" width="16" height="16" alt=" " style="margin-left:20px;" />&nbsp; <a href="<?=site_url('download_file/model_contract/word')?>" target="_blank" class="tuca">Word</a>

</div>

<p>



<p>

<strong>Upload Signed Model Contract:</strong>

<input name="file_name_box_1" id="file_name_box_1" type="text" class="input_field" value="" style="width:330px;" />

<input type="button" value="Browse" class="btn" name=""  onclick="javascript:openBrowse();"/>

</p>

<p>

<strong>Upload Photo ID:</strong>

<input name="file_name_box_2" id="file_name_box_2" type="text" class="input_field" value="" style="width:330px;" />

<input type="button" value="Browse" class="btn" name="" onclick="javascript:openBrowse2();"/>

</p>


<p>

<strong>&nbsp;</strong>

<a href="#" class="top_linkt" style="float:left;"><span>Take a Snapshot <img style="margin:0 0 0 4px;" src="<?=base_url()?>images/web_can_dvce.png" width="30" height="26" alt=" " /></span></a>

<span style=" font-size:10px; font-weight:bold; display:inline-block; padding:22px 0 0 15px;">*Web Camera Required</span>

</p>

<div class="clear diff_dv">&nbsp;</div>

<p><strong>Email:</strong>
<input type="text" value="" class="input_field validate[required,custom[email]]" name="email" id="email" />
<div id="rtn-email"></div>
</p>

<p><strong>Password:</strong>
<input type="password" value="" class="input_field validate[required,minSize[6]]" name="password" id="password" />
</p>

<p><strong>Confirm Password:</strong>
<input type="password" value="" class="input_field validate[required,equals[password]]" name="conf_password" id="conf_password" />
</p>



</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input name="submit" type="submit" id="submit" class="btn" value="Submit" />&nbsp;

<input name="" type="button" class="btn" value="Cancel" />

<!-- hidden file uploaders -->
<input name="file_signed_contract" type="file" id="file_signed_contract" style="display:none;"/>

<input name="img_photo_id" type="file" id="img_photo_id" style="display:none;"/>  

<!-- hidden file uploaders end -->
</form>

</div>

</div>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
              
<script>
function openBrowse(){
    document.getElementById("file_signed_contract").click();
}
function openBrowse2(){
    document.getElementById("img_photo_id").click();
}
$(document).ready(function(){
    $("#file_signed_contract").change(function(){
        var filename=$(this).val();
        $("#file_name_box_1").val(filename);
        
        //$("#upload_csv").submit();
    });
    $("#img_photo_id").change(function(){
        var filename2=$(this).val();
        $("#file_name_box_2").val(filename2);
        
        //$("#upload_csv").submit();
    });
});
</script>
<!--instance the validator engine ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});             
 	});
</script>


<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username").blur(function(){  
       		var username = $('#username').val();
   		if(username=="")
   		{
   			alert("You have not entered any username");
   		}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?= site_url('login/ajx_username_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else{
	      		$("#rtn-username").html('<span style="color:red"><b>Sorry, username is not available, please try another</b></span>');
	      	}			      
		    }
			});
   		}  		
       		
       		return false;
       });
       
        $("#email").blur(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else
			      		 $("#rtn-email").html('<span style="color:red"><b>Sorry, this email is already used</b></span>');				      					      				      			      
			     }
				});
    		return false;
       });             
 	});
</script>

