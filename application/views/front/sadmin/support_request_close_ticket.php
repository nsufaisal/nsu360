<!--popup -->
<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="#"><img src="<?= base_url(); ?>images/pp_logo.gif" width="188" height="59" alt=" " /></a></h1>
<a href="javascript:void(0);" onclick="closePP();" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2>Close Ticket</h2>
<?=$content;?>

<!--submit buttons -->
<div class="submit-buttons">
<a href="#" class="grey-button">Close Ticket</a>
<a href="javascript:void(0);" onclick="closePP();" class="grey-button">Cancel</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>
<!--/popup -->