<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">Edit a Model</h1>


<div class="clear"></div>


<!-- acc_tabs loading-->

<?php $this->load->view('front/studio/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">

<div class="edit_form">
<form action="<?=site_url('studio/mymodels/update/'.$model['member_id'])?>" method="post" name="model_edit" id="model_edit" accept-charset="utf-8">	

<div class="left">

<p>

<strong>Username:</strong>

<input name="username" id="username" type="text" class="input_field validate[required]" value="<?=$model['username']?>"/>

</p>
<?=form_error('username')?>
<div id="rtn-username"></div>

<p>

<strong>First Name:</strong>

<input name="first_name" id="first_name"  type="text" class="input_field validate[required,custom[onlyLetterSp]]" value="<?=$model['first_name']?>" />

</p>

<p>

<strong>Last Name:</strong>

<input name="last_name" id="last_name" type="text" class="input_field validate[required,custom[onlyLetterSp]" value="<?=$model['last_name']?>" />

</p>

<p style="padding:10px 0 28px;">
<?php 
	if($model['is_active']==0){
		?>
		<label style="font-weight:bold;"><input name="enable_model" id="enable_model" type="checkbox" value="1" /> Enable Model</label>

	<?php

	}
else{
	?>
	<label style="font-weight:bold;"><input name="disable_model" id="disable_model" type="checkbox" value="1" /> Disable Model</label>

	<?php
}
	?>

</p>


</div>	<!-- end left container -->



<div class="right">

<p>

<strong>Email:</strong>

<input name="email" id="email" type="text" class="input_field validate[required,custom[email]]" value="<?=$model['email']?>" />

</p>
<?=form_error('email')?>
<div id="rtn-email"></div>
<p>
<strong>Password:</strong>
<input name="password" id="password" value="" type="password" class="input_field validate[condRequired[new_password]]"  />
<div id="pass_err"></div>
<?=form_error('password')?>
</p>

<p>
<strong>Change Password:</strong>
<input name="new_password" id="new_password" type="password" value="" class="input_field validate[minSize[6],condRequired[conf_password]]"  />
<?=form_error('new_password')?>
</p>

<p>
<strong>Confirm Password:</strong>
<input type="password" name="conf_password"  value="" id="conf_password" class="input_field validate[condRequired[new_password],equals[new_password]]" />
</p>

<!-- <div id="rtn_pass_err"></div> -->

</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input name="submit" id="submit" type="submit" class="btn" value="Save" />&nbsp;

<input name="cancel"  onclick=" window.location .href ='<?=site_url ('studio/mymodels' )?> '"  type="button" class="btn" value="Cancel" /></a>


</div>
</form>
</div>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->



<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){
	 	$("#model_edit").validationEngine('attach', {promptPosition : "topRight", scroll: false,
	 	onValidationComplete : function(form, status){
	 		var flag=false; // raihan added
      	    var pass=$("#password").val();
      		var member_id=<?=$model['member_id']?>;
      		if(pass !='')
      		{
      			$.ajax({
  		 	  		type : "post" ,
           			data : {'member_id' : member_id, 'password':pass},
          			url: "<?=site_url('login/ajx_check_pass_with_id' )?> ",
          			async : false,
          			success : function (result){
		          	    if(result){	    
		          	    	form.validationEngine('detach');
			   				form.submit();
		          	    	
		          	    } 	          	     	
		          	    else
		          	    {
		          	    	$("#password").val('');
		          	    	$("#pass_err").html('<span style="color:red; font-weight:bold;">Your current password is wrong</span>');
		      	    			flag=false;
		          	    }        
               		}                                                                                                                                                           
      			});
	     }
	     else{
	     	if($("#new_password").val()==''&&$("#conf_password").val()==''&&IsEmail($("#email").val())&&$("#email").val()!=''&&IsLetters($("#last_name").val())&&$("#last_name").val()!=''&&IsLetters($("#first_name").val())&&$("#first_name").val()!=''&&$("#username").val()!=''){
	     	form.validationEngine('detach');
			form.submit();
			}
	     	
	     }
	     
	     
     } 
      	});	    
       
      });
      function IsEmail(email) {
		  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		}
		
		function IsLetters(username) {
		  var regex = /^[a-zA-Z]*$/;
		  return regex.test(username);
		}
</script>

<script>
	$("document").ready(function(){
		$("#username").change(function(){  
       	var username = $('#username').val();
   		if(username=="")
   		{
   			alert("You have not entered any username");
   		}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?= site_url('login/ajx_username_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else
	      	{
	      		var username = $("#username").val();
	      		$("#rtn-username").html('<span style="color:red"><b>Sorry, <span style="color:blue;">'+username+'</span> is not available, please try another</b></span>');
	      		$("#username").val('');
	      	}			      
		    }
			});
   		}  		
       		
       		return false;
       });
		
	});
	
	 $("#email").change(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else{
			      		var email = $("#email").val();
			      		 $("#rtn-email").html('<span style="color:red"><b>Sorry, <span style="color:blue;">'+email+'</span> is already used</b></span>');
			      		 $("#email").val('');				      		
			      	}
			      					      				      			      
			     }
				});
    		return false;
       });
	</script>
