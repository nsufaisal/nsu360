<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">View a Class Details</h1>


<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/sadmin/account_tabs');
	//var_dump($class);
	?>
	
	<!-- end account tabs -->


<div class="tabs_contents view_cont_y view_mdl_dtal">

<div class="left">
<p><strong>Class Title:</strong>	<?php echo $class['title']; ?></p>

<p><strong>Class Name: </strong>	<?php echo $class['name']; ?></p>

<p><strong>Section Title: </strong>	<?php echo $class['section_title']; ?></p>

<p><strong>Section Name:</strong>	<?php echo $class['section_name']; ?></p>

<p><strong>Description:</strong>	<?php echo $class['description']; ?></p>

</div>
<!-- end view cont left -->

<div class="right">

<a style="color: white" href="<?=site_url('sadmin/myclasses/edit/'.$class['class_id'])?>" class="top_linkt"><span>Edit</span></a>&nbsp;
<a style="color: white" href="javascript:void(0);" onclick="openForm('<?=site_url('sadmin/myclasses/remove/'.$class['class_id'])?>');" title="Remove" class="top_linkt"><span>Remove</span></a>

</div>

<div class="clear"></div>
</div>

</div>

</div>	<!-- end page body -->