<div id="page_body">

<div class="center">

<h1 class="">Edit My Account</h1>

<div class="clear"></div>


<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');?>

<!-- end account tabs -->


<div class="tabs_contents">

<form action="<?=site_url('sadmin/myaccount/update')?>" method="post" name="pageForm" id="std_myacnt_edit" accept-charset="utf-8">	


<div class="edit_form">
	
<div class="left">


<p>

<strong>First Name:</strong>

<input name="first_name" id="first_name" type="text" class="input_field validate[required,custom[onlyLetterSp]]" value="<?=$sadmin['first_name']?>" />

</p>

<p>

<strong>Last Name:</strong>

<input name="last_name" id="last_name" type="text" class="input_field validate[required,custom[onlyLetterSp]]" value="<?=$sadmin['last_name']?>" />

</p>


<p>

<strong>NID:</strong>

<input name="nid" id="nid" type="text" class="input_field validate[required]" value="<?=$sadmin['nid']?>" />

</p>


</div>	<!-- end left container -->



<div class="right">

<p>
<strong>Password:</strong>
<input name="password" id="password" value="" type="password" class="input_field"  />
<?=form_error('password')?>
<div id="err_password"></div>
</p>

<p>
<strong>Change Password:</strong>
<input name="new_password" id="new_password" type="password" value="" class="input_field validate[minSize[6]]"  />
<?=form_error('new_password')?>
<div id="err_new_pass"></div>
</p>

<p>
<strong>Confirm Password:</strong>
<input type="password" name="conf_password"  value="" id="conf_password" class="input_field validate[equals[new_password]]" />
<?=form_error('conf_password')?>
<div id="err_conf_pass"></div>         
</p>


</div>	<!-- end right container -->


<div class="clear"></div>

<div align="center" style="padding-top:20px;">

<input name="submit" id="submit" type="submit" class="btn" value="Save" />&nbsp;

<input name=""  onclick=" window.location .href ='<?=site_url ('sadmin/myaccount' )?> '" type="button" class="btn" value="Cancel" />

</div>

</div>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
</form>
<!--instance the validator engine ::rtn:: -->    
<script>		
	 	$("#pageForm").validationEngine('attach', {promptPosition : "topRight", scroll: false},
               {focusFirstField : true});		
</script>
