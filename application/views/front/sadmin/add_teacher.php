<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class=""><?php echo ($mode=='new')?'Add a Teacher':'Edit a Teacher'; ?></h1>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');

?>

<!-- end account tabs -->

<div class="tabs_contents">
	
<?php
	$link=site_url('sadmin/myteachers/save/0');
	if(isset($teacher['teacher_id']))
	{
		$link=site_url('sadmin/myteachers/save/'.$teacher['teacher_id']);
		
		//$member=$this->members_model->GetMemberImportantDetail($teacher['member_id']);
		
	}	
?>

<form  method="post" enctype="multipart/form-data" action="<?= $link?>" id="pageForm">

<?=validation_errors()?>

<div class="edit_form">

<div class="left">

<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" value="<?php if(isset($teacher['first_name'])) echo $teacher['first_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="<?php if(isset($teacher['last_name'])) echo $teacher['last_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Teacher ID:</strong>
<input type="text" value="<?php if(isset($teacher['inst_school_id'])) echo $teacher['inst_school_id'];?>" class="input_field validate[required]" name="inst_school_id" id="inst_school_id" />
</p>

<p><strong>Birthday:</strong>
	<?php
		if(isset($teacher['birthday']))
		{
			$birthday = explode('-', $teacher['birthday']);			
			$byear  = $birthday[0];
			$bmonth   = $birthday[1];
			$bday = $birthday[2];
		}

	?>
<select name="month" id="month" class="input_field validate[required]" style="width:145px;">
	<option value="" selected="selected">Month</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="01")?'selected="selected"':'';?> value="01">January</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="02")?'selected="selected"':'';?> value="02">February</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="03")?'selected="selected"':'';?> value="03">March</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="04")?'selected="selected"':'';?> value="04">April</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="05")?'selected="selected"':'';?> value="05">May</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="06")?'selected="selected"':'';?> value="06">June</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="07")?'selected="selected"':'';?> value="07">July</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="08")?'selected="selected"':'';?> value="08">August</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="09")?'selected="selected"':'';?> value="09">September</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="10")?'selected="selected"':'';?> value="10">October</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="11")?'selected="selected"':'';?> value="11">November</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="12")?'selected="selected"':'';?> value="12">December</option>
</select>&nbsp;

<select name="day" id="day" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Day</option>
<?php
for($i=1;$i<=31;$i++)
{
	if($i<10)
		$day='0'.$i;
	else 
		$day=$i;
	
	$selected='';
	if(isset($bday)){
		if(intval($bday)==intval($day))
			$selected='selected="selected"';
	} 
	echo '<option '.$selected.' value="'.$day.'">'.$day.'</option>';
}
?>
</select>&nbsp;

<select name="year" id="year" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1980;$i--)
	{
		$selected='';
		if(isset($byear)){
			if(intval($byear)==intval($i))
				$selected='selected="selected"';
		} 
		echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
</p>

<p><span style="font-weight:bold">Gender:</span> <span style="margin-left:185px; font-weight: bold;">Blood Group:</span><br/>
<select name="gender" class="input_field" style="width:220px;">
<option value="">Select</option>
<option <?php if(isset($teacher['gender'])) echo ($teacher['gender']=="M")?'selected="selected"':'';?> value="M">Male</option>
<option  <?php if(isset($teacher['gender'])) echo ($teacher['gender']=="F")?'selected="selected"':'';?> value="F">Female</option>
</select>&nbsp;

<select name="blood_group" id="blood_group" class="input_field validate[required]" style="width:220px;">
<option value="">Select</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="O+")?'selected="selected"':'';?> value="O+">O+</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="O-")?'selected="selected"':'';?> value="O-">O-</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="A+")?'selected="selected"':'';?> value="A+">A+</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="A-")?'selected="selected"':'';?> value="A-">A-</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="B+")?'selected="selected"':'';?> value="B+">B+</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="B-")?'selected="selected"':'';?> value="B-">B-</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="AB+")?'selected="selected"':'';?> value="AB+">AB+</option>
<option <?php if(isset($teacher['blood_group'])) echo ($teacher['blood_group']=="AB-")?'selected="selected"':'';?> value="AB-">AB-</option>
</select>
</p>

<p><strong>Teacher Mobile:</strong>
<input type="text" name="mobile" id="mobile" value="<?php if(isset($teacher['phone'])) echo $teacher['phone'];?> " class="input_field validate[required]" />
</p>

<p><strong>Join Date:</strong>
	<?php		
	if(isset($teacher['join_date']))
		{
			$joindate = explode('-', $teacher['join_date']);			
			$jyear  = $joindate[0];
			$jday   = $joindate[1];
			$jmonth = $joindate[2];
		}?>
<select name="join_month" id="join_month" class="input_field validate[required]" style="width:145px;">
	<option value="" selected="selected">Month</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="01")?'selected="selected"':'';?> value="01">January</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="02")?'selected="selected"':'';?> value="02">February</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="03")?'selected="selected"':'';?> value="03">March</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="04")?'selected="selected"':'';?> value="04">April</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="05")?'selected="selected"':'';?> value="05">May</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="06")?'selected="selected"':'';?> value="06">June</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="07")?'selected="selected"':'';?> value="07">July</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="08")?'selected="selected"':'';?> value="08">August</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="09")?'selected="selected"':'';?> value="09">September</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="10")?'selected="selected"':'';?> value="10">October</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="11")?'selected="selected"':'';?> value="11">November</option>
	<option <?php if(isset($jmonth)) echo ($jmonth=="12")?'selected="selected"':'';?> value="12">December</option>
</select>&nbsp;

<select name="join_day" id="join_day" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Day</option>
<?php
for($i=1;$i<=31;$i++)
{
	if($i<10)
		$day='0'.$i;
	else 
		$day=$i;
	
	$selected='';
	if(isset($jday)){
		if(intval($jday)==intval($day))
			$selected='selected="selected"';
	} 
	echo '<option '.$selected.' value="'.$day.'">'.$day.'</option>';
}
?>
</select>&nbsp;

<select name="join_year" id="join_year" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1980;$i--)
	{
		$selected='';
		if(isset($jyear)){
			if(intval($jyear)==intval($i))
				$selected='selected="selected"';
		} 
		echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
</p>

<p>
<strong>Acamemic Qualifications:</strong>
<textarea name="academic_qualification" id="academic_qualification" cols="" rows="" class="input_field validate[required]" style="height:40px;">
	<?php if(isset($teacher['academic_qualification'])) echo $teacher['academic_qualification'];?>
</textarea>
</p>
<p>
<strong>Present Address:</strong>
<textarea name="curr_address" id="curr_address" cols="" rows="" class="input_field validate[required]" style="height:40px;">
	<?php if(isset($teacher['curr_address'])) echo $teacher['curr_address'];?>
</textarea>
</p>

<p><strong>Spouse Name:</strong>
<input type="text" name="spouse_name" id="spouse_name" value="<?php if(isset($teacher['spouse_name'])) echo $teacher['spouse_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Emergency Contact Name:</strong>
<input type="text" name="emergency_contact" id="emergency_contact" value="<?php if(isset($teacher['emergency_contact'])) echo $teacher['emergency_contact'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Relation With Contact:</strong>
<input type="text" name="emergency_relation" id="emergency_relation" value="<?php if(isset($teacher['emergency_relation'])) echo $teacher['emergency_relation'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Contact's Mobile:</strong>
<input type="text" name="emergency_mobile" id="emergency_mobile" value="<?php if(isset($teacher['emergency_mobile'])) echo $teacher['emergency_mobile'];?>" class="input_field " />
</p>


</div>	<!-- end left container -->



<div class="right">

<p><strong>Class Teacher:</strong><?php //echo isset($teacher['class_teacher'])? $teacher['class_teacher']:'' ;?>
<select name="class_teacher" id="class_teacher" class="input_field validate[required]" >
	<option value="">Select</option>
	<?php foreach($classes as $class) :?>
		<option <?php if(isset($teacher['class_teacher'])) echo ($teacher['class_teacher']==$class['class_id'])?'selected="selected"':'';?> value="<?=$class['class_id']?>"><?=$class['title']?></option>	
	<?php  endforeach;?>
</select>

</p>

<p><strong>Section:</strong><?php //echo isset($teacher['class_section'])? $teacher['class_section']:'' ;?>
<select name="class_section" id="class_section" class="input_field">
	<option value="">Select</option>
	<?php foreach($sections as $section) :?>
		<option <?php if(isset($teacher['class_section'])) echo ($teacher['class_section']==$section['title'])?'selected="selected"':'';?>  value="<?php echo $section['title']; ?>"><?php echo $section['name']; ?></option>	
	<?php  endforeach;?>	
</select>

</p>


<p style="position: relative;"> <strong>Upload Teacher Photo:</strong><span class="file-size">Max File Size 1MB</span>
<input type="text"   name="file_name_box_1" onfocus=" javascript: openBrowse1(); " id="file_name_box_1" style="width:345px;" class="input_field" value="<?php if(isset($teacher['teacher_photo'])) echo $teacher['teacher_photo']; ?>" />&nbsp;
<input type="button" onclick=" javascript: openBrowse1(); " name="" id ="uploadButton" style="min-width: 70px;" class="btn" value="Browse" >
<?PHP if (form_error ('file_name_box_1' )) { ?><div ><?php echo form_error( 'file_name_box_1'); ?> </div><?PHP } ?>
</p>
<div id="err_userfile_1"></div>


<p><strong>Email:</strong>
<input type="text" name="email" id="email" value="<?php if(isset($member['email'])) echo $teacher['email'];?>" class="input_field validate[custom[email]]" />
</p>
<div id="rtn-email"></div>

<p><strong>Username:</strong>
<input type="text" value="<?php  echo isset($member['username'])? $member['username']:'';?>" class="input_field validate[minSize[3]]" name="username" id="username" />
</p>
<div id="rtn-username"></div>
<p><strong>Password:</strong>
<input type="password" value="" class="input_field validate[minSize[6]]" name="password" id="password" />
</p>

<p><strong>Confirm Password:</strong>
<input type="password" value="" class="input_field validate[equals[password]]" name="conf_password" id="conf_password" />
</p>

<!-- SALARY INFO -->
<h3>Salary Information</h3>
<p><strong>Basic Salary:</strong>
<input type="text" name="basic_salary" id="basic_salary" value="<?php if(isset($teacher['basic_salary'])) echo $teacher['basic_salary'];?>" class="input_field validate[required,custom[number]]" />
</p>
<p><strong>House Rent:</strong>
<input type="text" name="house_rent" id="house_rent" value="<?php if(isset($teacher['house_rent'])) echo $teacher['house_rent'];?>" class="input_field validate[required,custom[number]]" />
</p>


<p><strong>Medical Allowance:</strong>
<input type="text" name="medical_allowance" id="medical_allowance" value="<?php if(isset($teacher['medical_allowance'])) echo $teacher['medical_allowance'];?>" class="input_field validate[custom[number]]" />
</p>

<p><strong>Conveyance:</strong>
<input type="text" name="conveyance" id="conveyance" value="<?php if(isset($teacher['conveyance'])) echo $teacher['conveyance'];?>" class="input_field validate[custom[number]]" />
</p>

<p><strong>Other Allowance:</strong>
<input type="text" name="other_allowance" id="other_allowance" value="<?php if(isset($teacher['other_allowance'])) echo $teacher['other_allowance'];?>" class="input_field validate[custom[number]]" />
</p>

<input type="file" name="userfile_1" id="userfile_1" value="<?php if(isset($teacher['teacher_photo'])) echo $teacher['teacher_photo'];?>" style="display:none;"/>


</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input type="submit" value="<?php echo ($mode=='new')?'Save':'Save Changes'; ?>" class="btn" name="submit" />&nbsp;

<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/myteachers')?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->

<script>		
	$(document).ready(function(){          
	   $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true}); 
   });
</script>
<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<script>              
        $(document).ready( function(){ 
      $( "#userfile_1").change(function(){
               var filename= $(this).val();            
               var size = this.files[ 0].size;
               var name= this.files[ 0].name;
               var type= this.files[ 0].type;

	         	if(size> 1048576){
	                     $( "#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
	                      $( "#file_name_box_1").val('' );
	               }
	               else if(! (/\.(jpg|jpeg|png|gif)$/i).test(filename))
	               { //avi|wmv|mov
	                     $("#err_userfile_1").html('<span style="color:red;">*Only image file accepted (jpg/jpeg/png/gif)</span><br/><br/>');               
	                     $( "#file_name_box_1").val('');
	                            
	               }
	               else{
	                     $( "#err_userfile_1").html('' );
	                     $( "#file_name_box_1").val(name);
	               }
               
	         	
	             
               
           });
       });
</script>
<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username").blur(function(){  
       		var username = $('#username').val();
       		var mode='<?php echo $mode;?>';
			if(mode=="new")
			{
				if(username=="")
				{
					alert("You have not entered any username");
				}
				else
				{
					$("#rtn-username").html('<b>Checking... please wait..</b>');
					$.ajax({
					type: "post",
					data: {'username' : username},
					url: "<?= site_url('login/ajx_username_available')?>",
					success: function(result)
					{
						if(result)
						{
							 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
						}
						else
						{
							var username=$("#username").val();
							$("#rtn-username").html('<span style="color:red">Sorry, <span style="color:blue;">'+ username+'</span> is not available, please try another</span>');
							$("#username").val('');
						}			      
					}
					});
				}
			}	
			else
			{
				$("#rtn-username").html('<b>Checking... please wait..</b>');
				$.ajax({
				type: "post",
				data: {'username' : username},
				url: "<?= site_url('login/ajx_username_available')?>",
				success: function(result)
				{
					if(result)
					{
						 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
					}
					else
					{
						var username=$("#username").val();
						$("#rtn-username").html('<span style="color:red">Sorry, <span style="color:blue;">'+ username+'</span> is not available, please try another</span>');
						$("#username").val('');
					}			      
				}
				});
			}
       		
       		
       	});			
       		return false;
    });
       
        $("#email").blur(function(){          		
       		var email = $('#email').val(); 
       		var mode='<?php echo $mode;?>';
       		if(mode=="new")      			
       		{	$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else
			      		{
			      		 var email=$("#email").val();
			      		 $("#rtn-email").html('<span style="color:red">Sorry, <span style="color:blue;">'+email+'</span> is aleady being used</span>');
			      		 $("#email").val('');
			      		}
			      		 				      					      				      			      
			     }
				});} 
    		return false;
       });            
 	});
</script>