<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">
	<?php echo ($mode=='new')?'Add a Student':'Edit a Student'; ?>

</h1>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">

<?php

	$link=site_url('sadmin/mystudents/save/0');
	if(isset($student['student_id']))
		$link=site_url('sadmin/mystudents/save/'.$student['student_id']);
?>
<form  method="post" enctype="multipart/form-data" action="<?php echo $link; ?>" id="pageForm">

<?=validation_errors()?>

<div class="edit_form">

<div class="left">

<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" value="<?php if(isset($student['first_name'])) echo $student['first_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="<?php if(isset($student['last_name'])) echo $student['last_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Email:</strong>
<input type="text" name="email" id="email" value="<?php if(isset($student['email'])) echo $student['email'];?>" class="input_field validate[custom[email]]" />
</p>
<div id="rtn-email"></div>
<p><strong>Student ID:</strong>
<input type="text" value="<?php if(isset($student['std_school_id'])) echo $student['std_school_id'];?>" class="input_field validate[required]" name="std_school_id" id="std_school_id" />
</p>

<p><strong>Birthday:</strong>
	<?php
		if(isset($student['birthday']))
		{
			$birthday = explode('-', $student['birthday']);			
			$byear  = $birthday[0];
			$bmonth   = $birthday[1];
			$bday = $birthday[2];
		}

	?>
<select name="month" id="month" class="input_field validate[required]" style="width:145px;">
	<option value="" selected="selected">Month</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="01")?'selected="selected"':'';?> value="01">January</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="02")?'selected="selected"':'';?> value="02">February</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="03")?'selected="selected"':'';?> value="03">March</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="04")?'selected="selected"':'';?> value="04">April</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="05")?'selected="selected"':'';?> value="05">May</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="06")?'selected="selected"':'';?> value="06">June</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="07")?'selected="selected"':'';?> value="07">July</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="08")?'selected="selected"':'';?> value="08">August</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="09")?'selected="selected"':'';?> value="09">September</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="10")?'selected="selected"':'';?> value="10">October</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="11")?'selected="selected"':'';?> value="11">November</option>
	<option <?php if(isset($bmonth)) echo ($bmonth=="12")?'selected="selected"':'';?> value="12">December</option>
</select>&nbsp;

<select name="day" id="day" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Day</option>
<?php
for($i=1;$i<=31;$i++)
{
	if($i<10)
		$day='0'.$i;
	else 
		$day=$i;
	
	$selected='';
	if(isset($bday)){
		if(intval($bday)==intval($day))
			$selected='selected="selected"';
	} 
	echo '<option '.$selected.' value="'.$day.'">'.$day.'</option>';
}
?>
</select>&nbsp;

<select name="year" id="year" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1980;$i--)
	{
		$selected='';
		if(isset($byear)){
			if(intval($byear)==intval($i))
				$selected='selected="selected"';
		} 
		echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
</p>

<p><span style="font-weight:bold">Gender:</span> <span style="margin-left:185px; font-weight: bold;">Blood Group:</span><br/>
<select name="gender" class="input_field" style="width:220px;">
<option value="">Select</option>
<option <?php if(isset($student['gender'])) echo ($student['gender']=="M")?'selected="selected"':'';?> value="M">Male</option>
<option  <?php if(isset($student['gender'])) echo ($student['gender']=="F")?'selected="selected"':'';?> value="F">Female</option>
</select>&nbsp;

<select name="blood_group" id="blood_group" class="input_field validate[required]" style="width:220px;">
<option value="">Select</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="O+")?'selected="selected"':'';?> value="O+">O+</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="O-")?'selected="selected"':'';?> value="O-">O-</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="A+")?'selected="selected"':'';?> value="A+">A+</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="A-")?'selected="selected"':'';?> value="A-">A-</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="B+")?'selected="selected"':'';?> value="B+">B+</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="B-")?'selected="selected"':'';?> value="B-">B-</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="AB+")?'selected="selected"':'';?> value="AB+">AB+</option>
<option <?php if(isset($student['blood_group'])) echo ($student['blood_group']=="AB-")?'selected="selected"':'';?> value="AB-">AB-</option>
</select>
</p>

<p>
<strong>Present Address:</strong>
<textarea name="curr_address" id="curr_address" cols="" rows="" class="input_field validate[required]" style="height:40px;"><?php if(isset($student['curr_address'])) echo $student['curr_address'];?></textarea>
</p>

<p>
<strong>Permanent Address:</strong>
<textarea name="perm_address" id="perm_address" cols="" rows="" class="input_field validate[required]" style="height:40px;"><?php if(isset($student['perm_address'])) echo $student['perm_address'];?></textarea>
</p>

<p><span style="font-weight:bold">Admission Year:</span> <span style="margin-left:132px; font-weight: bold;">Admission Class:</span><br/>
<input type="text" style="width:200px;" value="<?php if(isset($student['admission_year'])) echo $student['admission_year'];?>" class="input_field validate[required]" name="admission_year" id="admission_year" placeholder="example: 2011" />

<!--
<select name="admission_year" id="admission_year" style="width:220px;" class="input_field validate[required]">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1990;$i--)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
-->
<select name="admission_class" id="admission_class" class="input_field validate[required]" style="width:220px;" />
	<option value="">Select</option> 
	<?php foreach($classes as $class) :?>
		<option <?php if(isset($student['admission_class'])) echo ($student['admission_class']==$class['class_id'])?'selected="selected"':'';?> value="<?=$class['class_id']?>"><?=$class['title']?></option>	
	<?php  endforeach;?>
</select>

</p>

<p><span style="font-weight:bold">Current Class:</span> <span style="margin-left:142px; font-weight: bold;">Current Section:</span><br/>
<select name="curr_class" id="curr_class" style="width:220px;" class="input_field validate[required]">
	<option value="">Select</option>
	<?php foreach($classes as $class) :?>
		<option <?php if(isset($student['curr_class'])) echo ($student['curr_class']==$class['class_id'])?'selected="selected"':'';?> value="<?=$class['class_id']?>"><?=$class['title']?></option>	
	<?php  endforeach;?>
</select>
<select name="curr_section" id="curr_section" class="input_field validate[required]" style="width:220px;">
	<option value="">Select</option>
	<?php foreach($sections as $section) :?>
		<option <?php if(isset($student['curr_section'])) echo ($student['curr_section']==$section['title'])?'selected="selected"':'';?>  value="<?php echo $section['title']; ?>"><?php echo $section['name']; ?></option>	
	<?php  endforeach;?>	
</select>

</p>

<p><span style="font-weight:bold">Class Shift:</span> <span style="margin-left:158px; font-weight: bold;">Transportation:</span><br/>
<select name="shift" id="shift" style="width:220px;" class="input_field validate[required]">
	<option value="">Select</option>
	<option <?php if(isset($student['shift'])) echo ($student['shift']==1)?'selected="selected"':'';?> value="1">Morning</option>
	<option <?php if(isset($student['shift'])) echo ($student['shift']==2)?'selected="selected"':'';?> value="2">Day</option>
</select>

<select name="transport" id="transport" class="input_field validate[required]" style="width:220px;">
	<option value="">Select</option>
	<option <?php if(isset($student['transport'])) echo ($student['transport']==1)?'selected="selected"':'';?> value="1">Yes</option>
	<option <?php if(isset($student['transport'])) echo ($student['transport']==2)?'selected="selected"':'';?> value="0">No</option>
</select>

</p>

<p><strong>Bus Stand:</strong>
<select name="bus_stand" class="input_field">
<option value="">Select</option>
	<?php foreach($bus_stoppages as $stoppage) :?>
		<option <?php if(isset($student['bus_stand'])) echo ($student['bus_stand']==$stoppage['stoppage_id'])?'selected="selected"':'';?> value="<?=$stoppage['stoppage_id']?>"><?=$stoppage['name']?></option>	
	<?php  endforeach;?>
</select>
</p>

</div>	<!-- end left container -->



<div class="right">

<p><strong>Father Name:</strong>
<input type="text" value="<?php if(isset($student['father_name'])) echo $student['father_name'];?>" class="input_field validate[required]" name="father_name" id="father_name" />
</p>

<p><strong>Father Occupaion:</strong>
<input type="text" value="<?php if(isset($student['father_occupation'])) echo $student['father_occupation'];?>" class="input_field validate[required]" name="father_occupation" id="father_occupation" />
</p>

<p><strong>Father Phone:</strong>
<input type="text" value="<?php if(isset($student['father_phone'])) echo $student['father_phone'];?>" class="input_field validate[required]" name="father_phone" id="father_phone" />
</p>

<p><strong>Mother Name:</strong>
<input type="text" value="<?php if(isset($student['mother_name'])) echo $student['mother_name'];?>" class="input_field validate[required]" name="mother_name" id="mother_name" />
</p>

<p><strong>Mother Occupaion:</strong>
<input type="text" value="<?php if(isset($student['mother_occupation'])) echo $student['mother_occupation'];?>" class="input_field validate[required]" name="mother_occupation" id="mother_occupation" />
</p>

<p><strong>Mother Phone:</strong>
<input type="text" value="<?php if(isset($student['mother_phone'])) echo $student['mother_phone'];?>" class="input_field" name="mother_phone" id="mother_phone" />
</p>

<p><strong>Local Guardian Name:</strong>
<input type="text" value="<?php if(isset($student['guardian_name'])) echo $student['guardian_name'];?>" class="input_field" name="guardian_name" id="guardian_name" />
</p>

<p><strong>Relation With Student:</strong>
<input type="text" value="<?php if(isset($student['guardian_relation'])) echo $student['guardian_relation'];?>" class="input_field" name="guardian_relation" id="guardian_relation" />
</p>

<p>
<strong>Guardian Address:</strong>
<textarea name="guardian_address" id="guardian_address" cols="" rows="" class="input_field validate[required]" style="height:40px;"><?php if(isset($student['guardian_address'])) echo $student['guardian_address'];?></textarea>
</p>
<p><strong>Guardian Phone:</strong>
<input type="text" value="<?php if(isset($student['guardian_phone'])) echo $student['guardian_phone'];?>" class="input_field" name="guardian_phone" id="guardian_phone" />
</p>

<p style="position: relative;"> <strong>Upload Student Photo:</strong><span class="file-size">Max File Size 1MB</span>
<input type="text"   name="file_name_box_1" onfocus=" javascript: openBrowse1(); " id="file_name_box_1" style="width:345px;" class="input_field" value="<?php if(isset($student['student_photo'])) echo $student['student_photo']; ?>"/>&nbsp;
<input type="button" onclick=" javascript: openBrowse1(); " name="" id ="uploadButton" style="min-width: 70px;" class="btn" value="Browse" >
<?PHP if (form_error ('file_name_box_1' )) { ?><div ><?php echo form_error( 'file_name_box_1'); ?> </div><?PHP } ?>
</p>
<div id="err_userfile_1"></div>
<p><strong>Username:</strong>
<input type="text" value="<?php if(isset($student['username'])) echo $student['username'];?>" class="input_field validate[minSize[3]]" name="username" id="username"  
	<?php //if(isset($student['username'])) echo 'readonly="readonly"';?> />
</p>
<div id="rtn-username"></div>
<p><strong>Password:</strong>
<input type="password" value="" class="input_field validate[minSize[6]]" name="password" id="password" />
</p>

<p><strong>Confirm Password:</strong>
<input type="password" value="" class="input_field validate[equals[password]]" name="conf_password" id="conf_password" />
</p>

<input type="file" name="userfile_1" id="userfile_1" value="<?php if(isset($student['student_photo'])) echo $student['student_photo'];?>" style="display:none;"/>


</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input type="submit" value="<?php echo ($mode=='new')?'Save':'Save Changes'; ?>" class="btn" name="submit" />&nbsp;

<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/mystudents')?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->

<script>		
	$(document).ready(function(){          
	   $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true}); 
   });
</script>
<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<script>              
        $(document).ready( function(){ 
      $( "#userfile_1").change(function(){
               var filename= $(this).val();            
               var size = this.files[ 0].size;
               var name= this.files[ 0].name;
               var type= this.files[ 0].type;
               if(size> 1048576){
                     $( "#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
                      $( "#file_name_box_1").val('' );
               }
               else if(! (/\.(jpg|jpeg|png|gif)$/i).test(filename))
               { //avi|wmv|mov
                      $("#err_userfile_1").html( '<span style="color:red;">*Only image file accepted (jpg/jpeg/png/gif)</span><br/><br/>');               
                     $( "#file_name_box_1").val('' );
                            
               }
               else{
                     $( "#err_userfile_1").html('' );
                     $( "#file_name_box_1").val(name);
               }
               
               
           });
       });
</script>
<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username").blur(function(){  
       		var username = $('#username').val();
       		var mode='<?php echo $mode;?>';
       	if(mode=="new")
       	{
       		if(username=="")
   			{
   			alert("You have not entered any username");
   			}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?= site_url('login/ajx_username_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else{
	      		var username=$("#username").val();
	      		$("#rtn-username").html('<span style="color:red">Sorry, <span style="color:blue;">'+ username+'</span> is not available, please try another</span>');
	      		$("#username").val('');
	      		}			      
		    }
			});
   		}
   		else{;}
       		
       		
       	}

       		return false;
       });
       
       
        $("#email").blur(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else
			      		{
			      		 var email=$("#email").val();
			      		 $("#rtn-email").html('<span style="color:red">Sorry, <span style="color:blue;">'+email+'</span> is aleady being used</span>');
			      		 $("#email").val('');
			      		}
			      		 				      					      				      			      
			     }
				});
    		return false;
       });             
 	});
</script>