<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">
	<?php echo ($mode=='new')?'Add a Class':'Edit a Class'; ?>

</h1>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">
<?//=var_dump($class)?>
<?php
	$link=site_url('sadmin/myclasses/save/0');
	if(isset($class['class_id']))
		$link=site_url('sadmin/myclasses/save/'.$class['class_id']);
?>
<form  method="post"  action="<?php echo $link; ?>" id="pageForm">

<?=validation_errors()?>

<?php
//var_dump($class);
?>

<div class="edit_form">

<div class="left">

<p><strong>Class Title:</strong>
<input type="text" name="title" id="title" value="<?php if(isset($class['title'])) echo $class['title'];?>" class="input_field validate[required]" />
</p>

<p><strong>Class Name:</strong>
<input type="text" name="name" id="name" value="<?php if(isset($class['name'])) echo $class['name'];?>" class="input_field validate[required]" />
</p>

<p><strong>Section Title:</strong>
<input type="text" name="section_title" id="section_title" value="<?php if(isset($class['section_title'])) echo $class['section_title'];?>" class="input_field" />
</p>

<p><strong>Section Name:</strong>
<input type="text" name="section_name" id="section_name" value="<?php if(isset($class['section_name'])) echo $class['section_name'];?>" class="input_field" />
</p>

<p><strong>Description:</strong>
<textarea type="text" name="description" id="description" value="" class="input_field" /><?php if(isset($class['description'])) echo $class['description'];?></textarea>
</p>
<div id="rtn-description"></div>


</div>	<!-- end left container -->

<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input type="submit" value="Save" class="btn" name="submit" />&nbsp;

<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/myclasses')?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->

<script>		
	$(document).ready(function(){          
	   $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true}); 
   });
</script>
<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#title").blur(function(){  
       		var title = $('#title').val();
   		if(title=="")
   		{
   			//alert("You have not entered any title");
   		}else{
			$("#rtn-title").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'title' : title},
           	url: "<?= site_url('login/ajx_title_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-title").html('<span style="color:green"><b>title Available</b></span>');
       	 	}
	      	else{
	      		var title=$("#title").val();
	      		$("#rtn-title").html('<span style="color:red">Sorry, <span style="color:blue;">'+ title+'</span> is not available, please try another</span>');
	      		$("#title").val('');
	      		}			      
		    }
			});
   		}  		
       		
       		return false;
       });     
        $("#name").blur(function(){  
       		var name = $('#name').val();
   		if(name=="")
   		{
   		   //alert("You have not entered any name");
   		}else{
			$("#rtn-name").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'name' : name},
           	url: "<?= site_url('login/ajx_name_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-name").html('<span style="color:green"><b>Name Available</b></span>');
       	 	}
	      	else{
	      		var name=$("#name").val();
	      		$("#rtn-name").html('<span style="color:red">Sorry, <span style="color:blue;">'+ name+'</span> is not available, please try another</span>');
	      		$("#name").val('');
	      		}			      
		    }
			});
   		}  		
       		
       		return false;
       });         
 	});
</script>