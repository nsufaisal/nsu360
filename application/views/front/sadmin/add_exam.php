<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class=""><?php echo ($mode=='new')?'Add Exam':'Edit Exam'; ?></h1>

<div class="filter-list">
	
<a class="top_linkt right" href="<?php echo isset($exam['exam_id'])? site_url('sadmin/myexams/add_marks/'.$exam['exam_id']):'';?>"><span><img src="<?=base_url();?>images/add-icon.gif" width="16" height="17" alt="" /> Add Marks</span></a>

</div>
<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');

?>

<!-- end account tabs -->

<div class="tabs_contents">
	
<?php
	$link=site_url('sadmin/myexams/save/0');
	
	if(isset($exam['exam_id']))
	{
		$link=site_url('sadmin/myexams/save/'.$exam['exam_id']);
		
	}	

	
?>

<form  method="post" enctype="multipart/form-data" action="<?= $link?>" id="add_exam">

<?=validation_errors()?>

<div class="edit_form">
<!--<p><strong>Section:</strong>-->

</p>
<div class="left">

<p><strong>Class Title</strong>
<select name="class_title" id="class_title" class="input_field">
	<option value="">Select</option>
	<?php foreach($classes as $class) :?>
		<option <?php if(isset($exam['class_title'])) echo ($exam['class_title']==$class['title'])?'selected="selected"':'';?> value="<?php echo $class['class_id']; ?>"><?php echo $class['title']; ?></option>	
	<?php  endforeach;?>	
</select>
</p>

<p><strong>Exam Title:</strong>
<input type="text" value="<?php echo isset($exam['exam_title'])? $exam['exam_title']:'';  ?>" class="input_field validate[required]" name="exam_title" id="exam_title" />
</p>

<p><strong>Exam Date:</strong>

	<?php		
	if(isset($exam['date']))
		{
			$examdate = explode('-', $exam['date']);			
			$eyear  = $examdate[0];
			$emonth   = $examdate[1];
			$eday = $examdate[2];
		}?>

<select name="month" id="month" class="input_field validate[required]" style="width:145px;">
	<option value="" selected="selected">Month</option>
	<option <?php if(isset($emonth)) echo ($emonth=="01")?'selected="selected"':'';?> value="01">January</option>
	<option <?php if(isset($emonth)) echo ($emonth=="02")?'selected="selected"':'';?> value="02">February</option>
	<option <?php if(isset($emonth)) echo ($emonth=="03")?'selected="selected"':'';?> value="03">March</option>
	<option <?php if(isset($emonth)) echo ($emonth=="04")?'selected="selected"':'';?> value="04">April</option>
	<option <?php if(isset($emonth)) echo ($emonth=="05")?'selected="selected"':'';?> value="05">May</option>
	<option <?php if(isset($emonth)) echo ($emonth=="06")?'selected="selected"':'';?> value="06">June</option>
	<option <?php if(isset($emonth)) echo ($emonth=="07")?'selected="selected"':'';?> value="07">July</option>
	<option <?php if(isset($emonth)) echo ($emonth=="08")?'selected="selected"':'';?> value="08">August</option>
	<option <?php if(isset($emonth)) echo ($emonth=="09")?'selected="selected"':'';?> value="09">September</option>
	<option <?php if(isset($emonth)) echo ($emonth=="10")?'selected="selected"':'';?> value="10">October</option>
	<option <?php if(isset($emonth)) echo ($emonth=="11")?'selected="selected"':'';?> value="11">November</option>
	<option <?php if(isset($emonth)) echo ($emonth=="12")?'selected="selected"':'';?> value="12">December</option>
</select>&nbsp;
<select name="day" id="day" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Day</option>
<?php
for($i=1;$i<=31;$i++)
{
	if($i<10)
		$day='0'.$i;
	else 
		$day=$i;
	
	$selected='';
	if(isset($eday)){
		if(intval($eday)==intval($eday))
			$selected='selected="selected"';
	} 
	echo '<option '.$selected.' value="'.$day.'">'.$day.'</option>';
}
?>
</select>&nbsp;

<select name="year" id="year" class="input_field validate[required]" style="width:145px;">
<option value="" selected="selected">Year</option>
<?php
	$year=date('Y');
	for($i=$year;$i>=1980;$i--)
	{
		$selected='';
		if(isset($eyear)){
			if(intval($eyear)==intval($i))
				$selected='selected="selected"';
		} 
		echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
	}
?>
</select>
</p>

<p><strong>Marks:</strong>
<input type="text" name="marks" id="marks" value="<?php echo isset($exam['marks'])? $exam['marks']:'';  ?> " class="input_field validate[required,custom[number]]" />
</p>


<p>
<strong>Weight:</strong>
<input name="weight" id="weight" type="text" value="<?php echo isset($exam['weight'])? $exam['weight']:'';  ?>" class="input_field validate[required,custom[number]]"/>

</p>


</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input type="submit" value="Save" class="btn" name="submit" />&nbsp;

<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/myexams')?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
<script type="text/javascript">
$(document).ready(function() {

$('#add_exam').validationEngine();

});

</script>

