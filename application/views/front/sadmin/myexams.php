<div id="page_body">

    <div class="center">

      <h1> Exam List</h1>
 

	<!-- acc_tabs loading-->	
	
	<?php $this->load->view('front/sadmin/account_tabs'); $selected_class=0;?>

      <!-- end account tabs -->

      <div class="tabs_contents acdivs">

      <form method="post" action=<?php echo ($selected_class==0)? '':site_url('sadmin/myexams/'.$selected_class);?>>

      <div class="filter-list">

      <p style="padding-top: 0; padding-bottom: 15px;">

			<label><strong>Filter By: </strong> </label>
            <select name="classes" id="classes" class="input_field" style="width: 144px;" onchange="this.form.submit();">
                <option value="0">All</option>
                <?php foreach($classes as $class) :?>
				<option <?php if(isset($class_value)) echo ($class_value==$class['class_id'])?'selected="selected"':'';?> value="<?php echo $class['class_id']; ?>" ><?php echo $class['title']; ?></option>	
				<?php  endforeach;?>
          </select>


<a class="top_linkt right" href="<?=site_url('sadmin/myexams/add')?>"><span><img src="<?=base_url();?>images/add-icon.gif" width="16" height="17" alt="" /> Add Exam</span></a>

<a class="top_linkt right" href="<?=site_url('sadmin/myexams/viewclasses')?>"><span><img src="<?=base_url();?>images/list_view_sel.gif" width="16" height="17" alt="" /> View Summary</span></a>

 </p>

      <div class="clear"></div>

      </div>

      </form>



        <!--tutorial tabel -->

        <div class="tutorial-tabel">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<th scope="col">Class Tiltle</th>
     <th scope="col">Exam Title</th>
    <th scope="col">Date</th>
    <th scope="col">Marks</th>  
    <th scope="col">Weight</th>  
    <th scope="col">Actions</th>
  </tr>

  <?php foreach($exams as $exam):?>
  	
  <tr>  
    <td><?=$exam['class_title'] ?></td>
    <td><?=$exam['exam_title'] ?></td>
    <td><?=$exam['date']?></td>
    <td><?=$exam['marks']?></td>
    <td><?=$exam['weight'] ?></td>
    <td><a href="<?php echo site_url('sadmin/myexams/view/'.$exam['exam_id']);?>">
    	<img src="<?=base_url();?>images/tutorial-action1.gif" width="21" height="21" alt="" /></a>&nbsp; 
    	<a href="<?php echo site_url('sadmin/myexams/edit/'.$exam['exam_id']);?>">
    	<img src="<?=base_url();?>images/edit-icon.gif" width="22" height="21" alt="" /></a> &nbsp;
    	<a href="javascript:void(0);" onClick="openForm('<?=site_url('sadmin/myexams/remove/'.$exam['exam_id']);?>');">
   		<img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt="" /></a>
   </td>
  </tr>
  <?php endforeach;?>

</table>
        </div>

        <!--/tutorial tabel -->

      </div>

      <!-- end tabs contents --> 

    </div>

  </div>
    <div id="mypopup" style="display: none;">
    <div class="popup">
    <!--header -->
    <div id="popup-header">
    <h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/pp_logo.gif" /></a></h1>
    <a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content">
    <h2 class="center-text">Button Code</h2>
    <p><textarea style="width: 100%; height: 50px;">{REPLACE}</textarea></p>
    </div>
    <!--/popup content -->
    </div>
  </div>
    <script>
  function check_code(id, title){
      var html = $("#mypopup").html();
      var html1 = '<a href="<?php echo site_url('store/item/')?>/'+id+'/'+title+'"><img src="<?php echo site_url('images/buybtn1.png')?>" alt="Buy '+title+' Now" title="Buy '+title+' Now" border=0></a>';
      html = html.replace('{REPLACE}', html1);
      openModal(html);
  }
    
  </script>
  