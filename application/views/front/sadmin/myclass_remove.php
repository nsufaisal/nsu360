<!--popup -->
<div class="popup">
    <!--header -->
    <div id="popup-header">
        <h1 class="popup-logo"><a href="<?= base_url(); ?>"><img height="59" width="188" alt=" " src="<?= base_url(); ?>images/pp_logo.gif" /></a></h1>
        <a href="javascript:void(0);" onclick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content">
        <h2 class="center-text">Remove a Model</h2>
        <p class="center-text" style="font-size:14px;">Are you sure you want to remove this Class? <br />
            This action CANNOT be undone!</p>
        <!--submit buttons -->
        <div class="submit-buttons">
            <a href="<?=site_url('sadmin/myclasses/remove_confirmation/'.$class_id)?>" class="grey-button">Confirm</a> <a href="<?=site_url('studio/mymodels')?> " onclick="closePP();" class="grey-button">Cancel</a>
        </div>
        <!--/submit buttons -->

    </div>
    <!--/popup content -->
</div>
<!--/popup -->