<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">View a Teacher</h1>

<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/sadmin/account_tabs');?>
	
	<!-- end account tabs -->

<div class="tabs_contents view_cont_y view_mdl_dtal">

<div class="left">

<div class="view_model_img">
  <img src="<?=base_url('uploads/teacher/photo/'.$teacher['teacher_photo']);?>" width="250" alt="Teacher Photo" /> 
  
</div>

<a href="<?=site_url('sadmin/myteachers/edit/'.$teacher['teacher_id'])?>" class="top_linkt"><span>Edit</span></a>&nbsp;
<a href="javascript:void(0);" onclick="openForm('<?=site_url('sadmin/myteachers/remove/'.$teacher['member_id'])?>');" title="Remove" class="top_linkt"><span>Remove</span></a>
</div>
<!-- end view cont left -->

<div class="right">

<p><strong>Teacher Name:</strong>	<?php echo $teacher['first_name'].' '.$teacher['last_name']; ?></p>

<p><strong>Teacher ID: </strong>	<?php echo $teacher['inst_school_id']; ?></p>

<p><strong>Phone/Email: </strong>	<?php echo $teacher['phone'].' / '.$teacher['email']; ?></p>

<p><strong>Class/Section: </strong>	<?php echo $teacher['class_teacher'].' / '.$teacher['class_section']; ?></p>

<p><strong>Birthday / Blood#:</strong>	<?php echo $teacher['birthday'].' / '.$teacher['blood_group']; ?></p>

<p><strong>Current Address:</strong>	<?php echo $teacher['curr_address']; ?></p>

<p><strong>Academic Qualification:</strong>	<?php echo $teacher['academic_qualification']; ?></p>

<p><strong>Emergency Contact:</strong>	<?php echo $teacher['emergency_contact']; ?></p>

<p><strong>Emergengency Mobile:</strong>	<?php echo $teacher['emergency_mobile']; ?></p>

<p><strong>Basic Salary:</strong>	<?php echo $teacher['basic_salary']; ?></p>

<p><strong>House Rent:</strong>	<?php echo $teacher['house_rent']; ?></p>

<p><strong>Medical Allowance:</strong>	<?php echo $teacher['medical_allowance']; ?></p>

<p><strong>Conveyance:</strong>	<?php echo $teacher['conveyance']; ?></p>

<p><strong>Other Allowance:</strong>	<?php echo $teacher['other_allowance']; ?></p>



</div>

<div class="clear"></div>
</div>

</div>

</div>	<!-- end page body -->