<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">View a Student</h1>

<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/sadmin/account_tabs');?>
	
	<!-- end account tabs -->

<div class="tabs_contents view_cont_y view_mdl_dtal">

<div class="left">

<div class="view_model_img">
  <img src="<?=base_url('uploads/student/photo/'.$student['student_photo']);?>" width="250" alt="Student Photo" /> 
  
</div>

<a href="<?=site_url('sadmin/mystudents/edit/'.$student['student_id'])?>" class="top_linkt"><span>Edit</span></a>&nbsp;
<a href="javascript:void(0);" onclick="openForm('<?=site_url('sadmin/mystudents/remove/'.$student['member_id'])?>');" title="Remove" class="top_linkt"><span>Remove</span></a>
</div>
<!-- end view cont left -->

<div class="right">

<p><strong>Student Name:</strong>	<?php echo $student['first_name'].' '.$student['last_name']; ?></p>

<p><strong>Student ID: </strong>	<?php echo $student['std_school_id']; ?></p>

<p><strong>Class/Section: </strong>	<?php echo $student['class'].' / '.$student['curr_section']; ?></p>

<p><strong>Shift:</strong>	<?php echo ($student['shift']==1)?'Morning':'Day'; ?></p>

<p><strong>Email:</strong>	<?php echo $student['email']; ?></p>

<p><strong>Birthday / Blood#:</strong>	<?php echo $student['birthday'].' / '.$student['blood_group']; ?></p>

<p><strong>Current Address:</strong>	<?php echo $student['curr_address']; ?></p>

<p><strong>Father Name:</strong>	<?php echo $student['father_name']; ?></p>

<p><strong>Occupation:</strong>	<?php echo $student['father_occupation']; ?></p>

<p><strong>Phone:</strong>	<?php echo $student['father_phone']; ?></p>

<p><strong>Mother Name:</strong>	<?php echo $student['mother_name']; ?></p>

<p><strong>Mother Occupation:</strong>	<?php echo $student['mother_occupation']; ?></p>

<p><strong>Guardian Name:</strong>	<?php echo $student['guardian_name']; ?></p>

<p><strong>Relation with Student:</strong>	<?php echo $student['guardian_relation']; ?></p>

<p><strong>Guardian Phone:</strong>	<?php echo $student['guardian_phone']; ?></p>


</div>

<div class="clear"></div>
</div>

</div>

</div>	<!-- end page body -->