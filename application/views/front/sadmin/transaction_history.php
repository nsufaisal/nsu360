
<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">Transaction History</h1>


<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/studio/account_tabs');?>
	
	<!-- end account tabs -->



<div class="tabs_contents">

<div class="tran_history_top">
<form action="#" method="get">

<div class="left" style="margin:0;">

<strong>Type:</strong>

<select class="input_field"><option>Lorem Ipsum</option></select>


<strong style="padding:0 15px;">|</strong>

<strong>Model:</strong>

<select class="input_field"><option>Lorem Ipsum</option></select>


<strong style="padding:0 15px;">|</strong>

<strong>Customer:</strong>

<select class="input_field"><option>Lorem Ipsum</option></select>


</div>
<!-- end left -->

<div class="left"  style="float:right">
	<input name="" type="text" class="input_field" value="End Date" style="width:95px;" />
    <a href="#" class="date_icon_s"><img src="<?= base_url(); ?>images/t_date_icon.gif" width="16" height="13" alt=" " /></a>
</div>

<div class="left" style="float:right">
	<input name="" type="text" class="input_field" value="Begin Date" style="width:95px;" />
    <a href="#" class="date_icon_s"><img src="<?= base_url(); ?>images/t_date_icon.gif" width="16" height="13" alt=" " /></a>
</div>

<div class="clear" style="height:13px;">&nbsp;</div>

<div class="srch_bx">

<span class="left">Transaction <a href="#"><img src="<?= base_url(); ?>images/t_srch_nav.png" width="10" height="7" alt=" " /></a></span>

<input name="" type="text" class="input_field left" value="Search" />

<input name="" type="submit" class="srch_sbmt" value="&nbsp;" />

</div>
<!-- end search box -->

<a href="#" class="right top_linkt"><span><img src="<?= base_url(); ?>images/dlr_icon.png" width="14" height="24" alt=" " style="margin-top:-2px;" /> View Financial Reports</span></a>

</form>
<div class="clear"></div>
</div>
<!-- end top part -->


<div class="ymp_table">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <th scope="col" width="80">Item ID <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="200">Time of Purchase <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="150">Transaction ID <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="170">Type of Content <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="150">Model <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col" width="110">Price <a href="#"><img src="<?= base_url(); ?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th scope="col">Actions</th>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr>
    <td><a href="#">Ipsum</a></td>
    <td>02-21-2013</td>
    <td><a href="#">Ipsum</a></td>
    <td>Lorem Ipsum</td>
    <td><a href="#">Lorem Ipsum</a></td>
    <td>$29.90</td>
    <td><a href="#"><img src="<?= base_url(); ?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a></td>
  </tr>
  
  <tr style="border-top:1px solid #e8e8e8;">
  	<td colspan="7" style="text-align:right;">Total of Sales: <span class="bld">$ 5.300.00</span></td>
  </tr>
  
  <tr>
  	<td colspan="7" style="text-align:right;">Total of Sales for the Current Pay Period: <span class="bld">$ 5.300.00</span></td>
  </tr>
  
</table>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->