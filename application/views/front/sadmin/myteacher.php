<div id="page_body">

    <div class="center">

      <h1>Teachers List</h1>

	<!-- acc_tabs loading-->	
	
	<?php
	 $this->load->view('front/sadmin/account_tabs');
	

	?>

      <!-- end account tabs -->

      <div class="tabs_contents acdivs">

          <form method="post" action="">

      <div class="filter-list">

      <p style="padding-top: 0; padding-bottom: 15px;">

			<label><strong>Filter By: </strong> </label>
            <select name="status" class="input_field" style="width: 144px;" onchange="this.form.submit();">
                <option value="all">All</option>
                <option value="0" >Lorem Ipsum</option>
                <option value="1">Lorem Ipsum</option>
                <option value="2" >Lorem Ipsum</option>
                <option value="3" >Lorem Ipsum</option>
                <option value="4" >Lorem Ipsum</option>
                <option value="5" >Lorem Ipsum</option>
            </select>
      <a class="top_linkt right" href="<?=site_url('sadmin/myteachers/add')?>"><span><img src="<?=base_url();?>images/add-icon.gif" width="16" height="17" alt="" /> Add a Teacher</span></a>

 </p>

      <div class="clear"></div>

      </div>

      </form>

        <!--tutorial tabel -->

        <div class="tutorial-tabel">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
     <th scope="col">Image</th>
    <th scope="col">Name</th>
    <th scope="col">Class Teacher</th>
    <th scope="col">Address</th>
    <th scope="col">Phone</th>
    <th scope="col">Email</th>      
    <th scope="col">Actions</th>
  </tr>
  
  <?php //var_dump($teachers); ?>
  <?php foreach($teachers as $teacher):?>
  	
  <tr>
    <td class="image-td"><img src="<?php echo base_url('./uploads/teacher/photo/'.$teacher['teacher_photo']);?>" height="31" alt="" ></td>
    <td><?=$teacher['first_name'].' '.$teacher['last_name'] ?></td>
    <td><?=$teacher['class_title'].'/ Sec: '.$teacher['class_section'] ?></td>
    <td><?=$teacher['curr_address']?></td>
    <td><?=$teacher['phone']?></td><?php  $member=$this->members_model->GetMemberImportantDetail($teacher['member_id']);?>
    <td><?=$member['email']?></td>
    
   
    <td><a href="<?php echo site_url('sadmin/myteachers/view/'.$teacher['teacher_id'])?>">
    	<img src="<?=base_url();?>images/tutorial-action1.gif" width="21" height="21" alt="" /></a>&nbsp; 
    	<a href="<?php echo site_url('sadmin/myteachers/edit/'.$teacher['teacher_id'])?>">
    	<img src="<?=base_url();?>images/edit-icon.gif" width="22" height="21" alt="" /></a> &nbsp;
    	<a href="javascript:void(0);" onClick="openForm('<?=site_url('sadmin/myteachers/remove/'.$teacher['teacher_id']);?>');">
   		<img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt="" /></a>
   </td>
  </tr>
  <?php endforeach;?>

</table>
        </div>

        <!--/tutorial tabel -->

      </div>

      <!-- end tabs contents --> 

    </div>

  </div>
    <div id="mypopup" style="display: none;">
    <div class="popup">
    <!--header -->
    <div id="popup-header">
    <h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/pp_logo.gif" /></a></h1>
    <a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content">
    <h2 class="center-text">Button Code</h2>
    <p><textarea style="width: 100%; height: 50px;">{REPLACE}</textarea></p>
    </div>
    <!--/popup content -->
    </div>
  </div>
    <script>
  function check_code(id, title){
      var html = $("#mypopup").html();
      var html1 = '<a href="<?php echo site_url('store/item/')?>/'+id+'/'+title+'"><img src="<?php echo site_url('images/buybtn1.png')?>" alt="Buy '+title+' Now" title="Buy '+title+' Now" border=0></a>';
      html = html.replace('{REPLACE}', html1);
      openModal(html);
  }
    
  </script>