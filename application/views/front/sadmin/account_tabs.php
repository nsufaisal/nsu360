<div class="acc_tabs">

<ul>

<li <?=(isset($sel) && $sel=='myaccount')?'class="active"':'' ?>><a href="<?=site_url('sadmin/myaccount')?>" class="mnav_1">My Account</a></li>

<li <?=(isset($sel) && $sel=='students')?'class="active"':'' ?>><a href="<?=site_url('sadmin/students')?>" class="mnav_1">Students</a></li>

<li <?=(isset($sel) && $sel=='instructors')?'class="active"':'' ?>><a href="<?=site_url('sadmin/instructors')?>" class="mnav_1">Instructors</a></li>

<li <?=(isset($sel) && $sel=='courses')?'class="active"':'' ?>><a href="<?=site_url('sadmin/courses')?>" class="mnav_1">Courses</a></li>

<li <?=(isset($sel) && $sel=='msg_center')?'class="active"':'' ?>><a href="#" class="mnav_1">Message Center</a></li>

<li <?=(isset($sel) && $sel=='notice_wall')?'class="active"':'' ?>><a href="#" class="mnav_1">Notice Wall</a></li>

</ul>

<div class="clear"></div>

</div>
