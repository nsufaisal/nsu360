<style>
	.loading-indicator{
		color:#737373;
		font-size:.9em;
		text-shadow: 1px;
		
	}
	.activ_error{
		
		background-image: linear-gradient(to bottom,#f2dede 0,#e7c3c3 100%);
		background-repeat: repeat-x;
		border-color: #dca7a7;
		text-shadow: 0 1px 0 rgba(255,255,255,0.2);
		-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25),0 1px 2px rgba(0,0,0,0.05);
		box-shadow: inset 0 1px 0 rgba(255,255,255,0.25),0 1px 2px rgba(0,0,0,0.05);
		color: #b94a48;
		background-color: #f2dede;
		padding: 5px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		font-size: 14px;
		line-height: 1.428571429;		
	}
</style>
<div id="page_body">
<div class="center">
<h1>Edit My Account</h1>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>

<div class="tabs_contents">

<form action="<?= site_url('instructor/myaccount/save')?>" method="post" name="membr_edit" id="membr_edit" accept-charset="utf-8" >
	
<div class="edit_form">

<div class="left">

<p>
<strong>First Name:</strong>
<input name="first_name" id="first_name" type="text" class="input_field validate[required,custom[onlyLetterSp]]"value="<?php echo isset($my_info['first_name'])?$my_info['first_name']:''; ?>"  />
</p>

<p>
<strong>Last Name:</strong>
<input name="last_name" id="last_name" type="text" class="input_field validate[required,custom[onlyLetterSp]]" value="<?php echo isset($my_info['last_name'])?$my_info['last_name']:''; ?>"  />
</p>

<p>
<strong>NSU ID:</strong>
<input name="inst_school_id" id="inst_school_id" readonly="readonly" type="text" class="input_field validate[required]" value="<?php echo isset($my_info['inst_school_id'])?$my_info['inst_school_id']:''; ?>" />
</p>

<p>
<strong>Initial:</strong>
<input name="instructor_initial" id="instructor_initial" type="text" class="input_field validate[required]" value="<?php echo isset($my_info['instructor_initial'])?$my_info['instructor_initial']:''; ?>" />
</p>

<p>
<strong>Email Address:</strong>
<input name="email" id="email" type="text" readonly="readonly" class="input_field validate[required,custom[email]]" value="<?php echo isset($my_info['email'])?$my_info['email']:''; ?>"  />
</p>

<p><strong>Department Name:</strong>
<select name="department_id" id="department_id" class="input_field validate[required]" >
	<option value="">Select</option>
	<?php foreach($departments as $dep) :?>
		<option <?php echo (isset($my_info['department_id']) && $my_info['department_id']==$dep['department_id'])?'selected="selected"':'';?> value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option>	
	<?php  endforeach;?>
</select>
</p>

</div>	<!-- end left container -->

<div class="right">

<p>
<strong>Phone Number:</strong>
<input name="phone" id="phone" type="text" class="input_field validate[required]" value="<?php echo isset($my_info['phone'])?$my_info['phone']:''; ?>"  />
</p>

<p>
<strong>Address:</strong>
<input name="curr_address" id="curr_address" type="text" class="input_field validate[required]" value="<?php echo isset($my_info['curr_address'])?$my_info['curr_address']:''; ?>"  />
</p>

<p>
	
<span style="font-weight:bold">Gender:</span> <span style="margin-left:185px; font-weight: bold;">Blood Group:</span><br/>
<select name="gender" class="input_field validate[required]" style="width:220px;">
	<option value="">Select</option>
	<option value="M" <?php echo (isset($my_info['gender']) && $my_info['gender']=='M')? 'selected="selected"':'';?>>Male</option>
	<option value="F" <?php echo (isset($my_info['gender']) && $my_info['gender']=='F')?'selected="selected"':'';?>>Female</option>
</select>&nbsp;

<select name="blood_group" id="blood_group" class="input_field validate[required]" style="width:220px;">
	<option value="">Select</option>
	<option value="O+" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='O+')?'selected="selected"':'';?>>O+</option>
	<option value="O-" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='O-')?'selected="selected"':'';?>>O-</option>
	<option value="A+" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='A+')?'selected="selected"':'';?>>A+</option>
	<option value="A-" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='A-')?'selected="selected"':'';?>>A-</option>
	<option value="B+" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='B+')?'selected="selected"':'';?>>B+</option>
	<option value="B-" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='B-')?'selected="selected"':'';?>>B-</option>
	<option value="AB+" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='AB+')?'selected="selected"':'';?>>AB+</option>
	<option value="AB-" <?php echo (isset($my_info['blood_group']) && $my_info['blood_group']=='AB-')?'selected="selected"':'';?>>AB-</option>
</select>
</p>

<p>
<strong>Password:</strong>
<input name="password" id="password" value="<?php echo set_value('password'); ?>" type="password" class="input_field"  />

<?php if(form_error('password')){ ?>			
	<div class="activ_error">
		<?php  echo form_error('password'); ?>
	</div>
<?php } ?>

</p>

<p>
<strong>Change Password:</strong>
<input name="new_password" id="new_password" value="<?php echo set_value('new_password'); ?>" type="password" class="input_field"  />

<?php if(form_error('new_password')){ ?>			
	<div class="activ_error">
		<?php  echo form_error('new_password'); ?>
	</div>
<?php } ?>

</p>

<p>
<strong>Confirm Password:</strong>
 <input type="password" name="conf_password" id="conf_password" value="<?php echo set_value('conf_password'); ?>" class="input_field validate[equals[new_password]]" />

<?php if(form_error('conf_password')){ ?>			
	<div class="activ_error">
		<?php  echo form_error('conf_password'); ?>
	</div>
<?php } ?>
        
</p>

</div>	<!-- end right container -->

<div class="clear"></div>

<div align="center" style="padding-top:15px;">
<input name="" type="submit" name="submit" id="submit" class="btn" value="Save" />&nbsp;
<input name="cancel_btn" type="button" onclick=" window.location .href ='<?=site_url ('instructor/myaccount' )?> '" class="btn" value="Cancel" />&nbsp;
<!--<a href="<?=site_url('member/myaccount')?>"><input name="" type="button" class="btn" value="Cancel" /></a> -->
</div>

</div>
</form>
</div>	<!-- end tabs contents -->

</div>
</div>
	<!--instance the validator engine ::rtn:: -->	
	<script>		
	 	$("#membr_edit").validationEngine('attach', {promptPosition : "topRight", scroll: false},
               {focusFirstField : true});
	 	
		
	</script>
	
<script>              
        $(document).ready( function(){ 
      $( "#userfile_1").change(function(){
               var filename= $(this).val();            
               var size = this.files[ 0].size;
               var name= this.files[ 0].name;
               var type= this.files[ 0].type;
               if(size> 1048576){
                     $( "#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
                     $( "#file_name_box_1").val('' );
               }
               else if(! (/\.(jpg|jpeg|png|gif)$/i).test(filename))
               { //avi|wmv|mov
                      $("#err_userfile_1").html( '<span style="color:red;">*Only image file accepted (jpg/jpeg/png/gif)</span><br/><br/>');               
                     $( "#file_name_box_1").val('' );                            
               }
               else{
                     $( "#err_userfile_1").html('' );
                     $( "#file_name_box_1").val(name);
               }              
           });
       });
</script>