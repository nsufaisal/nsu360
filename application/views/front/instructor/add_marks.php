<style>
	
	.input_field {
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    color: rgb(130, 130, 130);
    border: 1px solid rgb(219, 219, 219);
    padding: 8px;
    border-radius: 7px 7px 7px 7px;
    width: 32px;
    margin: 0px;
    box-shadow: 0px 1px rgb(245, 245, 245);
}
</style>
<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">Add Marks</h1>

<div class="clear"></div>

	<!-- acc_tabs loading-->

	<?php $this->load->view('front/sadmin/account_tabs');?>
	
	<!-- end account tabs -->

<div class="tabs_contents view_cont_y view_mdl_dtal" style="height:60 px;" >

<div class="right">

<div class="">
  
  
</div>

</div>
<!-- end view cont left -->

<div class="left" style="height:60 px;">

<p><strong>Class Title: </strong> <?php echo $exam['class_title']; ?></p>

<p><strong>Exam Title: </strong> <?php echo $exam['exam_title']; ?></p>

<p><strong>Date: </strong>	<?php echo $exam['date']; ?></p>

<p><strong>Marks:</strong>	<?php echo $exam['marks']; ?></p>

<p><strong>Weight:</strong>	<?php echo $exam['weight']; ?></p>


</div>

      <div class="clear"></div>

      </div>
      <?php 
     $link=site_url('sadmin/myexams/save_marks/'.$exam['exam_id']);
      ?>

        <!--tutorial tabel -->

  <div class="tutorial-tabel">
  <form  method="post" enctype="multipart/form-data" action="<?= $link?>" id="add_exam">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  
  <tr>
  	<th scope="col">Student Id</th>
     <th scope="col">Marks</th>
    <th scope="col">Action</th>  
  </tr>

  <?php foreach($students as $student):?>
  	
  <tr>  
    <td><input type="hidden" name="students[]" id="students" value="<?php  echo $student['student_id']; ?>"/><?php  echo $student['student_id']; ?></td>
    <td><input type="text" name="marks[]" id="marks" value=""  size="10" class="input_field validate[required,custom[number]]" /></td>
    <td><a href="<?php echo site_url('sadmin/myexams/view/');?>">
    	<img src="<?=base_url();?>images/tutorial-action1.gif" width="21" height="21" alt="" /></a>&nbsp; 
    	<a href="<?php echo site_url('sadmin/myexams/edit/');?>">
    	<img src="<?=base_url();?>images/edit-icon.gif" width="22" height="21" alt="" /></a> &nbsp;
    	<a href="javascript:void(0);" onClick="openForm('<?=site_url('sadmin/myexams/remove/');?>');">
   		<img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt="" /></a>
   </td>
   <td><input type="hidden" name="course_id[]" id="course_id" value="<?php  echo $student['course_id']; ?>"/></td>
   <td><input type="hidden" name="exam_id[]" id="exam_id" value="<?php  echo $student['exam_id']; ?>"/></td>
  </tr>
  <?php endforeach;?>

	</table>
	<div align="center" style="padding-top:20px;">

	<input type="submit" value="Save" class="btn" name="submit" />&nbsp;

	<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/myexams')?>'" type="button" class="btn" value="Cancel" />

</div>
	
	</form>
        </div>

        <!--/tutorial tabel -->

      </div>

      <!-- end tabs contents --> 

    </div>

  </div>
    <div id="mypopup" style="display: none;">
    <div class="popup">
    <!--header -->
    <div id="popup-header">
    <h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/pp_logo.gif" /></a></h1>
    <a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content">
    <h2 class="center-text">Button Code</h2>
    <p><textarea style="width: 100%; height: 50px;">{REPLACE}</textarea></p>
    </div>
    <!--/popup content -->
    </div>
  </div>
    <script>
  function check_code(id, title){
      var html = $("#mypopup").html();
      var html1 = '<a href="<?php echo site_url('store/item/')?>/'+id+'/'+title+'"><img src="<?php echo site_url('images/buybtn1.png')?>" alt="Buy '+title+' Now" title="Buy '+title+' Now" border=0></a>';
      html = html.replace('{REPLACE}', html1);
      openModal(html);
  }
    
  </script>
  