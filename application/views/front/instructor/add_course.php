<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">
	<?php echo ($mode=='new')? 'Add Course':'Edit Course';?>

</h1>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/sadmin/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">
<?//=var_dump($class)?>
<?php

		$link=site_url('sadmin/myclasses/save_course/0');
		
		if(isset($course['course_id']))
			$link=site_url('sadmin/myclasses/save_course/'.$course['course_id']);
		
		if($mode=='new')
			$m=0;
		else {
			$m=1;
		}
		
?>
<form  method="post"  action="<?php echo $link; ?>" id="pageForm">

<?=validation_errors()?>

<?php
//var_dump($teachers);
?>

<div class="edit_form">


<div class="left">
	
<p><strong>Class Title: <?php echo ($mode=='new')? $class['title']:$course['class_title']; ?></strong>
<input type="hidden" name="class_id" id="class_id" value="<?php echo ($mode=='new')? $class['class_id']:$course['class_id'];; ?>" class="input_field validate[required]" />
</p>

<p><strong>Course Title:</strong>
<input type="text" name="course_title" id="course_title" value="<?php echo isset($course['course_title'])? $course['course_title']:''; ?>" class="input_field validate[required]" />
</p>

<p><strong>Credit Hour:</strong>
<input type="text" name="credit_hour" id="credit_hour" value="<?php echo isset($course['credit_hour'])? $course['credit_hour']:''; ?>" class="input_field validate[required]" />
</p>

<p><strong>Description:</strong>
<textarea type="text" name="description" id="description" value="<?php echo isset($course['description'])? $course['description']:''; ?>" class="input_field" />
	<?php echo isset($course['description'])? $course['description']:''; ?>
	
</textarea>
</p>

<p><strong>Syllabus:</strong>
<textarea type="text" name="syllabus" id="syllabus" value="<?php echo isset($course['syllabus'])? $course['syllabus']:''; ?>" class="input_field" />
	<?php echo isset($course['syllabus'])? $course['syllabus']:''; ?>
	
</textarea>
</p>

<p><strong>Level:</strong>
	<select name="level" class="input_field" style="width: 144px;" onchange="">
                <option <?php if(isset($course['level'])) echo ($course['level']==1)? 'selected=="selected"':''; ?> value="1">Easiest</option>
                <option <?php if(isset($course['level'])) echo ($course['level']==2)? 'selected=="selected"':''; ?>value="2" >Easier</option>
                <option <?php if(isset($course['level'])) echo ($course['level']==3)? 'selected=="selected"':''; ?>value="3" >Easy</option>
                <option <?php if(isset($course['level'])) echo ($course['level']==4)? 'selected=="selected"':''; ?>value="4" >Hard</option>
                <option <?php if(isset($course['level'])) echo ($course['level']==5)? 'selected=="selected"':''; ?>value="5" >Hardier</option>
            </select>

</p>

<p><strong>Associated Teacher:</strong>
	
	<select name="teachers" id="teachers" class="input_field">
	<option value="">Select</option>
	<?php foreach($teachers as $teacher) :?>
		<option <?php if(isset($course['associated_teachers'])) echo ($course['associated_teachers']==$teacher['teacher_id'])?'selected="selected"':'';?> value="<?php echo $teacher['teacher_id'] ?>">
		<?php echo $teacher['first_name']." ".$teacher['last_name'];?>	
		</option>	
	<?php  endforeach;?>	
		
	</select>
	
	
</p>
<input type="hidden" name="mode" id="mode" value="<?php echo isset($m)? $m:'';?>"/>

<div id="rtn-description"></div>


</div>	<!-- end left container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">




<input type="submit" value="Save" class="btn" name="submit" />&nbsp;

<?php
if($mode=="new")
	$rlink='sadmin/myclasses/view/'.$class['class_id'];
else {
	$rlink='sadmin/myclasses/view_course/'.$course['course_id'];
}

 ?>

<input name="cancel_btn" onclick="window.location.href='<?=site_url($rlink)?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->

<script>		
	$(document).ready(function(){          
	   $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true}); 
   });
</script>
<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#title").blur(function(){  
       		var title = $('#title').val();
   		if(title=="")
   		{
   			//alert("You have not entered any title");
   		}else{
			$("#rtn-title").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'title' : title},
           	url: "<?= site_url('login/ajx_title_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-title").html('<span style="color:green"><b>title Available</b></span>');
       	 	}
	      	else{
	      		var title=$("#title").val();
	      		$("#rtn-title").html('<span style="color:red">Sorry, <span style="color:blue;">'+ title+'</span> is not available, please try another</span>');
	      		$("#title").val('');
	      		}			      
		    }
			});
   		}  		
       		
       		return false;
       });     
        $("#name").blur(function(){  
       		var name = $('#name').val();
   		if(name=="")
   		{
   		   //alert("You have not entered any name");
   		}else{
			$("#rtn-name").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'name' : name},
           	url: "<?= site_url('login/ajx_name_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-name").html('<span style="color:green"><b>Name Available</b></span>');
       	 	}
	      	else{
	      		var name=$("#name").val();
	      		$("#rtn-name").html('<span style="color:red">Sorry, <span style="color:blue;">'+ name+'</span> is not available, please try another</span>');
	      		$("#name").val('');
	      		}			      
		    }
			});
   		}  		
       		
       		return false;
       });         
 	});
</script>