<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="">
	<?php echo ($mode=='new')?'Add New Student':'Edit Student'; ?>

</h1>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">

<?php

	$link=site_url('instructor/mystudents/save/0');
	if(isset($student['student_id']))
		$link=site_url('instructor/mystudents/save/'.$student['student_id']);
?>
<form  method="post" enctype="multipart/form-data" action="<?php echo $link; ?>" id="pageForm">

<?=validation_errors()?>

<div class="edit_form">

<div class="left">

<p><strong>Student ID:</strong>
<input type="text" <?php if($mode=='edit') echo 'readonly="readonly"'; ?> value="<?php if(isset($student['student_id'])) echo $student['student_id'];?>" class="input_field validate[required,minSize[9],custom[integer]]" name="student_id" id="student_id" />
</p>
<div id="unique_student_id_msg"></div>

<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" value="<?php if(isset($student['first_name'])) echo $student['first_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="<?php if(isset($student['last_name'])) echo $student['last_name'];?>" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p>
<strong>Phone:</strong>
<input type="text" name="phone" id="phone" value="" class="input_field validate[required]" />
</p>

<p style="position: relative;"> <strong>Upload Student Photo:</strong><span class="file-size">Max File Size 320x340</span>
<input type="text"   name="file_name_box_1" onfocus=" javascript: openBrowse1(); " id="file_name_box_1" style="width:345px;" class="input_field validate[required]" value=" <?=isset( $tutorial['video_file' ])? $tutorial['video_file' ]:''?>">&nbsp;
<input type="button" onclick=" javascript: openBrowse1(); " name="" id ="uploadButton" style="min-width: 70px;" class="btn" value="Browse" >
<?PHP if (form_error ('file_name_box_1' )) { ?><div ><?php echo form_error( 'file_name_box_1'); ?> </div><?PHP } ?>
</p>
<div id="err_userfile_1" ></div>
<div class="clear" ></div>



</div>	<!-- end left container -->



<div class="right">

<p><strong>Department Name:</strong>
<select name="department_id" id="department_id" class="input_field validate[required]" >
	<option value="">Select</option>
	<?php foreach($departments as $dep) :?>
		<option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].' ('.$dep['initial'].')';?></option>	
	<?php  endforeach;?>
</select>
</p>

<p><strong>Course:</strong>
<select name="course_id" id="course_id" class="input_field validate[required]" >
	<option value="">Select</option>
	<?php foreach($courses as $course) :?>
		<option value="<?php echo $course['course_id'];?>"><?php echo $course['initial'].' ('.$course['section_no'].')';?></option>	
	<?php  endforeach;?>
</select>
</p>
   
<p><strong>Email:</strong>
<input type="text" name="email" id="email" value="" class="input_field validate[required,custom[email]]" />
</p>
<div id="unique_email_msg"></div>

<p><strong>Password:</strong>
<input type="password" name="password" id="password" value="" class="input_field validate[required,minSize[6]]" />
</p>

<p><strong>Password Again:</strong>
<input type="password" name="conf_password" id="conf_password" value="" class="input_field validate[required,minSize[6],equals[password]]" />
</p>

<!-- hidden file uploaders -->
<input type="file" name="userfile_1" id="userfile_1" style=" display:none ;"/>

</div>	<!-- end right container -->
<div class="clear"></div>

<div align="center" style="padding-top:20px;">
<input type="submit" value="Register" class="btn" name="submit" />&nbsp;
<input type="button" value="Cancel" class="btn cancel_btn" name="cancel_btn" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
<script>
    $('#curr_class').change(function(e) {
    	//alert(this.value);
        $('#curr_section').load('sadmin/mystudents/load_section/'+this.value);
    });
</script>
<script>		
	$(document).ready(function(){          
	   $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true}); 
   });
</script>

<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<script>              
        $(document).ready( function(){ 
      $( "#userfile_1").change(function(){
               var filename= $(this).val();            
               var size = this.files[ 0].size;
               var name= this.files[ 0].name;
               var type= this.files[ 0].type;
               if(size> 1048576){
                     $( "#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
                      $( "#file_name_box_1").val('' );
               }
               else if(! (/\.(jpg|jpeg|png|gif)$/i).test(filename))
               { //avi|wmv|mov
                      $("#err_userfile_1").html( '<span style="color:red;">*Only image file accepted (jpg/jpeg/png/gif)</span><br/><br/>');               
                     $( "#file_name_box_1").val('' );
                            
               }
               else{
                     $( "#err_userfile_1").html('' );
                     $( "#file_name_box_1").val(name);
               }
               
               
           });
       });
</script>
<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username").blur(function(){  
       		var username = $('#username').val();
       		var mode='<?php echo $mode;?>';
       	if(mode=="new")
       	{
       		if(username=="")
   			{
   			alert("You have not entered any username");
   			}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?= site_url('login/ajx_username_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else{
	      		var username=$("#username").val();
	      		$("#rtn-username").html('<span style="color:red">Sorry, <span style="color:blue;">'+ username+'</span> is not available, please try another</span>');
	      		$("#username").val('');
	      		}			      
		    }
			});
   		}
   		else{;}
       		
       		
       	}

       		return false;
       });
       
       
        $("#email").blur(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else
			      		{
			      		 var email=$("#email").val();
			      		 $("#rtn-email").html('<span style="color:red">Sorry, <span style="color:blue;">'+email+'</span> is aleady being used</span>');
			      		 $("#email").val('');
			      		}
			      		 				      					      				      			      
			     }
				});
    		return false;
       });             
 	});
</script>