<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/pp_logo.png" /></a></h1>
<a href="javascript:void(0);" onClick="closePP();" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content">
<h2>Remove Notice</h2>
<?=$content;?>

<!--submit buttons -->
<div class="submit-buttons">
<a href="<?php echo site_url('instructor/noticewall/remove_confirmation/'.$noticewall_id);?>" onClick="closePP();" class="grey-button">OK</a>
<a href="javascript:void(0)" onClick="closePP();" class="grey-button">Close</a>
</div>
<!--/submit buttons -->

</div>
<!--/popup content -->
</div>