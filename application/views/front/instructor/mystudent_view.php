<style>
	.myaccount_avatar{
		max-height: 300px;
		overflow:none;
		margin:0 auto;
		padding:5px;
		float:right;
	}
	.polaroid {
  position: relative;
}

.polaroid img {
  border: 8px solid #fff;
  -webkit-box-shadow: 2px 2px 2px #777;
     -moz-box-shadow: 2px 2px 2px #777;
          box-shadow: 2px 2px 2px #777;
}

</style>
<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> View Student </h1>

<div class="right top_link_dv">

</div>

<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac">

<div class="left">

<div class="myaccount_avatar polaroid">

<?php 
	$avatar_name='unknown_avatar.png';
	
	if($student_info['gender']=='M')
		$avatar_name='unknown_avatar_male.png';
?>

<img src="<?php echo base_url('uploads/student/'.$student_id.'/'.$student_info['photo']);?>" 
        
        onerror="this.src=' <?php echo base_url(); ?>images/<?php echo $avatar_name;?>'" width="220" alt="Avatar" />
		
</div>

</div>	<!-- end left container -->

<div class="right">

<p>

<strong>NSU ID:</strong>

<?php echo isset($student_info['student_id'])?$student_info['student_id']:''; ?>

</p>

<p>

<strong>First Name:</strong>

<?php echo isset($student_info['first_name'])?$student_info['first_name']:''; ?>

</p>



<p>

<strong>Last Name:</strong>

<?php echo isset($student_info['last_name'])?$student_info['last_name']:''; ?>

</p>



<p>

<strong>Email Address:</strong>

<?php echo isset($student_info['email'])?$student_info['email']:''; ?>

</p>

<p>

<strong>Phone Number:</strong>

<?php echo isset($student_info['phone'])?$student_info['phone']:''; ?>

</p>

<p>

<strong>Gender:</strong>

<?php echo isset($student_info['gender'])?$student_info['gender']:''; ?>

</p>

<p>

<strong>Blood Group:</strong>

<?php echo isset($student_info['blood_group'])?$student_info['blood_group']:''; ?>

</p>


<p>

<strong>Address:</strong>

<?php echo isset($student_info['curr_address'])?$student_info['curr_address']:''; ?>

</p>

<br/>
<a href="<?php echo site_url('instructor/mystudent');?>" class="top_linkt"><span> Back </span></a>


</div>	<!-- end right container -->

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>


<!--instance the validator engine ::rtn:: -->	
<script>
    $("#pageForm").validationEngine('attach', {promptPosition: "topRight", scroll: false},
    {focusFirstField: true});
</script>