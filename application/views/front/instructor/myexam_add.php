<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>js/juitime/jquery-ui-timepicker-addon.css"/>
<script src="<?php echo site_url('js/juitime/jquery-ui-timepicker-addon.js') ?>"></script>
<script src="<?php echo site_url('js/juitime/jquery-ui-sliderAccess.js') ?>"></script>

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class=""><?php echo ($mode=='new')?'Add Exam':'Edit Exam'; ?></h1>

<div class="filter-list">
	
<a class="top_linkt right" href="<?php echo isset($exam['exam_id'])? site_url('sadmin/myexams/add_marks/'.$exam['exam_id']):'';?>"><span>
	<img src="<?=base_url();?>images/add-icon.gif" width="16" height="17" alt="" /> Add Marks</span></a>

</div>
<div class="clear"></div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');

?>

<!-- end account tabs -->

<div class="tabs_contents">
	
<?php
	$link=site_url('sadmin/myexams/save/0');
	
	if(isset($exam['exam_id']))
	{
		$link=site_url('sadmin/myexams/save/'.$exam['exam_id']);
		
	}	

	
?>

<form  method="post" enctype="multipart/form-data" action="<?= $link?>" id="add_exam">

<?=validation_errors()?>

<div class="edit_form">
<!--<p><strong>Section:</strong>-->

</p>
<div class="left">

<p><strong>Class Title</strong>
<select name="class_title" id="class_title" class="input_field">
	<option value="">Select</option>
	<?php foreach($classes as $class) :?>
		<option <?php if(isset($exam['class_title'])) echo ($exam['class_title']==$class['title'])?'selected="selected"':'';?> value="<?php echo $class['class_id']; ?>"><?php echo $class['title']; ?></option>	
	<?php  endforeach;?>	
</select>
</p>

<p><strong>Course Title</strong>

</p>

<p><strong>Exam Title:</strong>
<input type="text" value="<?php echo isset($exam['exam_title'])? $exam['exam_title']:'';  ?>" class="input_field validate[required]" name="exam_title" id="exam_title" />
</p>

<p id="publish_time_div" > <span class="date-inputs left"> <strong>Date:</strong>
         <?php date_default_timezone_set('asia/dhaka'); ?>
        <input name="publish_time" id="publish_time" type="text" class="input_field" 
        value="<?php echo date('Y-m-d H:i'); ?>" />
        <a href="#" class="date-icon" onclick="$('#publish_time').focus(); return false;" >
        	<img src="<?= base_url(); ?>images/date-input-icon.gif" width="16" height="13" alt="" /></a>
    </span>  </p>

<div class="clear"></div>
<p><strong>Marks:</strong>
<input type="text" name="marks" id="marks" value="<?php echo isset($exam['marks'])? $exam['marks']:'';  ?> " class="input_field validate[required,custom[number]]" />
</p>


<p>
<strong>Weight:</strong>
<input name="weight" id="weight" type="text" value="<?php echo isset($exam['weight'])? $exam['weight']:'';  ?>" class="input_field validate[required,custom[number]]"/>

</p>


</div>	<!-- end right container -->


<div class="clear"></div>



<div align="center" style="padding-top:20px;">

<input type="submit" value="Save" class="btn" name="submit" />&nbsp;

<input name="cancel_btn" onclick="window.location.href='<?=site_url('sadmin/myexams')?>'" type="button" class="btn" value="Cancel" />

</div>
</div>

<!-- hidden file uploaders -->

<!-- hidden file uploaders end -->

</form>

</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
<script type="text/javascript">
$(document).ready(function() {

$('#add_exam').validationEngine();

});

 $('#publish_time').datetimepicker({
		dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });
</script>

