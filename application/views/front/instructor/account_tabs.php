<div class="acc_tabs">

<ul>

<li <?=(isset($sel) && $sel=='myaccount')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/myaccount');?>" class="mnav_1">My Account</a></li>

<li <?=(isset($sel) && $sel=='my_courses')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/course');?>" class="mnav_1">My Courses</a></li>

<li <?=(isset($sel) && $sel=='my_students')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/mystudent'); ?>" class="mnav_1">My Students</a></li>

<li <?=(isset($sel) && $sel=='my_exams')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/exam');?>" class="mnav_1">Exams / Grades</a></li>

<li <?=(isset($sel) && $sel=='msg_center')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/message');?>" class="mnav_1">Message Center</a></li>

<li <?=(isset($sel) && $sel=='notice_wall')?'class="active"':'' ?>><a href="<?php echo site_url('instructor/noticewall');?>" class="mnav_1">Notice Wall</a></li>

</ul>

<div class="clear"></div>

</div>
