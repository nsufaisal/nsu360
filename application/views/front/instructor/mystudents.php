<link href="<?PHP echo base_url()?>css/o_table.css" rel="stylesheet" type="text/css" />

<style>
    .black_overlay
    {
        display: none;
        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }

    #fancybox-loading
    {
        left: 45% !important;
        top: 15% !important;
    }
/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}

#select_course{
	margin-top:40px;
}
.select_region
{
	width:82%;
	margin:0 auto;
	padding:10px;
	background: #EBEBEB;
}

</style>
<div id="page_body">

<div class="center">

<h1 class="">My Students</h1>

<div class="right top_link_dv">
  
</div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">

<div class="tran_history_top">
<form action="#" method="get">


<div class="select_box4">
<p id="drop11">Course</p>
<select name="" onchange="this.form.submit();" class="dropdown">
<option>Age</option>
</select>
</div>

<div class="select_box4">
<p id="drop12">Semester</p>
<select name="" onchange="this.form.submit();" class="dropdown">
<option>Age</option>
</select>
</div>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Students..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>	<!-- end right area -->


</form>
<div class="clear"></div>
</div>
<!-- end top part -->
<style>
	.activate_now{
		color:#262626;
		background-color:#c4ac25;
		font-size:.8em;
		border:1px solid #333333;
		padding:3px;
	}
	
	.activate_now:hover{
		color:#408140;
		background-color:#D0E9C6;
		cursor:pointer;
	}
	td a {
		text-decoration:none;
	}
</style>

<div id="home_cont">

<table class="oTable" id="trxTable">
<?php if(sizeof($my_students)>0){ ?>
  <thead>
    <tr>
    	<th>Student ID</th>
    	<th>Name</th>
    	<th>Phone / Mobile</th>
    	<th>Email</th>
        <!--<th class="txtRight">Amount</th> -->
        <th>Major</th>
        <th>Course</th>
        <th>Status</th>
        <th></th>
  </tr>
  </thead>
<?php  } ?>
  <tbody>
  	
  <?php
  
  	foreach($my_students AS $student){


  		if($student['approval']==1){
  ?>
      <tr class="">  	
		<td class="nowrap"><?php echo $student['student_id'];?></td>
  		<td><?php echo $student['first_name'].' '.$student['last_name'];?></td>
  		<td><?php echo $student['phone'];?></td>
  		<td><?php echo $student['email'];?></td>
    	<td><?php echo $student['major'];?></td>
    	<td><?php echo $student['course'].'('.$student['section_no'].')';?></td>
    	<td>Verified</td>
    	<td>
    		<a href="<?php echo site_url('instructor/mystudent/remove/'.$student['enrole_id']);?>" class="fancybox"><img src="<?php echo base_url('images/DeleteRed.png');?>" height="18" alt="delete" /></a>
	   		&nbsp;<a href="<?php echo site_url('instructor/mystudent/view/'.$student['member_id']);?>" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    	</td>
    </tr>					
			
  <?php 
		} 
		else{
  ?>
  	  	
  	<tr class="oPending">  	
		<td class="nowrap"><?php echo $student['student_id'];?></td>
  		<td><?php echo $student['first_name'].' '.$student['last_name'];?></td>
  		<td><?php echo $student['phone'];?></td>
  		<td><?php echo $student['email'];?></td>
    	<td><?php echo $student['major'];?></td>
    	<td><?php echo $student['course'].'('.$student['section_no'].')';?></td>
    	<td class="approve_bttn"><a href="<?php echo site_url('instructor/mystudent/ajax_approve_student_enrole/'.$student['enrole_id']);?>" ><span class="activate_now">Activate Now</span></a></td>
    	<td>
    		<a href="javascript:void(0);" onClick="openForm('<?php echo site_url('instructor/mystudent/remove/'.$student['enrole_id']);?>');" ><img src="<?php echo base_url('images/DeleteRed.png');?>" height="18" alt="delete" /></a>
	   		&nbsp;<a href="<?php echo site_url('instructor/mystudent/view/'.$student['member_id']);?>" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    	</td>
    </tr>
  			
  <?php	}
	} ?>
       
  </tbody>
</table>

<div class="clear"></div>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->

<script>
	$(document).ready(function(){  
	$(".approve_bttn").click(function(){
		var li=$(this).find('a').attr("href");
		if (typeof li  !== "undefined")
		{
			$.ajax({
			        type: "POST",
			        url: li,
			        data: {'type': 1}, 
			        success: function (data) {
			        	
			        },
			    });
			$(this).html('<span style="color:green">Verified</span>');
		}
		return false;
	});
	
	$("body").on({
    ajaxStart: function() { 
        $(this).addClass("loading"); 
    },
    ajaxStop: function() { 
        $(this).removeClass("loading"); 
    }    
});
	});
</script>