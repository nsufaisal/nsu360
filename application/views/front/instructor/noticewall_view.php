<div id="page_body">

<div class="center">

<h1 class=""> Notice Post View</h1>



<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>


<!-- end message center tab -->

<div class="tabs_contents">

<div class="edit_form">
	<style>
		.notice_view{
			width:80%;
			margin:0 auto;
			padding: 15px;
		}
		#notice_head h1 {
			font-size: 34px;
			font-family: 'Helvetica Neue',Arial,sans-serif;
			font-weight: bold;
			letter-spacing: 0px;
			line-height: 1em;
			color: #1b1e1f;
		}
		
		#notice_head .course_name{
			
			background:#006666;
			color:#ededed;
			padding:4px;
			font-size:.9em;
			float:right;
			font-weight:600;
		}

		#notice_author_date{
			margin-top:10px;
			font-size: 14px;
			padding: 1px 3px;
			background: #ced4d6;
			color: #616161;
			float: left;
			font-style: italic;
			
		}
		#notice_main{
			margin-top:20px;						
		}
		#notice_main p{
			font-size:1.2em;
		}
		
	</style>
	<div class="notice_view">
		<div id="notice_head">
			<h1><?php echo $notice_post['title'];?></h1>
			<?php if(count($notice_courses)>0):?>
			<span class="course_name">
				<?php
					$bar='';
					foreach ($notice_courses as $course){
						echo $bar.$course['initial'].'('.$course['section_no'].')';
						$bar=' / ';
					} ?>
				</span>
				<?php endif;?>
			<div id="notice_author_date">
				<p>Last Updated: <?php echo date('l jS \of F Y | h:i:s A',$notice_post['post_date']); ?></p>
			</div>
			<div class="clear"></div>
			<div id="notice_main">
				<?php echo $notice_post['notice_body'];?>
			</div>
		</div>
	</div>

</div> <!-- end right -->

<div class="clear"></div>




<div class="clear"></div>



<div align="center" style="padding:10px 0 0;">


<input name="edit" type="button" onclick=" window.location.href ='<?=site_url('instructor/noticewall/edit/'.$notice_post['noticewall_id'] )?> '" id="edit" class="btn" value="Edit" />&nbsp;

<input name="delete" onClick="openForm('<?=site_url('instructor/noticewall/remove/'.$notice_post['noticewall_id']);?>');" type="button" id="delete" class="btn" value="Delete" />&nbsp;

<input name="back" id="back"  onclick=" window.location .href ='<?=site_url('instructor/noticewall' )?> '"  type="button" class="btn" value="Back" />

</div>

</div>
<!-- end ac div 2 -->


</form>

</div>

<div class="clear"></div>



</div>

</div>