<!--popup -->
<div class="popup">
    <!--header -->
    <div id="popup-header">
        <h1 class="popup-logo"><a href="<?= base_url(); ?>"><img height="59" width="188" alt=" " src="<?= base_url(); ?>images/pp_logo.gif" /></a></h1>
        <a href="javascript:void(0);" onclick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content">
        <h2 class="center-text">Remove a Student</h2>
        <p class="center-text" style="font-size:14px;"><?php echo $content;?></p>
        <!--submit buttons -->
        <div class="submit-buttons">
            <a href="<?php echo site_url('instructor/mystudent/remove_confirmation/'.$enrole_id);?>" class="grey-button">Confirm</a> 
            <a href="javascript:void(0);" onclick="closePP();" class="grey-button">Cancel</a>
        </div>
        <!--/submit buttons -->

    </div>
    <!--/popup content -->
</div>
<!--/popup -->