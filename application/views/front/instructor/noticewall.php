<div id="page_body">

<div class="center">

<h1 class="">My Notice Wall</h1>
<div class="right top_link_dv">

<a href="<?=site_url('instructor/noticewall/edit')?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Post New Notice</span></a>
      
</div>

<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>

<!-- end account tabs -->

<div class="tabs_contents">

<div class="tran_history_top">
<div class="clear"></div>
<!-- some thing -->


<div class="filter-list">
      <label><strong>Semester:</strong> </label>

<select name="semester" onchange="page_search_submit()" class="input_field" >
<option value="">Select</option>
<?php
	if(isset($semesters))
	{
		foreach($semesters as $semester){ ?>		
		<option <?php echo ($semester['is_current']=='1')? 'selected':''; ?> value="<?php echo $semester['semester_id'];?>"><?php echo $semester['title'];?></option>
	
	<?php		
		}
	}
	
?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Notice..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="clear"></div>
</div>
<!-- end top part -->

<div id="home_cont">
<div class="ymp_table">

<table id="myTable" width="100%" border="0" cellspacing="0" cellpadding="0">
   <thead>
  <tr>
    <th class="rtn-thead" scope="col" width="10%">sl# <a href="javascript:void(0);"><img src="<?php echo base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th class="rtn-thead" scope="col" width="15%">Title <a href="javascript:void(0);"><img src="<?php echo base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th class="rtn-thead" scope="col" width="35%">Notice<a href="javascript:void(0);"><img src="<?php echo base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
    <th class="rtn-thead" scope="col" width="20%">Publish Time <a href="javascript:void(0);"><img src="<?php echo base_url();?>images/x_table_link.png" width="12" height="10" alt=" " /></a></th>
	<th class="rtn-thead" scope="col" width="15%">Actions</th>
    
  </tr>
   </thead>
  <tbody>
<?php
$sl_no=1;
function getExcerpt($str , $startPos=0 , $maxLength=100 ) {
        if(strlen ($str ) > $maxLength) {
               $excerpt   = substr ($str , $startPos , $maxLength -3);
               $lastSpace = strrpos ($excerpt , ' ') ;
               $excerpt   = substr ($excerpt , 0 , $lastSpace );
               $excerpt  .= '...';
        } else {
               $excerpt = $str ;
        }       
        return $excerpt;
}

foreach ($my_notices as $notice): ?>
<tr>  
  	<td ><?php echo $sl_no;?></td>    	
    <td ><?php echo getExcerpt(strip_tags($notice['title']),0,30);?></td>  
    <td ><?php echo getExcerpt(strip_tags($notice['notice_body']),0,60);?></td>
    <td ><?php echo date('Y-m-d / g:i a',$notice['post_date']);?></td>
    <td>
    	<a href="<?php echo site_url('instructor/noticewall/view/'.$notice['noticewall_id']); ?>"><img src="<?=base_url();?>images/x_tbl_action.gif" width="21" height="21" alt=" " /></a>&nbsp;&nbsp;
    	<a href="<?=site_url('instructor/noticewall/edit/'.$notice['noticewall_id'])?>"><img src="<?=base_url();?>images/tabel-edit-icon.png" width="22" height="22" alt=" " /></a>&nbsp;
      	<a href="javascript:void(0);" onClick="openForm('<?=site_url('instructor/noticewall/remove/'.$notice['noticewall_id']);?>');"><img src="<?=base_url();?>images/x_dlt_icon.png" width="22" height="22" alt=" " /></a>
    <td>

</tr >

<?php 
$sl_no++;
endforeach; ?>

</tbody>

</table>

</div>
 <script>
	$(function(){
		$('#myTable').tablesorter({sortList: [[0,0]], locale: 'de', widgets: ['zebra'],
			 headers: { 6: { sorter: false} },
			  useUI: true});
	});
	    $(".rtn-thead").click(function () {   
        $(this).css('color','#EE178C')
        .siblings()
        .css('color','#4F4F4F');      
    });
</script>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
