<link href="<?PHP echo base_url()?>css/o_table.css" rel="stylesheet" type="text/css" />


<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> My Courses<sup style="color:green; font-size:.5em;font-style: italic;" title="Instructor"> instructor</sup></h1>

<div class="right top_link_dv">
	
<?php if(isset($advising_approval) && $advising_approval){ ?>

<a href="<?php echo site_url('instructor/course/add'); ?>" class="top_linkt"><span><img src="<?=base_url()?>images/add-icon.gif"  width="14" height="18" alt=" " />Add / Remove</span></a>
 
<?php } ?> 
     
</div>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/instructor/account_tabs');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac" style="min-height:330px;">

<div class="tran_history_top">
<div class="clear"></div>
<!-- some thing -->


<div class="filter-list">
      <label><strong>Semester:</strong> </label>

<select name="semester" style="width: 144px;" onchange="page_search_submit()" class="input_field" >
<option value="">Select</option>
<?php
	if(isset($semesters))
	{
		foreach($semesters as $semester){ ?>		
		<option <?php echo ($semester['is_current']=='1')? 'selected':''; ?> value="<?php echo $semester['semester_id'];?>"><?php echo $semester['title'];?></option>
	
	<?php		
		}
	}
	
?>
</select>

<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Course..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="clear"></div>
</div>

<div id="home_cont">
<table class="oTable" id="trxTable">
<?php if(sizeof($my_courses)>0){ ?>
  <thead>
    <tr>
    	<th>Course Initial</th>
    	<th>Course Title</th>
        <th>Class Time</th>
        <!--<th class="txtRight">Amount</th> -->
        <th>Class Room</th>
        <th></th>
  </tr>
  </thead>
<?php  } ?>
  <tbody>
  	
  <?php
  
  	foreach($my_courses AS $my_course){

	$instructor='TBA';
	
	if(isset($my_course['instructor_initial']) && $my_course['instructor_initial'] !='')
	{
		$instructor='<a href="'.site_url().'instructors/profile/'.$my_course['instructor_id'].'">'.$my_course['instructor_initial'].'</a>';
	}

  ?>
      <tr class="">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td><?php echo $my_course['course_title'];?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		
    	</td>
    </tr>				
			

  			
  <?php	} ?>
       
  </tbody>
</table>

<div class="clear"></div>

</div>

                <!-- end bottom form -->

</div>	<!-- end tabs contents -->

</div>

</div>
</div>


<!--instance the validator engine ::rtn:: -->	
