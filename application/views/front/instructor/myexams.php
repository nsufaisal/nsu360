<link href="<?PHP echo base_url()?>css/o_table.css" rel="stylesheet" type="text/css" />

<div id="page_body">

    <div class="center">

      <h1> Exams</h1>

<div class="right top_link_dv">
<a class="top_linkt right" href="<?=site_url('instructor/exam/add')?>"><span><img src="<?=base_url();?>images/add-icon.gif" width="16" height="17" alt="" /> New Exam</span></a>
</div>
	<!-- acc_tabs loading-->	
	
	<?php $this->load->view('front/instructor/account_tabs'); $selected_class=0;?>

      <!-- end account tabs -->

      <div class="tabs_contents acdivs">

      <form method="post" action=<?php echo ($selected_class==0)? '':site_url('sadmin/myexams/'.$selected_class);?>>

      <div class="filter-list">

<label><strong>Semester: </strong> </label>
<select name="semester" style="width: 144px;" onchange="page_search_submit()" class="input_field" >
<option value="">Select</option>
<?php
	if(isset($semesters))
	{
		foreach($semesters as $semester){ ?>		
		<option <?php echo ($semester['is_current']=='1')? 'selected':''; ?> value="<?php echo $semester['semester_id'];?>"><?php echo $semester['title'];?></option>
	
	<?php		
		}
	}
	
?>
</select>  
      </div>

      </form>

        <!--tutorial tabel -->

<div id="home_cont">
	
<table class="oTable" id="trxTable">
  <thead>
  <tr>
  	<th class="rtn-thead">Exam Title</th>
    <th class="rtn-thead">Course</th>
    <th class="rtn-thead">Marks</th>
    <th class="rtn-thead">Date</th>
    <th class="rtn-thead">Venue</th>  
    <th class="rtn-thead"></th>
  </tr>
  </thead>
  <tbody>
 
 <?php if(sizeof($exams)>0){ ?> 	
 
  <?php foreach($exams as $exam):?>

  <tr>  
    <td><?php echo $exam['title']; ?></td>
    <td><?php echo $exam['course'].'('.$exam['section_no'].')'; ?></td>
    <td><?php echo $exam['exam_mark'].' <span style="font-size:.7em;">('.$exam['weight'].'%)</span>'; ?></td>
    <td><?php echo date('Y-m-d / g:i a',$exam['exam_date']);?></td>
    <td><?php echo $exam['venue']; ?></td>
    <td><a href="#">
    	<img src="<?=base_url();?>images/tutorial-action1.gif" width="21" height="21" alt="" /></a>&nbsp; 
    	<a href="#">
    	<img src="<?=base_url();?>images/edit-icon.gif" width="22" height="21" alt="" /></a> &nbsp;
    	<a href="javascript:void(0);" onClick="openForm('<?=site_url('sadmin/myexams/remove/'.$exam['exam_id']);?>');">
   		<img src="<?=base_url();?>images/DeleteRed.png" width="22" height="20" alt="" /></a>
   </td>
  </tr>
  <?php endforeach;?>		
			

  			
  <?php	} ?>
       
  </tbody>
</table>
</div>

        <!--/tutorial tabel -->

      </div>

      <!-- end tabs contents --> 

    </div>

  </div>

    <script>
	$(function(){
		$('#trxTable').tablesorter({sortList: [[0,0]], locale: 'de', widgets: ['zebra'],
			 headers: { 6: { sorter: false} },
			  useUI: true});
	});
	    $(".rtn-thead").click(function () {   
        $(this).css('color','#f08036')
        .siblings()
        .css('color','#4F4F4F');      
    });
</script>