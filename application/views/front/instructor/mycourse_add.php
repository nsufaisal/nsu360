<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>js/multiselect/chosen.css"/>
<script src="<?php echo site_url('js/multiselect/chosen.jquery.min.js') ?>"></script>

<style>

html {
font-family: sans-serif;
}

body {
font-family: sans-serif;
font-size: 13px;
line-height: 1.4;
color: #252523;
}

table {
	border-collapse: separate;
	border-spacing: 2px;
	border-color: gray;
}

td, th {
display: table-cell;
vertical-align: inherit;
}

.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}

tr.oPending td {
background-color: #f7f7f6;
}


.oPending {
font-style: italic;
}


.oMute, .oPending, .oFormHelp {
color: #7c7c7a;
}
.oTable, .oReportTable {
border-collapse: separate;
border-spacing: 0;
}


.oTable tbody tr:first-child td:first-child, .oReportTable tbody tr:first-child td:first-child {
-moz-border-radius-topleft: 7px;
-webkit-border-top-left-radius: 7px;
border-top-left-radius: 7px;
}

.oTable tbody tr:first-child td, .oReportTable tbody tr:first-child td {
border-top-width: 3px;
}

oTable td, .oReportTable td {
border: 1px solid #ededeb;
border-top-width: 0;
border-left-width: 0;
border-right-width: 0;
padding: 8px 11px 7px;

}

.oTable th, .oReportTable th {
color: #252523;
padding: 6px 15px 6px 11px;
font-size: 12px;
font-weight: 700;
}
.oTable td:first-child, .oReportTable td:first-child {
border-left-width: 3px;
}

.oTable td:last-child, .oReportTable td:last-child {
border-right-width: 3px;
}

.oTable tbody tr:last-child {
-moz-border-radius-topright: 7px;
-webkit-border-top-right-radius: 7px;
border-top-right-radius: 7px;
}

.oTable td, .oReportTable td {
	border: 1px solid #ededeb;
	border-top-width: 0;
	border-left-width: 0;
	border-right-width: 0;
	padding: 8px 11px 7px;
}

.nowrap {
white-space: nowrap;
}

.txtRight, table .txtRight td, table .txtRight th, .data .txtRight th, .data .txtRight td, table thead th.txtRight, table tbody td.txtRight {
text-align: right!important;
}

#home_cont{
	padding-top:20px;
}
#home_cont table{
	margin:0 auto;
		
}

.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url("<?php echo base_url('images/ajax-loader2.gif');?>") 
                50% 50% 
                no-repeat;
}
/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}

#select_course{
	margin-top:40px;
}
.select_region
{
	width:82%;
	margin:0 auto;
	padding:10px;
	background: #EBEBEB;
}

</style>

<div id="page_body">

<div class="center ymp_my_acnt">

<h1 class="left"> Add New Course(s)<sup style="color:green; font-size:.5em;font-style: italic;" title="Instructor"> instructor</sup></h1>

<div class="clear"></div>

        <!-- acc_tabs loading-->
<?php $this->load->view('front/instructor/account_tabs');?>

<div class="tabs_contents">

<div class="edit_form form_ymp_mac" style="min-height:330px;">

<div class="tran_history_top">
	
<div class="clear"></div>
<!-- some thing -->


<div class="clear"></div>
</div>

<div id="home_cont">
	
<table class="oTable" id="trxTable">
<?php if(sizeof($my_courses)>0){ ?>
  <thead>
    <tr>
    	<th>Course Initial</th>
    	<th>Course Title</th>
        <th>Class Time</th>
        <!--<th class="txtRight">Amount</th> -->
        <th>Class Room</th>
        <th></th>
  </tr>
  </thead>
<?php  } ?>
  <tbody>
  	
  <?php
  	foreach($my_courses AS $my_course){ ?>

 	  	
  	<tr class="">  	
		<td class="nowrap"><?php echo $my_course['course_initial'].'('.$my_course['section_no'].')';?></td>
  		<td><?php echo $my_course['course_title'];?></td>
        <td><?php echo $my_course['class_day'].' '. $my_course['class_time'];?></td>
    	<td><?php echo $my_course['room_no'];?></td>
    	<td>
    		<a href="javascript:void(0);" ><img src="<?php echo base_url('images/file-icon.png');?>" height="18" alt="view"></a>&nbsp;
    		<a href="javascript:void(0);" onClick="openForm('<?php echo site_url('instructor/course/remove/'.$my_course['id']);?>');"><img src="<?php echo base_url('images/DeleteRed.png');?>" height="18" alt="delete" /></a>
    	</td>
    </tr>
  			
  <?php	}  ?>
       
  </tbody>
</table>

<div class="clear"></div>

<div id="select_course">

<div class="select_region">

<form name="add_course_form" method="post" id="pageForm" action="" >

<strong>Select Course</strong>

<select name="semester_course_id[]" multiple="multiple" id="course_list" style="width:550px;" class="input_field validate[required]">

<option value=""></option>

<?php foreach ($available_courses as $course): ?>
	
<?php 
	$instructor=' [TBA] ';
	
	if(isset($course['instructor_initial']) && $course['instructor_initial'] !='')
	{
		$instructor=' ['.$course['instructor_initial'].'] ';
	}
?>

<option value="<?php echo $course['id'] ?>"><?php echo $course['course_initial'].'('.$course['section_no'].')'.$instructor.' / '.$course['class_day'].' '.$course['class_time'].' ['.$course['course_title'].']'; ?></option>

<?php endforeach; ?>	

</select>

<input type="button" name="submit" id="submit_btn" style="margin-left: 10px;" class="btn " value="Add" />
	 
</form>		


</div>

</div>
</div>


</div>	<!-- end tabs contents -->

</div>

</div>

</div>

<div class="modal"><!-- Place at bottom of page --></div>
<script>		
	$(document).ready(function(){          
      
       $("#course_list").chosen();
       
       $("#submit_btn").click(function(){
       		
       		var course_value=$('#course_list').val();	
       		
       		if(course_value<1)
       		{
       			$(".select_region").effect( "pulsate", {times:2}, 200 );
       			
       		}
       		else
       		{
       			$.ajax({
			        type: "POST",
			        async:false,
			        url: "<?php echo site_url('instructor/course/ajax_add_course'); ?>",
			        data: {'new_courses': course_value}, 
			        success: function (data) {
			        	$("#home_cont").html(data);
			        },
			    });
      		}      	

       	return false;
       });
            
 	});

$("body").on({
    ajaxStart: function() { 
        $(this).addClass("loading"); 
    },
    ajaxStop: function() { 
        $(this).removeClass("loading"); 
    }    
});
</script>