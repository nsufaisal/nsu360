<script type="text/javascript" src="<?=base_url()?>js/tinymce/tinymce.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>js/multiselect/chosen.css"/>
<script src="<?php echo site_url('js/multiselect/chosen.jquery.min.js') ?>"></script>
<div id="page_body">

<div class="center">

<h1 class=""> 
<?php if($add_edit=='add')
     	echo 'Post New Notice';
	else {
			echo 'Edit Notice Post';
}?>
</h1>



<!-- acc_tabs loading-->

<?php $this->load->view('front/instructor/account_tabs');?>


<!-- end message center tab -->
<div class="tabs_contents">
	<?php $link='';
	if(isset($my_notice['noticewall_id'])){
		$link='/'.$my_notice['noticewall_id'];
	}?>
<form action="<?=site_url('instructor/noticewall/post'.$link)?>" name="pageFrom" id="pageForm" method="post">

<?=validation_errors()?>

<div class="edit_form">
<div class="left">

<p>

<strong>Title:</strong>

<input name="title" id="title" type="text" class="input_field validate[required]" value="<?php echo isset($my_notice['noticewall_id'])?$my_notice['title']:'';?>" />

</p>
<div id="err_receipient"></div>
<p>

<strong>Course(s):</strong>

<!-- start -->
<?php if(isset($my_notice['noticewall_id']))
	$mycourses = explode(';', $my_notice['semester_course_ids']);?>
<select name="courses[]" id="courses" multiple="multiple" class="input_field validate[required]">
    <!--	<option value="">Select</option>  -->
    <option value="public_wall" <?php if(isset($my_notice['noticewall_id']))
    									if($my_notice['is_public_post']==1)
											echo 'selected="selected"';?>>Post on Public Wall</option>
    <?php foreach ($my_courses as $course): ?>
        <option <?php echo (isset($my_notice['noticewall_id']) && in_array($course['id'],$mycourses))?'selected="selected"':'';?> value="<?php echo $course['id'] ?>"><?php echo $course['initial'].'('.$course['section_no'].')'; ?></option>

    <?php endforeach; ?>	
    ?>

</select>
                                
<!-- end -->
</p>

</div> <!-- end class left -->
<div class="right">

<p>

<strong>Notice Type:</strong>

<select name="notice_type" id="notice_type" class="input_field validate[required]">
	<option value="">Select</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==1)?'selected="selected"':'';?> value="1">Class Cancel</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==2)?'selected="selected"':'';?> value="2" >Exam/Assignment</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==3)?'selected="selected"':'';?> value="3">Grade Publish</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==4)?'selected="selected"':'';?> value="4">Make-up Class</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==5)?'selected="selected"':'';?> value="5">Make-up Exam</option>
	<option <?php echo (isset($my_notice['noticewall_id']) && $my_notice['notice_type']==6)?'selected="selected"':'';?> value="6">Other</option>
</select>
</p>
<div id="err_receipient"></div>

</div> <!-- end right -->

<div class="clear"></div>


<p>

<strong>Notice Body:</strong>

<textarea name="notice_body" id="notice_body" cols="" rows="" class="input_field validate[required]" style="height:95px;">
	<?php echo isset($my_notice['noticewall_id'])?$my_notice['notice_body']:'';?>
</textarea>

</p>


<div class="clear"></div>



<div align="center" style="padding:10px 0 0;">

<input name="submit" type="submit" id="submit" class="btn" value="Post" />&nbsp;

<input name="cancel" id="cancel"  onclick=" window.location .href ='<?=site_url('instructor/noticewall' )?> '"  type="button" class="btn" value="Cancel" />

</div>

</div>
<!-- end ac div 2 -->


</form>

</div>

<div class="clear"></div>



</div>

</div>

<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});
            
 	});
$("#courses").chosen({'max_selected_options': 20});
</script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ],
});

  $("#submit").click(function() {
    var msg = tinyMCE.get('notice_body').getContent();
    if(msg=='')
    {
    	/* alert("Notice Body cannot be left empty!"); */
    	openForm('<?=site_url('instructor/noticewall/empty_msg');?>');
    	return false;
    }
  });
</script>