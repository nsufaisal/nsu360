<div id="page_body">

<div class="center">

<h1 class="">Notice Wall</h1>

<div class="tabs_contents">

<div class="tran_history_top">
<div class="clear"></div>
<!-- some thing -->


<div class="filter-list">
 
<div class="advmodel_search">
<input name="" type="text" class="input_field" value="" placeholder="Search Notice..."/>
<input name="" type="image" src="<?php echo base_url();?>images/srch_btn.png" />
</div>
</div>

<div class="clear"></div>
</div>
<!-- end top part -->

<div id="home_cont">
<style>
	.notice_row{
		border-top:2px solid #d3e0e6; 
		border-radius:10px;
		margin:3px;
		padding:4px;
	}
	.notice_row a{
		text-decoration:none;
	}
	
	.notice_row:hover{

		background:#ffffff;
	}
	.notice_head{
		
	} 
	
	.notice_head .post_date{
		font-style:italic;
		font-size:.9em;
		color:#1873A5;
		float:right;
	} 
	
	.notice_head .notice_title{
		color:#5c5c59;
		font-size:1.15em;
		font-weight:bold;
	}
	
	.notice_main{
		color:#656558;
	}
	
	
</style>
<?php
$sl_no=1;
function getExcerpt($str , $startPos=0 , $maxLength=100 ) {
        if(strlen ($str ) > $maxLength) {
               $excerpt   = substr ($str , $startPos , $maxLength -3);
               $lastSpace = strrpos ($excerpt , ' ') ;
               $excerpt   = substr ($excerpt , 0 , $lastSpace );
               $excerpt  .= '...';
        } else {
               $excerpt = $str ;
        }       
        return $excerpt;
}

foreach ($public_notices as $notice): ?>

<div class="notice_row">
	<a href="<?php echo site_url('noticewall/view/'.$notice['noticewall_id']);?>">
	<div class="notice_head">
		<span class="post_date"><?php echo date('Y-m-d / g:i a',$notice['post_date']);?></span>
		<div class="notice_title"><?php echo getExcerpt(strip_tags($notice['title']),0,120);?></div>
	</div>
	<div class="notice_main">
		<?php echo getExcerpt(strip_tags($notice['notice_body']),0,330);?>
	</div>
	</a>
</div>

<?php endforeach; ?>

</div>

<div class="clear"></div>
</div>	<!-- end tabs contents -->



</div>

</div>	<!-- end page body -->
