<style>
	.activ_error{
		
		background-image: linear-gradient(to bottom,#f2dede 0,#e7c3c3 100%);
		background-repeat: repeat-x;
		border-color: #dca7a7;
		text-shadow: 0 1px 0 rgba(255,255,255,0.2);
		-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25),0 1px 2px rgba(0,0,0,0.05);
		box-shadow: inset 0 1px 0 rgba(255,255,255,0.25),0 1px 2px rgba(0,0,0,0.05);
		color: #b94a48;
		background-color: #f2dede;
		padding: 15px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		font-size: 14px;
		line-height: 1.428571429;		
	}
</style>

<div id="page_body">
<div class="center">
<h1>Account Activation</h1>
<?php echo $content;?>
<form action="<?php echo site_url('login/activation_confirm'); ?>" name="pageForm" id="pageForm" method="post" accept-charset="utf-8">
<div class="support_form">
<?php echo validation_errors();?>
<p>
	
<?php if(isset($error_msg)):?>

	<div class="activ_error"><b>Oh Snap!</b> Wrong activation key or email provided! Please check the email you received after registration.</div>
<?php endif;?>
</p>
<p>
<strong>Email:</strong>
<input name="activ_username" id="activ_username" type="text" class="input_field validate[required,custom[email]]" />
</p>

<p>
<strong>Activation Key:</strong>
<input name="activation_key" id="activation_key" type="text" class="input_field validate[required]" />
</p>


<div align="center" style="padding-top:15px;">
<input name="submit" id="submit" type="submit" class="btn" value="Activate" />&nbsp;
<input name="" onclick=" window.location .href ='<?php echo site_url ('home'); ?>'" type="button" class="btn" value="Cancel" />

</div>

</div>
</form>

</div>
</div>

<!--instance the validator engine ::rtn:: -->    
<script>              
   $("#pageForm").validationEngine( 'attach', {promptPosition : "topRight", scroll: false},
   {focusFirstField : true });              
</script>
