    <h1>Not a Member? Join Now for FREE	</h1>
    
   <?=$content;?>
    <form action="<?= site_url('register/check')?>" method="post" id="pageForm" accept-charset="utf-8" >
      <div class="edit_form" style="padding-top: 15px">
        <div class="left" style="background: url(images/devider.gif) repeat-y right; border: 0; width: 453px">
          <p> <strong>User Name:</strong>
            <input type="text" name="username" id="username" class="input_field validate[required]" value="<?= set_value('username') ?>" style="width: 360px; margin-right: 5px" /><button id="username_check" name="username_check" class="btn" style="min-width: 61px;">Check</button> <br />
            	<span id="rtn-username">
	            <?php             
		            if(form_error('username')){ echo form_error('username');}				
					else{ echo 'Click "Check" to see if username is available.';	}
				?>
				</span>
           
          </p>
          <p> <strong>Password:</strong>
            <input type="password" name="password" id="password" class="input_field validate[required,minSize[6]]" value="<?= set_value('password') ?>"/>
            <?php             
	            if(form_error('password')){ echo form_error('password');}				
				else{ echo '<span id="rtn-password">At least six characters required.</span>';	}
			?>
            
          </p>
          <p> <strong>Confirm Password:</strong>
            <input type="password" name="conf_password" id="conf_password" class="input_field validate[required,equals[password]]" value="<?= set_value('conf_password') ?>"/>
            <?= form_error('conf_password') ?>
          </p>
          <p> <strong>Email Address:</strong>            
            <input type="text" name="email" id="email" class="input_field validate[required,custom[email]]" value="<?= set_value('email') ?>"/>
            <span id="rtn-email">
	            <?php             
		            if(form_error('email')){ echo form_error('email');}				
					else{ echo 'Use a valid email address as we need to email you to confirm your membership.</span>';	}
				?>
			</span>
            
          </p>
         
         <p style="padding-top: 10px"><input id="agreed_terms" name="agreed_terms" class="validate[required]" type="checkbox" value="" /> &nbsp; <strong style="display: inline-block;">I have read and agree to the</strong> <a href="<?=site_url('terms')?>" style="color:#0090F0;">Terms of Service</a></p>
        </div>
        <!-- end left container -->
        
        <div class="right">
        <p>Answer these security questions to help retrieve your username/password if lost:</p>
        <br/>
         <p>
         	<strong>1. <?=$security_qs[0]['question']?></strong>
			<input name="security_ans1" id="security_ans1" type="text" class="input_field validate[required]" value="" placeholder="your answer..."/>
			
			</p>
			<br/>
			<p>
         	<strong>2. <?=$security_qs[1]['question']?></strong>
			<input name="security_ans2" id="security_ans2" type="text" class="input_field validate[required]" value="" placeholder="your answer..."/>
			
			</p>
			<br/>
			<p>
         	<strong>3. <?=$security_qs[2]['question']?></strong>
			<input name="security_ans3" id="security_ans3" type="text" class="input_field validate[required]" value="" placeholder="your answer..."/>
			
			</p>       
          
        </div>
        <!-- end right container -->
        
        <div class="clear"></div>
        <div align="center" style="padding-top:15px;">
          <input type="submit" value="Register" class="btn" name="" />
          &nbsp;
          <input type="button" value="Cancel" class="btn cancel_btn" name="cancel_btn" />
        </div>
      </div>
    </form>
    
		

<!--instance the validator engine ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});             
 	});
</script>


<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username_check").click(function(){  
       		var username = $('#username').val();
   		if(username=="")
   		{
   			alert("You have not entered any username");
   		}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?= site_url('login/ajx_username_available')?>",
	       	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else{
	      		$("#rtn-username").html('<span style="color:red"><b>Sorry, <span style="color:blue;">'+username+'</span>  is not available, please try another username</b></span>');
	      		$('#username').val('');
	      	}			      
		    }
			});
   		}  		
       		
       		return false;
       });
       
        $("#email").blur(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('Use a valid email address as we need to email you to confirm your membership.');
			      	else
			      	{
			      		$("#rtn-email").html('<span style="color:red"><b>Sorry, <span style="color:blue;">'+ email+ '</span>  is already used</b></span>');
			      		$("#email").val('');
			      	}
			      		 				      					      				      			      
			     }
				});
    		return false;
       });             
 	});
</script>

