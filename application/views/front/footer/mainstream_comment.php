
<!--popup -->
<div class="popup">
    <!--header -->
    <div id="popup-header">
        <h1 class="popup-logo"><a href="#"><img height="59" width="188" alt=" " src="<?= base_url(); ?>images/pp_logo.gif" /></a></h1>
        <a href="#" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content popup-form">
        <h2>Comment</h2>
        <form action="#" method="post">
            <p style="position:relative;">
                <label>Message:</label>
                <span class="abs_date">22-12-2013</span>
                <textarea name="" cols="" rows="" class="input_field" style="margin: 5px 0 15px 0;"></textarea></p>
            <!--submit buttons -->
            <div class="submit-buttons" style="text-align:right">
                <input name="" type="submit" value="Post" class="btn" style="min-width:75px;" />&nbsp;
                <input name="" type="reset" value="Cancel" class="btn" style="min-width:75px;" />
            </div>

        </form>

    </div>
    <!--/submit buttons -->

</div>
<!--/popup content -->

<!--/popup -->
