<style>
	.loading-indicator{
		color:#737373;
		font-size:.9em;
		text-shadow: 1px;
		
	}
</style>
<h1>Student Sign Up</h1>
<p><?php echo $content; ?></p>

<form action="<?=site_url('footer/student/register')?>" enctype= "multipart/form-data"  method="post" id="pageForm" accept-charset="utf-8" >
<div class="edit_form" style="padding-top: 15px">
<div class="left" style="background: url(images/devider.gif) repeat-y right; border: 0; width: 453px">

<p><strong>Student ID:</strong>
<input type="text" value="" class="input_field validate[required,minSize[9],custom[integer]]" name="student_id" id="student_id" />
</p>
<div id="unique_student_id_msg"></div>

<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p>
<strong>Phone:</strong>
<input type="text" name="phone" id="phone" value="" class="input_field validate[required]" />
</p>

<p style="position: relative;"> <strong>Upload Student Photo:</strong><span class="file-size">Max File Size 320x340</span>
<input type="text"   name="file_name_box_1" onfocus=" javascript: openBrowse1(); " id="file_name_box_1" style="width:345px;" class="input_field" value=" <?=isset( $tutorial['video_file' ])? $tutorial['video_file' ]:''?>">&nbsp;
<input type="button" onclick=" javascript: openBrowse1(); " name="" id ="uploadButton" style="min-width: 70px;" class="btnmy" value="Browse" >
<?PHP if (form_error ('file_name_box_1' )) { ?><div ><?php echo form_error( 'file_name_box_1'); ?> </div><?PHP } ?>
</p>
<div id="err_userfile_1" ></div>
<div class="clear" ></div>
</div>	<!-- end left container -->
        
<div class="right">
<p><strong>Department Name:</strong>
<select name="department_id" id="department_id" class="input_field validate[required]" >
	<option value="">Select</option>
	<?php foreach($departments as $dep) :?>
		<option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option>	
	<?php  endforeach;?>
</select>
</p>

<p>
	
	<strong>Degree Major: <span class="loading-indicator" id="loading-indicator"><img src="<?php echo base_url('images/loading2.gif');?>" /></span></strong>
<select name="degree_major" id="degree_major" class="input_field validate[required]" >
	<option value="0">Select</option>
<?php 
	foreach($degree_majors as $major)
	{		
		echo "<option value=".$major['major_id'].">".$major['name']." (".$major['initial'].")</option>";			
	}
?>
</select>
</p>
   
<p><strong>Email:</strong>
<input type="text" name="email" id="email" value="" class="input_field validate[required,custom[email]]" />
</p>
<div id="unique_email_msg"></div>

<p><strong>Password:</strong>
<input type="password" name="password" id="password" value="" class="input_field validate[required,minSize[6]]" />
</p>

<p><strong>Password Again:</strong>
<input type="password" name="conf_password" id="conf_password" value="" class="input_field validate[required,minSize[6],equals[password]]" />
</p>

<!-- hidden file uploaders -->
<input type="file" name="userfile_1" id="userfile_1" style=" display:none ;"/>

</div>	<!-- end right container -->
<div class="clear"></div>

<div align="center" style="padding-top:20px;">
<input type="submit" value="Register" class="btnmy" name="submit" />&nbsp;
<input type="button" value="Cancel" class="btnmy cancel_btn" name="cancel_btn" />
</div>
</div>
</form>

<!--instance the validator engine ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});             
 	});
</script>

<script>
   function openBrowse1(){
   		document.getElementById("userfile_1" ).click();
  }       
</script>
<!--instance the validator engine ::rtn:: -->    

<script>              
        $(document).ready( function(){ 
      $( "#userfile_1").change(function(){
               var filename= $(this).val();            
               var size = this.files[ 0].size;
               var name= this.files[ 0].name;
               var type= this.files[ 0].type;
               if(size> 1048576){
                     $( "#err_userfile_1").html('<span style="color:red;">*File size exceeds 1MB limit</span><br/><br/>');
                      $( "#file_name_box_1").val('' );
               }
               else if(! (/\.(jpg|jpeg|png|gif)$/i).test(filename))
               { //avi|wmv|mov
                      $("#err_userfile_1").html( '<span style="color:red;">*Only image file accepted (jpg/jpeg/png/gif)</span><br/><br/>');               
                     $( "#file_name_box_1").val('' );
                            
               }
               else{
                     $( "#err_userfile_1").html('' );
                     $( "#file_name_box_1").val(name);
               }
               
               
           });
       });
</script>
<script>
	$(document).ready(function(){
		$("#email").blur(function(){          		
       		var email = $('#email').val(); 
       			if(email=='')
       			$("#unique_email_msg").html('');      			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?= site_url('login/ajx_email_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#unique_email_msg").html('');
			      	else{			      		
			      		 $("#unique_email_msg").html('<span style="color:red">Sorry, </span><span style="color:blue;font-weight:bold">'+email+'</span> <span style="color:red">has already been used by another user</span><br/>');
			      		 $("#email").val('');	
			      	}
			      					      					      				      			      
			     }
				});
    		return false;
       }); 
       
       $("#student_id").blur(function(){          		
       		var student_id = $('#student_id').val(); 
       			if(email=='')
       			$("#unique_sid_msg").html('');      			
       			$.ajax({
	          	type: "post",
	           	data: {'student_id' : student_id},
	           	url: "<?= site_url('footer/student/ajx_student_id_available')?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#unique_student_id_msg").html('');
			      	else{			      		
			      		 $("#unique_student_id_msg").html('<span style="color:red">Sorry, </span><span style="color:blue;font-weight:bold">'+student_id+
			      		 '</span> <span style="color:red">has already been used by another user. </span><a href="<?php echo site_url('user_support/feedback_support');?>"> Complain Now</a><br/>');
			      		 $("#student_id").val('');	
			      	}
			      					      					      				      			      
			     }
				});
    		return false;
       });  
	});
</script>
<script>
	$('.loading-indicator').hide();
	$('#department_id').change(function() {
	$('#loading-indicator').show();
    var department_id=this.value;
	$.ajax({
        type: "POST",
        url: "<?php echo site_url('footer/student/ajx_load_majors'); ?>",
        data: {'department_id': department_id}, 
        success: function (data) {
        	$("select#degree_major").html(data);
        	$('#loading-indicator').hide();
        },
    });
     
    });
	

</script>


