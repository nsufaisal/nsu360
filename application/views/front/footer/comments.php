<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to our site</title>

<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/my_styles.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />
</head>

<body class="popup-wrapper">
<!--popup -->
<div class="popup">
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?=base_url();?>"><img height="59" width="188" alt=" " src="<?=base_url();?>images/pp_logo.gif" /></a></h1>
<a href="javascript:void(0);" onclick="closePP();" class="close-button">Close (X)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content popup-form">
<h2>Comment</h2>
<form action="#" method="post">
<p style="position:relative;">
<label>Message:</label>
<span class="abs_date">22-12-2013</span>
<textarea name="" cols="" rows="" class="input_field" style="margin: 5px 0 15px 0;"></textarea></p>
<!--submit buttons -->
<div class="submit-buttons" style="text-align:right">
<input name="" type="submit" value="Post" class="btn" style="min-width:75px;" />&nbsp;
<input name="" type="reset" value="Cancel" class="btn" style="min-width:75px;" onclick="closePP();"/>
</div>

</form>

</div>
<!--/submit buttons -->

</div>
<!--/popup content -->

<!--/popup -->
</body>
</html>