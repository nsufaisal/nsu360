<h1>Instructor Sign Up</h1>
<?= $signupcontent;?>

<form action="<?=site_url('footer/instructor/register')?>" method="post" id="pageForm" accept-charset="utf-8" >

<div class="edit_form" style="padding-top: 15px">
<div class="left" style="background: url(images/devider.gif) repeat-y right; border: 0; width: 453px">

<p><strong>Preferred Username:</strong>
<input type="text" value="" class="input_field validate[required]" name="username" id="username" />
<div id="rtn-username"></div><p style='font-size: 90%; padding-top: 0; margin-top:0;'>e.g: jamal578 or Your ID</p>
</p>
<?php echo form_error('username'); ?>
<p><strong>First Name:</strong>
<input type="text" name="first_name" id="first_name" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Last Name:</strong>
<input type="text" name="last_name" id="last_name" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>

<p><strong>Instructor Initial:</strong>
<input type="text" name="initial" id="initial" value="" class="input_field validate[required,custom[onlyLetterSp]]" />
</p>





</div>	<!-- end left container -->
        
<div class="right">

<p><strong>Department Name:</strong>
<select name="department_id" id="department_id" class="input_field validate[required]" >
    <option value="">Select</option>
    <?php foreach($departments as $dep) :?>
        <option value="<?php echo $dep['department_id'];?>"><?php echo $dep['name'].'('.$dep['initial'].')';?></option> 
    <?php  endforeach;?>
</select>
</p>

<p><strong>Email:</strong>
<input type="text" value="" class="input_field validate[required,custom[email]]" name="email" id="email" />
<div id="rtn-email"></div>
</p>

<p><strong>Mobile:</strong>
<input type="text" value="" class="input_field validate[required,custom[phone]]" name="phone" id="phone" />
<div id="rtn-phone"></div>
</p>

<p><strong>Password:</strong>
<input type="password" value="" class="input_field validate[required,minSize[6]]" name="password" id="password" />
</p>

<p><strong>Confirm Password:</strong>
<input type="password" value="" class="input_field validate[required,equals[password]]" name="conf_password" id="conf_password" />
</p>

</div>	<!-- end right container -->
<div class="clear"></div>
<div align="center" style="padding-top:20px;">
<input type="submit" value="Register" class="btn" name="submit" />&nbsp;
<input type="button" value="Cancel" class="btn cancel_btn" name="cancel_btn" />
</div>
</div>
</form>

<!--instance the validator engine ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});             
 	});
</script>

<!--check user availability ::rtn:: -->	
<script>		
	$(document).ready(function(){          
       $("#username").blur(function(){  
       		var username = $('#username').val();
   		if(username=="")
   		{
   			// alert("You have not entered any username");
   		$("#rtn-username").html('<span style="color:red"><b>No username given!</b></span>');

   		}else{
			$("#rtn-username").html('<b>Checking... please wait..</b>');
			$.ajax({
           	type: "post",
          	data: {'username' : username},
           	url: "<?php echo  site_url('login/ajx_username_available'); ?>",
           	success: function(result)
	       	{
       	 	if(result){
       	 		 $("#rtn-username").html('<span style="color:green"><b>Username Available</b></span>');
       	 	}
	      	else{
	      		$("#rtn-username").html('<span style="color:red"><b>Sorry, username is not available, please try another</b></span>');
	      		$('#username').val("");
	      	}			      
		    }
			});
   		}  		
       		
       		return false;
       });
       
        $("#email").blur(function(){          		
       		var email = $('#email').val();       			
       			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	url: "<?php echo site_url('login/ajx_email_available'); ?>",
		       	success: function(result){
		       	 	if(result)
		       	 		 $("#rtn-email").html('');
			      	else
			      	{
			      		$("#rtn-email").html('<span style="color:red"><b>Sorry, this email has already been used</b></span>');
			      		 $('#email').val(''); 	
			      	}
			      		 			      					      				      			      
			     }
				});
    		return false;
       });             
 	});
</script>

