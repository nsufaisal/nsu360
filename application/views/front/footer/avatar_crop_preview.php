<!--popup -->
<div class="popup" style="width:700px;">
    <!--header -->
    <div id="popup-header" style="width:700px;">
        <h1 class="popup-logo"><a href="<?= base_url(); ?>"><img height="59" width="188" alt=" " src="<?= base_url(); ?>images/pp_logo.gif" /></a></h1>
        <a href="javascript:void(0);" onclick="closePP();" class="close-button">Close (X)</a>
    </div>
    <!--/header -->

    <!--popup content -->
    <div class="popup-content" style="width:700px;">
        <h2 class="center-text">Select area and Crop</h2>


		<div class="wrap">
			<!-- image preview area-->
			<img id="uploadPreview" style="display:none;"/>
			
			<!-- image uploading form -->
		</div><!--wrap-->


        <!--submit buttons -->
        <div class="submit-buttons">
           
            <a href="javascript:void(0);" onclick="closePP();" class="grey-button">Cancel</a>
        </div>
        <!--/submit buttons -->

    </div>
    <!--/popup content -->
</div>
<!--/popup -->
<script>
$(document).ready(function() {
	p.fadeOut();

		// prepare HTML5 FileReader
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("userfile_1").files[0]);

		oFReader.onload = function (oFREvent) {
	   		p.attr('src', oFREvent.target.result).fadeIn();
		};
});
</script>