
<div id="page_body">
    <div class="center">
        <h1>News</h1>
        <div id="twocols_cont">

            <div class="left" id="leftcol">

                <?php echo $footer_news_content; ?>

            </div>	<!-- end left column -->

            <div class="right" id="rightcol">
                <div class="recentnews">
                    <h5>Recent Posts</h5>
                    <?php echo $footer_news_recent_post; ?>
                </div>

                <div class="recentcomments">
                    <h5>Recent Comments</h5>
                    <?php echo $footer_news_recent_comments; ?>
                </div>
            </div>	<!-- end right column here -->

            <div class="clear"></div>
        </div>	<!-- end two cloumns container -->
    </div>
</div>	<!-- end page body -->