<div id="page_body">
<div class="center">
<h1>User Support</h1>

<div class="support_sections">
<h5><a href="<?=site_url('user_support/technical_support')?>">Technical Support</a></h5>
<?=$technical_content;?>
</div>

<div class="support_sections">
<h5><a href="<?=site_url('user_support/feedback_support')?>">Feedback</a></h5>
<?=$feedback_content;?>
</div>

<!--
<div class="support_sections">
<h5><a href="<?=site_url('customer_services/billing_support')?>">Billing Support</a></h5>
<?=$billing_content;?>
</div>
-->

</div>
</div>