<div id="header">
	<div id="head_patteren">
		<div class="center">
			<div class="left" id="logo"><a href="<?=base_url();?>"><img src="<?=base_url();?>images/logo.png" alt="nsu360.com" title="nsu360.com" /></a></div>
			<div class="right" id="right_head">
			
				<div id="userarea">Welcome, <strong>
					<!-- User & Login Status Check -->
					<?PHP 
						if ($this->session->userdata('username')!=FALSE)
						{
							if($this->session->userdata('user_type')!=FALSE)
							{
								$user_type=$this->session->userdata('user_type');
								if($user_type=='SA'){
									echo '<a href="'.site_url('sadmin/my_profile').'">'.$this->session->userdata('username').'</a>';
								}
								else {
									echo $this->session->userdata('username');
								}
							}
						}							
						else{
							echo "Guest";
						}			
						echo '!</strong> ';
												
						if($this->session->userdata('user_type')!=FALSE){
							$user_type=$this->session->userdata('user_type');
							if($user_type=='FU')
								$page='member';
							else if($user_type=='IN')
								$page='instructor';
							else if($user_type=='ST') {
								$page='student';
							}
							else if($user_type=='SA') {
								$page='sadmin';
							}
							else {
								$page='member';
							}
							
							echo '<a href="'.site_url($page.'/myaccount').'" class="myaccount">My Account</a>';
						}				
						?>						 
						
						<?PHP if ($this->session->userdata('member_id')==FALSE) { ?>
							<a href="<?= site_url('login/fancy_login')?>" class="logout fancybox">Sign In</a>
							<?PHP } else { ?> 
							<a href="<?= site_url('logout')?>" class="logout">Sign Out</a>
					  	<?PHP }?>
						
					 <a href="#"><img src="<?=base_url();?>images/facebook.png" alt="facebook" title="facebook" /></a> 
					</div>
				
			<div id="bookmark">
				<span id="account_links"><a href="#">How it Works</a><span>|</span><a href="<?php echo site_url('user_support');?>">User Support</a></span>

				</div> 
			
			</div>	<!-- end right part -->
			<div class="clear"></div>
		</div>
	</div>
</div>

<div id="nav">
	<div class="center">
		<ul>
			<li <?=(isset($sel) && $sel=='home')?'class="active"':'' ?>><a href="<?php echo site_url('home'); ?>" class="nav_1">HOME</a></li>
			<li <?=(isset($sel) && $sel=='about')?'class="active"':'' ?>><a href="#" class="nav_2">ABOUT</a></li>
			<li <?=(isset($sel) && $sel=='course_list')?'class="active"':'' ?>><a href="<?php echo site_url('course');?>" class="nav_3">COURSE</a></li>
			<li <?=(isset($sel) && $sel=='instructor_list')?'class="active"':'' ?>><a href="#" class="nav_4">INSTRUCTOR</a></li>
			<li <?=(isset($sel) && $sel=='blog')?'class="active"':'' ?>><a href="#" class="nav_5">ARTICLE/BLOG</a></li>
			<li <?=(isset($sel) && $sel=='noticewall')?'class="active"':'' ?>><a href="<?php echo site_url('noticewall');?>" class="nav_6">NOTICE WALL</a></li>
		</ul>
		<div class="clear"></div>
	</div>
</div>