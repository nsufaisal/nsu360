<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/my_styles.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />


<h1>Forgot Password</h1>

<?php echo $content;?>

<div class="for_form">
<form id="pageForm1" action="<?php echo site_url('login/forgot_password_confirmation');?>" method="post">
<strong>Email:</strong>
<input name="email" id="email" type="text" class="input_field validate[required,custom[email]]" value="" style="width:670px;" />
<input type="submit" value="Submit" class="btnmy " name="submit" id="submit" />
<input type="button" value="Cancel" class="btnmy cancel_btn" name="cancel_btn" />
<div id="rtn-email-err" style="margin-left:40px;"></div>

</form>
</div>

<!--instance the validator engine ::rtn:: -->	
<script>
	 $(document).ready(function(){
	 	$("#pageForm1").validationEngine('attach', {promptPosition : "topRight", scroll: false,
         
	 	onValidationComplete : function(form, status){
	 		var email = $('#email').val(); 
	  		var flag=false;
			$.ajax({
	          	type: "post",
	           	data: {'email' : email},
	           	async:      false,
	           	url: "<?php echo site_url('login/ajx_email_available');?>",
		       	success: function(result){
		       		status=result;
		       //		alert(result);
		       	 	if(result)
		       	 			$("#rtn-email-err").html('<span style="color:red"><b>*Invalid user email</span>');	
		       	 	else{
			      		$("#rtn-password").html('');
						flag=true; 		
			      	}			      		 				      					      				      			      
			     }
		   });
		   if(flag){
		      		form.validationEngine('detach');
		      		form.submit();
		      	}
	 	}
	 	});           
              
     });

</script>
	<script>
		$(document).ready(coolSliderLink());
	</script>

<!--check user availability ::rtn:: -->	
<!-- <script>		
	$(document).ready(function(){       
           
        $("#submit").click(function(){          		
       			
   			var email = $('#email').val();       			
   			$.ajax({
          	type: "post",
           	data: {'email' : email},
           	url: "<?=site_url('login/ajx_email_available')?>",
	       	success: function(result){
	       	 	if(result){
	       	 		$("#rtn-email-err").html('<span style="color:red"><b>*Invalid user email</span>');		
	       	 		return false;
	       	 	}	
	       	 	      					      					      				      			      
		     }
			});
       }); 
                   
 	});
</script> -->