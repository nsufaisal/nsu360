<style>
	.conf_msg p,.conf_msg h1,.conf_msg ul li {
		font-family: ‘American Typewriter’,‘Courier New’,Courier,Monaco;
	}
	.conf_msg h1{
		color:#1B83BB;
	}
	.conf_msg p, .conf_msg ul li{
		font-size:1.2em;
	}
</style>


<div class="conf_msg">

<h1 class="page_tab_head"><?php echo $confirmation_title;?></h1>
	<?php echo $confirmation_content;?>	
</div>
