<link href="<?=base_url();?>css/reset.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/my_styles.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url();?>css/popup.css" rel="stylesheet" type="text/css" />


<div class="popup-wrapper">

<!--popup -->
<div class="popup sign_in_pp" >
<!--header -->
<div id="popup-header">
<h1 class="popup-logo"><a href="<?=base_url();?>"><img src="<?=base_url();?>images/logo.png" width="188" height="59" alt=" " /></a></h1>
<a href="<?=site_url()?>" class="close-button">Close (x)</a>
</div>
<!--/header -->

<!--popup content -->
<div class="popup-content ymp_pp_cont">
<h2>Sign In</h2>
<?=$content;?>
<?= validation_errors()?>
<div class="sign_in_form edit_form">
<form action="<?= site_url('login/user_auth')?>" method="post" id="loginForm" accept-charset="utf-8" >
<p>
<strong>Username:</strong>
<input name="email" id="email" type="text" class="input_field validate[required,custom[email]]" data-prompt-position="topLeft:280,-2" value="<?= set_value('email', '') ?>" />
<span id="rtn-email"></span>
</p>

<p>
<strong>Password:</strong>
<input name="password" id="password" type="password" class="input_field validate[required]" data-prompt-position="topLeft:280,-2" />
<span id="rtn-password"></span>
</p>

<!--submit buttons -->
<div class="pp_sbmt" style="padding-top:5px;">

<div class="left_inpt">
	<a  href="<?php echo site_url('footer/student'); ?>">Register as Student</a>  |
	<a class="INL_CoolSlider" href="<?= site_url('login/forgot')?>">Forgot Password</a>  |
<!--	<a class="INL_CoolSlider" href="<?= site_url('login/forgot_user_email')?>">Forgot Username</a> -->
</div>

<div class="right_inpt">
    <input name="submit" id="submit" type="submit" class="btnmy" value="Login" />
    <input name="cancel" onclick="window.location.href='<?= site_url()?>'" type="button" class="btnmy" value="Cancel" />
</div>

<div class="clear"></div>
</div>
<!--/submit buttons -->

</form>
</div>

</div>
<!--/popup content -->
</div>

</div>
<!--/popup -->

	<!--instance the validator engine ::rtn:: -->
	<script>		
		 $(document).ready(function(){
		 	$("#loginForm").validationEngine('attach', {scroll: false,
             
		 	onValidationComplete : function(form, status){
		 		var email = $('#email').val(); 
		 		var password=$('#password').val();
		 		var flag=false;
				$.ajax({
		          	type: "post",
		           	data: {'email' : email,'password' : password},
		           	async:      false,
		           	url: "<?php site_url('login/ajx_user_credential_check');?>",
			       	success: function(result){
			       	//	alert(result);
			       		status=result;
			       	 	if(result){
			       	 		$("#rtn-password").html('');
							flag=true;
			       	 		}			       	 			 
			       	 	else{
				      		 $("#rtn-password").html('<span style="color:red"><b> *Incorrect username or password</b></span>');
			       	 			
				      	}			      		 				      					      				      			      
				     }
			   });
			   if(flag){
			      		form.validationEngine('detach');
			      		form.submit();
			      	}
		 	}
		 	});
              
     });
	</script>
	<script>
		$(document).ready(coolSliderLink());
	</script>

