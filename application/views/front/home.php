<link href="<?PHP echo base_url() ?>css/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.touchSwipe.min.js"></script>

<style type="text/css">
    .black_overlay
    {
        display: none;
        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:1001;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }

    #fancybox-loading
    {
        left: 45% !important;
        top: 15% !important;
    }

    
</style>
<link href="<?PHP echo base_url()?>css/home_page.css" rel="stylesheet" type="text/css" />
<div id="page_body">
<div class="center loader_position">


<div id="home_cont">

<h1><span class="smart_school">NSU360</span> Online University Management Service</h1>
<p>
	<span class="smart_school">NSU360</span> is an easy to use web service for any university wishes to mange their academics online.
	Currently it is being used by several instructors in North South University. However, it is designed in such a way, any institution can use it, out of the box. <span class="smart_school">NSU360</span> team aspires
	to become one of the leading web services for online university management in Bangladesh. Currently this is in development phase. A beta version would be released
	soon for free use.  Our geneours thanks to a great community including students, instructors, developers and last but not the least the well-wishers are
	supporting the team, endlessly.
</p>
<div class="left" id="HM_left" style="height:300px; overflow: hidden;">

<?php
function getExcerpt($str , $startPos=0 , $maxLength=100 ) {
        if(strlen ($str ) > $maxLength) {
               $excerpt   = substr ($str , $startPos , $maxLength -3);
               $lastSpace = strrpos ($excerpt , ' ') ;
               $excerpt   = substr ($excerpt , 0 , $lastSpace );
               $excerpt  .= '...';
        } else {
               $excerpt = $str ;
        }       
        return $excerpt;
}
?>
<link href="<?php echo base_url();?>css/marketing.css" rel="stylesheet">
      
          <style>
        
/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
.carousel {
  height: 300px;
  margin-bottom: 10px;
}
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
  z-index: 10;
}

/* Declare heights because of positioning of img element */
.carousel .item {
  height: 300px;
  background-color: #9da487;
}
.carousel-inner > .item > img {
  position: relative;
  top: 0;
  left: 0;
  min-width: 100%;
  height: 300px;
}
#myCarousel .carousel-control{
    height: 300px;
}

    </style>

    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height:400px;">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo base_url('images/banners/carousel-1.jpg');?>" alt="First slide">
        </div>
        <div class="item">
          <img src="<?php echo base_url('images/banners/carousel-2.jpg');?>" alt="Second slide">
        </div>
        <div class="item">
          <img src="<?php echo base_url('images/banners/carousel-3.jpg');?>" alt="Third slide">
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
      </div>



<div class="clear"></div>



</div>	<!-- end left container -->

<div class="right" id="HM_right">

<div class="TRM_cont">

<div class="MS_title" style="margin-top:0"><span>Notice Wall</span></div>
<div class="noticepost" >
<ul class="notice_list" id="notice_list_front">
	<?php foreach($course_notice as $notice):?>
		<a href="<?php echo site_url('noticewall/view/'.$notice['noticewall_id']);?>"><li><?php echo '<span class="post_time">'. date("j M,y, g:i a",$notice['post_date']).'<br/></span>
			<span class="notice_title">'.getExcerpt($notice['title'],0,50);?></span></li></a>
	<?php endforeach;?>
	
</ul>
<div class="clear"></div>

</div>
<div class="clear"></div>
</div>	<!-- end Newest cont -->
<div class="view_button"><a href="<?php echo site_url('noticewall');?>" style="float:right;">View All Notices</a></div>
</div>	<!-- end right -->

<div class="clear"></div>


 <div class="row-fluid">
      <div id="testimonials">
         <div class="quote-container">
            <blockquote class="span7 offset1"><span class="smart_school">NSU360</span> University Management System, rightly serves in giving us a complete visibility— interactions between students
            	and students has become a milestone. Grades, notices are more transparent than ever before. Thanks to the development team.
               <br/><br/><small>Lorem Ipsum, Dean, School of Business</small>
            </blockquote>

         </div>
      </div>
   </div>

<!--small icon six -->
   <div class="row-fluid">
      <h2>Bring all school components together</h2>
   </div>
   <ul class="feature-block feature-thumbnails row-fluid">
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/student-icon.png" alt="" width="40" height="40" style="top:15px">
         <p class="details"><strong>Student:</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque <a href="/voice-conferencing">Instant voice conferencing</a>.</p>
      </li>
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/teacher-icon.png" alt="" width="35" height="35" style="left:10px; top:15px">
         <p class="details"><strong>Teacher:</strong> Aenean massa. <a href="/remote-screen-sharing">remote screen sharing</a> tools all meeting participants can independently share and view multiple screens at once.</p>
      </li>
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/exam-mark-icon.png" alt="" width="40" height="40" style="top:15px">
         <p class="details"><strong>Exam Marks:</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <a href="/business-chat">Business chat</a> lets you message anyone in your team efficiently.</p>
      </li>
   </ul>
   
   <ul class="feature-block feature-thumbnails row-fluid">
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/noticewall-icon.png" alt="" width="54" height="54" style="left:-10px; top:5px">
         <p class="details"><strong>Notice Wall:</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
      </li>
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/tuition-fees-icon.png" alt="" width="40" height="40" style="top:14px">
         <p class="details"><strong>Tution Fee Collection:</strong> Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>
      </li>
      <li class="span4 thumbnail">
         <img src="<?php echo base_url();?>images/home/grade-icon.png" alt="" width="40" height="40" style="top:15px">
         <p class="details"><strong>Grade Sheets:</strong> Deepen ties with co-workers using <a href="/online-video-conferencing">online video conferencing</a>. See reactions side-by-side with content.</p>
      </li>
   </ul>

   <div class="ready-for row-fluid feature-block">
      <h2>Ready for your organization</h2>
      <ul class="top-icon-thumbnails">
         <li class="thumbnail span4">
            <img src="<?php echo base_url();?>images/home/icon-secure.png" alt="">
            <p class="lead">Private and Secure</p>
            <p class="details">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>
         </li>
         <li class="thumbnail span4">
            <img src="<?php echo base_url();?>images/home/icon-seamless.png" alt="">
            <p class="lead">Simple and Seamless</p>
            <p class="details">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>
         </li>
         <li class="thumbnail span4">
            <img src="<?php echo base_url();?>images/home/icon-scalable.png" alt="">
            <p class="lead">Scalable and Flexible</p>
            <p class="details">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.</p>
         </li>
      </ul>
   </div>


<!-- small icon six ends -->

<!-- ask for ur org -->
  <style>
	.account-creation-form{
		margin: 2px;
		width:60%;	
		display: inline;float:left;	
	}
	.span5{
		width:30%;
		float:right;
		
	}
	#try-now{
		width:100%;
	}
	
	.controls-row {
		width:70%;
		margin:0 auto;
	}
	.span11{
		color:#0072BE;
		font-weight:bold;
		font-family: Arial, Helvetica, sans-serif;
		padding-left:10px;
	}
	.head{
		margin:0 auto;
	}
	
	.span5 ul li{
		font-size:1.5em;
		list-style:square;	
		margin-left:15px;	
		color:#316b82;
	}


  </style>
  
   <div class="try-now-form row-fluid" id="try-now">
      <div class="account-creation-form row-fluid" >
         <div class="span6">
            <p class="head" style="margin-left:5%;">Try <span class="smart_school">NSU360</span> for free*</p>
            <form name="order_software" id='pageForm' method="post" action="<?php echo site_url();?>home/order_software/">
            <div class="sign-up-view">

   <div class="controls-row">
      <div class="controlgroup span6">
           <input class="span11 validate[required]" type="text" placeholder="First Name" id="first_name" value="" name="first_name" >
      </div>
   </div>
   <div class="controls-row">
      <div class="control-group">
         <label class="span12 control-label">
            <input class="span11 validate[required]" type="text" placeholder="Last Name" name="last_name" id="last_name" value="" />
         </label>
         <span class="help-block hide"></span>
      </div>
   </div>
   
   <div class="controls-row">
      <div class="control-group">
         <label class="span12 control-label">
            <input class="span11 validate[required,custom[email]]" type="text" placeholder="Email" name="user_email" id="user_email" value="" />
         </label>
         <span class="help-block hide"></span>
      </div>
   </div>

   
   <div class="controls-row phone-number">
      <div class="control-group">
         <label class="span8 control-label">
            <div class="span12">
               <input class="span11 validate[required]" type="text" name="mobile" id="mobile" placeholder="Mobile No." value="" />
            </div>
         </label>
         <span class="help-block hide"></span>
      </div>
   </div>
   
      <div class="controls-row">
      <div class="control-group">
         <label class="span8 control-label">
            <div class="span12">
               <input class="span11 validate[required,minSize[2]]" type="text" name="institution_name" id="institution_name" placeholder="Institution Name" value="" />
            </div>
         </label>
         <span class="help-block hide"></span>
      </div>
   </div>

   
   <div class="controls-row create-row">
      <input type="submit" name="submit" value="Submit Request" class="btn-lg btn-success" />
      <span class="fine-print span7" style="margin-top:15px;">
         By submitting this form you are requesting the <br/><span class="smart_school">NSU360</span> team a free trial for your university .
      </span>
   </div>
</div>
</form>
         </div>
         
      </div>
      <div class="span5" style="width:36%;float: right; margin-top:50px;">
            <h2>Enjoy your Free Trial</h2>
            <ul>
               <li>Up to six months period</li>
               <li>Enjoy the complete application</li>
               <li>Our team will customize the system according to your need</li>
               <li>Data-entry cost should be paid at actual</li>
            </ul>
         </div>
   </div>
</div>


<!-- end ask for ur org -->
</div>	<!-- end home main container -->
</div>
<div class="black_overlay" id="fade"></div>
<!--instance the validator engine ::rtn:: -->   
<script>        
    $(document).ready(function(){          
       $("#pageForm").validationEngine('attach',{promptPosition : "topRight", scroll: false},
       {focusFirstField : true});             
    });
</script>
<script>
	    $(document).ready(function() {

        $('div.loader_position').css('position', 'relative');
        $('div.loader_position').append(
                loading = $('<div id="fancybox-loading"><div>')
                );
    });
</script>
<script>
    $(document).ready(function(){
    $('#notice_list_front').carouFredSel({
        width: 'auto',
        height: 'inherit',
        prev: '.prev',
        next: '.next',
        direction : "up",
        align: "center",
        auto: {
            play: true,
            delay: 6
        },
        scroll : {
            items: 1,
            duration: 800
        },
        items : {
            visible : 7
            //start:  Math.floor(Math.random() * 20)
        },
        mousewheel: true,
        swipe: {
            onMouse: true,
            onTouch: true
        }
    });
});
</script>