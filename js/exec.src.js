var exec = {
    setAjaxLoader: function(el, msg) {
        var loadmsg = '';
        if (msg) {
            loadmsg = ' ' + msg;
        }
        $(el).ajaxStart(function() {
            $(this).text("Loading ..." + loadmsg);
            $(this).show();
        });
        $(el).ajaxComplete(function() {
            $(this).fadeOut(1000);
            $(this).html("Loading ...");
        });
    },
    _ajaxjson: function(myConfig) {
        this.myConfig = myConfig;
        var strConfig = myConfig;
        var vurlref = strConfig.urlref;
        var vparameter = strConfig.parameter;
        var vtype = strConfig.type;
        var verr = strConfig.err;
        var datatext = $.ajax({
            url: vurlref,
            data: 'js=1&' + vparameter,
            type: vtype,
            dataType: "json",
            success: function(data) {
                eval(strConfig.fnoutput+'(data);');
            },
            error: function(data) {
                return false;
            }
        });
    },
    _ajaxjsonparam: function(myConfig) {
        this.myConfig = myConfig;
        var strConfig = myConfig;
        var vurlref = strConfig.urlref;
        var vparameter = strConfig.parameter;
        var vtype = strConfig.type;
        var verr = strConfig.err;
        var output = strConfig.output;
        var datatext = $.ajax({
            url: vurlref,
            data: 'js=1&' + vparameter,
            type: vtype,
            dataType: "json",
            success: function(data) {
                eval(strConfig.fnoutput+'(data,output);');
            },
            error: function(data) {
                return false;
            }
        });
    },
    _ajax: function(myConfig) {
        this.myConfig = myConfig;
        var strConfig = myConfig;
        var vurlref = strConfig.urlref;
        var vparameter = strConfig.parameter;
        var vtype = strConfig.type;
        var verr = strConfig.err;
        $.ajax({
            url: vurlref,
            data: 'js=1&' + vparameter,
            type: vtype,
            success: function(data) {
                $(''+strConfig.output).html(data);
            },
            error: function(data) {
                return false;
            }
        });
    },
    _ajaxhtml: function(myConfig) {
        this.myConfig = myConfig;
        var strConfig = myConfig;
        var vurlref = strConfig.urlref;
        var vparameter = strConfig.parameter;
        var vtype = strConfig.type;
        var verr = strConfig.err;
        var output = strConfig.output;
        $.ajax({
            url: vurlref,
            data: 'js=1&' + vparameter,
            type: vtype,
            success: function(data) {
                eval(strConfig.fnoutput+'(data,output);');
            },
            error: function(data) {
                return false;
            }
        });
    },    
    _url: function(url){
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) { //test for MSIE x.x;
            location.replace(url);
        } else
        { window.location.replace(url); }
    },
    _dialogmsg: function(myConfig) {
        var response = false;
        $(myConfig.idmsg).html(myConfig.message);
        var data = $(myConfig.idmsg).dialog({
            resizable: false,
            height: 'auto',
            width: 'auto',
            modal: true,
            async: false,
            title: myConfig.title,
            buttons: myConfig.buttons
        }).responseText;
        return response;
    }
}
/*$(window).scroll(function() {
	//$('#progresstop').css('top', $(this).scrollTop() + "px");
});*/